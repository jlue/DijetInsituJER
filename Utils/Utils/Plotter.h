#pragma once

// C++ include(s)
#include <iostream>
#include <string>
#include <vector>

// ROOT include(s)
#include <Rtypes.h>
#include <TStyle.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <THStack.h>
#include <TF1.h>
#include <TF2.h>
#include <TF3.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TGraph2D.h>
#include <TGraph2DErrors.h>
#include <TEfficiency.h>
#include <TProfile.h>
#include <TRatioPlot.h>
#include <TString.h>
#include <TLatex.h>
#include <TLegend.h>

// local includes
#include "Utils/Utils.h"

class Plotter
{

public:

	Plotter(std::string name="Plotter");
	~Plotter(){}

  // open output
  void openFile( const std::string& fileName, const std::string& fileType="pdf" , const std::string& outDir="") {
    m_filePath = outDir.empty() ? fileName : outDir+"/"+fileName;
    m_filePath += "."+fileType;
    if (fileType=="pdf") m_canvas->Print((m_filePath+"[").c_str());
  }
	void closeFile( std::string filePath="" ) {
    if (filePath.empty()) filePath = m_filePath;
    m_canvas->Print((filePath+"]").c_str());
  }
  void savePlot( std::string filePath="" ) { 
    makePlot();
    if (filePath.empty()) filePath = m_filePath;
    m_canvas->Print(filePath.c_str());
    clearLegend();
    m_canvas->Clear("D");
  }

  // style
  TStyle* makeStyle(const std::string& styleName="ATLAS"); 
  void applyStyle(TStyle* style=nullptr);
  // updating style
  void setMargins(int topMargin, int bottomMargin, int leftMargin, int rightMargin);
  void setText(int font, float size);

  // canvas & pad creation
  TCanvas* makeCanvas(int w, int h);
  TPad* makePad();
  void makePads(int nrow=1, std::initializer_list<float> hs={}, int ncol=1, std::initializer_list<float> ws={});
  void makePads(int nrow, std::vector<float> hs, int ncol, std::vector<float> ws);

  // get canvas / pad
  TCanvas* getCanvas() {
    m_canvas->cd();
    return m_canvas;
  }
  TPad* getPad(int row=1, int col=1) {
    m_row = row;
    m_col = col;
    getCanvas();
    m_currentPad = m_pads[row-1][col-1];
    m_currentPad->cd();
    return m_currentPad;
  }
  TH1* getAxis(int row=1, int col=1) {
    m_row = row;
    m_col = col;
    getPad(row,col);
    m_currentAxis = m_axes[row-1][col-1];
    return m_currentAxis;
  }

  // axis creation
  TH1* makeAxis(std::string xtitle, float xmin, float xmax, std::string ytitle, float ymin, float ymax);
  TH1* makeAxis(int row, int col, std::string xtitle, float xmin, float xmax, std::string ytitle, float ymin, float ymax);
  // settings
  void logAxis(bool logx=false, bool logy=true);
  void shareAxis(bool sharex=true, bool sharey=false);

  // plot styles
  void setMarker(TH1* hist, int mcolor=kBlack, int mstyle=kFullCircle, float msize=1.0);
  void setLine(TH1* hist, int lcolor=kBlack, int lstyle=kSolid, int lwidth=2.0);
  void setFill(TH1* hist, int fcolor=kWhite, float fstyle=0.0);
  void setMarker(TGraph* gr, int mcolor=kBlack, int mstyle=kFullCircle, float msize=1.0);
  void setLine(TGraph* gr, int lcolor=kBlack, int lstyle=kSolid, int lwidth=2.0);
  void setFill(TGraph* gr, int fcolor=kWhite, float fstyle=0.0);
  void setLabel(TObject* plot, const std::string& label, const std::string& opt="lepf");

  // global labels & logo
  void addLabel(float x, float y, const std::string& label="", int col=kBlack);
  void addLogo(float x, float y, const std::string& label="", const std::string& logo="#bf{#it{ATLAS}}"); 

  // book plot
  void addHist(TH1* hist, const std::string& opt="ep");
  void addFtn(TF1* ftn, const std::string& opt="l");
  void addGraph(TGraph* gr, const std::string& opt="ep");
  // perform on current plot
  void setMarker(int mcolor=kBlack, int mstyle=kFullCircle, float msize=1.0);
  void setLine(int lcolor=kBlack, int lstyle=kSolid, int lwidth=2.0);
  void setFill(int fcolor=kWhite, float fstyle=1.0);
  void setLabel(const std::string& label, const std::string& opt="lepf");

  // legend creation
  TLegend* makeLegend(float x1, float x2, float y1, float y2, int ncol=1);

  // line guides
  void drawLine(float x1, float y1, float x2, float y2, int lineColor=kBlack, int lineStyle=kDotted, int lineWidth=1);
  void drawVLine(float x, int lineColor=kBlack, int lineStyle=kDotted, int lineWidth=1);
  void drawHLine(float y, int lineColor=kBlack, int lineStyle=kDotted, int lineWidth=1);

  // draw everything in plot
  void makePlot();

private:

  // clear plot
  void clearPads();
  void clearLegend();

  // output
	std::string m_name;
  std::string m_filePath;

  float m_canvasWidth, m_canvasHeight;
  float m_topMargin, m_bottomMargin, m_leftMargin, m_rightMargin;

  int m_nrow, m_ncol;
  int m_row, m_col;
  std::vector<float> m_padWidth, m_padHeight;
  std::vector<float> m_padX, m_padY;

  bool m_sharex, m_sharey;
  bool m_logx, m_logy;
  float m_xmin, m_xmax;
  float m_ymin, m_ymax;

  // plot style
  TStyle* m_style;

  // canvas
	TCanvas* m_canvas;
  // pads
  std::vector<std::vector<TPad*>> m_pads;
  TPad* m_currentPad;
  // axis
  std::vector<std::vector<TH1*>> m_axes;
  TH1* m_currentAxis;
  // legend
	TLegend* m_legend;

  // plot being processed
  TH1* m_currentPlot;

  // all plots
  TObjArray* m_plotObjects;
  std::vector<std::string> m_drawOptions;

	// plotted histograms
  std::vector<TObject*> m_legendObjects;
  std::vector<std::string> m_legendLabels;
  std::vector<std::string> m_legendOptions;

};
