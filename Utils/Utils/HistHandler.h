#pragma once

#include <string>
#include <vector>
#include <map>
#include <unordered_map>

#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"

#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH1FBootstrap.h"
#include "BootstrapGenerator/TH2FBootstrap.h"

#include "TProfile.h"
#include "TProfile2D.h"

#include "TEfficiency.h"

#include "Utils/Utils.h"
#include "Utils/Config.h"

class HistHandler
{

public:
  
  HistHandler(std::string name, BootstrapGenerator* bootGen=nullptr) :
      m_name(name),
      m_bootGen(bootGen)
  {
    if (!bootGen) m_bootGen = new BootstrapGenerator("bootstrapGenerator","bootstrapGenerator",1);
    initCut();
    initWeight();
  }
  ~HistHandler()
  {
    for (auto hist : m_hist1D) delete hist.second;
    for (auto hist : m_hist2D) delete hist.second;
    for (auto hist : m_hist3D) delete hist.second;
    for (auto boot : m_boot1D) delete boot.second;
    for (auto boot : m_boot2D) delete boot.second;
    for (auto prof : m_prof1D) delete prof.second;
    for (auto prof : m_prof2D) delete prof.second;
    for (auto eff  : m_eff)    delete eff.second;
  }

  // cuts & weights
  // init: always @ beginning of sequence (called in constructor) & pass-through
  void initCut()
  {
    m_passCut = true;
  }
  // apply: print out for the first full sequence, save cut result
  void applyCut(const std::string& cname, bool pass)
  {
    if (!eleInVec(cname,m_cutList)) {
      info(Form("Cut appplied: %s",cname.c_str()),Form("HistHandler.%s::applyCut",m_name.c_str()));
      m_cutList.push_back(cname);
    }
    m_passCut = m_passCut && pass;
  }
  bool passCut() { return m_passCut; }
  // init: always @ beginning of sequence (called in constructor) with value 1.0
  void initWeight()
  {
    m_weight = 1.0;
  }
  // apply: print out for the first full sequence, save weight value
  void applyWeight(const std::string& wname, float weight)
  {
    if (!eleInVec(wname,m_weightList)) {
      info(Form("Weight applied: %s",wname.c_str()),Form("HistHandler.%s::applyWeight",m_name.c_str()));
      m_weightList.push_back(wname);
    }
    m_weight *= weight;
  }

  // booking

  // histograms
  // archetypical TH(1/2/3)F classes
  // additionally support TH(1/2/3 planned)FBootstrap for weighted toys
  // 1D
  void bookHist(const std::string& xvar, std::vector<float> xbins, int nrep=0) {
    bookHist({xvar},{xbins},{},nrep);
  }
  void bookHist(const std::string& xvar, std::vector<float> xbins, const std::string& label, int nrep=0) {
    bookHist({xvar},{xbins},{label},nrep);
  }
  void bookHist(const std::string& xvar, std::vector<float> xbins, const std::initializer_list<std::string>& labels, int nrep=0) {
    bookHist({xvar},{xbins},labels,nrep);
  }
  // 2D & 3D
  void bookHist(const std::initializer_list<std::string>& vars, const std::initializer_list<std::vector<float>>& bins, int nrep=0) {
    bookHist(vars,bins,{},nrep);
  }
  void bookHist(const std::initializer_list<std::string>& vars, const std::initializer_list<std::vector<float>>& bins, const std::string& label, int nrep=0) {
    bookHist(vars,bins,{label},nrep);
  }
  // master case
  void bookHist(const std::initializer_list<std::string>& vars, const std::initializer_list<std::vector<float>>& binList, const std::initializer_list<std::string>& labels, int nrep=0) {
    std::string hname = histName(vars,labels);
    m_hlist.push_back(hname);
    m_bootstrap[hname] = false;
    m_profile[hname] = false;
    m_efficiency[hname] = false;
    int ndim = vars.size();
    std::vector<std::vector<float>> bins(binList.begin(),binList.end());
    if (bins.size()!=ndim) error(Form("Ndim mismatch between variables (%i) and binnings (%lu): %s",ndim,bins.size(),hname.c_str()),Form("HistHandler.%s::bookHist",m_name.c_str()));
    switch(ndim) {
      case 1:
      {
        TH1F* hist1D = new TH1F(hname.c_str(),"",bins[0].size()-1,&bins[0][0]);
        m_hist1D[hname] = hist1D;
        if (nrep) {
          TH1FBootstrap* boot1D = new TH1FBootstrap(*hist1D,nrep,m_bootGen);
          m_boot1D[hname] = boot1D;
          m_bootstrap[hname] = true;
        } 
        break;
      }
      case 2:
      {
        TH2F* hist2D = new TH2F(hname.c_str(),"",bins[0].size()-1,&bins[0][0],bins[1].size()-1,&bins[1][0]);
        m_hist2D[hname] = hist2D;
        if (nrep) {
          TH2FBootstrap* boot2D = new TH2FBootstrap(*hist2D,nrep,m_bootGen);
          m_boot2D[hname] = boot2D;
          m_bootstrap[hname] = true;
        }
        break;
      }
      case 3:
      {
        TH3F* hist3D = new TH3F(hname.c_str(),"",bins[0].size()-1,&bins[0][0],bins[1].size()-1,&bins[1][0],bins[2].size()-1,&bins[2][0]);
        m_hist3D[hname] = hist3D;
        break;
      }
    }
  }

  // profiles
  // special type for mapped value = mean +/- stddev of a random variable
  // bin contents are filled, not set
  void bookProf(const std::string& name, const std::string& xvar, const std::vector<float> xbins) {
    bookEff(name,{xvar},{xbins});
  }
  void bookProf(const std::string& name, const std::initializer_list<std::string>& varList, const std::initializer_list<std::vector<float>>& binList) {
    std::string pname = profName(name,varList);
    m_bootstrap[pname] = false;
    m_profile[pname] = true;
    m_efficiency[pname] = false;
    // TEfficiency doesn't have a constructor with Float_t* arguments...
    std::vector<std::string> vars(varList);
    std::vector<std::vector<float>> bins(binList);
    int ndim = varList.size();
    if (bins.size()!=ndim) error(Form("Ndim mismatch between variables (%i) and binnings (%lu): %s",ndim,bins.size(),pname.c_str()),Form("HistHandler.%s::fillHist",m_name.c_str()));
    std::vector<float> xbins = bins[0], ybins = bins[1];
    std::vector<double> xbins_d = std::vector<double>(xbins.begin(), xbins.end()), ybins_d = std::vector<double>(ybins.begin(), ybins.end());
    switch(ndim) {
      case 1:
      {
        TProfile* prof1D = new TProfile(pname.c_str(),"",xbins.size()-1,&xbins_d[0]);
        m_prof1D[pname] = prof1D;
        break;
      }
      case 2:
      {
        TProfile2D* prof2D = new TProfile2D(pname.c_str(),"",xbins.size()-1,&xbins_d[0],ybins.size()-1,&ybins_d[0]);
        m_prof2D[pname] = prof2D;
        break;
      }
    }
    m_hlist.push_back(pname);
  }

  // efficiency
  // special class for mapped value = probability of random variable +/- binominal errors
  // bin contents are filled, not set
  void bookEff(const std::string& name, const std::string& xvar, const std::vector<float> xbins) {
    bookEff(name,{xvar},{xbins});
  }
  void bookEff(const std::string& name, const std::initializer_list<std::string>& varList, const std::initializer_list<std::vector<float>>& binList) {
    std::string ename = effName(name,varList);
    m_bootstrap[ename] = false;
    m_profile[ename] = false;
    m_efficiency[ename] = true;
    // TEfficiency doesn't have a constructor with Float_t* arguments...
    std::vector<std::string> vars(varList);
    std::vector<std::vector<float>> bins(binList);
    int ndim = varList.size();
    if (bins.size()!=ndim) error(Form("Ndim mismatch between variables (%i) and binnings (%lu): %s",ndim,bins.size(),ename.c_str()),Form("HistHandler.%s::fillHist",m_name.c_str()));
    std::vector<float> xbins = bins[0], ybins = bins[1];
    std::vector<double> xbins_d = std::vector<double>(xbins.begin(), xbins.end()), ybins_d = std::vector<double>(ybins.begin(), ybins.end());
    TEfficiency* eff(nullptr);
    switch(ndim) {
      case 1:
        eff = new TEfficiency(ename.c_str(),"",xbins.size()-1,&xbins_d[0]);
        break;
      case 2:
        eff = new TEfficiency(ename.c_str(),"",xbins.size()-1,&xbins_d[0],ybins.size()-1,&ybins_d[0]);
        break;
    }
    m_eff[ename] = eff;
    m_hlist.push_back(ename);
  }

  void genToys(unsigned int run, unsigned int evt, unsigned int mc=0)
  {
    if (m_bootGen) {
      m_bootGen->Generate(run,evt,mc);
    } else {
      warn("BootstrapGenerator not found!",Form("HistHandler.%s::genToys",m_name.c_str()));
    }
  }

  // histogram-filling
  // 1D
  void fillHist(const std::string& xvar, float xval, float w=1.0) 
  {
    fillHist({xvar},{xval},{},w);
  }
  void fillHist(const std::string& xvar, float xval, const std::string& label, float w=1.0) 
  {
    fillHist({xvar},{xval},{label},w);
  }
  void fillHist(const std::string& xvar, float xval, const std::initializer_list<std::string>& labels, float w=1.0) 
  {
    fillHist({xvar},{xval},labels,w);
  }
  void fillHist(const std::initializer_list<std::string>& vars, const std::initializer_list<float>& vals, float w=1.0) 
  {
    fillHist(vars, vals, {}, w);
  }
  void fillHist(const std::initializer_list<std::string>& vars, const std::initializer_list<float>& vals, const std::string& label, float w=1.0) 
  {
    fillHist(vars, vals, {label}, w);
  }
  void fillHist(const std::initializer_list<std::string>& vars, const std::initializer_list<float>& valList, const std::initializer_list<std::string>& labels, float w=1.0) 
  {
    if (!m_passCut) return;
    std::string hname = histName(vars,labels);
    if (!eleInVec(hname,m_hlist)) {
      error(Form("Histogram not booked: %s",hname.c_str()),Form("HistHandler.%s::fillHist",hname.c_str()));
      return;
    }
    int ndim = vars.size();
    std::vector<float> vals(valList.begin(),valList.end());
    if (vals.size()!=ndim) error(Form("Ndim mismatch between variables (%i) and values (%lu): %s",ndim,vals.size(),hname.c_str()),Form("HistHandler.%s::fillHist",m_name.c_str()));
    switch(ndim) { 
      case 1:
      {
        float xval = vals[0];
        m_hist1D.at(hname)->Fill(xval,m_weight*w);  
        if (m_bootstrap.at(hname)) {
          m_boot1D.at(hname)->Fill(xval,m_weight*w);  
        }
        break;
      }
      case 2:
      {
        float xval = vals[0], yval = vals[1], zval = vals[2];
        m_hist2D.at(hname)->Fill(xval,yval,m_weight*w);  
        if (m_bootstrap.at(hname)) {
          m_boot2D.at(hname)->Fill(xval,yval,m_weight*w);  
        }
        break;
      }
      case 3:
      {
        float xval = vals[0], yval = vals[1], zval = vals[2];
        m_hist3D.at(hname)->Fill(xval,yval,zval,m_weight*w);  
        break;
      }
    }
  }

  // profile
  void fillProf(const std::string& yvar, float yval, const std::string& xvar, float xval, float w=1.0)
  {
    fillProf(yvar,yval,{xvar},{xval},w);
  }
  void fillProf(const std::string& zvar, float zval, const std::initializer_list<std::string>& varList, const std::initializer_list<float>& valList, float w=1.0)
  {
    if (!m_passCut) return;
    std::string pname = profName(zvar,varList);
    if (!eleInVec(pname,m_hlist)) {
      warn(Form("Profile %s not booked",pname.c_str()),Form("HistHandler.%s::fillProf",m_name.c_str()));
      return;
    }
    int ndim = varList.size();
    if (valList.size()!=ndim) error(Form("Ndim mismatch between variables (%i) and values (%lu): %s",ndim,valList.size(),pname.c_str()),Form("HistHandler.%s::fillHist",m_name.c_str()));
    std::vector<float> vals(valList.begin(),valList.end());
    switch(ndim) {
      case 1:
      {
        float x = vals[0];
        m_prof2D.at(pname)->Fill(x,zval,m_weight*w);  
        break;
      }
      case 2:
      {
        float x = vals[0], y = vals[1];
        m_prof2D.at(pname)->Fill(x,y,zval,m_weight*w); 
        break;
      }
    }
  }

  // effficiency
  void fillEff(const std::string& evar, bool pass, const std::string& xvar, float xval)
  {
    fillEff(evar,pass,{xvar},{xval});
  }
  void fillEff(const std::string& evar, bool pass, const std::initializer_list<std::string>& varList, const std::initializer_list<float>& valList)
  {
    if (!m_passCut) return;
    std::string ename = effName(evar,varList);
    if (!eleInVec(ename,m_hlist)) {
      error(Form("Efficiency %s not booked",ename.c_str()),Form("HistHandler.%s::fillEff",m_name.c_str()));
      return;
    }
    int ndim = varList.size();
    if (valList.size()!=ndim) error(Form("Ndim mismatch between variables (%i) and values (%lu): %s",ndim,valList.size(),ename.c_str()),Form("HistHandler.%s::fillHist",m_name.c_str()));
    std::vector<float> vals(valList.begin(),valList.end());
    switch(ndim) {
      case 1:
        m_eff.at(ename)->Fill(pass,vals[0]);
        break;
      case 2:
        m_eff.at(ename)->Fill(pass,vals[0],vals[1]);
        break;
    }
  }

  template <typename TH>
  TH getHist(const std::string& hname);

  // write histograms into a .root file
  void writeHists(const std::string& folder, TFile* output)
  {
    TDirectory* outdir = output->mkdir(folder.c_str());
    outdir->cd();
    // go through each booking (in order),
    // find the mapped hist/boot/prof object,
    // and write it into file 
    info(Form("Histograms written to: %s",outdir->GetPath()),Form("HistHandler.%s::writeHists",m_name.c_str()));
    if (m_bootGen) {
      info(Form("    %s",m_bootGen->GetName()),Form("HistHandler.%s::writeHist",m_bootGen->GetName()));
      m_bootGen->Write(m_bootGen->GetName(),TObject::kOverwrite);
    }
    for (const auto& hist : m_hlist) {
      info("    "+hist,Form("HistHandler.%s::writeHist",m_name.c_str()));
      // search for histograms/bootstraps
      if (m_bootstrap.at(hist)) {
        if (m_boot1D.find(hist)!=m_boot1D.end()) m_boot1D.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
        if (m_boot2D.find(hist)!=m_boot2D.end()) m_boot2D.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
      } else {
        if (m_hist1D.find(hist)!=m_hist1D.end()) m_hist1D.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
        if (m_hist2D.find(hist)!=m_hist2D.end()) m_hist2D.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
        if (m_hist3D.find(hist)!=m_hist3D.end()) m_hist3D.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
      } 
      // search for mappings/efficiencies
      if (m_profile.at(hist)) {
        if (m_prof1D.find(hist)!=m_prof1D.end()) m_prof1D.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
        if (m_prof2D.find(hist)!=m_prof2D.end()) m_prof2D.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
      }
      if (m_efficiency.at(hist)) {
        if (m_eff.find(hist)!=m_eff.end()) m_eff.at(hist)->Write(hist.c_str(),TObject::kOverwrite);
      } 
    }
    info("========================================",Form("HistHandler.%s::writeHist",m_name.c_str()));
  }

private:

  // make unique histogram/profile/mapping names out of specified observable names.
  // the naming convention is as follows:
  // - hist: var1:var2:var3[comma-delimited labels]
  // - prof: <obs>(x,y)["]
  // - eff: eff[var](x,y)
  std::string histName(std::initializer_list<std::string> varList, const std::initializer_list<std::string>& labelList={}) {
    std::vector<std::string> vars(varList);
    std::vector<std::string> labels(labelList);
    return (labels.size() ? joinStrV(vars,":")+"["+joinStrV(labels,",")+"]" : joinStrV(vars,":"));
  }
  std::string profName(const std::string& avgvar, std::initializer_list<std::string> varList, const std::initializer_list<std::string>& labelList={}) {
    std::vector<std::string> vars(varList);
    std::vector<std::string> labels(labelList);
    return (labels.size() ? "<"+avgvar+">("+joinStrV(vars,",")+")["+joinStrV(labels,",")+"]" : "<"+avgvar+">("+joinStrV(vars,",")+")");
  }
  std::string effName(const std::string& evar, std::initializer_list<std::string> varList) {
    std::vector<std::string> vars(varList);
    return "eff["+evar+"]("+joinStrV(vars,",")+")";
  }

  // handler name
  std::string m_name;

  // cut/weight bookkepper
  bool m_passCut;
  std::vector<std::string> m_cutList;
  // compound applyWeight calls until Fill called for a histogram
  float m_weight;
  std::vector<std::string> m_weightList;

  // plain histograms
  std::unordered_map<std::string,TH1F*> m_hist1D;
  std::unordered_map<std::string,TH2F*> m_hist2D;
  std::unordered_map<std::string,TH3F*> m_hist3D;
  // bootstrapped histograms
  BootstrapGenerator* m_bootGen;
  std::unordered_map<std::string,bool> m_bootstrap;
  std::unordered_map<std::string,TH1FBootstrap*> m_boot1D;
  std::unordered_map<std::string,TH2FBootstrap*> m_boot2D;
  // profiles
  std::unordered_map<std::string,bool> m_profile;
  std::unordered_map<std::string,TProfile*> m_prof1D;
  std::unordered_map<std::string,TProfile2D*> m_prof2D;
  // efficiencies
  std::unordered_map<std::string,bool> m_efficiency;
  std::unordered_map<std::string,TEfficiency*> m_eff;

  // list containing order of booked histograms
  std::vector<std::string> m_hlist;

};

// getHist specializations
template <>
inline TH1F* HistHandler::getHist<TH1F*>(const std::string& hname)
{
  return m_hist1D.at(hname);
}
template <>
inline TH1FBootstrap* HistHandler::getHist<TH1FBootstrap*>(const std::string& hname)
{
  return m_boot1D.at(hname);
}
template <>
inline TH2F* HistHandler::getHist<TH2F*>(const std::string& hname)
{
  return m_hist2D.at(hname);
}
template <>
inline TH2FBootstrap* HistHandler::getHist<TH2FBootstrap*>(const std::string& hname)
{
  return m_boot2D.at(hname);
}
