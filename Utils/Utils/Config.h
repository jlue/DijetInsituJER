#pragma once

// STL
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

// ROOT
#include "TEnv.h"
#include "THashList.h"
#include "TString.h"

// local
#include "Utils.h"

/********************
 * Config:
 *   class to read settings from text files
 *   relies on root's TEnv
 *
 * Usage:
 *   Config settings("Hgamma.config");
 *   std::string gamContainerName = settings.getStr("PhotonContainer");
 *   std::string elContainerName  = settings.getStr("ElectronContainer");
 *   std::vector<std::string> systShifts = settings.getStrV("Systematics");
 *
 */

/* \brief Class that handles reading input from a configuration text file
 *        based on root's TEnv format, i.e. key-value pairs
 *  \author Dag Gillberg
 */
class Config {
private:
  TEnv m_env; // TEnv objects holding the settings database

public:

  // \brief Config constructor
  // \param fileName name of text file with user-specified settings
  Config(std::string fileName);
  Config(const Config &config);
  Config();
  virtual ~Config() {}

  Config &operator=(const Config &rhs); // assignment operator

  // \brief Access a string value from the config database. Exception thrown if no entry exist.
  std::string getStr(std::string key);

  // \brief Access a string value from the config database. Default value used if no entry exist.
  std::string getStr(std::string key, std::string dflt);

  // \brief Access a vector of strings from the config database
  std::vector<std::string>  getStrV(std::string key);
  std::vector<std::string>  getStrV(std::string key, std::vector<std::string> dflt);

  // \brief Access an integer from the config database. Exception thrown if no entry exist.
  int getInt(std::string key, int dflt);

  // \brief Access an integer from the config database. Default value used if no entry exist.
  int getInt(std::string key);
  
  std::vector<int> getIntV(std::string key);
  std::vector<int> getIntV(std::string key, std::vector<int>dflt);

  // \brief Access a boolean from the config database. Exception thrown if no entry exist.
  bool getBool(std::string key);

  // \brief Access a boolean from the config database. Default value used if no entry exist
  bool getBool(std::string key, bool dflt);

  // \brief Access a real number from the config database
  float getNum(std::string key);
  float getNum(std::string key, float dflt);

  // \brief Access a vector of floats from the config database
  std::vector<float> getNumV(std::string key);
  std::vector<float> getNumV(std::string key, std::vector<float>dflt);

  // \brief returns true if the key is defined
  bool isDefined(std::string key);

  // \brief Add more user specified settings to the
  void addFile(std::string fileName);

  // \brief Set value
  inline void setBool(const std::string& key, bool value) { m_env.SetValue(key.c_str(), Form("%i",(int)value)); }
  inline void setInt(const std::string& key, int value) { m_env.SetValue(key.c_str(), Form("%i",value)); }
  inline void setNum(const std::string& key, float value) { m_env.SetValue(key.c_str(), Form("%g",value)); }
  inline void setStr(const std::string& key, const std::string& value) { m_env.SetValue(key.c_str(), value.c_str()); }
  inline void setIntV(const std::string& key, std::vector<int> value) { 
    std::vector<std::string> strv;
    for (auto v : value) {
      strv.push_back(Form("%i",v));
    }
    auto writeout = joinStrV(strv," ");
    setStr(key,writeout);
  }
  inline void setNumV(const std::string& key, std::vector<float> value) { 
    std::vector<std::string> strv;
    for (auto v : value) {
      strv.push_back(Form("%g",v));
    }
    auto writeout = joinStrV(strv," ");
    setStr(key,writeout);
  }
  inline void setStrV(const std::string& key, std::vector<std::string> value) { 
    setStr(key,joinStrV(value," "));
  }

  // \brief accessor to the TEnv database
  inline const TEnv *getTable() { return &m_env; }

  // \brief prints the TEnv database to screen
  void printTable();

  // \brief writes TEnv database to a file
  void writeTable(const std::string& filePath);

private:
  // \brief ensures that there is a value in the database assocated with key
  //        if not, abort with error message
  void ensureDefined(std::string key);
  inline void copyTable(const TEnv &env);
};

inline
Config &Config::operator=(const Config &rhs)
{
  rhs.m_env.Copy(m_env);
  copyTable(rhs.m_env);
  return *this;
}

inline
void Config::copyTable(const TEnv &env)
{
  m_env.GetTable()->Delete();
  THashList *hl = 0;

  if ((hl = env.GetTable())) {
    TIter next(hl);
    TEnvRec *env_rec = 0;

    while ((env_rec = (TEnvRec*)next.Next())) {
      m_env.SetValue(env_rec->GetName(), env_rec->GetValue());
    }
  }
}

