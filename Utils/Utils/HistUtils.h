#pragma once

#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "THStack.h"

#include "TProfile.h"
#include "TEfficiency.h"

#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
#include "TGraph2DErrors.h"
#include "TGraphAsymmErrors.h"

#include "TF1.h"

#include "Utils/Utils.h"
#include "Utils/Config.h"

// HistUtils
// (C++,STL) <-> TH1 <-> TGraph/TF1

namespace HistUtils
{

	template <typename TH>
	TH* copyHist(const std::string& dest, TH* source)
	{
		return (TH*)source->Clone(dest.c_str());
	}

	TH1F* makeHist(const std::string& hname, std::vector<float> bins);
	TH2F* makeHist(const std::string& hname, std::vector<float> xbins, std::vector<float> ybins);

	// histogram transformations
	// absolute value
	void abs(TH1F* hist);
	void abs(TH2F* hist);
	// rebinning
	int rebin(TH1F* hist, int rebin=-1);
	// integral / normalization
	float normalize(TH1F* hist, float normalizeTo=1.0);
	float normalize(TH1F* hist, TH1* normalizeTo);

	// histogram operations
	// bin edges
	std::vector<float> getBins(TH1F* hist);
	std::vector<float> getBins(TH2F* hist, int axis=1);
	// bin entries
	std::vector<float> getBinContents(TH1F* hist);
	std::vector<float> getBinErrors(TH1F* hist);
	// h1 (+) h2
	TH1F* addInQuadrature(TH1F* h1, TH1F* h2);
	TH1F* addInQuadrature(std::vector<TH1F*> hists);
	TH2F* addInQuadrature(TH2F* h1, TH2F* h2);
	TH2F* addInQuadrature(std::vector<TH2F*> hists);
	// 2D/3D -> 1D projection 
	TH1F* project(TH2F* xy, int axis=1, int bin=-1);
	TH1F* project(TProfile2D* xy, int axis=1, int bin=-1);
	TH1F* project(TH3F* xyz, int axis1, int bin1, int bin2=-1);
	// stack histograms
	// default: on top of each other
	THStack* stack(const std::string& name, std::vector<TH1F*> hists);
	// join histograms
	// {1D's} -> 2D
	TH2F* join(const std::string& name, std::vector<TH1F*> hists, std::vector<float> bins, int axis=1);

  //graph operations
  // construct graph out of histograms containing (x,y) values (& errors)
  TGraphAsymmErrors* makeGraph(const std::string& name, unsigned int npts, TH1* xval, TH1* yval, bool doxerr=false, bool doyerr=true);
  TH1F* ratio(TH1* hist, TGraphErrors* graph);
  TH1F* ratio(TGraphErrors* hist, TH1* graph);

  // function opertions
  // get histogram with bin content f(x) at bin centre x
  TH1F* makeHist(const std::string& name, TF1* ftn, int nbins=100, float xmin=0, float xmax=1.0);
  TH1F* makeHist(const std::string& name, TF1* ftn, std::vector<float> bins);
  TH1F* ratio(TH1F* hist, TF1* ftn);

}