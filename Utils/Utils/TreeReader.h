#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <type_traits>

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1I.h"
#include "TString.h"

#include "Utils/Utils.h"
#include "Utils/Config.h"

struct BranchReader
{
public:
  BranchReader(const std::string& branch) : m_branch(branch) {}
  virtual ~BranchReader() = default;
  std::string m_branch;
};
template <typename T_val>
struct ValueReader : public BranchReader
{
public:
  ValueReader (const std::string& branch, T_val dflt) :
    BranchReader(branch),
    m_val(dflt)
  {};
  void connectBranch(TChain* tree) {
    m_val_ptr = &m_val;
    tree->SetBranchStatus(m_branch.c_str(), 1);
    tree->SetBranchAddress(m_branch.c_str(), m_val_ptr);
  }
  T_val* get() {
    if (std::is_pointer<T_val>::value) return *(T_val**)m_val_ptr;
    return (T_val*)m_val_ptr;
  }
  T_val* operator->() { return get(); }
  T_val& operator*() { return *get(); }
private:
  T_val m_val;
  T_val* m_val_ptr;
};
template <typename T_ele>
struct VectorReader : public BranchReader
{
public:
  VectorReader (const std::string& branch, std::initializer_list<T_ele> dflt) :
    BranchReader(branch),
    m_vec(std::vector<T_ele>(dflt))
  {};
  void connectBranch(TChain* tree) {
    m_vec_ptr = &m_vec;
    tree->SetBranchStatus(m_branch.c_str(), 1);
    tree->SetBranchAddress(m_branch.c_str(), &m_vec_ptr);
  }
  std::vector<T_ele>* get() {
    return (std::vector<T_ele>*)m_vec_ptr;
  }
  std::vector<T_ele>* operator->() { return get(); }
  std::vector<T_ele>& operator*() { return *get(); }
private:
  std::vector<T_ele> m_vec;
  std::vector<T_ele>* m_vec_ptr;
};
    
class TreeReader
{

public:

  // constructor
  TreeReader(std::string name);
  // destructor 
  ~TreeReader();

public:

  // tree(s) to process
  // read from files (own tree)
  TChain* openTree(const std::string& treeName, const std::string& filePath);
  TChain* openTree(const std::string& treeName, const std::vector<std::string>& filePaths);
  // provide tree directly (external tree)
  void openTree(TChain* tree);
  void openTree(TTree* tree);

  // read tree entries 
  bool loopEntries(unsigned int interval=1, unsigned long long int start=0, long long int end=0);

  // access value branch
  template <typename T_val>
  T_val* readValue (const std::string& branch, T_val dflt) {
    ValueReader<T_val>* valueReader(nullptr);
    m_readBranches.push_back(branch);
    valueReader = new ValueReader<T_val>(branch, dflt); 
    valueReader->connectBranch(m_tree);
    m_valueReader[branch] = valueReader;
    return valueReader->get();
  }
  // access vector branch
  template <typename T_ele>
  std::vector<T_ele>* readVector (const std::string& branch, std::initializer_list<T_ele> dflt={}) {
    VectorReader<T_ele>* vectorReader(nullptr);
    m_readBranches.push_back(branch);
    vectorReader = new VectorReader<T_ele>(branch,dflt); 
    vectorReader->connectBranch(m_tree);
    m_vectorReader[branch] = vectorReader;
    return vectorReader->get();
  }

private:

  // tree objects
  std::string m_name;
  std::string m_treeName;
  TChain* m_tree;
  long long m_ientry;
  long long m_nentries;
  bool m_ownTree;

  // branches
  std::vector<std::string> m_readBranches;
  std::unordered_map<std::string,BranchReader*> m_valueReader;
  std::unordered_map<std::string,BranchReader*> m_vectorReader;

};