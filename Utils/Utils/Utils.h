#pragma once

// C++ includes(s):
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <vector>
#include <map>
#include <unordered_map>

// ROOT include(s):
#include "TROOT.h"
#include "TObject.h"
#include "TSystem.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TGraph2D.h"
#include "TGraph2DErrors.h"

// 1*GeV = 1000*MeV
static const float GeV(1000.0), invGeV(1.0/GeV);

// system operations
void timer();
void info(const std::string& msg,  const std::string& context="");
void warn(const std::string& msg,  const std::string& context="");
void error(const std::string& msg, const std::string& context="", bool fatal=false);

// vector operations
// make vector out of delimitted string
std::vector<std::string> vectorizeStr(std::string str, std::string sep = " ");
std::vector<int>         vectorizeInt(std::string str, std::string sep = " ");
std::vector<float>       vectorizeNum(std::string str, std::string sep = " ");
// join vector into delimitted string
std::string joinStrV(std::vector<std::string> strings, std::string delim = " ");
// check if object is in vector
bool eleInVec(int element, std::vector<int> vector);
bool eleInVec(const std::string& element, std::vector<std::string> vector);
// binning vectors
std::vector<float> make_lin(int N, float min, float max);
std::vector<float> makeLogVec(int N, float min, float max);
std::vector<float> makeFiner(std::vector<float> bins, int R);
int getBinNumber(float value, std::vector<float> bins);
std::vector<float> getBinEdges(float value, std::vector<float>bins);
// value is under/overflow
bool isUnderflow(float value, std::vector<float> bins);
bool isOverflow(float value, std::vector<float> bins);
bool isInRange(float value, std::vector<float> bins);

// error propagation
// add/subtract in quadrature
float subQ(float x, float y);
float addQ(float x, float y);
float addQErr(float x, float ex, float y, float ey);
float subQErr(float x, float ex, float y ,float ey);
// error propagation of a product
float prodErr(float x, float ex, float y, float ey);
float divErr(float x, float ex, float y, float ey, float cov=0.0);

// histograms
// booking
TH1F* makeHist(std::string hname, std::string title, std::vector<float> bins);
TH2F* makeHist2D(std::string hname, std::string title, std::vector<float> xbins, std::vector<float> ybins);
TH3F* makeHist3D(std::string hname, std::string title, std::vector<float> xbins, std::vector<float> ybins, std::vector<float> zbins);
// filling
void fillHist(TH1F* hist, float x, float w);
void fillHist2D(TH2F* hist, std::vector<float>x, float w);
void fillHist3D(TH3F* hist, std::vector<float>x, float w);

// bin edges
std::vector<float> getBins(TH1* hist, unsigned int axis=1);
// bin entries
std::vector<float> getBinContents(TH1F* hist);
std::vector<float> getBinErrors(TH1F* hist);

// input/output file operations
bool fileExists(std::string fn);
TFile* openInputFile(std::string ifName);
TFile* openOutputFile(std::string ofName, bool overWrite=false);
void closeFile(TFile*& file);

template <class T>
T* findObject (const std::string& histName, bool verbose = true) {
    T* hist = (T*)gROOT->FindObject(histName.c_str());
    if (!hist && verbose) error(Form("findObject: %s not found!",histName.c_str()));
    return hist;
}
template <class T, class TF>
T* getObject (const std::string& histPath, TF* inputFile, bool verbose = true) {
    T* hist(nullptr); inputFile->GetObject(histPath.c_str(),hist);
    if (!hist && verbose) error(Form("getObject: %s not found!",histPath.c_str()));
    return hist;
}