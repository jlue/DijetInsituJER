#include "Utils/Utils.h"

// printouts
void timer()
{
  static bool first=true;
  static time_t start;
  if(first) { std::cout << "timer started"; first=false; std::time(&start); }
  time_t aclock; std::time( &aclock );
  char tbuf[25]; std::strncpy(tbuf, std::asctime( std::localtime( &aclock ) ),24);
  tbuf[24]=0;
  std::cout << " ( "<< std::difftime( aclock, start) <<" s elapsed )" << std::endl;
}
void error(const std::string& msg, const std::string& context, bool fatal)
{
  if (context.length()>=22) {
    std::string truncated_context = context.substr(0,22);
    std::cout << std::left << std::setw(25) << std::setfill('.')
              << truncated_context
              << "[ERROR]   "
              << msg << std::endl;
  } else {
    std::cout << std::left << std::setw(25) << std::setfill(' ')
              << context
              << "[ERROR]   "
              << msg << std::endl;
  }
  if (fatal) exit(1);
}
void warn(const std::string& msg, const std::string& context)
{
  if (context.length()>=22) {
    std::string truncated_context = context.substr(0,22);
    std::cout << std::left << std::setw(25) << std::setfill('.')
              << truncated_context
              << "[WARNING] "
              << msg << std::endl;
  } else {
    std::cout << std::left << std::setw(25) << std::setfill(' ')
              << context
              << "[WARNING] "
              << msg << std::endl;
  }
}
void info(const std::string& msg, const std::string& context)
{
  if (context.length()>=22) {
    std::string truncated_context = context.substr(0,22);
    std::cout << std::left << std::setw(25) << std::setfill('.')
              << truncated_context
              << "[INFO]    "
              << msg << std::endl;
  } else {
    std::cout << std::left << std::setw(25) << std::setfill(' ')
              << context
              << "[INFO]    "
              << msg << std::endl;
  }
}

// vector operations
std::vector<std::string> vectorizeStr(std::string str, std::string sep)
{
  std::vector<std::string> result;
  TString tstr = TString(str.c_str());
  TObjArray *strings = tstr.Tokenize(sep.c_str());

  if (strings->GetEntries() == 0) { delete strings; return result; }

  TIter istr(strings);

  while (TObjString *os = (TObjString *)istr()) {
    // the number sign and everything after is treated as a comment
    if (os->GetString()[0] == '#') break;
    result.push_back(os->GetString().Data());
  }

  delete strings;
  return result;
}
std::vector<int> vectorizeInt(std::string str, std::string sep)
{
  std::vector<int> result;
  std::vector<std::string> vecS = vectorizeStr(str, sep);
  
  for (uint i = 0; i < vecS.size(); ++i)
  { result.push_back(atoi(vecS[i].c_str())); }
  
  return result;
}
std::vector<float> vectorizeNum(std::string str, std::string sep)
{
  std::vector<float>result;
  std::vector<std::string> vecS = vectorizeStr(str, sep);

  for (uint i = 0; i < vecS.size(); ++i)
  { result.push_back(atof(vecS[i].c_str())); }

  return result;
}
std::string joinStrV(std::vector<std::string> strings, std::string delim)
{
  std::string ret;
  for(const auto &s : strings) {
    if(ret!="") ret += delim;
    ret += s;
  }
  return ret;
}
bool eleInVec(int element, std::vector<int> vector) {
  return std::find(vector.begin(),vector.end(),element)!=vector.end();
}
bool eleInVec(const std::string& element, std::vector<std::string> vector)
{
  return std::find(vector.begin(),vector.end(),element)!=vector.end();
}

// binning operations
std::vector<float> make_lin(int N, float min, float max) {
  std::vector<float>vec; float dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}
std::vector<float> makeLogVec(int N, float min, float max) {
  std::vector<float> vec; double dx=(log(max)-log(min))/N;
  for (int i=0;i<=N;++i) vec.push_back(exp(log(min)+i*dx));
  return vec;
}

std::vector<float> makeFiner(std::vector<float> bins, int R)
{
  if(R<1)  { error(Form("Wrong value for granularity (%d).",R),"makeFiner"); return bins; }
  if(R==1) { return bins; }
  std::vector<float>finerBins;
  for (size_t iBin = 0; iBin < bins.size()-1; iBin++)
  {
    float width = bins[iBin+1] - bins[iBin];
    for (size_t iR = 0; iR < R; iR++)
    {
      finerBins.push_back( bins[iBin] + iR*width/R );
    }
  }
  finerBins.push_back(bins[bins.size()-1]);
  return finerBins;
}
std::vector<float> getBinEdges(float value, std::vector<float> bins) {
  int binNumber = getBinNumber(value,bins);
  if ( 1 <= binNumber && binNumber <= bins.size() ) {
    return std::vector<float>{bins.at(binNumber-1),bins.at(binNumber)};
  } else {
    error(Form("Value %g is not within range of bins.",value), "getBinEdges");
    return std::vector<float>{};
  }
}
int getBinNumber(float value, std::vector<float> bins) {
  if (value < bins.front()) return 0;
  for ( int iedge=0 ; iedge<bins.size() ; ++iedge) {
    if (bins[iedge] > value) return iedge;
  }
  return bins.size();
}
bool isUnderflow(float value, std::vector<float> bins) {
  return getBinNumber(value,bins)==0;
}
bool isOverflow(float value, std::vector<float> bins) {
  return getBinNumber(value,bins)==bins.size();
}
bool isInRange(float value, std::vector<float> bins) {
  return !isUnderflow(value,bins) && !isOverflow(value,bins);
}

// error propagation
float subQ(float x, float y) { 
  float val = sqrt(x*x-y*y); 
  return ( (std::isinf(val)||std::isnan(val)) ? 0.0 : val );
}
float addQ(float x, float y) { return sqrt(x*x+y*y); }
float addQErr(float x, float ex, float y, float ey) { return sqrt((x*x*ex*ex+y*y*ey*ey)/(x*x+y*y)); }
float subQErr(float x, float ex, float y, float ey) { 
  float val = sqrt((x*x*ex*ex+y*y*ey*ey)/(x*x-y*y)); 
  return ( (std::isinf(val)||std::isnan(val)) ? 0.0 : val );
}
// error propagation of a product
float prodErr(float x, float ex, float y, float ey) { return sqrt(pow(ex/x,2)+pow(ey/y,2))*(x*y); }
float divErr(float x, float ex, float y, float ey, float cov) { return sqrt(pow(ex/x,2)+pow(ey/y,2)-2*cov/(x*y))*(x/y); }

// histograms
TH1F* makeHist(std::string hname, std::string title, std::vector<float>bins) {
  TH1F* h = new TH1F(hname.c_str(),title.c_str(),bins.size()-1,&bins[0]);
  return h;
}
void fillHist(TH1F* hist, float x, float w){
  if (!hist) warn("fillHist : could not find histogram.");
  hist->Fill(x,w);
}
TH2F* makeHist2D(std::string hname, std::string title, std::vector<float>xbins, std::vector<float>ybins) {
  TH2F* h = new TH2F(hname.c_str(),title.c_str(),xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  return h;
}
void fillHist2D(TH2F* hist, std::vector<float>x, float w){
  if (x.size()!=2) error("fillHist2D: must fill histogram with a 2D vector!");
  if (!hist) warn("fillHist : could not find histogram.");
  hist->Fill(x[0],x[1],w);
}
TH3F* makeHist3D(std::string hname, std::string title, std::vector<float>xbins, std::vector<float>ybins, std::vector<float>zbins) {
  TH3F* h = new TH3F(hname.c_str(),title.c_str(),xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0],zbins.size()-1,&zbins[0]);
  return h;
}
void fillHist3D(TH3F* hist, std::vector<float>x, float w){
  if (x.size()!=3) error("fillHist3D: must fill histogram with a 3D vector!");
  if (!hist) warn("fillHist : could not find histogram.");
  hist->Fill(x[0],x[1],x[2],w);
}

// histogram bins
std::vector<float> getBins(TH1* hist, unsigned int axis) {
  std::vector<float> bins;

  switch(axis) {
    case 1:
      for (int ix=0 ; ix<=hist->GetNbinsX() ; ++ix) {
        bins.push_back(hist->GetXaxis()->GetBinLowEdge(ix+1));
      }
      break;
  case 2:
    for (int iy=0 ; iy<=hist->GetNbinsY() ; ++iy) {
      bins.push_back(hist->GetYaxis()->GetBinLowEdge(iy+1));
    }
    break;
  case 3:
    for (int iz=0 ; iz<=hist->GetNbinsZ() ; ++iz) {
      bins.push_back(hist->GetZaxis()->GetBinLowEdge(iz+1));
    }
    break;
  }
  return bins;
}
// bin contents
std::vector<float> getBinContents(TH1F* hist) {
  std::vector<float> contents;
  for (int ix=0 ; ix<=hist->GetNbinsX() ; ++ix) {
    contents.push_back(hist->GetBinContent(ix+1));
  }
  return contents;
}
std::vector<float> getBinErrors(TH1F* hist) {
  std::vector<float> errors;
  for (int ix=0 ; ix<=hist->GetNbinsX() ; ++ix) {
    errors.push_back(hist->GetBinError(ix+1));
  }
  return errors;
}

TGraph2DErrors* makeGraph2D(TH2F* x, TH2F* y, TH2F* z, TH2F* ex, TH2F* ey, TH2F* ez)
{
  int nxbins = z->GetNbinsX(), nybins = z->GetNbinsY();
  if ( nxbins != x->GetNbinsX() ) error("makeGraph : (x,z) values don't have the same dimension!");
  if ( nybins != y->GetNbinsY() ) error("makeGraph : (y,z) values don't have the same dimension!");
  TGraph2DErrors* graph = new TGraph2DErrors();
  int ipt=0;
  for (int ix=0 ; ix<nxbins ; ++ix) {
    for (int iy=0 ; iy<nybins ; ++iy) {
      if ( not std::isfinite(x->GetBinContent(ix+1,iy+1)) ) continue;
      if ( not std::isfinite(y->GetBinContent(ix+1,iy+1)) ) continue;
      if ( not std::isfinite(z->GetBinContent(ix+1,iy+1)) ) continue;
      if ( z->GetBinError(ix+1,iy+1)==0 ) continue;
      graph->SetPoint(ipt+1,x->GetBinContent(ix+1,iy+1),y->GetBinContent(ix+1,iy+1),z->GetBinContent(ix+1,iy+1));
      if (ez) {
        if (not std::isfinite(ez->GetBinContent(ix+1,iy+1))) continue;
        graph->SetPointError(ipt+1,0.0,0.0,ez->GetBinContent(ix+1,iy+1));
      } else {
        graph->SetPointError(ipt+1,0.0,0.0,z->GetBinError(ix+1,iy+1));
      }
      ++ipt;
    }
  }
  graph->SetName(y->GetName());
  return graph;
}
TGraph2DErrors* makeGraph2D(TProfile2D* x, TProfile2D* y, TH2F* z, TH2F* ex, TH2F* ey, TH2F* ez)
{
  int nxbins = z->GetNbinsX(), nybins = z->GetNbinsY();
  if ( nxbins != x->GetNbinsX() ) error("makeGraph : (x,z) values don't have the same dimension!");
  if ( nybins != y->GetNbinsY() ) error("makeGraph : (y,z) values don't have the same dimension!");
  TGraph2DErrors* graph = new TGraph2DErrors();
  int ipt=0;
  for (int ix=0 ; ix<nxbins ; ++ix) {
    for (int iy=0 ; iy<nybins ; ++iy) {
      if ( not std::isfinite(x->GetBinContent(ix+1,iy+1)) ) continue;
      if ( not std::isfinite(y->GetBinContent(ix+1,iy+1)) ) continue;
      if ( not std::isfinite(z->GetBinContent(ix+1,iy+1)) ) continue;
      if ( z->GetBinError(ix+1,iy+1)==0 ) continue;
      graph->SetPoint(ipt+1,x->GetBinContent(ix+1,iy+1),y->GetBinContent(ix+1,iy+1),z->GetBinContent(ix+1,iy+1));
      graph->SetPointError(ipt+1,0.0,0.0,z->GetBinError(ix+1,iy+1));
      if (ez) {
        if (not std::isfinite(ez->GetBinContent(ix+1,iy+1))) continue;
        graph->SetPointError(ipt+1,0.0,0.0,ez->GetBinContent(ix+1,iy+1));
      }
      ++ipt;
    }
  }
  graph->SetName(y->GetName());
  return graph;
}

// input/output file operations
bool fileExists(std::string fn)
{
  return (!gSystem->AccessPathName(fn.c_str()));
}
TFile* openInputFile(std::string ifName) {
  info("Input file:  "+ifName,"openInputFile");
  if (fileExists(ifName)) {
    return new TFile(ifName.c_str(),"READ");
  } else {
    error("File does not exists.","openInputFile");
    return nullptr;
  }
}
TFile* openOutputFile(std::string ofName, bool forceOverwrite) {
  info("Output file:  "+ofName,"openOutputFile");
  if (fileExists(ofName)) {
    if (!forceOverwrite) {
      error("File already exists.","openOutputFile",true);
    } else {
      warn("Overwriting.","openOutputFile");
    }
  }
  return new TFile(ofName.c_str(),"RECREATE");
}
void closeFile(TFile*& file) {
  file->Close();
  info(Form("Closing file:  %s",file->GetPath()),"closeFile");
}