#include "Utils//TreeReader.h"

// destructor
TreeReader::~TreeReader()
{
  for (auto reader : m_valueReader) delete reader.second; 
  for (auto reader : m_vectorReader) delete reader.second; 
  if (m_ownTree) delete m_tree;
}

// constructor
TreeReader::TreeReader(std::string name) :
  m_name(name),
  m_treeName(""),
  m_ownTree(false),
  m_tree(nullptr),
  m_ientry(0),
  m_nentries(0)
{}

// read tree from filepath(s)
TChain* TreeReader::openTree(const std::string& treeName, const std::string& filePath)
{
  m_treeName = treeName;
  m_tree = new TChain(treeName.c_str(),treeName.c_str());
  m_tree->AddFile(filePath.c_str(),TTree::kMaxEntries,treeName.c_str()); 
  m_ownTree = true;
  return m_tree;
}
TChain* TreeReader::openTree(const std::string& treeName, const std::vector<std::string>& filePaths)
{
  for (const auto& fp : filePaths) {
    openTree(treeName,fp);
  }
  return m_tree;
}
// input tree object
void TreeReader::openTree(TChain* tree)
{
  m_treeName = tree->GetName();
  m_tree = tree;
  m_ownTree = false;
  info("Opened "+m_treeName,TString::Format("TreeReader.%s::openTree",m_name.c_str()).Data());
  info(TString::Format("Number of entries = %i",m_tree->GetEntries()).Data(),TString::Format("TreeReader.%s::openTree",m_name.c_str()).Data());
}
void TreeReader::openTree(TTree* tree)
{
  openTree((TChain*)tree);
}

bool TreeReader::loopEntries(unsigned int interval, unsigned long long int start, long long int end) {

  // first entry
  static bool firstEntry=true;
  if (firstEntry) {
    firstEntry=false;
    info("========================================",TString::Format("TreeReader.%s::loopEntries",m_name.c_str()).Data());
    m_ientry = start;
    m_nentries = (end>0) ? end : m_tree->GetEntries();
  } 

  // progress tracker
  if (m_ientry>0 && m_ientry%100000==0) info(TString::Format("Entry number = %lli",m_ientry).Data(),TString::Format("TreeReader.%s::readTree",m_name.c_str()).Data());

  // process entry or finish
  bool allDone = false;
  while (m_ientry%interval!=0) ++m_ientry;  // increment entries until ith interval (default=every one) hit
  if (m_ientry<m_nentries) {  // entry to be processed
    m_tree->GetEntry(m_ientry);
    ++m_ientry;
  } else {  // all entries processed
    allDone = true;
    info("========================================",TString::Format("TreeReader.%s::loopEntries",m_name.c_str()).Data());
  }
  return !allDone;
}