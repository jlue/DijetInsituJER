#include "Utils/Config.h"
#include "Utils/TreeReader.h"
#include "Utils/HistHandler.h"
#include "Utils/Plotter.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class Config+;
#pragma link C++ class TreeReader+;
#pragma link C++ class HistHandler+;
#pragma link C++ class Plotter+;

#endif


