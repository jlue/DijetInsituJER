#include "Utils/Config.h"

/// ROOT include(s):
#include "TSystem.h"
#include "THashList.h"

Config::Config()
  : m_env("env")
{
  // Must have no pointer initialization, for CINT
  m_env.IgnoreDuplicates(true);
}

Config::Config(const Config &config)
  : Config()
{
  config.m_env.Copy(m_env);
  copyTable(config.m_env);
}

Config::Config(std::string fileName)
  : Config()
{
  addFile(fileName);
}

/*
Config::Config(TEnv *env) : Config() {
  m_env->Copy(env);
}
 */

void Config::ensureDefined(std::string key)
{
  if (!isDefined(key.c_str())) { error("No value found for " + key, "Config::ensureDefined", true); }
}

bool Config::isDefined(std::string key)
{
  return m_env.Defined(key.c_str());
}

std::string Config::getStr(std::string key)
{
  ensureDefined(key.c_str());
  return m_env.GetValue(key.c_str(), "");
}

std::string Config::getStr(std::string key, std::string dflt)
{
  return m_env.GetValue(key.c_str(), dflt.c_str());
}

int Config::getInt(std::string key)
{
  ensureDefined(key.c_str());
  return m_env.GetValue(key.c_str(), -99);
}

int Config::getInt(std::string key, int dflt)
{
  return m_env.GetValue(key.c_str(), dflt);
}

bool Config::getBool(std::string key, bool dflt)
{
  return m_env.GetValue(key.c_str(), dflt);
}

bool Config::getBool(std::string key)
{
  ensureDefined(key.c_str());
  return getBool(key, false);
}

float Config::getNum(std::string key)
{
  ensureDefined(key.c_str());
  return m_env.GetValue(key.c_str(), -99.0);
}

float Config::getNum(std::string key, float dflt)
{
  if (isDefined(key.c_str())) {
    return m_env.GetValue(key.c_str(),-99.0);
  }
  return dflt;
}

std::vector<std::string> Config::getStrV(std::string key)
{
  ensureDefined(key.c_str());
  return vectorizeStr(m_env.GetValue(key.c_str(), ""), " \t");
}

std::vector<std::string> Config::getStrV(std::string key, std::vector<std::string> dflt)
{
  if (isDefined(key.c_str())) {
    return vectorizeStr(m_env.GetValue(key.c_str(), ""), " \t");
  }
  return dflt;
}

std::vector<int> Config::getIntV(std::string key)
{
  ensureDefined(key.c_str());
  return vectorizeInt(m_env.GetValue(key.c_str(), ""), " \t");
}

std::vector<int> Config::getIntV(std::string key, std::vector<int>dflt)
{
  if (isDefined(key.c_str())) {
    return vectorizeInt(m_env.GetValue(key.c_str(), ""), " \t");
  }
  return dflt;
}

std::vector<float> Config::getNumV(std::string key)
{
  ensureDefined(key.c_str());
  return vectorizeNum(m_env.GetValue(key.c_str(), ""), " \t");
}

std::vector<float> Config::getNumV(std::string key, std::vector<float>dflt)
{
  if (isDefined(key.c_str())) {
    return vectorizeNum(m_env.GetValue(key.c_str(), ""), " \t");
  }
  return dflt;
}

void Config::printTable()
{
  TIter next(m_env.GetTable());
  while (TEnvRec *er = (TEnvRec *) next()) {
    printf("  %-60s%s\n", Form("%s:", er->GetName()), er->GetValue());
  }
}
void Config::writeTable(const std::string& filePath)
{
  m_env.WriteFile(filePath.c_str());
  // TIter next(m_env.GetTable());
  // std::ofstream of;
  // of.open(filePath.c_str());
  // while (TEnvRec *er = (TEnvRec *) next()) {
  //   of << Form("%-60s%s\n", Form("%s:", er->GetName()), er->GetValue());
  // }
  // of.close();
}

void Config::addFile(std::string fileName)
{
  // settings read in by files should not overwrite values set by setStr()
  TEnv env;
  int status = env.ReadFile(fileName.c_str(), EEnvLevel(0));

  if (status != 0) { error("Cannot read settings file " + fileName); }

  TIter next(env.GetTable());

  while (TEnvRec *er = (TEnvRec *) next()) {
    if (!isDefined(er->GetName())) { setStr(er->GetName(), er->GetValue()); }
  }
}

