// package include(s)
#include "Utils/Plotter.h"

//==============================================================================
// Plotter Class
//==============================================================================

Plotter::Plotter (std::string name) :
	m_name(name),
  m_nrow(0), m_ncol(0),
  m_logx(false), m_logy(false),
  m_sharex(false), m_sharey(false),
  m_legend(nullptr)
{
  makeCanvas(800,600);
  makePad();
  makeStyle();
  applyStyle();
}

TCanvas* Plotter::makeCanvas(int w, int h) {
  m_canvasWidth = w; m_canvasHeight = h;
  TCanvas* c = findObject<TCanvas>("canvas",false);
  if (c) {
    m_canvas = c; 
    m_canvas->SetCanvasSize(w,h);
  } else {
    m_canvas = new TCanvas("canvas","canvas",w,h);
  }
  m_canvas->SetWindowSize(w + (w - m_canvas->GetWw()), h + (h - m_canvas->GetWh()));
  m_canvas->cd();
  return m_canvas;
}

TPad* Plotter::makePad() {
  makePads(1,{1},1,{1});
  return m_pads[0][0];
}
void Plotter::makePads(int nrow, std::initializer_list<float> hs, int ncol, std::initializer_list<float> ws) {
  std::vector<float> widths = ws.size() ? std::vector<float>(ws) : std::vector<float>(ncol,1.0);
  std::vector<float> heights = hs.size() ? std::vector<float>(hs) : std::vector<float>(nrow,1.0);
  makePads(nrow,heights,ncol,widths);
}
void Plotter::makePads(int nrow, std::vector<float> hs, int ncol, std::vector<float> ws) {
  clearPads();
  m_nrow = nrow; m_ncol = ncol;
  // compute total
  float totalWidth = 0.0; for (auto& w : ws) totalWidth += w;
  float totalHeight = 0.0; for (auto& h : hs) totalHeight += h;
  // normalize
  for (auto& w : ws) m_padWidth.push_back(w/totalWidth);
  for (auto& h : hs) m_padHeight.push_back(h/totalHeight);
  // start at top left corner and traverse to bottom right on canvas
  float x = 0.0, y= 1.0;
  m_padX = {x}; m_padY = {y};
  for (int icol = 1 ; icol <= ncol ; ++icol) {
    x += m_padWidth[icol-1];
    m_padX.push_back(x);
  }
  for (int irow = 1 ; irow <=nrow ; ++irow) {
    y -= m_padHeight[irow-1];
    m_padY.push_back(y);
  }
  // make pads of specified sizes & calculated places
  for (int irow=0 ; irow < nrow ; ++irow) {
    std::vector<TPad*> rowPads;
    std::vector<TH1*> rowAxes;
    for (int icol = 0; icol < ncol ; ++icol) {
      std::string padName = Form("pad(%i,%i)",irow+1,icol+1);
      TPad* pad = findObject<TPad>(padName,false);
      if (!pad) pad = new TPad(padName.c_str(),padName.c_str(),0,0,1,1);
      pad->SetFillStyle(4000);
      pad->Draw();
      rowPads.push_back(pad);
      std::string axisName = Form("axis(%i,%i)",irow+1,icol+1);
      TH2F* axis = findObject<TH2F>(axisName,false);
      if (!axis) axis = new TH2F(axisName.c_str(),"",10,0,1,10,0,1);
      rowAxes.push_back(axis);
    }
    m_pads.push_back(rowPads);
    m_axes.push_back(rowAxes);
  }
}

TH1* Plotter::makeAxis(int row, int col, std::string xtitle, float xmin, float xmax, std::string ytitle, float ymin, float ymax) {
  TH1* axis = getAxis(row,col);
  axis->GetXaxis()->Set(10,xmin,xmax);
  axis->GetXaxis()->SetTitle(xtitle.c_str());
  axis->GetYaxis()->Set(10,ymin,ymax);
  axis->GetYaxis()->SetTitle(ytitle.c_str());
  axis->Draw("axis");
  return axis;
}
TH1* Plotter::makeAxis(std::string xtitle, float xmin, float xmax, std::string ytitle, float ymin, float ymax) {
  return makeAxis(1,1,xtitle,xmin,xmax,ytitle,ymin,ymax);
}

void Plotter::logAxis (bool logx, bool logy)
{
  m_logx = logx; m_logy = logy;
  m_currentPad->SetLogx(logx);
  m_currentPad->SetLogy(logy);
  m_currentPad->Draw();
  m_currentAxis->GetXaxis()->SetMoreLogLabels(logx);
  m_currentAxis->GetYaxis()->SetMoreLogLabels(logy);
  m_currentAxis->Draw("axis");
}
void Plotter::shareAxis (bool sharex, bool sharey)
{
  m_sharex = sharex; m_sharey = sharey;
  for (int irow=0 ; irow < m_nrow ; ++irow) {
    for (int icol = 0; icol < m_ncol ; ++icol) {
      TPad* pad = getPad(irow+1,icol+1);
      TH1* axis = getAxis(irow+1,icol+1);
      float xTickScale = 1.0/(m_padX[icol+1]-m_padX[icol])*std::min(m_canvasWidth/m_canvasHeight,(float)1.0);
      float yTickScale = 1.0/(m_padY[irow]-m_padY[irow+1])*std::min(m_canvasHeight/m_canvasWidth,(float)1.0);
      float cellTopMargin = m_topMargin+m_padY[irow];
      float cellBottomMargin = m_bottomMargin+1-m_padY[irow+1];
      float cellLeftMargin = m_leftMargin+m_padX[icol];
      float cellRightMargin = m_rightMargin+1-m_padX[icol+1];
      if ( sharex ) {
        pad->SetLogx(m_logx);
        axis->GetXaxis()->Set(10,m_axes[m_row-1][m_col-1]->GetXaxis()->GetXmin(),m_axes[m_row-1][m_col-1]->GetYaxis()->GetXmax());
        if (irow!=m_nrow-1) {
          axis->GetXaxis()->SetLabelSize(0);
          axis->GetXaxis()->SetTitleSize(0);
        }
        axis->GetXaxis()->SetMoreLogLabels(m_logx);
        axis->GetYaxis()->ChangeLabel(1,-1,0);
        axis->GetYaxis()->ChangeLabel(-1,-1,0);
        cellTopMargin = m_topMargin+(1-m_topMargin-m_bottomMargin)*m_padY[irow];
        cellBottomMargin = m_bottomMargin+(1-m_topMargin-m_bottomMargin)*(1-m_padY[irow+1]);
        xTickScale *= 0.5;
      }
      if ( sharey ) {
        pad->SetLogy(m_logy);
        axis->GetYaxis()->Set(10,m_axes[m_row-1][m_col-1]->GetYaxis()->GetXmin(),m_axes[m_row-1][m_col-1]->GetYaxis()->GetXmax());
        if (icol!=0) {
          axis->GetYaxis()->SetLabelSize(0);
          axis->GetYaxis()->SetTitleSize(0);
        }
        axis->GetYaxis()->SetMoreLogLabels(m_logy);
        axis->GetXaxis()->ChangeLabel(1,-1,0);
        axis->GetXaxis()->ChangeLabel(-1,-1,0);
        cellLeftMargin = m_leftMargin+(1-m_leftMargin-m_rightMargin)*m_padX[icol];
        cellRightMargin = m_rightMargin+(1-m_leftMargin-m_rightMargin)*(1-m_padX[icol+1]);
        yTickScale *= 0.5;
      }
      pad->SetTopMargin(cellTopMargin);
      pad->SetBottomMargin(cellBottomMargin);
      pad->SetLeftMargin(cellLeftMargin);
      pad->SetRightMargin(cellRightMargin);
      axis->GetXaxis()->SetTickLength(m_style->GetTickLength("x")*xTickScale);
      axis->GetYaxis()->SetTickLength(m_style->GetTickLength("y")*yTickScale);
      pad->Draw();
      axis->Draw("axis");
    }
  }
}

void Plotter::setMarker (TH1* hist, int mcolor, int mstyle, float msize)
{
  hist->SetMarkerColor(mcolor);
  hist->SetMarkerStyle(mstyle);
  hist->SetMarkerSize(msize);
}
void Plotter::setLine (TH1* hist, int lcolor, int lstyle, int lwidth)
{
  hist->SetLineColor(lcolor);
  hist->SetLineStyle(lstyle);
  hist->SetLineWidth(lwidth);
}
void Plotter::setFill (TH1* hist, int fcolor, float fstyle)
{
  if (fstyle>1) {
    hist->SetFillColor(fcolor);
    hist->SetFillStyle(fstyle);
  } else {
    hist->SetFillColorAlpha(fcolor,fstyle);
  }
}
void Plotter::setMarker (TGraph* gr, int mcolor, int mstyle, float msize)
{
  gr->SetMarkerColor(mcolor);
  gr->SetMarkerStyle(mstyle);
  gr->SetMarkerSize(msize);
}
void Plotter::setLine (TGraph* gr, int lcolor, int lstyle, int lwidth)
{
  gr->SetLineColor(lcolor);
  gr->SetLineStyle(lstyle);
  gr->SetLineWidth(lwidth);
}
void Plotter::setFill (TGraph* gr, int fcolor, float fstyle)
{
  if (fstyle>1) {
    gr->SetFillColor(fcolor);
    gr->SetFillStyle(fstyle);
  } else {
    gr->SetFillColorAlpha(fcolor,fstyle);
  }
}
void Plotter::setLabel(TObject* plot , const std::string& label, const std::string& opt) {
  m_legendObjects.push_back(plot);
  m_legendLabels.push_back(label);
  m_legendOptions.push_back(opt);
}

TLegend* Plotter::makeLegend(float x1, float x2, float y1, float y2, int ncol) {
  getCanvas();
  m_legend = new TLegend(x1, y1, x2, y2);
  m_legend->SetNColumns(ncol);
  for (unsigned int ientry(0) ; ientry<m_legendObjects.size() ; ++ientry) {
    m_legend->AddEntry(m_legendObjects.at(ientry),m_legendLabels[ientry].c_str(),m_legendOptions[ientry].c_str());
  }
  m_legend->Draw("same");
  return m_legend;
}

void Plotter::addLabel (float x, float y, const std::string& label, int col) {
  int currentRow = m_row;
  int currentCol = m_col;
  // go to global canvas to draw label
  getCanvas();
  TLatex l;
  l.SetNDC();
  l.SetTextFont(m_style->GetTextFont());
  l.SetTextSize(m_style->GetTextSize());
  l.SetTextColor(col);
  l.DrawLatex(x,y,label.c_str());
  // go back to pad being plotted
  getPad(currentRow,currentCol);
}
void Plotter::addLogo (float x, float y, const std::string& label, const std::string& logo)
{
  getCanvas();
  TLatex l;
  l.SetNDC();
  l.SetTextFont(m_style->GetTextFont());
  l.SetTextSize(m_style->GetTextSize());
  l.SetTextColor(kBlack);
  l.DrawLatex(x,y,Form("%s %s",logo.c_str(),label.c_str()));
}

void Plotter::drawLine (float x1, float y1, float x2, float y2, int lineColor, int lineStyle, int lineWidth) {
  TGraph* line = new TGraph(2);
  line->SetPoint(0, x1, y1);
  line->SetPoint(1, x2, y2);
  line->SetLineWidth(lineWidth);
  line->SetLineStyle(lineStyle);
  line->SetLineColor(lineColor);
  line->Draw("L same");
}
void Plotter::drawVLine (float x, int lineColor, int lineStyle, int lineWidth) {
  float x1=x, x2=x;
  float y1=-1e7, y2=1e7;
  drawLine(x1,y1, x2,y2, lineColor,lineStyle,lineWidth);
}
void Plotter::drawHLine (float y, int lineColor, int lineStyle, int lineWidth) {
  float y1=y, y2=y;
  float x1 = -1e7, x2 = 1e7;
  drawLine(x1,y1, x2,y2, lineColor,lineStyle,lineWidth);
}

void Plotter::addHist(TH1* hist, const std::string& opt) {
  m_currentPlot = hist;
  m_currentPlot->Draw(Form("%s same",opt.c_str()));
}
void Plotter::addFtn(TF1* ftn, const std::string& opt) {
  m_currentPlot = ftn->GetHistogram();
  m_currentPlot->Draw(Form("%s same",opt.c_str()));
}
void Plotter::addGraph(TGraph* gr, const std::string& opt) {
  m_currentPlot = (TH1*)gr;
  gr->Draw(opt.c_str());
}

void Plotter::setMarker (int mcolor, int mstyle, float msize)
{
  setMarker(m_currentPlot,mcolor,mstyle,msize);
}
void Plotter::setLine (int lcolor, int lstyle, int lwidth)
{
  setLine(m_currentPlot,lcolor,lstyle,lwidth);
}
void Plotter::setFill (int fcolor, float fstyle)
{
  setFill(m_currentPlot,fcolor,fstyle);
}
void Plotter::setLabel(const std::string& label, const std::string& opt) {
  setLabel((TObject*)m_currentPlot,label,opt);
}

void Plotter::makePlot() {
  for (int irow=0 ; irow < m_nrow ; ++irow) {
    for (int icol=0; icol < m_ncol ; ++icol) {
      m_pads[irow][icol]->Modified();
      m_pads[irow][icol]->Update();
    }
  }
  m_canvas->Modified();
  m_canvas->Update();
}

//==============================================================================
// Plot Style (Default: ATLAS)
//==============================================================================

// Style definition
//------------------------------------------------------------------------------
TStyle* Plotter::makeStyle(const std::string& styleName)
{
  m_style = new TStyle(styleName.c_str(),(styleName+" plot style").c_str());

  // use plain black on white colors
  Int_t icol=0; // WHITE
  m_style->SetFrameBorderMode(icol);
  m_style->SetFrameFillColor(icol);
  m_style->SetCanvasBorderMode(icol);
  m_style->SetCanvasColor(icol);
  m_style->SetPadBorderMode(icol);
  m_style->SetPadColor(icol);
  m_style->SetStatColor(icol);
  // 2D histogram color palette
  gStyle->SetPalette(kBird);

  // set the paper & margin sizes
  m_style->SetPaperSize(20,26);

  // set margin sizes
  m_style->SetPadTopMargin(0.05);
  m_style->SetPadBottomMargin(0.15);
  m_style->SetPadLeftMargin(0.15);
  m_style->SetPadRightMargin(0.05);

  // set title offsets (for axis label)
  m_style->SetTitleXOffset(1.50);
  m_style->SetTitleYOffset(1.50);

  // font / size 
  int font=43; // Helvetica
  float tsize=24;
  m_style->SetTextFont(font);
  m_style->SetTextSize(tsize);
  m_style->SetTitleFont(font,"x");
  m_style->SetTitleSize(tsize,"x");
  m_style->SetLabelFont(font,"x");
  m_style->SetLabelSize(tsize,"x");
  m_style->SetTitleFont(font,"y");
  m_style->SetTitleSize(tsize,"y");
  m_style->SetLabelFont(font,"y");
  m_style->SetLabelSize(tsize,"y");
  m_style->SetTitleFont(font,"z");
  m_style->SetTitleSize(tsize,"z");
  m_style->SetLabelFont(font,"z");
  m_style->SetLabelSize(tsize,"z");
  m_style->SetLegendFont(font);
  m_style->SetLegendTextSize(tsize);

  // get rid of X error bars
  // m_style->SetErrorX(0.0001);
  // get rid of error bar caps
  m_style->SetEndErrorSize(0.0);

  // no title / stats / fit box
  m_style->SetOptTitle(0);
  m_style->SetOptStat(0);
  m_style->SetOptFit(0);

  // put tick marks on top and RHS of plots
  m_style->SetPadTickX(1);
  m_style->SetPadTickY(1);

  return m_style;
}
void Plotter::applyStyle (TStyle* style)
{
  std::string styleName = style ? style->GetName() : m_style->GetName();
  gROOT->SetStyle(styleName.c_str());
  gROOT->ForceStyle();
  m_canvas->UseCurrentStyle();
  for (int irow=0 ; irow < m_nrow ; ++irow) {
    for (int icol=0; icol < m_ncol ; ++icol) {
      m_pads[irow][icol]->UseCurrentStyle();
      m_axes[irow][icol]->UseCurrentStyle();
    }
  }
}
void Plotter::setMargins(int topMargin, int bottomMargin, int leftMargin, int rightMargin) {
  m_style->SetPadTopMargin(m_topMargin);
  m_style->SetPadBottomMargin(m_bottomMargin);
  m_style->SetPadLeftMargin(m_leftMargin);
  m_style->SetPadRightMargin(m_rightMargin);
  applyStyle();
}

void Plotter::clearPads() {
  m_pads.clear();
  m_axes.clear();
  m_padX.clear(); m_padWidth.clear();
  m_padY.clear(); m_padHeight.clear();
}

void Plotter::clearLegend() {
  m_legendObjects.clear();
  m_legendLabels.clear();
  m_legendOptions.clear();
}