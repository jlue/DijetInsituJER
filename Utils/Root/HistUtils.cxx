#include "Utils/HistUtils.h"

namespace HistUtils
{

TH1F* makeHist(const std::string& hname, std::vector<float> bins) {
  TH1F* h = new TH1F(hname.c_str(),hname.c_str(),bins.size()-1,&bins[0]);
  return h;
}
TH2F* makeHist(const std::string& hname, std::vector<float> xbins, std::vector<float> ybins) {
  TH2F* h = new TH2F(hname.c_str(),hname.c_str(),xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  return h;
}

// HISTOGRAM DATA
std::vector<float> getBins(TH1F* x)
{
  std::vector<float> bins;
  for (int ix=0 ; ix<x->GetNbinsX()+1 ; ++ix)
    bins.push_back(x->GetXaxis()->GetBinLowEdge(ix+1));
  return bins;
}
std::vector<float> getBins(TH2F* xy, int axis)
{
  std::vector<float> bins;
  switch(axis) {
    case 1: {
      for (int ix=0 ; ix<xy->GetNbinsX()+1 ; ++ix) {
        bins.push_back(xy->GetXaxis()->GetBinLowEdge(ix+1));
      }
      break;
    }
    case 2: {
      for (int iy=0 ; iy<xy->GetNbinsY()+1 ; ++iy) {
        bins.push_back(xy->GetYaxis()->GetBinLowEdge(iy+1));
      }
      break;
    }
  }
  return bins;
}

// HISTOGRAM TRANSFORMATIONS
void abs(TH2F* hist)
{
  for (int ix=0 ; ix<hist->GetNbinsX() ; ++ix) {
    for (int iy=0 ; iy<hist->GetNbinsY() ; ++iy) {
      float err = hist->GetBinError(ix+1,iy+1);
      hist->SetBinContent(ix+1,iy+1,fabs(hist->GetBinContent(ix+1,iy+1)));
      hist->SetBinError(ix+1,iy+1,err);
    }
  }
}
int rebin(TH1F* hist, int rebin)
{
  if (rebin<0) {
    // optimal binning using Scott's Choice
    double N=hist->GetEffectiveEntries();
    double optWidth = 3.5*hist->GetRMS()/pow(N,1.0/3.0);
    int Nbins=hist->GetNbinsX();
    double range=hist->GetBinLowEdge(Nbins+1)-hist->GetBinLowEdge(1);
    int optRebin=1;
    double prevWidth=range/Nbins;
    for (int i=1;i<Nbins;++i) {
      if (Nbins%i!=0) continue;
      double binWidth=range/Nbins*i;
      // optimistic
      if (binWidth<optWidth) optRebin=i;
      if (binWidth>=optWidth) break;
      prevWidth=binWidth;
    }
    hist->Rebin(optRebin);
    return optRebin;
  } else if (rebin>=1) {
    // manual rebinning
    hist->Rebin(rebin);
    return rebin;
  } else {
    // did not rebin
    return 1;
  }
}
float normalize(TH1F* hist, float normalizeTo)
{
  float normalizationFactor = normalizeTo / hist->Integral("width");
  hist->Scale(normalizationFactor);
  return normalizationFactor;
}
float normalize(TH1F* hist, TH1F* normalizeTo)
{
  float normalizationFactor = normalizeTo->Integral("width") / hist->Integral("width");
  hist->Scale(normalizationFactor);
  return normalizationFactor;
}

TH1F* project(TH2F* xy, int axis, int bin)
{
  TH1F* proj(nullptr);
  switch(axis) {
    case 1: { proj = (TH1F*)xy->ProjectionX(TString::Format("%s_projx%i",xy->GetName(),bin).Data(),bin,bin); break; }
    case 2: { proj = (TH1F*)xy->ProjectionY(TString::Format("%s_projy%i",xy->GetName(),bin).Data(),bin,bin); break; }
    default: error(TString::Format("Cannot project %s on axis=%i",xy->GetName(),axis).Data(),"project");
  }
  return proj;
}
TH1F* project(TProfile2D* xy, int axis, int bin)
{
  TH1F* proj(nullptr);
  switch(axis) {
    case 1: { proj = (TH1F*)xy->ProjectionX(TString::Format("%s_projx%i",xy->GetName(),bin).Data(),bin,bin); break; }
    case 2: { proj = (TH1F*)xy->ProjectionY(TString::Format("%s_projy%i",xy->GetName(),bin).Data(),bin,bin); break; }
    default: error(TString::Format("Cannot project %s on axis=%i",xy->GetName(),axis).Data(),"project");
  }
  return proj;
}
TH1F* project(TH3F* xyz, int axis, int bin1, int bin2)
{
  TH1F* proj(nullptr);
  switch(axis) {
    case 1: { proj = (TH1F*)xyz->ProjectionX(TString::Format("%s_projx%i%i",xyz->GetName(),bin1,bin2).Data(),bin1,bin1,bin2,bin2); break; }
    case 2: { proj = (TH1F*)xyz->ProjectionY(TString::Format("%s_projy%i%i",xyz->GetName(),bin1,bin2).Data(),bin1,bin1,bin2,bin2); break; }
    case 3: { proj = (TH1F*)xyz->ProjectionZ(TString::Format("%s_projz%i%i",xyz->GetName(),bin1,bin2).Data(),bin1,bin1,bin2,bin2); break; }
    default: error(TString::Format("Cannot project %s on axis=%i",xyz->GetName(),axis).Data(),"project");
  }
  return proj;
}

TH2F* join(const std::string& name, std::vector<TH1F*> hists, std::vector<float> bins, int axis)
{
  TH2F* joined(nullptr);
  if (axis==1) {
    std::vector<float>  ybins = HistUtils::getBins(hists[0]);
    joined = new TH2F(name.c_str(),"",hists.size(),&bins[0],hists[0]->GetNbinsX(),&ybins[0]);
    for (int ix=0 ; ix<joined->GetNbinsX() ; ++ix) {
      for (int iy=0 ; iy<joined->GetNbinsY() ; ++iy) {
        joined->SetBinContent(ix+1,iy+1,hists[ix]->GetBinContent(iy+1));
        joined->SetBinError(ix+1,iy+1,hists[ix]->GetBinError(iy+1));
      }
    }
  } else if (axis==2) {
    std::vector<float>  xbins = HistUtils::getBins(hists[0]);
    joined = new TH2F(name.c_str(),"",hists[0]->GetNbinsX(),&xbins[0],hists.size(),&bins[0]);
    for (int ix=0 ; ix<joined->GetNbinsX() ; ++ix) {
      for (int iy=0 ; iy<joined->GetNbinsY() ; ++iy) {
        joined->SetBinContent(ix+1,iy+1,hists[iy]->GetBinContent(ix+1));
        joined->SetBinError(ix+1,iy+1,hists[iy]->GetBinError(ix+1));
      }
    }
  }
  return joined;
}

TH1F* addInQuadrature(TH1F* h1, TH1F* h2)
{
  TH1F* h = (TH1F*)h1->Clone();
  for (int ibin=0 ; ibin<h1->GetNbinsX() ; ++ibin) {
    h->SetBinContent(ibin+1,addQ(h1->GetBinContent(ibin+1), h2->GetBinContent(ibin+1)));
    h->SetBinError(ibin+1,0.0);
  }
  return h;
}
TH2F* addInQuadrature(TH2F* h1, TH2F* h2)
{
  TH2F* h = (TH2F*)h1->Clone();
  for (int ix=0 ; ix<h1->GetNbinsX() ; ++ix) {
    for (int iy=0 ; iy<h1->GetNbinsY() ; ++iy ) {
      h->SetBinContent(ix+1,iy+1,addQ(h1->GetBinContent(ix+1,iy+1), h2->GetBinContent(ix+1,iy+1)));
      h->SetBinError(ix+1,iy+1,0.0);
    }
  }
  return h;
}

TGraphAsymmErrors* makeGraph(const std::string& name, unsigned int npts, TH1* xval, TH1* yval, bool doxerr, bool doyerr)
{
  TGraphAsymmErrors* graph = new TGraphAsymmErrors(npts);
  graph->SetName(name.c_str());
  for (int ix=0 ; ix<npts ; ++ix) {
    if ( !std::isfinite(xval->GetBinContent(ix+1)) ) continue;
    if ( !std::isfinite(yval->GetBinContent(ix+1)) ) continue;
    float x = xval->GetBinContent(ix+1);
    float y = yval->GetBinContent(ix+1);
    float xerrlow = doxerr ? x - xval->GetBinLowEdge(ix+1) : 0.0;
    float xerrup = doxerr ? xval->GetBinLowEdge(ix+2) - x : 0.0;
    float yerr = doyerr ? yval->GetBinError(ix+1) : 0.0;
    graph->SetPoint(ix,x,y);
    graph->SetPointError(ix,xerrlow,xerrup,yerr,yerr);
  }
  return graph;
}

TH1F* makeHist(const std::string& name, TF1* ftn, int nbins, float xmin, float xmax)
{
  // ftn->SetNpx(nbins);
  // if (xmax>xmin) ftn->SetRange(xmin,xmax);
  TH1F* hist = new TH1F(name.c_str(),"",nbins,xmin,xmax);
  for (int ix=0 ; ix<hist->GetNbinsX() ; ++ix) {
    hist->SetBinContent(ix+1,ftn->Eval(hist->GetBinCenter(ix+1)));
    hist->SetBinError(ix+1,0.0);
  }
  return hist;
}
TH1F* makeHist(const std::string& name, TF1* ftn, std::vector<float> bins)
{
  TH1F* hist = new TH1F(name.c_str(),"",bins.size()-1,&bins[0]);
  for (int ix=0 ; ix<hist->GetNbinsX() ; ++ix) {
    hist->SetBinContent(ix+1,ftn->Eval(hist->GetBinCenter(ix+1)));
    hist->SetBinError(ix+1,0.0);
  }
  return hist;
}

}
