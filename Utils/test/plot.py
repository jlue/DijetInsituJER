import ROOT

plotter = ROOT.Plotter()
plotter.makeCanvas(1000,500)
plotter.makePad()
plotter.openFile("test.pdf")

plotter.makeAxis("x",-1,1,"y",0.0,50.0)

hist = ROOT.TH1F("h","h",10,-1,1)
hist.FillRandom("gaus",100)

plotter.addHist(hist)
plotter.setMarker(ROOT.kRed,ROOT.kFullCircle,1.2)

plotter.savePlot()

plotter.closeFile()
