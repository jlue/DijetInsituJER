#!/usr/bin/env python

import os
import subprocess

# argument parser
import argparse
parser = argparse.ArgumentParser(description='DijetHistograms inputs, configs, and output')
parser.add_argument('--files', dest='files', type=str, nargs='+', required=True,
help='files containing histograms to add')
parser.add_argument('--histogram', dest='histogram', type=str, required=True,
help='histogram path/name')
parser.add_argument('--output', dest='output', type=str, default='./hist_add.root',
help='output file')
parser.add_argument('--draw', action='store_true')
parser.add_argument('--force', action='store_true')

parser.add_argument('--TH1I', action='store_true')
parser.add_argument('--TH1F', action='store_true')
parser.add_argument('--TH1D', action='store_true')

args = parser.parse_args()

from ROOT import TFile, TH1I, TH1F, TH1D, THStack

histName = args.histogram

# copy over histogram structure from first file
firstHist = TH1D()
if args.TH1I:
  firstHist.__class__ = TH1I
if args.TH1F:
  firstHist.__class__ = TH1F
if args.TH1D:
  firstHist.__class__ = TH1D

firstFile = TFile(args.files[0],"read")
firstFile.GetObject(histName,firstHist)
histTotal = firstHist.Clone()
histTotal.Reset()

for file in args.files:
  # get each file
  histFile = TFile(file,"read")
  # same type (TH1I/F/D) as first
  histToAdd = firstHist
  # overwrite it with histogram to be added
  histFile.GetObject(histName,histToAdd)
  # add it to the total
  histTotal.Add(histToAdd)

# write histogram to output file
if args.force:
  if os.path.isfile(args.output):
    os.remove(args.output)
outFile = TFile(args.output,"new")
histTotal.Write()

# draw (if set)
if (args.draw):
  histTotal.Draw()
