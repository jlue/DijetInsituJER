################################################################################
# Project: Utils
# HEP data analysis utilities
################################################################################

find_package( ROOT COMPONENTS Core RIO Hist Tree TreePlayer MathCore Physics Gpad Graf )

atlas_subdir( Utils )

atlas_depends_on_subdirs( PUBLIC
                          BootstrapGenerator )

atlas_add_root_dictionary( UtilsLib UtilsDictSource
                           ROOT_HEADERS Utils/*.h Root/LinkDef.h
                           EXTERNAL_PACKAGES ROOT )

atlas_add_library( UtilsLib Utils/*.h Root/*.cxx ${UtilsDictSource}
                   PUBLIC_HEADERS Utils
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES} BootstrapGeneratorLib
                   )

atlas_install_python_modules( python/*.py )
atlas_install_scripts( scripts/* )
atlas_install_data( data/* )
