// package include(s)
#include "JetResponseFitter/JetResponseFitter.h"

ClassImp(JetResponseFitter)

//-----------------------------------------------------------------------//
// Constructor 
//-----------------------------------------------------------------------//
JetResponseFitter::JetResponseFitter(Config settings) :
  m_fineHist(0), m_fitHist(0),
  m_fitOpt("QNBRSL"), m_fitCol(kRed),
  m_lNlines(0), m_rNlines(0),
  m_min0(0.0), m_max0(2.0), 
  m_xLeft(0.15), m_xRight(0.65),
  m_yTop(0.85), m_dy(0.05)
{
  m_fitName = settings.getStr("ResponseFitter.Fit");
  m_fitOpt = settings.getStr("ResponseFitter.Fit.Opt");
  m_Nsigma = settings.getNum("ResponseFitter.Fit.Nsigma");
  m_fitParam = settings.getStr("ResponseFitter."+m_fitName+".Function");
  m_parNames = settings.getStrV("ResponseFitter."+m_fitName+".Parameters");
  m_norm = settings.getInt("ResponseFitter."+m_fitName+".Norm",0);
  m_mus = settings.getIntV("ResponseFitter."+m_fitName+".Mus",{1});
  m_sigmas = settings.getIntV("ResponseFitter."+m_fitName+".Sigmas",{2});
  for (int ipar=0 ; ipar<m_parNames.size() ; ++ipar) {
    std::pair<int,double> p0{ipar,settings.getNum(Form("%s.Init.%i",m_fitName.c_str(),ipar),1.0)};
    m_p0s.push_back(p0);
  }
}

//-----------------------------------------------------------------------//
// Various fitting functions
//-----------------------------------------------------------------------//
TF1 *JetResponseFitter::fitResponse(TH1 *Hist) {
  // import data
  m_fineHist = Hist; 
  m_fitHist = (TH1*)Hist->Clone();
  int rebin = optimalRebin(Hist);
  m_fitHist->Rebin(rebin);
  m_fitHist->Scale(1.0/m_fitHist->Integral("width"));

  // perform fit(s)
  TF1 *f = performFit(m_fitHist);

  // output best fit
  return f;
}
//-----------------------------------------------------------------------//

TF1 *JetResponseFitter::performFit(TH1 *fitHist, double fitMin) {
  // inital range
  double minx = m_min0;
  double maxx = m_max0;
  static int fiti=0;
  m_fit = new TF1(Form("fit%d",++fiti), m_fitParam.c_str(), minx, maxx); 
  m_fit->SetLineWidth(3); m_fit->SetLineColor(m_fitCol);

  m_fit->SetParameter(m_norm,fitHist->GetMaximum());
  for (const auto mu : m_mus) {
    m_fit->SetParameter(mu,fitHist->GetMean());
  }
  for (const auto sig : m_sigmas) {
    m_fit->SetParameter(sig,fitHist->GetRMS());
  }
  for (const auto p0 : m_p0s) {
    m_fit->SetParameter(p0.first,p0.second);
  }

  m_fit->SetParLimits(2,0,0.5);
  // Invoke 3 times TH1::Fit below :

  // 1st) . "Hack" to make the fits converge
  m_fitRes = fitHist->Fit(m_fit,"Q0 BR LS");
  if ( m_stopWhenFitFails) {
    if (int(m_fitRes)!=0)  return m_fit;
    if ( ( minx>getMean() ) || ( maxx<getMean() ) ) return m_fit;
  }
  
  // 2nd) 
  setFitRange(fitMin);
  m_fit->GetRange(minx, maxx);
  // temporary stores the fit result in case of failure  
  TFitResultPtr tmp=fitHist->Fit(m_fit,"Q0 BR LS");
  if ( m_stopWhenFitFails) {
    bool success = (int(tmp) ==0);
    if (success) success=  ( minx<getMean() ) && ( maxx>getMean() )  ;
    if ( !success )  {
      // fit failed
      // Thus restore previous parameters and exit
      m_fit->SetParameters( m_fitRes->GetParams() );
      return m_fit;
    }
  }
  // acquire the TFitResultPtr
  m_fitRes = tmp;
  
  // 3rd)
  setFitRange(fitMin); tmp = fitHist->Fit(m_fit,m_fitOpt.c_str());
  m_fit->GetRange(minx, maxx);
  if ( m_stopWhenFitFails) {
    bool success = (int(tmp) ==0);
    if (success) success= ( minx<getMean() ) && ( maxx>getMean() )   ;
    if ( !success )  {
      // restore previous parameters
      if ( int(m_fitRes)==0) m_fit->SetParameters( m_fitRes->GetParams() );
    }
  } else m_fitRes = tmp;
  return m_fit;
}

//-----------------------------------------------------------------------//
// Functions to control the fit
//-----------------------------------------------------------------------//
void JetResponseFitter::setFitRange(double fitMin) {
  double mean = getPeak(), sigma = getSigma();
  double minx = mean - m_Nsigma*sigma, maxx = mean + m_Nsigma*sigma;
  if (minx<fitMin) minx=fitMin;
  m_fit->SetRange(minx,maxx);
}

//-----------------------------------------------------------------------//
// Functions to retrieve fit variables
//-----------------------------------------------------------------------//
TF1* JetResponseFitter::getFit() {
  if (m_fit==NULL) error("Somethign went wrong. Can't access fit function!");
  return m_fit;
}
TF1* JetResponseFitter::getExtendedFit() {
  if (m_fit==NULL) error("Somethign went wrong. Can't access fit function!");
  TF1 *extfit = (TF1*)getFit()->Clone();
  extfit->SetRange(0.0,3.0);
  extfit->SetLineStyle(2);
  return extfit;
}
//-----------------------------------------------------------------------//
TH1* JetResponseFitter::getHist() {
  if (!m_fitHist) error("Somethign went wrong. Can't access fitted Histgram!");
  return m_fitHist;
}
//-----------------------------------------------------------------------//
TH1* JetResponseFitter::getFineHist() {
  if (!m_fineHist) error("Somethign went wrong. Can't access data Histgram!");
  return m_fineHist;
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getMu() {
  return getFit()->GetParameter(1);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getMuError() {
  return getFit()->GetParError(1);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getSigma() {
  return fabs(getFit()->GetParameter(2));
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getSigmaError() {
  return getFit()->GetParError(2);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getMean() {
  return getFit()->Mean(0.0,3.0);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getMeanError() {
  return getFit()->GetParError(1);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getWidth() {
  // return TMath::Sqrt(getFit()->Variance(0.01,2.99));
  return fabs(getFit()->GetParameter(2));
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getWidthError() {
  return getFit()->GetParError(2);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getRelWidth() {
  return getWidth()/getPeak();
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getRelWidthError() {
  return getSigmaError();
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getPeak() {
  return getFit()->GetMaximumX(0,2);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getMedian() {
  return getFitQuantile(0.5);
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getFitQuantile(double frac) {
  if (frac<=0||frac>=1) error(Form("Can't access quantile for fraction %.1f",frac));
  double min=getMean()-5.0*getWidth(), max=getMean()+5.0*getWidth();
  int Nsteps=10000;
  TF1 *fit = getFit(); double Atot=fit->Integral(min,max), A=0, dx=(max-min)/Nsteps;
  for (int i=0;i<Nsteps;++i) {
    double xi=min+i*dx;
    A+=fit->Eval(xi)*dx; // f(x)*dx
    if (A>Atot*frac) return xi-0.5*dx;
  }
  error("getFitQuantile failed!");
  return 0;
}
//-----------------------------------------------------------------------//
// Bogdan's implementation based on Root functions, with log(#bins) complexity

double JetResponseFitter::getHistQuantile(double frac, Int_t verbose) {
   if (frac<=0||frac>=1) error(Form("Can't access Hist quantile for fraction %.1f",frac));
   TH1D *h = (TH1D*) getFineHist();
   TH1D *h2 = new TH1D(*h); // copy where changes in the bin contents will be implemented
   Double_t entries = h2->GetEntries();

   // include underflow and overflow bin contents in the computation of the quantiles ("getFitQuantiles" function in root ignores underflow and overflow bin contents)
   Int_t Nbins = h2->GetNbinsX();
   h2->SetBinContent( 1, h2->GetBinContent(0)+h2->GetBinContent(1) );
   h2->SetBinContent( Nbins, h2->GetBinContent(Nbins)+h2->GetBinContent(Nbins+1) );
   h2->ComputeIntegral();

   Double_t integral = h2->GetSumOfWeights();
   if ( verbose ) { std::cout << std::endl << "Computing quantiles..." << std::endl << "integral = " << integral << "  entries = " << entries << "  underflow = " << h2->GetBinContent(0) << "  overflow =" << h2->GetBinContent(Nbins+1) << std::endl; }

   Int_t nq = 1;
   Double_t xq[nq];  // position where to compute the quantiles in [0,1]
   Double_t yq[nq];  // array to contain the quantiles
   xq[0] = frac;

   if ( h2->GetBinContent(0)/integral > xq[0] ) { error("quantile will be biassed (underflow). Consider extending the range of the Histgram towards lower values."); }
   if ( h2->GetBinContent(Nbins+1)/integral > (1. - xq[0]) ) { error("quantile will be biassed (overflow). Consider extending the range of the Histgram towards larger values."); }

   h2->GetQuantiles(nq,yq,xq);

   if ( verbose ) { std::cout << "Requested quantile: " << xq[0] << ";  Result: " << yq[0]  << std::endl; }
   delete h2;

   return yq[0];
}

/*
double JetResponseFitter::getHistQuantile(double frac) {
  if (frac<=0||frac>=1) error(Form("Can't access Hist quantile for fraction %.1f",frac));
  TH1 *h = getFineHist();
  int N=h->GetNbinsX();
  double sum=0, tot=h->Integral(0,N+1);
  for (int bin=0;bin<=N+1;++bin) {
    sum+=h->GetBinContent(bin);
    if (sum/tot>frac) return h->GetBinLowEdge(bin+1)-h->GetBinWidth(bin+1)/2;
  }
  error("getHistQuantile failed!");
  return 0;
}
*/
//-----------------------------------------------------------------------//
double JetResponseFitter::getChi2() {
  return getFit()->GetChisquare();
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getNdof() {
  return getFit()->GetNDF();
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getChi2Ndof() {
  return getChi2()/getNdof();
}
//-----------------------------------------------------------------------//
double JetResponseFitter::getChi2Prob() {
  return TMath::Prob(getChi2(),getNdof());
}

//-----------------------------------------------------------------------//
// Functions to draw and print fit info
//-----------------------------------------------------------------------//
void JetResponseFitter::drawFitAndHist(double minx, double maxx) {
  if (minx<maxx) getHist()->SetAxisRange(minx, maxx, "X");
  getHist()->Draw("same"); 
  drawExtendedFit();
  resetTextCounters();
}
//-----------------------------------------------------------------------//
void JetResponseFitter::printFitInfo() {
  drawTextRight(m_fitName.c_str(),m_fitCol);
  drawTextRight(Form("#it{#chi}^{2}/#it{n}_{dof}: %.2f / %i",
		     getChi2(),(int)getNdof()),m_fitCol);
  drawTextRight(Form("mean: (%.2f#pm%.2f)%%",
		     (getMean()-1.0)*100,
		     getMeanError()*100),m_fitCol);
  drawTextRight(Form("width: (%.2f#pm%.2f)%%",
		     getWidth()*100,
		     getWidthError()*100),m_fitCol);
}
//-----------------------------------------------------------------------//
void JetResponseFitter::drawExtendedFit(double minx, double maxx) {
  getFit()->Draw("same");
  TF1 *extfit = (TF1*)getFit()->Clone();
  extfit->SetRange(0.01,2.0);
  extfit->SetLineStyle(2); extfit->Draw("same");
  if (m_fitName=="ExpGausExp") {
    float a = extfit->GetParameter(3);
    float b = extfit->GetParameter(4);
    float ax = extfit->GetParameter(1) - extfit->GetParameter(2)*a;
    float bx = extfit->GetParameter(1) + extfit->GetParameter(2)*b;
    TGraph* aline = new TGraph(2);
    aline->SetPoint(0,ax,1e-7);
    aline->SetPoint(1,ax,extfit->Eval(ax)*1.5);
    TGraph* bline = new TGraph(2);
    bline->SetPoint(0,bx,1e-7);
    bline->SetPoint(1,bx,extfit->Eval(bx)*1.5);
    aline->SetLineColor(m_fitCol);
    bline->SetLineColor(m_fitCol);
    aline->SetLineWidth(2);
    bline->SetLineWidth(2);
    aline->SetLineStyle(kDotted);
    bline->SetLineStyle(kDotted);
    aline->Draw("same");
    bline->Draw("same");
  }
  if (m_fitName=="ExpGaus") {
    float a = extfit->GetParameter(3);
    float ax = extfit->GetParameter(1) - extfit->GetParameter(2)*a;
    TGraph* aline = new TGraph(2);
    aline->SetPoint(0,ax,1e-7);
    aline->SetPoint(1,ax,extfit->Eval(ax)*1.5);
    aline->SetLineColor(m_fitCol);
    aline->SetLineWidth(2);
    aline->SetLineStyle(kDotted);
    aline->Draw("same");
  }
}
//-----------------------------------------------------------------------//
void JetResponseFitter::drawTextLeft(std::string txt, int col) {
  drawText(m_xLeft,m_yTop-m_dy*(m_lNlines++),txt.c_str(),col);
}
//-----------------------------------------------------------------------//
void JetResponseFitter::drawTextRight(std::string txt, int col) {
  drawText(m_xRight,m_yTop-m_dy*(m_rNlines++),txt.c_str(),col);
}
//-----------------------------------------------------------------------//
void JetResponseFitter::drawText(double x, double y, std::string txt, int col) {
  static TLatex *tex = new TLatex(); tex->SetNDC(); tex->SetTextFont(42);
  tex->SetTextSize(0.04);
  tex->SetTextColor(col); tex->DrawLatex(x,y,txt.c_str());
}

//-----------------------------------------------------------------------//
// Functions to rebin
//-----------------------------------------------------------------------//
int JetResponseFitter::optimalRebin(TH1 *h)
{
  int method=1;

  // Get optimal bin withs using Scott's Choise
  double N=h->GetEffectiveEntries();
  double optWidth = 3.5*h->GetRMS()/TMath::Power(N,1.0/3);
  int Nbins=h->GetNbinsX();
  double range=h->GetBinLowEdge(Nbins+1)-h->GetBinLowEdge(1);
  int rebin=1;
  double prevWidth=range/Nbins;
  for (int i=1;i<Nbins;++i) {
    if (Nbins%i!=0) continue;
    double binWidth=range/Nbins*i;
    //if (binWidth>maxWidth) continue;
  
    if (method==1) {
      // optimistic
      if (binWidth<optWidth) rebin=i;
    } else if (method==2) {
      if (TMath::Abs(binWidth-optWidth) < 
          TMath::Abs(prevWidth-optWidth)) rebin=i;
    }
    else rebin=i; // method 3

    if (binWidth>optWidth) break;
    prevWidth=binWidth;
  }
  bool _vebose=false;
  if (_vebose) {
    printf("\n%s\n  RMS: %.3f, Neff: %.3f\n",h->GetName(),h->GetRMS(),N);
    printf("  Opt width: %6.3f, Hist binwidth: %6.3f => Rebin: %d\n",optWidth,range/Nbins,rebin);
  }
  return rebin;
}
