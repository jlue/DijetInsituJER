#include "JetResponseFitter/JetResolutionFitter.h"
 
ClassImp(JetResolutionFitter)

JetResolutionFitter::JetResolutionFitter(const std::string& name) :
  m_name(name),
  m_pTmin(20.0), m_pTmax(3000.0),
  m_fitOpt("QN BRS EX0"),
  m_fitColor(kBlue), m_lineStyle(kSolid), m_fillAlpha(0.10),
  m_fit(new TF1("JER_fit","TMath::Sqrt(TMath::Sq([0]/x)+TMath::Sq([1]/TMath::Sqrt(x))+TMath::Sq([2]))",20.0,3000.0)),
  m_fitRes(nullptr),m_fitErr(nullptr),
  m_data(nullptr)
{
  initTerms(2.0,1.0,0.1);
  limitTerm("N",0.0,10.0);
  limitTerm("S",0.0,10.0);
  limitTerm("C",0.01,0.10);
}
JetResolutionFitter::~JetResolutionFitter()
{
  if (m_data)   delete m_data;
  if (m_fit)    delete m_fit;
  if (m_fitErr) delete m_fitErr;
}
TF1* JetResolutionFitter::fitResolution(TGraphAsymmErrors* data, float pTmin, float pTmax)
{
  // get data
  m_data = (TGraphAsymmErrors*)data->Clone();

  // initialize fit parameters
  m_fit->SetParameters(m_Ni,m_Si,m_Ci);

  // check if specified fit range makes sense, apply if so
  setFitRange(pTmin,pTmax);
  m_fit->SetRange(m_pTmin,3000.0);

  // fit
  m_fitRes = m_data->Fit(m_fit,m_fitOpt.c_str());
  return m_fit;
}
TF1* JetResolutionFitter::fitResolution(TGraphErrors* data, float pTmin, float pTmax)
{
  // get data
  m_data = (TGraphAsymmErrors*)data->Clone();

  // initialize fit parameters
  m_fit->SetParameters(m_Ni,m_Si,m_Ci);

  // check if specified fit range makes sense, apply if so
  setFitRange(pTmin,pTmax);
  m_fit->SetRange(m_pTmin,3000.0);

  // fit
  m_fitRes = m_data->Fit(m_fit,m_fitOpt.c_str());
  return m_fit;
}

void JetResolutionFitter::drawFit()
{
  m_data->SetMarkerStyle(kFullCircle);
  m_data->SetMarkerColor(kBlack);
  m_data->Draw("P same");

  getFit()->SetLineColor(m_fitColor);
  getFit()->SetFillColorAlpha(m_fitColor,m_fillAlpha);
  getFit()->SetLineStyle(m_lineStyle);
  getFit()->Draw("L same");

  m_fitExt = (TF1*)getFit()->Clone();
  m_fitExt->SetRange(0.5*m_pTmin,2*m_pTmax);
  m_fitExt->SetLineColor(m_fitColor);
  m_fitExt->SetFillColorAlpha(m_fitColor,0.0);
  m_fitExt->SetLineStyle(kDashed);
  m_fitExt->Draw("L same");
}
void JetResolutionFitter::printFit(float x, float y)
{
  // drawCaption(x,y,{
  //   Form("#it{N} = %.2f#pm%.2f",getN(),getNError()),
  //   Form("#it{S} = %.2f#pm%.2f",getS(),getSError()),
  //   Form("#it{C} = %.1f#pm%.1f",getC()*100,getCError()*100),
  // });
}