#include "JetResponseFitter/ConvolutionFitter.h"

ConvolutionFitter::ConvolutionFitter(Config settings) :
  m_inputFit(nullptr), m_responseFit(nullptr),
  m_inputHist(nullptr), m_outputHist(nullptr),
  m_xmin(-2.0), m_xmax(2.0)
{
  // fit options
  m_inputFitOpt = settings.getStr("ConvolutionFitter.Input.Opt","Q0 BR L");
  m_outputFitOpt = settings.getStr("ConvolutionFitter.Output.Opt","Q0 BR");

  // set Nsigma parameters
  m_inputNsigma = settings.getNum("ConvolutionFitter.Input.Nsigma",2.50);
  m_outputNsigma = settings.getNum("ConvolutionFitter.Output.Nsigma",1.25);

  // construct input fit
  m_inputFitName = settings.getStr("ConvolutionFitter.Input","Gaus");
  m_inputFit = new TF1(m_inputFitName.c_str(),settings.getStr("ConvolutionFitter.Input."+m_inputFitName+".Function").c_str(),-2.0,2.0);
  // set parameter names
  std::vector<std::string> inputParNames = settings.getStrV("ConvolutionFitter.Input."+m_inputFitName+".Parameters");
  for (int ipar=0 ; ipar<inputParNames.size() ; ++ipar) {
    m_inputFit->SetParName(ipar,inputParNames.at(ipar).c_str());
  }
  // set parameter initial values & limits
  for (int ipar=0 ; ipar<m_inputFit->GetNpar() ; ++ipar) {
    m_inputFit->SetParameter(ipar,settings.getNum(Form("ConvolutionFitter.Input.%s.Init.%i",m_inputFitName.c_str(),ipar),1.0));
  }
  for (int ipar=0 ; ipar<m_inputFit->GetNpar() ; ++ipar) {
    std::vector<float> parLims = settings.getNumV(Form("ConvolutionFitter.Input.%s.Limits.%i",m_inputFitName.c_str(),ipar),{1.0,1.0});
    float parLimLow = parLims.at(0);
    float parLimUp = parLims.at(1);
    if (parLimLow==parLimUp) continue;
    m_inputFit->SetParLimits(ipar,parLimLow,parLimUp);
  }
  // save histogram-based parameter indices
  m_inputMus = settings.getIntV(Form("ConvolutionFitter.Input.%s.Mus",m_inputFitName.c_str()),{});
  m_inputSigmas = settings.getIntV(Form("ConvolutionFitter.Input.%s.Sigmas",m_inputFitName.c_str()),{});
  m_inputNorm = settings.getInt(Form("ConvolutionFitter.Input.%s.Norm",m_inputFitName.c_str()),0);

  // construct response fit
  // gaussian PDF
  m_responseFitName = settings.getStr("ConvolutionFitter.Response","Gaus");
  m_responseFit = new TF1(m_responseFitName.c_str(),settings.getStr("ConvolutionFitter.Response."+m_responseFitName+".Function").c_str(),-2.0,2.0);
  // set parameter names
  std::vector<std::string> respParNames = settings.getStrV("ConvolutionFitter.Response."+m_responseFitName+".Parameters");
  for (int ipar=0 ; ipar<respParNames.size() ; ++ipar) {
    m_responseFit->SetParName(ipar,respParNames.at(ipar).c_str());
  }
  // set parameter initial values & limits
  for (int ipar=0 ; ipar<m_responseFit->GetNpar() ; ++ipar) {
    m_responseFit->SetParameter(ipar,settings.getNum(Form("ConvolutionFitter.Response.%s.Init.%i",m_responseFitName.c_str(),ipar),1.0));
  }
  for (int ipar=0 ; ipar<m_responseFit->GetNpar() ; ++ipar) {
    std::vector<float> parLims = settings.getNumV(Form("ConvolutionFitter.Response.%s.Limits.%i",m_responseFitName.c_str(),ipar),{1.0,1.0});
    float parLimLow = parLims.at(0);
    float parLimUp = parLims.at(1);
    if (parLimLow==parLimUp) continue;
    m_responseFit->SetParLimits(ipar,parLimLow,parLimUp);
  }

  // construct convolution fit
  m_convolution = new TF1Convolution(m_inputFit,m_responseFit,-3.0,3.0);
  m_convolution->SetNofPointsFFT(6000);
  m_convolutionFit = new TF1(TString(m_inputFit->GetName())+"(x)"+TString(m_responseFit->GetName()),*m_convolution,-3.0,3.0,m_convolution->GetNpar());
  // restore parameter names from input & response functions
  for (int ipar_m_inputFit=0; ipar_m_inputFit<m_inputFit->GetNpar(); ipar_m_inputFit++) {
    m_convolutionFit->SetParName(ipar_m_inputFit,m_inputFit->GetParName(ipar_m_inputFit));
  }
  for (int ipar_resp=0 ; ipar_resp<m_responseFit->GetNpar() ; ipar_resp++) {
    m_convolutionFit->SetParName(ipar_resp+m_inputFit->GetNpar(),m_responseFit->GetParName(ipar_resp));
  }
}

void ConvolutionFitter::fitConvolution(TH1* inputHist, TH1* outputHist)
{
  // free memory
  if (m_inputHist) delete m_inputHist;
  if (m_outputHist) delete m_outputHist;

  // import data
  // normalize & rebin
  m_inputHist = (TH1F*)inputHist->Clone(); HistUtils::rebin(m_inputHist); HistUtils::normalize(m_inputHist);
  m_outputHist = (TH1F*)outputHist->Clone(); HistUtils::rebin(m_outputHist); HistUtils::normalize(m_outputHist);

  // initialize fit parameters based on settings & histogram
  for (int imu=0 ; imu<m_inputMus.size() ; ++imu) {
    m_inputFit->SetParameter(m_inputMus.at(imu),m_inputHist->GetMean());
  }
  for (int isig=0 ; isig<m_inputSigmas.size() ; ++isig) {
    m_inputFit->SetParameter(m_inputSigmas.at(isig),m_inputHist->GetStdDev());
  }
  m_inputFit->SetParameter(m_inputNorm,m_inputHist->GetMaximum());
  m_inputFit->SetParLimits(m_inputNorm,0.75*m_inputHist->GetMaximum(),1.25*m_inputHist->GetMaximum());

  // perform input signal fit
  adjustFitRange(m_inputFit,m_inputHist,m_inputNsigma);
  m_inputHist->Fit(m_inputFit,m_inputFitOpt.c_str());
  adjustFitRange(m_inputFit,m_inputHist,m_inputNsigma);
  m_inputHist->Fit(m_inputFit,m_inputFitOpt.c_str());

  // fix input fit parameters
  for (int ipar_input=0 ; ipar_input<m_inputFit->GetNpar() ; ++ipar_input) {
    m_convolutionFit->FixParameter(ipar_input,m_inputFit->GetParameter(ipar_input));
  }
  // free-floating normalization
  m_convolutionFit->ReleaseParameter(m_inputNorm);

  // set existing response fit parameters & limits
  for (int ipar_resp=0 ; ipar_resp<m_responseFit->GetNpar() ; ++ipar_resp) {
    m_convolutionFit->SetParameter(ipar_resp+m_inputFit->GetNpar(),
                                   m_responseFit->GetParameter(ipar_resp));
    double parLimLow, parLimUp;
    m_responseFit->GetParLimits(ipar_resp,parLimLow,parLimUp);
    if (parLimLow != parLimUp)
      m_convolutionFit->SetParLimits(ipar_resp+m_inputFit->GetNpar(),
                                     parLimLow,parLimUp);
  }

  // perform convolution fit
  adjustFitRange(m_convolutionFit,m_outputHist,m_outputNsigma);
  m_outputHist->Fit(m_convolutionFit,m_outputFitOpt.c_str());
  
  // // set detector response parameters
  // for (int ipar_reco=0 ; ipar_reco<m_convolutionFit->GetNpar() ; ++ipar_reco) {
  //   int ipar_det = m_responseFit->GetParNumber(m_convolutionFit->GetParName(ipar_reco));
  //   m_responseFit->SetParameter(ipar_det,m_convolutionFit->GetParameter(ipar_reco));
  //   m_responseFit->SetParError(ipar_det,m_convolutionFit->GetParError(ipar_reco));
  // }
}
void ConvolutionFitter::fitResponse(TH1* outputHist)
{
  // free memory
  if (m_outputHist) delete m_outputHist;

  // import data
  // normalize & rebin
  m_outputHist = (TH1F*)outputHist->Clone(); HistUtils::rebin(m_outputHist); HistUtils::normalize(m_outputHist);

  // fix input fit parameters
  for (int ipar_input=0 ; ipar_input<m_inputFit->GetNpar() ; ++ipar_input) {
    m_convolutionFit->FixParameter(ipar_input,m_inputFit->GetParameter(ipar_input));
  }
  // free-floating normalization
  m_convolutionFit->ReleaseParameter(m_inputNorm);

  // set existing response fit parameters & limits
  for (int ipar_resp=0 ; ipar_resp<m_responseFit->GetNpar() ; ++ipar_resp) {
    m_convolutionFit->SetParameter(ipar_resp+m_inputFit->GetNpar(),
                                   m_responseFit->GetParameter(ipar_resp));
    double parLimLow, parLimUp;
    m_responseFit->GetParLimits(ipar_resp,parLimLow,parLimUp);
    if (parLimLow != parLimUp)
      m_convolutionFit->SetParLimits(ipar_resp+m_inputFit->GetNpar(),
                                     parLimLow,parLimUp);
  }
  
  // m_convolutionFit_out = m_convolutionFit;
  
  // if(m_firstbins){
  //   // m_convolutionFit_out->FixParameter(0,0);
  //   m_convolutionFit_out->FixParameter(1,0);
  //   m_convolutionFit_out->FixParameter(2,0);
  //   m_convolutionFit_out->FixParameter(3,0);
  // }

  // perform convolution fit
  adjustFitRange(m_convolutionFit,m_outputHist,m_outputNsigma);
  m_outputHist->Fit(m_convolutionFit,m_outputFitOpt.c_str());
  
  // set detector response parameters
  for (int ipar_reco=0 ; ipar_reco<m_convolutionFit->GetNpar() ; ++ipar_reco) {
    int ipar_det = m_responseFit->GetParNumber(m_convolutionFit->GetParName(ipar_reco));
    m_responseFit->SetParameter(ipar_det,m_convolutionFit->GetParameter(ipar_reco));
    m_responseFit->SetParError(ipar_det,m_convolutionFit->GetParError(ipar_reco));
  }
}


void ConvolutionFitter::drawFit() {
  
  // format fit functions
  getInputFit()->SetNpx(200);
  getConvolutionFit()->SetNpx(200);
  
  // data & fit
  getInputHist()->SetMarkerSize(0.0);
  getInputHist()->SetMarkerColor(kBlue);
  getInputHist()->SetLineColor(kBlue);
  getInputFit()->SetLineColor(kBlue);

  getOutputHist()->SetMarkerStyle(kFullCircle);
  getOutputHist()->SetMarkerColor(kBlack);
  getOutputHist()->SetLineColor(kBlack);
  getConvolutionFit()->SetLineColor(kRed);
  
  getInputHist()->Draw("E same");
  getInputFit()->Draw("L same");
  getOutputHist()->Draw("E same");
  getConvolutionFit()->Draw("L same");
}
