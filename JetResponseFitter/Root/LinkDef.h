#include "JetResponseFitter/JetResponseFitter.h"
#include "JetResponseFitter/JetResolutionFitter.h"
#include "JetResponseFitter/ConvolutionFitter.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ class JetResponseFitter+;
#pragma link C++ class JetResolutionFitter+;
#pragma link C++ class ConvolutionFitter+;
#pragma link C++ class TemplateFitter+;

#endif



