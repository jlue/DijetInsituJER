#pragma once

/*
 *  Common code to fit jet response distributions
 *  for example direct balance or asymmetry distributions
 *    pTreco/pTtruth, pTreco/pTref, pTref = photon or Z pT
 *    Asym = (pT1-pT2)/(pT1+pT2)*2
 *
 *  Dag Gillberg, Oct 21, 2012 dag.gillberg AT cern.ch
 */

// C++ includes
#include <stdio.h>
#include <iostream>

// ROOT include(s)
#include "Riostream.h"
#include "TString.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TLatex.h"
#include "TMath.h"
#include "TFitResult.h"

// package include(s)
#include "Utils/Utils.h"
#include "Utils/Config.h"

class JetResponseFitter : public TObject 
{

public:
  
  //  Constructor. 
  //  Parameters:
  //  - Nsigma: how many sigmas away from mean to fit (default=1.5)
  JetResponseFitter(Config settings); 
  ~JetResponseFitter() {};

  void setNsigma(double ns){m_Nsigma=ns;}
  void setFitFunction(std::string fitName, std::string fitParam){ m_fitName=fitName; m_fitParam=fitParam; }
  
  TF1* fitResponse(TH1 *h);
  int  optimalRebin(TH1 *h);
  void drawFitAndHist(double minx=-999, double maxx=-1000);
  void drawExtendedFit(double minx=0, double maxx=3);

  void setGaus() { setFitFunction("Gaussian","gaus"); };
  void setExpGausExp() {
    setFitFunction("ExpGausExp",
      "(x-[1])/[2] < -1.0*abs([3]) ? [0]*TMath::Exp([3]^2/2+abs([3])*((x-[1])/abs([2]))) : \
       ( (x-[1])/[2] > abs([4]) ? [0]*TMath::Exp([4]^2/2-abs([4])*((x-[1])/abs([2]))) : \
       [0]*TMath::Gaus(x,[1],[2],0) )");
  };
  void setExpGaus() {
    setFitFunction("ExpGaus",
      "(x-[1])/[2] < -1.0*abs([3]) ? [0]*TMath::Exp([3]^2/2+abs([3])*((x-[1])/abs([2]))) : \
      [0]*TMath::Gaus(x,[1],[2],0)");
  };
  void setGausExp() {
    setFitFunction("ExpGaus",
       "( (x-[1])/[2] > abs([3]) ? [0]*TMath::Exp([3]^2/2-abs([4])*((x-[1])/abs([2]))) : \
       [0]*TMath::Gaus(x,[1],[2],0)");
  };

  void setFitOpt(std::string opt) { m_fitOpt=opt; };
  
  // The x-position of the peak
  double getPeak();
  double getMedian();
  double getFitQuantile(double frac);
  double getNeg2SigFitQuantile() { return getFitQuantile(2.27501319480000186e-02); };
  double getNeg1SigFitQuantile() { return getFitQuantile(1.58655253931499984e-01); };
  double getPos1SigFitQuantile() { return getFitQuantile(8.41344746068499960e-01); };
  double getPos2SigFitQuantile() { return getFitQuantile(9.77249868051999981e-01); };
  double getHistQuantile(double frac, Int_t verbose=0);
  double getHistQuantileError() { return getFineHist()->GetBinWidth(1)/2; };
  double getHistMedian() { return getHistQuantile(0.5); };
  double getNeg2SigHistQuantile() { return getHistQuantile(2.27501319480000186e-02); };
  double getNeg1SigHistQuantile() { return getHistQuantile(1.58655253931499984e-01); };
  double getPos1SigHistQuantile() { return getHistQuantile(8.41344746068499960e-01); };
  double getPos2SigHistQuantile() { return getHistQuantile(9.77249868051999981e-01); };
  
  // fit parameters
  // mu parameter
  double getMu();
  double getMuError();
  // sigma parameter
  double getSigma();
  double getSigmaError();
  // mean
  double getMean();
  double getMeanError();
  // width
  double getWidth();
  double getWidthError();
  // jet energy resolution
  double getRelWidth();
  double getRelWidthError();

  // fit quality
  double getChi2();
  double getNdof();
  double getChi2Ndof();
  double getChi2Prob();

  TF1* getFit();
  TF1* getExtendedFit();
  TH1* getHist();
  TH1* getFineHist();

  double getHistMean() { return getFineHist()->GetMean(); };
  double getHistMeanError() { return getFineHist()->GetMeanError(); };

  void setFitColor(int col) { m_fitCol=col; };
  void drawText(double x, double y, std::string txt, int col=kBlack);
  void drawTextLeft(std::string txt, int col=kBlack);
  void drawTextRight(std::string txt, int col=kBlack);
  void resetTextCounters() { m_lNlines=0; m_rNlines=0; };
  void printFitInfo();
  void setDefaultMinMax(double min0, double max0) {m_min0=min0; m_max0=max0;}
  
  TFitResultPtr getFitResult() {return m_fitRes;}
  void stopWhenFitFails(bool s){m_stopWhenFitFails=s;}

private:

  void setFitRange(double fitMin);

  TF1 *performFit(TH1 *Hist, double fitMin=-1);

  std::string m_fitName;
  double m_Nsigma;
  std::string m_fitParam;

  std::vector<std::string> m_parNames;
  int m_norm;
  std::vector<int> m_mus;
  std::vector<int> m_sigmas;
  std::vector<std::pair<int,double>> m_p0s;

  TF1* m_fit;

  TH1 *m_fineHist, *m_fitHist;
  int m_fitCol;
  
  std::string m_fitOpt;
  double m_min0, m_max0, m_xLeft, m_xRight, m_yTop, m_dy;
  int m_lNlines, m_rNlines;

  TFitResultPtr m_fitRes;
  bool m_stopWhenFitFails = false;

  ClassDef(JetResponseFitter, 1)
};
