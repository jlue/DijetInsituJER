#pragma once

#include "Utils/Utils.h"
#include "Utils/HistUtils.h"

#include "TF1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TGraphAsymmErrors.h"
#include "TFitResult.h"
#include "TVirtualFitter.h"

class JetResolutionFitter : public TObject
{

public:

  JetResolutionFitter(const std::string& name="");
  ~JetResolutionFitter();

  void initTerms(float N, float S, float C) {
    m_Ni = N;
    m_Si = S;
    m_Ci = C;
  }
  void limitTerm(const std::string& termName, float low, float up) {
    int termNumber = m_fit->GetParNumber(termName.c_str());
    m_fit->SetParLimits(termNumber,low,up);
  }

  void setFitOpt(const std::string& opt) { 
    m_fitOpt = opt; 
  };
  void setFitRange(float pTmin, float pTmax) { 
    if (pTmax>pTmin) {
      m_pTmin = pTmin;
      m_pTmax = pTmax;
    }
  }

  TF1* fitResolution(TGraphAsymmErrors* data, float pTmin=0, float pTmax=0);
  TF1* fitResolution(TGraphErrors* data, float pTmin=0, float pTmax=0);

  void printFit(float x, float y);
  void drawFit();

  // fit results & parameters
  TGraphAsymmErrors* getData() { return m_data; };
  TF1* getFit() { return m_fit; };
  // fit parameters
  float getN() { return fabs(getFit()->GetParameter(0)); };
  float getS() { return fabs(getFit()->GetParameter(1)); };
  float getC() { return fabs(getFit()->GetParameter(2)); };
  float getNErr() { return getFit()->GetParError(0); };
  float getSErr() { return getFit()->GetParError(1); };
  float getCErr() { return getFit()->GetParError(2); };

  void setFitLine(int fitColor=kBlue, int lineStyle=kSolid, float fillAlpha=0.10) { m_fitColor=fitColor; m_lineStyle=lineStyle; m_fillAlpha=fillAlpha; };

private:

  // name
  std::string m_name;

  // data
  TGraphAsymmErrors* m_data;

  // fit
  TF1* m_fit;
  TFitResultPtr m_fitRes;

  // initial parameter values
  float m_Ni, m_Si, m_Ci;

  // option
  std::string m_fitOpt;
  // range
  float m_pTmin, m_pTmax;

  // fit drawing
  TF1* m_fitExt;
  TH1F* m_fitErr;
  int m_fitColor;
  int m_lineStyle;
  float m_fillAlpha;

  ClassDef(JetResolutionFitter, 1)
};
