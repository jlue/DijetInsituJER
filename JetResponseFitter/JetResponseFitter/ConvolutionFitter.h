#pragma once

#include "TH1F.h"
#include "TF1.h"
#include "TF1Convolution.h"

#include "Utils/Utils.h"
#include "Utils/Config.h"
#include "Utils/HistUtils.h"

class ConvolutionFitter
{
  
public:
  
  ConvolutionFitter(Config settings);
  ~ConvolutionFitter() {
    // histograms
    delete m_inputHist;
    delete m_outputHist;
    // fit functions
    delete m_inputFit;
    delete m_responseFit;
    delete m_convolutionFit;
    delete m_convolution;
  }
  
  void setNsigma(float inputNsigma, float outputNsigma) {
    m_inputNsigma = inputNsigma;
    m_outputNsigma = outputNsigma;
  }
  
  // fit settings
  void boundFitRange(float xmin, float xmax) {m_xmin = xmin; m_xmax = xmax;};
  // set fit parameters
  // these methods only work if each parameter in function is assigned a unique name
  void setFitParameter(std::string parName, float parValue) {
    m_inputFit->SetParameter(m_inputFit->GetParNumber(parName.c_str()),parValue);
  }
  void fixFitParameter(std::string parName, float parValue) {
    m_inputFit->FixParameter(m_inputFit->GetParNumber(parName.c_str()),parValue);
    m_convolutionFit->FixParameter(m_convolutionFit->GetParNumber(parName.c_str()),parValue);
  }
  void limitFitParameter(std::string parName, float parLimLow, float parLimUp) {
    m_inputFit->SetParLimits(m_inputFit->GetParNumber(parName.c_str()),parLimLow,parLimUp);
  };

  // fit & draw results
  // fit methods
  void fitConvolution(TH1* inputHist, TH1* outputHist);
  void fitResponse(TH1* outputHist);
  
  // draw print fit info
  void drawFit();
  
  // access histograms & fits
  TH1F* getInputHist() { return m_inputHist; }
  TH1F* getOutputHist() { return m_outputHist; }
  TF1* getInputFit() {return m_inputFit; }
  TF1* getConvolutionFit() { return m_convolutionFit; }
  TF1* getResponseFit() { return m_responseFit; }
  
  // access fit parameters
  float getRespMu() { return getConvolutionFit()->GetParameter(m_inputFit->GetNpar()); }
  float getRespMuError() { return getConvolutionFit()->GetParError(m_inputFit->GetNpar()); }
  float getRespSigma() { return abs(getConvolutionFit()->GetParameter(1+m_inputFit->GetNpar())); }
  float getRespSigmaError() { return getConvolutionFit()->GetParError(1+m_inputFit->GetNpar()); }
  // access fit statistics
  // input signal
  float getInputChi2() { return getInputFit()->GetChisquare(); }
  float getInputNdf() { return getInputFit()->GetNDF(); }
  float getInputChi2Ndf() { return getInputChi2()/getInputNdf(); }
  float getInputProb() { return TMath::Prob(getInputChi2(),getInputNdf()); }
  // output signal
  float getConvChi2() { return getConvolutionFit()->GetChisquare(); }
  float getConvNdf() { return getConvolutionFit()->GetNDF(); }
  float getConvChi2Ndf() { return getConvChi2()/getConvNdf(); }
  float getConvProb() { return TMath::Prob(getConvChi2(),getConvNdf()); }
  
private:

  // fit functions
  // input
  std::string m_inputFitName;
  TF1* m_inputFit;
  // response
  std::string m_responseFitName;
  TF1* m_responseFit;
  // convolution
  TF1Convolution* m_convolution;
  TF1* m_convolutionFit;
  TF1* m_convolutionFit_out;
  
  // fit settings
  std::string m_inputFitOpt;
  std::string m_outputFitOpt;
  float m_inputNsigma;
  float m_outputNsigma;
  // input norm, mu, sigma parameter indices
  int m_inputNorm;
  std::vector<int> m_inputMus;
  std::vector<int> m_inputSigmas;
  // min & max x-range
  float m_xmin, m_xmax;
  
  // histograms
  TH1F* m_inputHist;
  TH1F* m_outputHist;
  
  // self-adjust fit range
  void adjustFitRange(TF1* fit, TH1F* hist, float Nsigma) {
    float mu = hist->GetMean();
    float sigma = hist->GetStdDev();
    float a = std::max<float>(m_xmin,mu-Nsigma*sigma);
    float b = std::min<float>(m_xmax,mu+Nsigma*sigma);
    fit->SetRange(a,b);
  }
};
