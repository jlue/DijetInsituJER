#pragma once

#include "Utils/Utils.h"
#include "Utils/HistUtils.h"
#include "Utils/Plotter.h"

#include "JetResponseFitter/JetResponseFitter.h"

namespace Simulation
{

class Response {

public:

  Response(const std::string& name, TH2F* pT_eta_bins);
  ~Response();

  void fitJetResponse(int pTbin, int etabin, TH1F* pTresp, JetResponseFitter* respFitter);
  TH2F* getJetResolution() { return m_JER; }
  void writeResults(TDirectory* outDir=nullptr);

private:

  // name
  std::string m_name;
  // jet JES/JER(pT,eta) map
  TH2F* m_JES;
  TH2F* m_JER;

};

}
