#pragma once

// bootstrap histograms
#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"

// helpers
#include "Utils/Utils.h"
#include "Utils/Config.h"
#include "Utils/HistUtils.h"
#include "Utils/Plotter.h"

// fitting tools
#include "JetResponseFitter/ConvolutionFitter.h"
#include "JetResponseFitter/JetResponseFitter.h"
#include "JetResponseFitter/JetResolutionFitter.h"

// package include(s)
#include "MeasureResolution/Response.h"
#include "MeasureResolution/Balance.h"
#include "MeasureResolution/NoiseStochConst.h"

namespace Resolution
{

struct MethodSwitch
{
public:
  bool balance;
  bool bisector;
  bool response;
  bool stdSub;
  bool convFit;
  bool fwdFold;
  bool foldRef;
  bool foldProbe;
  MethodSwitch(const std::vector<std::string> &algoVec)
  { 
    balance   = eleInVec("balance",  algoVec);
    bisector  = eleInVec("bisector", algoVec);
    response  = eleInVec("response", algoVec);
    stdSub    = eleInVec("stdSub",   algoVec);
    convFit   = eleInVec("convFit",  algoVec);
    fwdFold   = eleInVec("fwdFold",  algoVec);
    foldRef   = fwdFold && eleInVec("foldRef",  algoVec);
    foldProbe = fwdFold && eleInVec("foldProbe",  algoVec);
  }
};

class Measurement
{
  
public:

  Measurement(std::string name, Config settings);
  ~Measurement(){
    delete m_plotter;
    delete m_convFitter;
    delete m_resolFitter;
    delete m_respFitter;
  }

  // measure JER call
  void measureResolution(TDirectory* dataFolder, TDirectory* mcFolder);
  // in-situ JER
  void insitu(TDirectory* dataFolder, TDirectory* mcFolder);
  void simulation(TDirectory* dataFolder);
  
  // write outputs
  void writeResults(std::string outputName, TFile* outputFile);
  
// internal methods
private:

  void readMC(TDirectory* mcFolder);
  void readData(TDirectory* dataFolder);

// settings
private:

  std::string m_inputName;
  Config m_settings;

  std::string m_jetAlgo;
  std::string m_systematic;
  std::string m_variation;
  
  Resolution::MethodSwitch* m_method;
  
  bool m_bootstrap;

  // fitter parameters
  float m_Nsigma_A_true;
  float m_Nsigma_A_reco;
  float m_Nsigma_R;

  // input files
  TDirectory* m_dataFolder; std::string m_dataInfo;
  TDirectory* m_mcFolder; std::string m_mcInfo;

  // settings flags
  bool m_printInfo;
  bool m_drawFits;

// tools
private:

  // plots
  Plotter* m_plotter = nullptr;

  // simulated jet response
  JetResponseFitter* m_respFitter = nullptr;
  Simulation::Response* m_response = nullptr;

  // asymmetry convolution
  ConvolutionFitter* m_convFitter = nullptr;
  Insitu::Balance* m_balance = nullptr;

  // forward-folded templates
  // TemplateFitter* m_tmplFitter = nullptr;
  // Insitu::FoldTemplates* m_foldTmpls = nullptr;

  // (N,S,C) parametrization
  JetResolutionFitter* m_resolFitter = nullptr;
  std::map<std::string,NoiseStochConst*> m_nscFit;

// results
private:

  // original (pT,eta) bins
  TH2F* m_pT_eta_bins;

  std::vector<float> m_pT_bins;
  int m_npTbins;

  std::vector<float> m_eta_bins;
  int m_netabins;

  // rebinned (pT,eta)
  std::vector<std::vector<float>> m_pT_eta_rebins;
  std::vector<int> m_netarebins;

  // central (pT,eta) values
  TProfile2D* m_ref_pT;
  TProfile2D* m_probe_pT;
  TProfile2D* m_dijet_pTavg;

  // bootstrap replicas
  BootstrapGenerator* m_bootGen = nullptr;
  int m_Ntoys = 0;

  // asymmetry
  TH2F* m_ref_asym;
  std::vector<TH2F*> m_probe_asym;
  TH2F* m_ref_asym_true;
  std::vector<TH2F*> m_probe_asym_true;
  TH2FBootstrap* m_ref_asym_toys;
  std::vector<TH2FBootstrap*> m_probe_asym_toys;
  TH2FBootstrap* m_ref_asym_true_toys;
  std::vector<TH2FBootstrap*> m_probe_asym_true_toys;

  // forward-folding
  std::vector<TH1F*> m_ref_pTdiff;
  std::vector<TH1F*> m_ref_pTdiff_true;
  std::vector<TH2F*> m_ref_pTdiff_templates;

  // response
  TH3F* m_jet_resp;

private:
  
  // determine if histogram is sufficient for JER measurement
  bool isGoodHist(TH1* hist);
  
};

}