#pragma once

#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"

#include "Utils/Utils.h"
#include "Utils/HistUtils.h"
#include "Utils/Plotter.h"

#include "JetResponseFitter/JetResolutionFitter.h"

class NoiseStochConst 
{

public:

  NoiseStochConst(const std::string& name, TH2F* pT_eta_bins, BootstrapGenerator* bootGen=nullptr);
  ~NoiseStochConst() {
    delete m_JER_fit;
    if (m_bootstrap) delete m_JER_fits;
  }
  
  //  fit JER(pT) at eta bin to be added to JER(pT,eta) map
  void fitJetResolution(int etabin, TH1F* jet_pT_map, TH1F* JER_map, JetResolutionFitter* resolFitter);
  void fitJetResolution(int etabin, TH1F* jet_pT_map, TH1FBootstrap* JER_map, JetResolutionFitter* resolFitter);

  // write result histograms to file
  void writeResults(TDirectory* outDir=nullptr);

private:

  bool m_bootstrap;
  
private:

  // algorithm name
  std::string m_name;

  // JER fits
  TH2F* m_JER_fit;
  TH2FBootstrap* m_JER_fits;

};