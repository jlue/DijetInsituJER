#pragma once

#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"

#include "Utils/Utils.h"
#include "Utils/HistUtils.h"
#include "Utils/Plotter.h"

#include "JetResponseFitter/ConvolutionFitter.h"
#include "JetResponseFitter/JetResolutionFitter.h"

namespace Insitu
{

class Balance {

public:

  Balance(const std::string& name, TH2F* pT_eta_bins, BootstrapGenerator* bootGen=nullptr);
  ~Balance();
  
  // fit asymmetry widths in reference (pT) / probe (pT,eta) bins
  void fitReferenceWidth(int pTbin, TH1F* recoHist, TH1F* trueHist, ConvolutionFitter* convFitter);
  void fitReferenceWidth(int pTbin, TH1FBootstrap* recoToys, TH1FBootstrap* trueToys, ConvolutionFitter* convFitter);
  void fitProbeWidth(int pTbin, int etabin, TH1F* recoHist, TH1F* trueHist, ConvolutionFitter* convFitter);
  void fitProbeWidth(int pTbin, int etabin, TH1FBootstrap* recoToys, TH1FBootstrap* trueToys, ConvolutionFitter* convFitter);

  // compute JER from reference balance -> probe balance
  // convolution / width subtraction
  void subtractRefResolution(TProfile2D* pTref=nullptr, TProfile2D* pTprobe=nullptr, TProfile2D* pTavg=nullptr);

  // access JER results
  TH1F* getReferenceResolution() { return m_ref_insituJER; }
  TH1FBootstrap* getReferenceResolutions() { return m_ref_insituJER_toys; }
  TH2F* getProbeResolution() { return m_probe_insituJER; }
  TH2FBootstrap* getProbeResolutions() { return m_probe_insituJER_toys; }

  // write result histograms to file
  void writeResults(TDirectory* outDir=nullptr);

private:

  // methods called inside subtractRefResolution
  // subtractRefResolution -> fromRefWidth : JER in reference region
  // subtractRefResolution -> fromProbWidth : JER in probe region
  TH1F* fromRefWidth(TH1F* ref_asymWidth)
  {
    TH1F* ref_insituJER = HistUtils::copyHist("ref_"+m_name+"JER",ref_asymWidth);
    ref_asymWidth->Scale(1/sqrt(2));
    return ref_asymWidth;
  }
  TH2F* fromProbeWidth(TH1F* ref_insituJER, TH2F* probe_asymWidth, TProfile2D* ref_pT_map=nullptr, TProfile2D* probe_pT_map=nullptr, TProfile2D* dijet_pTavg_map=nullptr)
  {
    TH2F* probe_insituJER = HistUtils::copyHist("probe_"+m_name+"JER",probe_asymWidth);
    for (int ipt=0 ; ipt<probe_asymWidth->GetNbinsX() ; ++ipt) {
      float refJER = ref_insituJER->GetBinContent(ipt+1);
      for (int ieta=0 ; ieta<probe_asymWidth->GetNbinsY() ; ++ieta) {
        // nominal
        float asymWidth = probe_asymWidth->GetBinContent(ipt+1,ieta+1);
        float asymWidthError = probe_asymWidth->GetBinError(ipt+1,ieta+1);
        // (pTavg,pTref,pTprobe)
        float JER=0.0;
        if (ref_pT_map && probe_pT_map && dijet_pTavg_map) {
          float pTprobe = probe_pT_map->GetBinContent(ipt+1,ieta+1);
          float pTref = ref_pT_map->GetBinContent(ipt+1,ieta+1);
          float pTavg = dijet_pTavg_map->GetBinContent(ipt+1,ieta+1);
          JER = sqrt(pow(asymWidth*(pTavg*pTavg/pTprobe/pTref),2) - pow(refJER,2));
        } else {
          JER = sqrt(pow(asymWidth,2) - pow(refJER,2));
        }
        float JERError = asymWidthError;  // ignore refefence region uncertainty
        if ( !std::isfinite(JER) ) continue;
        probe_insituJER->SetBinContent(ipt+1,ieta+1,JER);
        probe_insituJER->SetBinError(ipt+1,ieta+1,JERError);
      }
    }
    return probe_insituJER;
  }
  
private:

  bool m_bootstrap;

  bool m_stdSub;
  bool m_convFit;
  
private:

  // algorithm name
  std::string m_name;

  // asymmetry widths
  // reconstructed
  TH1F* m_ref_asymWidth_reco;
  TH2F* m_probe_asymWidth_reco;
  TH1FBootstrap* m_ref_asymWidth_reco_toys;
  TH2FBootstrap* m_probe_asymWidth_reco_toys;
  // particle-level
  TH1F* m_ref_asymWidth_true;
  TH2F* m_probe_asymWidth_true;
  TH1FBootstrap* m_ref_asymWidth_true_toys;
  TH2FBootstrap* m_probe_asymWidth_true_toys;
  // detector
  TH1F* m_ref_asymWidth_det;
  TH2F* m_probe_asymWidth_det;
  TH1FBootstrap* m_ref_asymWidth_det_toys;
  TH2FBootstrap* m_probe_asymWidth_det_toys;
  
  // JER results
  TH1F* m_ref_insituJER;
  TH2F* m_probe_insituJER;
  TH1FBootstrap* m_ref_insituJER_toys;
  TH2FBootstrap* m_probe_insituJER_toys;

};

}
