#include "MeasureResolution/Response.h"

Simulation::Response::Response(const std::string& name, TH2F* pT_eta_bins) :
  m_name(name)
{
  std::vector<float> pT_bins = HistUtils::getBins(pT_eta_bins,1), eta_bins = HistUtils::getBins(pT_eta_bins,2);
  m_JER = HistUtils::makeHist(Form("jet_%sJER",m_name.c_str()),pT_bins,eta_bins);
}
Simulation::Response::~Response()
{
  delete m_JER;
}
void Simulation::Response::fitJetResponse(int pTbin, int etabin, TH1F* pTresp, JetResponseFitter* respFitter)
{
  respFitter->fitResponse(pTresp);
  m_JER->SetBinContent(pTbin,etabin,respFitter->getRelWidth());
  m_JER->SetBinError(pTbin,etabin,respFitter->getRelWidthError());
}
void Simulation::Response::writeResults(TDirectory* outDir) {
  if (outDir) outDir->cd();
  m_JER->Write(Form("%sJER",m_name.c_str()));
}