#include "MeasureResolution/Measurement.h"

Resolution::Measurement::Measurement (std::string jetAlgo, Config settings) :
  m_jetAlgo(jetAlgo),
  m_settings(settings)
{
  // measurement method
  m_method = new Resolution::MethodSwitch(m_settings.getStrV("Methods"));
  // bootstrap toys
  m_bootstrap = m_settings.getBool("Bootstrap",false);

  // eta-rebinning
  for (const auto& pTbin : settings.getStrV(jetAlgo+".eta_rebins")) {
    std::vector<float> eta_rebins = m_settings.getNumV(Form("%s.%s.eta_rebins",m_jetAlgo.c_str(),pTbin.c_str()),{});
    m_pT_eta_rebins.push_back(eta_rebins);
    m_netarebins.push_back(eta_rebins.size()-1);
  }

  // fitters & plotters
  m_respFitter = new JetResponseFitter(m_settings);
  m_convFitter = new ConvolutionFitter(m_settings);
  // m_tmplFitter = new TemplateFitter();
  m_resolFitter = new JetResolutionFitter();
  m_plotter = new Plotter();
}

void Resolution::Measurement::readData(TDirectory* dataFolder)
{
  // dijet asymmetry
  if (m_method->balance) {
    m_ref_asym = getObject<TH2F>("dijet_pTavg:asym[ref_etadet]",dataFolder);
    if (m_bootstrap) {
      dataFolder->GetObject(("bootstrapGenerator"),m_bootGen); 
      m_ref_asym_toys = getObject<TH2FBootstrap>("dijet_pTavg:asym_toys[ref_etadet]",dataFolder);
      if (m_ref_asym_toys) m_ref_asym = (TH2F*)m_ref_asym_toys->GetNominal();
    }
    m_probe_asym.clear();
    m_probe_asym_toys.clear();
    for (int ieta=0 ; ieta<m_netabins ; ++ieta) {
      float etalow = m_pT_eta_bins->GetYaxis()->GetBinLowEdge(ieta+1);
      float etaup = m_pT_eta_bins->GetYaxis()->GetBinLowEdge(ieta+2);
      std::string etaBinName = Form("%geta%g",etalow,etaup);
      TH2F* probe_asym = getObject<TH2F>(Form("dijet_pTavg:asym[probe_etadet%i]",ieta+1),dataFolder);
      if (m_bootstrap) {
        TH2FBootstrap* probe_asym_toys = getObject<TH2FBootstrap>(Form("dijet_pTavg:asym_toys[probe_etadet%i]",ieta+1),dataFolder);
        m_probe_asym_toys.push_back(probe_asym_toys);
        if (probe_asym_toys) probe_asym = (TH2F*)probe_asym_toys->GetNominal();
      }
      m_probe_asym.push_back(probe_asym);
    }
  }

  // dijet pTdiff
  if (m_method->fwdFold) {
    // // reference region
    // for (int ipt=0 ; ipt<m_npTbins ; ++ipt) {
    //   m_ref_pTdiff.push_back(getObject<TH1F>(Form("pTdiff[dijet_pTavg%i,ref_etadet]",ipt+1),dataFolder));
    // }
  }

  return;  // read all Data inputs
}

void Resolution::Measurement::readMC(TDirectory* mcFolder)
{
  // (pT,eta) binning
  mcFolder->GetObject(("pT_bins:eta_bins"),m_pT_eta_bins);
  m_pT_bins = getBins(m_pT_eta_bins,1);
  m_eta_bins = getBins(m_pT_eta_bins,2);
  m_npTbins = m_pT_eta_bins->GetNbinsX();
  m_netabins = m_pT_eta_bins->GetNbinsY();
  m_ref_pT = getObject<TProfile2D>("<ref_pT>(dijet_pTavg,probe_etadet)",mcFolder);
  m_probe_pT = getObject<TProfile2D>("<probe_pT>(dijet_pTavg,probe_etadet)",mcFolder);

  // particle-level asymmetry
  if (m_method->balance) {
    mcFolder->GetObject(("dijet_pTavg:asym_true[ref_etadet]"),m_ref_asym_true);
    if (m_bootstrap) {
      mcFolder->GetObject(("dijet_pTavg:asym_true_toys[ref_etadet]"),m_ref_asym_true_toys);
      if (m_ref_asym_true_toys) m_ref_asym_true = (TH2F*)m_ref_asym_true_toys->GetNominal();
    }
    m_probe_asym_true.clear();
    m_probe_asym_true_toys.clear();
    if (!m_ref_asym_true && m_ref_asym_true_toys) m_ref_asym_true = (TH2F*)m_ref_asym_true_toys->GetNominal();
    for (int ieta=0 ; ieta<m_netabins ; ++ieta) {
      float etalow = m_pT_eta_bins->GetYaxis()->GetBinLowEdge(ieta+1);
      float etaup = m_pT_eta_bins->GetYaxis()->GetBinLowEdge(ieta+2);
      std::string etaBinName = Form("%geta%g",etalow,etaup);
      TH2F* probe_asym_true = getObject<TH2F>(Form("dijet_pTavg:asym_true[probe_etadet%i]",ieta+1),mcFolder);
      if (m_bootstrap) {
        TH2FBootstrap* probe_asym_true_toys = getObject<TH2FBootstrap>(Form("dijet_pTavg:asym_true_toys[probe_etadet%i]",ieta+1),mcFolder);
        m_probe_asym_true_toys.push_back(probe_asym_true_toys);
        if (probe_asym_true_toys) probe_asym_true = (TH2F*)probe_asym_true_toys->GetNominal();
      }
      m_probe_asym_true.push_back(probe_asym_true);
    }
  }

  // pTdiff_true & templates
  if (m_method->fwdFold) {
    // // reference region
    // for (int ipt=0 ; ipt<m_npTbins ; ++ipt) {
    //   m_ref_pTdiff_true.push_back(getObject<TH1F>(Form("pTdiff_true[dijet_pTavg%i,ref_etadet]",ipt+1),mcFolder));
    //   m_ref_pTdiff_templates.push_back(getObject<TH2F>(Form("r:pTdiff_fold[dijet_pTavg%i,ref_etadet]",ipt+1),mcFolder));
    //   // for (int ieta=0 ; ieta<m_netabins ; ++ieta) {
    //   // }
    // }
  }

  // pT response
  m_jet_resp = getObject<TH3F>("jet_pT_true:jet_etadet:jet_resp_pT",mcFolder);

  return;  // read all MC inputs
}

void Resolution::Measurement::insitu(TDirectory* dataFolder, TDirectory* mcFolder)
{
  info("In-situ Measurement","Resolution::Measurement::insitu");

  // read input files
  readMC(mcFolder);
  readData(dataFolder);

  // dijet balance (asymmetry convolution)
  if (m_method->balance) {

    m_bootstrap ? m_balance = new Insitu::Balance("insitu",m_pT_eta_bins,m_bootGen):
                  m_balance = new Insitu::Balance("insitu",m_pT_eta_bins);

    // start plots
    m_plotter->openFile(m_jetAlgo+"_balance");

    // 1. perform convolution fits to extract reference/probe widths
    // loop over pT bins
    for ( int ipt=0 ; ipt<m_pT_eta_rebins.size() ; ++ipt) {
      float pTlow = m_pT_eta_bins->GetXaxis()->GetBinLowEdge(ipt+1);
      float pTup = m_pT_eta_bins->GetXaxis()->GetBinLowEdge(ipt+2);
      info(Form("    pT [GeV]: %.f -- %.f",pTlow,pTup),"Resolution::Measurement::insitu");

      // reference
      info("        eta: reference","Resolution::Measurement::insitu");
      TH1F* ref_asym = (TH1F*)(m_ref_asym->ProjectionY(Form("ref_asym_pT%i",ipt),ipt+1,ipt+1));
      TH1F* ref_asym_true = (TH1F*)(m_ref_asym_true->ProjectionY(Form("ref_asym_true_pT%i",ipt),ipt+1,ipt+1));
      // skip if histograms not good enough for fits
      if (not isGoodHist(ref_asym) || not isGoodHist(ref_asym_true)) continue;
      // compute width
      m_balance->fitReferenceWidth(ipt+1,ref_asym,ref_asym_true,m_convFitter);
      delete ref_asym;
      delete ref_asym_true;
      // bootstrap replicas
      if (m_bootstrap && m_bootGen) {
        TH1FBootstrap* ref_asym_toys = (TH1FBootstrap*)(m_ref_asym_toys->ProjectionY(Form("ref_asym_toys_pT%i",ipt+1),ipt+1,ipt+1));
        TH1FBootstrap* ref_asym_true_toys = (TH1FBootstrap*)(m_ref_asym_true_toys->ProjectionY(Form("ref_asym_true_toys_pT%i",ipt+1),ipt+1,ipt+1));
        m_balance->fitReferenceWidth(ipt+1,ref_asym_toys,ref_asym_true_toys,m_convFitter);
        delete ref_asym_toys;
        delete ref_asym_true_toys;
      }

      // loop over eta bins
      for (int ieta=0 ; ieta<m_netarebins[ipt] ; ++ieta) {
        float etalow = m_pT_eta_rebins[ipt][ieta];
        float etaup = m_pT_eta_rebins[ipt][ieta+1];
        info(Form("        eta: %.1f -- %.1f",etalow,etaup),"Resolution::Measurement::insitu");

        // probe
        TH1F* probe_asym = (TH1F*)(m_probe_asym.at(ieta)->ProjectionY(Form("probe_asym_pT%i_eta%i",ipt+1,ieta+1),ipt+1,ipt+1));
        TH1F* probe_asym_true = (TH1F*)(m_probe_asym_true.at(ieta)->ProjectionY(Form("probe_asym_true_pT%i_eta%i",ipt+1,ieta+1),ipt+1,ipt+1));
        // skip if histograms not good enough for fits
        if (not isGoodHist(probe_asym) || not isGoodHist(probe_asym_true)) continue;
        // compute width
        m_balance->fitProbeWidth(ipt+1,ieta+1,probe_asym,probe_asym_true,m_convFitter);
        delete probe_asym;
        delete probe_asym_true;
        // bootstrap replicas
        if (m_bootstrap && m_bootGen) {
            TH1FBootstrap* probe_asym_toys = (TH1FBootstrap*)(m_probe_asym_toys.at(ieta)->ProjectionY(Form("probe_asym_toys_pT%i_eta%i",ipt+1,ieta+1),ipt+1,ipt+1));
            TH1FBootstrap* probe_asym_true_toys = (TH1FBootstrap*)(m_probe_asym_true_toys.at(ieta)->ProjectionY(Form("probe_asym_true_toys_pT%i_eta%i",ipt+1,ieta+1),ipt+1,ipt+1));
            m_balance->fitProbeWidth(ipt+1,ieta+1,probe_asym_toys,probe_asym_true_toys,m_convFitter);
            delete probe_asym_toys;
            delete probe_asym_true_toys;
        }

        // plot fit
        m_plotter->makeAxis("Dijet asymmetry", -3.0*m_convFitter->getInputHist()->GetRMS(),m_convFitter->getInputHist()->GetRMS()*3.0,
                            "Normalized [a.u.]", 0.0,m_convFitter->getInputHist()->GetMaximum()*1.5);
        // particle-level
        m_plotter->addHist(m_convFitter->getInputHist());
        m_plotter->setLabel("particle-level","ep");
        m_plotter->setLine(kBlue,kSolid);
        m_plotter->setMarker(kBlue,kOpenCircle);
        m_plotter->addFtn(m_convFitter->getInputFit());
        m_plotter->setLine(kBlue,kSolid);
        // reconstructed
        m_plotter->addHist(m_convFitter->getOutputHist());
        m_plotter->setLabel("reconstructed","ep");
        m_plotter->setLine(kBlack,kSolid);
        m_plotter->setMarker(kBlack,kFullCircle);
        m_plotter->addFtn(m_convFitter->getConvolutionFit());
        m_plotter->setLine(kRed,kSolid);
        // labels
        // m_plotter->addLogo(0.2,0.85,"Internal");
        // m_plotter->addLabel(0.2,0.80,"#it{#sqrt{s}} = 13 TeV, 44 fb^{-1}, dijets");
        // m_plotter->addLabel(0.2,0.75,"Anti-#it{k}_{t} #it{R} = 0.4 (PFlow+JES)");
        m_plotter->addLabel(0.6,0.75,Form("#it{#chi}^{2} / #it{N}_{dof} = %.2f / %g",m_convFitter->getInputChi2(),m_convFitter->getInputNdf()),kBlue);
        m_plotter->addLabel(0.6,0.70,Form("#it{#chi}^{2} / #it{N}_{dof} = %.2f / %g",m_convFitter->getConvChi2(),m_convFitter->getConvNdf()),kRed);
        m_plotter->addLabel(0.6,0.85,Form("%.f < #it{p}_{T}^{avg} [GeV] < %.f",pTlow,pTup));
        m_plotter->addLabel(0.6,0.80,Form("%.1f < #||{#it{#eta}_{det}} < %.1f",etalow,etaup));
        // save
        m_plotter->savePlot();
      }  // eta bin
    }  // pT bin

    // 2. subtract reference resolution from probe widths
    m_balance->subtractRefResolution();

    // 3. fit JER to (N,S,C) parametrization
    m_nscFit["insitu"] = new NoiseStochConst("insitu",m_pT_eta_bins,m_bootGen);
    TH2F* JER_map = m_balance->getProbeResolution();
    TH2FBootstrap* JER_maps = m_bootstrap ? m_balance->getProbeResolutions() : nullptr;
    // loop over eta bin
    for (int ieta=0 ; ieta<m_netabins ; ++ieta) {
      // get JER & pT map projection
      TH1F* pT = HistUtils::project(m_probe_pT,1,ieta+1);
      TH1F* JER = HistUtils::project(JER_map,1,ieta+1);
      m_nscFit["insitu"]->fitJetResolution(ieta+1,pT,JER,m_resolFitter);
      // bootstrap replicas
      if (m_bootstrap) {
        TH1FBootstrap* JERs = (TH1FBootstrap*)JER_maps->ProjectionX(Form("%s_eta%i",JER_maps->GetName(),ieta+1),ieta+1,ieta+1);
        m_nscFit["insitu"]->fitJetResolution(ieta+1,pT,JERs,m_resolFitter);
      }
      // draw fit
      m_resolFitter->drawFit();
    }

    // close plots
    m_plotter->closeFile();

    info("----------------------------------------","Resolution::Measurement::insitu");
  }  // balance

  // forward-folding
  if (m_method->fwdFold) {
    // m_plotter->openFile(m_jetAlgo+"_fwdFold");
    // m_foldTmpls = new Insitu::FoldTemplates("FF",m_pT_bins);
    // for (int ipt=0 ; ipt<m_npTbins ; ++ipt) {
    //   float pTlow = m_pT_eta_bins->GetXaxis()->GetBinLowEdge(ipt+1);
    //   float pTup = m_pT_eta_bins->GetXaxis()->GetBinLowEdge(ipt+2);
    //   info(Form("    pT [GeV]: %.f -- %.f",pTlow,pTup),"Resolution::Measurement::insitu");
    //   // if (not isGoodHist(m_ref_pTdiff.at(ipt)) || not isGoodHist(m_ref_pTdiff_templates.at(ipt))) continue;
    //   m_foldTmpls->fitTemplates(ipt+1,m_ref_pTdiff[ipt],m_ref_pTdiff_templates[ipt],m_tmplFitter);
    //   info(Form("    r = %.2f",m_tmplFitter->getR()),"Resolution::Measurement::insitu");

    //     m_plotter->makeAxis("Fold parameter #it{r}",0.9,1.1,
    //                         "#it{#chi}^{2}/#it{dof}",0.0,10.0);
    //     // particle-level
    //     m_plotter->addHist(m_tmplFitter->getChi2("r"),"hist");
    //     m_plotter->setLine(kBlack,kSolid,2);
    //     m_plotter->savePlot();

    //     m_plotter->makeAxis("Dijet #Delta#it{p}_{T} [GeV]", -200.0,200.0,
    //                         "Normalized [a.u.]",0.0,m_tmplFitter->getFitHist()->GetMaximum()*1.5);
    //     // particle-level
    //     m_plotter->addHist(m_tmplFitter->getRecoHist(),"ep");
    //     m_plotter->setMarker(kBlack,kFullCircle,1.2);
    //     m_plotter->addHist(m_tmplFitter->getFitHist(),"hist");
    //     m_plotter->setLine(kRed,kSolid,2);
    //     m_plotter->savePlot();
    // }
    // m_plotter->closeFile();
  }

  return;  // finished in-situ measurement
}

void Resolution::Measurement::simulation(TDirectory* mcFolder)
{
  info("Jet Response Simulation","Resolution::Measurement::simulation");


  // detector response simulation
  if (m_method->response) {

    readMC(mcFolder);
    m_response = new Simulation::Response("MC",m_pT_eta_bins);
    // m_plotter->openPDF(m_jetAlgo+"_response");

    // measure JER(pT,eta)
    for ( int ipt=0 ; ipt<m_pT_eta_rebins.size() ; ++ipt) {
      float pTlow = m_pT_eta_bins->GetXaxis()->GetBinLowEdge(ipt+1);
      float pTup = m_pT_eta_bins->GetXaxis()->GetBinLowEdge(ipt+2);
      info(Form("    pT [GeV]: %.f -- %.f",pTlow,pTup),"Resolution::Measurement::simulation");

      for (int ieta=0 ; ieta<m_netarebins[ipt] ; ++ieta) {
        float etalow = m_pT_eta_rebins[ipt][ieta];
        float etaup = m_pT_eta_rebins[ipt][ieta+1];
        info(Form("        eta: %.1f -- %.1f",etalow,etaup),"Resolution::Measurement::simulation");

        // get response histogram
        TH1F* resp = HistUtils::project(m_jet_resp,3,ipt+1,ieta+1);
        // fit width/mean
        if (not isGoodHist(resp) ) continue;
        m_response->fitJetResponse(ipt+1,ieta+1,resp,m_respFitter);
        // plot fit
        // m_plotter->makeAxis(1.0-resp->GetRMS()*4.0,1.0+resp->GetRMS()*4.0,
        //                     0.0,m_respFitter->getHist()->GetMaximum()*1.50,
        //                     "#it{p}_{T}^{reco} / #it{p}_{T}^{true}", "Normalized (arb. units)");
        // m_respFitter->drawFitAndHist();
        // m_plotter->labelPlot(0.2,0.9,Form("%.f < #it{p}_{T}^{true} [GeV] < %.f",pTlow,pTup));
        // m_plotter->labelPlot(0.2,0.85,Form("%.1f < #||{#it{#eta}_{det}} < %.1f",etalow,etaup));
        // m_plotter->labelPlot(0.6,0.9,Form("#it{#chi}^{2} / #it{N}_{dof} = %.2f / %g",m_respFitter->getChi2(),m_respFitter->getNdof()));
        // m_plotter->labelPlot(0.6,0.85,Form("#it{#mu} = (%.2f #pm %.2f)%%",(m_respFitter->getMu()-1.0)*100,100*m_respFitter->getMuError()));
        // m_plotter->labelPlot(0.6,0.80,Form("#it{#sigma} = (%.2f #pm %.2f)%%",100*m_respFitter->getSigma(),100*m_respFitter->getSigmaError()));
        // m_plotter->savePDF();
      }
    }

    // fit JER to (N,S,C) parametrization
    m_nscFit["MC"] = new NoiseStochConst("MC",m_pT_eta_bins);
    TH2F* JER_map = m_response->getJetResolution();
    // for each eta bin
    for (int ieta=0 ; ieta<m_netabins ; ++ieta) {
      // get JER & pT map projection
      TH1F* pT = HistUtils::project(m_probe_pT,1,ieta+1);
      TH1F* JER = HistUtils::project(JER_map,1,ieta+1);
      // nominal
      m_nscFit["MC"]->fitJetResolution(ieta+1,pT,JER,m_resolFitter);

      // m_plotter->makeAxis(150.0,3000.0,0.0,0.15,"Jet #it{p}_{T} [GeV]","Relative jet energy resolution");
      // m_plotter->logAxis(true,false);
      // m_plotter->plotGraphMarker(m_resolFitter->getData(),kBlack,kFullCircle);
      // m_plotter->plotFtnLine(m_resolFitter->getFit(),kRed,kSolid);
      // m_resolFitter->drawFit();
      // m_plotter->savePDF();
      // store fit result
    }

    // m_plotter->closePDF();
  }

  info("----------------------------------------","Resolution::Measurement::simulation");

  return;  // finished simulation measurement
}

void Resolution::Measurement::writeResults( std::string outName , TFile* outFile )
{
  // create folder in output file
  info("========================================","Resolution::Measurement::writeResults");
  info(Form("Results being written to: %s",outFile->GetPath()),"Resolution::Measurement::writeResults");
  outFile->cd();
  TDirectory* outFolder = outFile->mkdir(outName.c_str(),outName.c_str());
  outFolder->cd();
  
  // insitu results
  if (m_method->balance) {
    m_balance->writeResults(outFolder);
  }
  if (m_method->fwdFold) {
    // m_foldTmpls->writeResults(outFolder);
  }
  // simulation ressults
  if (m_method->response) {
    m_response->writeResults(outFolder);
  }

  // write fits
  for (auto nscFit : m_nscFit) {
    nscFit.second->writeResults(outFolder);
  }

  info("========================================","Resolution::Measurement::writeResults");
}

bool Resolution::Measurement::isGoodHist(TH1* hist)
// determine if histogram is "good" enough to do fit
{
  // histogram must exist
  if (not hist) {
    warn(Form("histogram %s not found",hist->GetName()),"isGoodHist");
    return false;
  }
  // must have entries
  if (hist->GetEntries()==0) return false;
  // must have sufficient statistics
  if (hist->GetEffectiveEntries()<100) return false;
  // pass all checks: histogram is good
  return true;
}
