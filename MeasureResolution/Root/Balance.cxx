#include "MeasureResolution/Balance.h"

Insitu::Balance::Balance(const std::string& name, TH2F* pT_eta_bins, BootstrapGenerator* bootGen) :
  m_name(name),
  m_bootstrap(bootGen),
  m_stdSub(false),
  m_convFit(false)
{
  std::vector<float> pT_bins = HistUtils::getBins(pT_eta_bins,1), eta_bins = HistUtils::getBins(pT_eta_bins,2);
  m_ref_asymWidth_det    = HistUtils::makeHist("ref_asymWidth_det",pT_bins);
  m_probe_asymWidth_det  = HistUtils::makeHist("probe_asymWidth_det",pT_bins,eta_bins);
  m_ref_asymWidth_true   = HistUtils::makeHist("ref_asymWidth_true",pT_bins);
  m_probe_asymWidth_true = HistUtils::makeHist("probe_asymWidth_true",pT_bins,eta_bins);
  m_ref_asymWidth_reco   = HistUtils::makeHist("ref_asymWidth_reco",pT_bins);
  m_probe_asymWidth_reco = HistUtils::makeHist("probe_asymWidth_reco",pT_bins,eta_bins);
  if (m_bootstrap) {
    m_ref_asymWidth_det_toys    = new TH1FBootstrap(*m_ref_asymWidth_det,    bootGen->GetNReplica(),bootGen);
    m_ref_asymWidth_det_toys->SetName(Form("%s_toys",m_ref_asymWidth_det_toys->GetName()));
    m_probe_asymWidth_det_toys  = new TH2FBootstrap(*m_probe_asymWidth_det,  bootGen->GetNReplica(),bootGen);
    m_probe_asymWidth_det_toys->SetName(Form("%s_toys",m_probe_asymWidth_det_toys->GetName()));
    m_ref_asymWidth_true_toys   = new TH1FBootstrap(*m_ref_asymWidth_true,   bootGen->GetNReplica(),bootGen);
    m_ref_asymWidth_true_toys->SetName(Form("%s_toys",m_ref_asymWidth_true_toys->GetName()));
    m_probe_asymWidth_true_toys = new TH2FBootstrap(*m_probe_asymWidth_true, bootGen->GetNReplica(),bootGen);
    m_probe_asymWidth_true_toys->SetName(Form("%s_toys",m_probe_asymWidth_true_toys->GetName()));
    m_ref_asymWidth_reco_toys   = new TH1FBootstrap(*m_ref_asymWidth_reco,   bootGen->GetNReplica(),bootGen);
    m_ref_asymWidth_reco_toys->SetName(Form("%s_toys",m_ref_asymWidth_reco_toys->GetName()));
    m_probe_asymWidth_reco_toys = new TH2FBootstrap(*m_probe_asymWidth_reco, bootGen->GetNReplica(),bootGen);
    m_probe_asymWidth_reco_toys->SetName(Form("%s_toys",m_probe_asymWidth_reco_toys->GetName()));
  }
}
Insitu::Balance::~Balance()
{
  delete m_ref_insituJER;
  delete m_probe_insituJER;
}

void Insitu::Balance::fitReferenceWidth(int pTbin, TH1F* recoHist, TH1F* trueHist, ConvolutionFitter* convFitter)
{
  m_convFit = convFitter;
  convFitter->fitConvolution(trueHist,recoHist);
  m_ref_asymWidth_det->SetBinContent(pTbin,convFitter->getRespSigma());
  m_ref_asymWidth_det->SetBinError(pTbin,convFitter->getRespSigmaError());
}
void Insitu::Balance::fitReferenceWidth(int pTbin, TH1FBootstrap* recoHist, TH1FBootstrap* trueHist, ConvolutionFitter* convFitter)
{
  m_convFit = convFitter;
  // nominal
  convFitter->fitConvolution(trueHist->GetNominal(),recoHist->GetNominal());
  m_ref_asymWidth_det_toys->GetNominal()->SetBinContent(pTbin,convFitter->getRespSigma());
  m_ref_asymWidth_det_toys->GetNominal()->SetBinError(pTbin,convFitter->getRespSigmaError());
  // toys
  for (int itoy=0 ; itoy< recoHist->GetNReplica() ; ++itoy) {
    convFitter->fitConvolution(trueHist->GetReplica(itoy),recoHist->GetReplica(itoy));
    m_ref_asymWidth_det_toys->GetReplica(itoy)->SetBinContent(pTbin,convFitter->getRespSigma());
    m_ref_asymWidth_det_toys->GetReplica(itoy)->SetBinError(pTbin,convFitter->getRespSigmaError());
  }
}
void Insitu::Balance::fitProbeWidth(int pTbin, int etabin, TH1F* recoHist, TH1F* trueHist, ConvolutionFitter* convFitter)
{
  m_convFit = convFitter;
  convFitter->fitConvolution(trueHist,recoHist);
  m_probe_asymWidth_det->SetBinContent(pTbin,etabin,convFitter->getRespSigma());
  m_probe_asymWidth_det->SetBinError(pTbin,etabin,convFitter->getRespSigmaError());
}
void Insitu::Balance::fitProbeWidth(int pTbin, int etabin, TH1FBootstrap* recoHist, TH1FBootstrap* trueHist, ConvolutionFitter* convFitter)
{
  m_convFit = convFitter;
  // nominal
  convFitter->fitConvolution(trueHist->GetNominal(),recoHist->GetNominal());
  m_probe_asymWidth_det_toys->GetNominal()->SetBinContent(pTbin,etabin,convFitter->getRespSigma());
  m_probe_asymWidth_det_toys->GetNominal()->SetBinError(pTbin,etabin,convFitter->getRespSigmaError());
  // toys
  for (int itoy=0 ; itoy< recoHist->GetNReplica() ; ++itoy) {
    convFitter->fitConvolution(trueHist->GetReplica(itoy),recoHist->GetReplica(itoy));
    m_probe_asymWidth_det_toys->GetReplica(itoy)->SetBinContent(pTbin,etabin,convFitter->getRespSigma());
    m_probe_asymWidth_det_toys->GetReplica(itoy)->SetBinError(pTbin,etabin,convFitter->getRespSigmaError());
  }
}

void Insitu::Balance::subtractRefResolution(TProfile2D* ref_pT_map, TProfile2D* probe_pT_map, TProfile2D* dijet_pTavg_map)
{
  info("Computing JER(pT,eta) from ref and probe dijet balance",Form("Insitu::Balance.%s::computeResolution",m_name.c_str()));

  // reference
  m_ref_insituJER = fromRefWidth(m_ref_asymWidth_det);
  // bootstrap
  if (m_bootstrap) {
    m_ref_insituJER_toys = new TH1FBootstrap(*m_ref_asymWidth_det_toys);
    m_ref_insituJER_toys->Scale(1/sqrt(2));
  }

  // probe
  m_probe_insituJER = fromProbeWidth(m_ref_insituJER,m_probe_asymWidth_det,ref_pT_map,probe_pT_map,dijet_pTavg_map);
  // bootstrap
  if (m_bootstrap) {
    m_probe_insituJER_toys = new TH2FBootstrap(*m_probe_asymWidth_det_toys);
    // nominal
    TH1F* ref_insituJER = (TH1F*)m_ref_insituJER_toys->GetNominal();
    TH2F* probe_asymWidth_det = (TH2F*)m_probe_asymWidth_det_toys->GetNominal();
    TH2F* probe_insituJER = fromProbeWidth(ref_insituJER,probe_asymWidth_det,ref_pT_map,probe_pT_map,dijet_pTavg_map);
    m_probe_insituJER_toys->SetNominal(*probe_insituJER);
    // toys
    for (int itoy=0 ; itoy<m_probe_insituJER_toys->GetNReplica() ; ++itoy) {
      TH1F* ref_insituJER = (TH1F*)m_ref_insituJER_toys->GetReplica(itoy);
      TH2F* probe_asymWidth_det = (TH2F*)m_probe_asymWidth_det_toys->GetReplica(itoy);
      TH2F* probe_insituJER = fromProbeWidth(ref_insituJER,probe_asymWidth_det,ref_pT_map,probe_pT_map,dijet_pTavg_map);
      m_probe_insituJER_toys->SetReplica(itoy,*probe_insituJER);
    }
  }

  // set names
  m_ref_insituJER->SetName(("ref_"+m_name+"JER").c_str());
  m_probe_insituJER->SetName(("probe_"+m_name+"JER").c_str());
  if (m_bootstrap) {
    m_ref_insituJER_toys->SetName(("ref_"+m_name+"JER_toys").c_str());
    m_probe_insituJER_toys->SetName(("probe_"+m_name+"JER_toys").c_str());
  }

  // bootstrap mean+/-stddev
  if (m_bootstrap) {
    m_ref_insituJER_toys->SetValBootstrapMean();
    m_ref_insituJER_toys->SetErrBootstrapRMS();
    m_ref_insituJER = (TH1F*)m_ref_insituJER_toys->GetNominal();
    m_probe_insituJER_toys->SetValBootstrapMean();
    m_probe_insituJER_toys->SetErrBootstrapRMS();
    m_probe_insituJER = (TH2F*)m_probe_insituJER_toys->GetNominal();
  }

}

void Insitu::Balance::writeResults(TDirectory* outDir)
{
  if (outDir) outDir->cd();

  // fitted asymmetry width results
  m_ref_asymWidth_det->Write();
  m_probe_asymWidth_det->Write();
  m_ref_insituJER->Write();
  m_probe_insituJER->Write();
  if (m_bootstrap) {
    m_ref_asymWidth_det_toys->Write();
    m_probe_asymWidth_det_toys->Write();
    m_ref_insituJER_toys->Write();
    m_probe_insituJER_toys->Write();
  }
}
