#include "MeasureResolution/Measurement.h"
#include "MeasureResolution/Balance.h"
#include "MeasureResolution/Response.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ class Measurement+;
#pragma link C++ class Simulation::Response+;
#pragma link C++ class Insitu::Balance+;
// #pragma link C++ class Insitu::FoldTemplates+;
#pragma link C++ class NoiseStochConst+;

#endif


