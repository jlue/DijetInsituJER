#include "MeasureResolution/NoiseStochConst.h"

NoiseStochConst::NoiseStochConst(const std::string& name, TH2F* pT_eta_bins, BootstrapGenerator* bootGen) :
  m_name(name),
  m_bootstrap(bootGen)
{
  std::vector<float> pT_bins = HistUtils::getBins(pT_eta_bins,1), eta_bins = HistUtils::getBins(pT_eta_bins,2);
  m_JER_fit = HistUtils::makeHist(Form("%sJER_fit",m_name.c_str()),makeLogVec(100,pT_bins.front(),pT_bins.back()),eta_bins);
  if (m_bootstrap) {
    m_JER_fits = new TH2FBootstrap(*m_JER_fit,bootGen->GetNReplica(),bootGen);
    m_JER_fits->SetName(Form("%sJER_fits",m_name.c_str()));
  }
}

// nominal histogram versio
void NoiseStochConst::fitJetResolution(int etabin, TH1F* pT_map, TH1F* JER_map, JetResolutionFitter* resolFitter)
{
  // get minimum pT of JER measurement
  std::vector<float> pTbins = HistUtils::getBins(pT_map);
  float pTmin = pTbins.front();
  float pTmax = pTbins.back();
  int minpts = 0;
  for (int ipt=0 ; ipt<JER_map->GetNbinsX() ; ++ipt) {
    if (!JER_map->GetBinContent(ipt+1)) ++minpts;
    else { pTmin = pT_map->GetBinContent(minpts+1); break; }
  }
  // get maximum pT of JER measurement
  int npts = 0;
  for (int ipt=minpts ; ipt<JER_map->GetNbinsX() ; ++ipt) {
    if (JER_map->GetBinContent(ipt+1)) ++npts;
    else { pTmax = pT_map->GetBinContent(ipt+1); break; }
  }
  // make TGraph of JER(pT) for fit
  TGraphAsymmErrors* data = HistUtils::makeGraph(Form("%sJER(pT)[eta%i]",m_name.c_str(),etabin),npts,pT_map,JER_map);
  // perform fit
  resolFitter->fitResolution(data,pTmin,pTmax);
  // fill JER(pT,eta) map with fitted values
  for (int ipt=0 ; ipt<m_JER_fit->GetNbinsX() ; ++ipt) {
    m_JER_fit->SetBinContent(ipt+1,etabin,resolFitter->getFit()->Eval(m_JER_fit->GetXaxis()->GetBinCenter(ipt+1)));
  }
}
// bootstrap version
void NoiseStochConst::fitJetResolution(int etabin, TH1F* pT_map, TH1FBootstrap* JER_maps, JetResolutionFitter* resolFitter)
{
  // get minimum pT of JER measurement
  std::vector<float> pTbins = HistUtils::getBins(pT_map);
  float pTmin = pTbins.front();
  float pTmax = pTbins.back();
  // get maximum pT of JER measurement
  int npts = 0;
  for (int ipt=0 ; ipt<JER_maps->GetNominal()->GetNbinsX() ; ++ipt) {
    if (JER_maps->GetNominal()->GetBinContent(ipt+1)) ++npts;
    else { pTmax = pT_map->GetBinContent(ipt+1); break; }
  }
  // toys
  for (int itoy=0 ; itoy<JER_maps->GetNReplica() ; ++itoy) {
    // make TGraph of JER(pT) for fit
    TH1F* JER_map = (TH1F*)JER_maps->GetReplica(itoy);
    TGraphAsymmErrors* data = HistUtils::makeGraph(Form("%sJER(pT)[eta%i]_toy%i",m_name.c_str(),etabin,itoy),npts,pT_map,JER_map);
    // perform fit
    resolFitter->fitResolution(data,pTmin,pTmax);
    // fill JER(pT,eta) map with fitted values
    for (int ipt=0 ; ipt<m_JER_fit->GetNbinsX() ; ++ipt) {
      m_JER_fits->GetReplica(itoy)->SetBinContent(ipt+1,etabin,resolFitter->getFit()->Eval(m_JER_fit->GetXaxis()->GetBinCenter(ipt+1)));
    }
  }
  // nominal
  // make TGraph of JER(pT) for fit
  TH1F* JER_map = (TH1F*)JER_maps->GetNominal();
  TGraphAsymmErrors* data = HistUtils::makeGraph(Form("%sJER(pT)[eta%i]",m_name.c_str(),etabin),npts,pT_map,JER_map);
  // perform fit
  resolFitter->fitResolution(data,pTmin,pTmax);
  // fill JER(pT,eta) map with fitted values
  for (int ipt=0 ; ipt<m_JER_fit->GetNbinsX() ; ++ipt) {
    m_JER_fits->GetNominal()->SetBinContent(ipt+1,etabin,resolFitter->getFit()->Eval(m_JER_fit->GetXaxis()->GetBinCenter(ipt+1)));
  }
}

void NoiseStochConst::writeResults(TDirectory* outDir)
{
  if (outDir) outDir->cd();
  if (m_bootstrap) {
    m_JER_fits->SetValBootstrapMean();
    m_JER_fits->SetErrBootstrapRMS();
    m_JER_fits->Write(Form("%sJER_fits",m_name.c_str()),TObject::kOverwrite);
    m_JER_fit = (TH2F*)m_JER_fits->GetNominal();
    m_JER_fit->Write(Form("%sJER_fit",m_name.c_str()),TObject::kOverwrite);
  } else {
    m_JER_fit->Write(Form("%sJER_fit",m_name.c_str()),TObject::kOverwrite);
  }
}
