#include "TDirectory.h"
#include "TFile.h"

#include "Utils/Utils.h"
#include "Utils/Config.h"

#include "MeasureResolution/Measurement.h"

int main(int argc, char** argv) {

  // suppress warnings
  gErrorIgnoreLevel = kWarning;

  // parse arguments
  std::string jetAlgo               = argv[1];
  std::vector<std::string> systVar  = vectorizeStr(argv[2],":");
  std::vector<std::string> cfgPaths = vectorizeStr(argv[3],",");
  std::string inName                = argv[4];
  std::string inFile                = argv[5];
  std::string outName               = argv[6];
  std::string outPath               = argv[7];

  // config files
  Config settings = Config();
  for (const auto& cfg : cfgPaths) {
    settings.addFile(cfg);
  }
  if (systVar.size()) {
    settings.setStr("Systematic",systVar[0]);
    settings.setStr(systVar[0],systVar[1]);
  }
  settings.addFile(inFile);

  // input file -> folder
  TFile* dataFile        = openInputFile(settings.getStr("File."+settings.getStr("Reconstructed")));
  TFile* mcFile          = openInputFile(settings.getStr("File."+settings.getStr("ParticleLevel")));
  TDirectory* dataFolder = dataFile->GetDirectory(inName.c_str());
  TDirectory* mcFolder   = mcFile->GetDirectory(inName.c_str());

  // run JER measurement
  Resolution::Measurement* meas = new Resolution::Measurement(jetAlgo,settings);
  meas->insitu(dataFolder,mcFolder);
  meas->simulation(mcFolder);

  // write to output file
  TFile* outFile = openOutputFile(outPath,false);
  meas->writeResults(outName,outFile);
  outFile->Close();
}



