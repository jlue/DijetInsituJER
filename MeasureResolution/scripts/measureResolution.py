#!/usr/bin/env python

from MeasureResolution.jetSettings import jetAlgos,treeSysts,histSysts,fitSysts,systVars
from MeasureResolution.runLocal import runLocal
from MeasureResolution.submitJob import submitJob

import os

# argument parser
import argparse
parser = argparse.ArgumentParser(description='Resolution measurement input files, config, and output')
# jet algorithms to run over
parser.add_argument('--jets', dest='jets', type=str, default='pflow',
    help='jet algo(s) to run over')
parser.add_argument('--files', dest='files', nargs='*',
    help='text file containing input file paths')
parser.add_argument('--outDir', dest='outDir', type=str, default='./',
    help='output directory')
parser.add_argument('--force', dest='force', action='store_true',
    help='output directory')
parser.add_argument('--data', dest='data', action='store_true')
parser.add_argument('--mc',   dest='mc',   action='store_true')
parser.add_argument('--submit', action='store_true',
    help='submit jobs')
parser.add_argument('--systs', action='store_true', default=False,
    help='run over systematics')
parser.add_argument('--systsOnly', dest='systsOnly', action='store_true', default=False,
    help='run over systematics only')
args = parser.parse_args()

def makeOutputName(jetAlgo,syst='',var=''):
    if (syst and var):
        return jetAlgo+"_"+syst+"__"+var
    elif (not syst and not var):
        return jetAlgo
    else:
        raise Exception('Failed to parse job name for (systematic: {}, variation: {}).'.format(syst,var))

def makeArguments(jetAlgo,syst='',var='',configs=[],inName='',inFiles=[],outName='',outDir='./',force=False):
    configs = ','.join(configs)
    inFiles = ','.join([os.path.abspath(file) for file in inFiles])
    outPath = os.path.join(os.path.abspath(outDir),outName+'.root')
    if force:
        return [jetAlgo,syst+":"+var,configs,inName,inFiles,outName,outPath,'force']
    else:
        return [jetAlgo,syst+":"+var,configs,inName,inFiles,outName,outPath]

# run/submit C++ program
if __name__ == "__main__":

    # configs
    configs = ["MeasureResolution/pT_eta_rebins.cfg","JetResponseFitter/asym_fit_settings.cfg","JetResponseFitter/resp_fit_settings.cfg"]
    if args.data:
        configs.append("MeasureResolution/data_config.cfg")
    elif args.mc:
        configs.append("MeasureResolution/mc_config.cfg")

    # input files
    files = ["MeasureResolution/files.cfg"]
    if args.files:
        files = args.files

    # command to run
    jobExecutable = '{}/bin/measureResolution'.format(os.environ['DijetInsituJER_DIR']) 
    # configs
    configs = ['{}/data/{}'.format(os.environ['DijetInsituJER_DIR'],cfg) for cfg in configs]
    # input files
    files = ['{}/data/{}'.format(os.environ['DijetInsituJER_DIR'],file) for file in files]
    # check output directory exists
    outDir = os.path.abspath(args.outDir)
    if not os.path.isdir(outDir):
        os.makedirs(outDir)

    # run over jets
    for jetAlgo in jetAlgos[args.jets]:

        # nominal
        if args.systsOnly:
            pass
        else:
            jobName = 'MeasureResolution_'+makeOutputName(jetAlgo)
            jobArgs = makeArguments(jetAlgo,'','',configs,jetAlgo,files,jetAlgo,args.outDir,args.force)
            submitJob(jobName,jobExecutable,jobArgs,jobTime=60) if args.submit else runLocal(jobName,jobExecutable,jobArgs)

        # systematics
        if not args.systs and not args.systsOnly:
            continue

        # existing systematics (tree)
        for syst in treeSysts[jetAlgo]:
            for var in systVars[syst]:
                jobName = 'MeasureResolution_'+makeOutputName(jetAlgo,syst,var)
                jobArgs = makeArguments(jetAlgo,syst,var,configs,makeOutputName(jetAlgo,syst,var),files,makeOutputName(jetAlgo,syst,var),args.outDir,args.force)
                submitJob(jobName,jobExecutable,jobArgs,jobTime=60) if args.submit else runLocal(jobName,jobExecutable,jobArgs)
        # existing systematics (hist)
        for syst in histSysts[jetAlgo]:
            for var in systVars[syst]:
                jobName = 'MeasureResolution_'+makeOutputName(jetAlgo,syst,var)
                jobArgs = makeArguments(jetAlgo,syst,var,configs,makeOutputName(jetAlgo,syst,var),files,makeOutputName(jetAlgo,syst,var),args.outDir,args.force)
                submitJob(jobName,jobExecutable,jobArgs,jobTime=60) if args.submit else runLocal(jobName,jobExecutable,jobArgs)
        # new systematics (fit)
        for syst in fitSysts[jetAlgo]:
            for var in systVars[syst]:
                jobName = 'MeasureResolution_'+makeOutputName(jetAlgo,syst,var)
                jobArgs = makeArguments(jetAlgo,syst,var,configs,jetAlgo,files,makeOutputName(jetAlgo,syst,var),args.outDir,args.force)
                submitJob(jobName,jobExecutable,jobArgs,jobTime=60) if args.submit else runLocal(jobName,jobExecutable,jobArgs)

