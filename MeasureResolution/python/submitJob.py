#!/usr/bin/env python

from collections import OrderedDict

import os
import subprocess
import time

def submitJob(jobName,jobExecutable,jobArgs,jobTime=60):
    # check for condor (lxplus)
    p = subprocess.Popen("which condor_submit".split(), stdin=subprocess.PIPE,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate("")
    if not p.returncode:
        jobFlavourTime=OrderedDict(
            [("espresso", 20),("microcentry", 60),("longlunch", 120),("workday",480)]
        )
        jobFlavour='espresso'
        for flav in jobFlavourTime:
            if jobTime >= jobFlavourTime[flav]:
                jobFlavour = flav
        submitCondor(jobName,jobExecutable,jobArgs,jobFlavour)
    # check for slurm (cedar)
    p = subprocess.Popen("which sbatch".split(), stdin=subprocess.PIPE,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate("")
    if not p.returncode:
        submitSlurm(jobName,jobExecutable,jobArgs,jobTime)

# submit condor job
def submitCondor(jobName,jobExecutable,jobArgs,jobFlavour):
    logDir = 'logs'
    if not os.path.exists(logDir):
        os.mkdir(logDir)
    submitScript = jobName + ".submit"
    with open(submitScript, 'w') as f:
        f.write('universe             = vanilla\n')
        f.write('executable           = {}\n'.format(jobExecutable))
        f.write('arguments            = {}\n'.format(' '.join(jobArgs)))
        f.write('getenv               = True\n')
        f.write('transfer_input_files = \n')
        f.write('batch_name           = {}.\n'.format(jobName))
        f.write('output               = '+logDir+'/{}.$(ClusterId).out\n'.format(jobName))
        f.write('error                = '+logDir+'/{}.$(ClusterId).err\n'.format(jobName))
        f.write('log                  = '+logDir+'/{}.$(ClusterId).log\n'.format(jobName))
        f.write('stream_output        = True\n')
        f.write('stream_error         = True\n')
        f.write('request_cpus         = 1\n')
        f.write('request_disk         = 5000\n')
        f.write('request_memory       = 2000\n')
        f.write('+JobFlavour          = {}\n'.format(jobFlavour))
        f.write('queue\n')
        f.close()

    # submit job
    print("Job: "+jobName)
    os.system('condor_submit ' + submitScript)

    # delete submission script
    os.remove(submitScript)
    # space out submissions by 1 second
    time.sleep(1)

#!/usr/bin/env python

# submit sbatch job
def submitSlurm(jobName,jobExecutable,jobArgs,jobTime):
    logDir = 'logs'
    if not os.path.exists(logDir):
        os.mkdir(logDir)
    
    # make job script
    jobScript = '{}.sh'.format(jobName)
    with open(jobScript, 'w') as f:
        f.write('#!/bin/bash\n')
        f.write('cd {}/../\n'.format(os.environ['DijetInsituJER_DIR']))
        f.write('asetup --restore\n'.format(os.environ['DijetInsituJER_DIR']))
        f.write('source {}/setup.sh\n'.format(os.environ['DijetInsituJER_DIR']))
        f.write('{} {}'.format(jobExecutable,' '.join(jobArgs)))

    # make batch script
    submitScript = jobName + ".submit"
    os.system('batchScript "source {}" -o {}'.format(jobScript,submitScript))

    # call batch script with settings
    submitCmdAndArgs = ['/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/wrappers/containers/slurm/sbatch']
    hour, minute = divmod(jobTime,60)
    submitCmdAndArgs.append('--time={}:{}:00'.format(hour,minute))
    submitCmdAndArgs.append('--cpus-per-task=1')
    submitCmdAndArgs.append('--mem=2000M')
    submitCmdAndArgs.append('--export=NONE')
    submitCmdAndArgs.append('--job-name={}'.format(jobName))
    submitCmdAndArgs.append('--output={}/%x.out'.format(logDir))
    submitCmdAndArgs.append(submitScript)

    # submit job
    print("Job: "+jobName)
    os.system(' '.join(submitCmdAndArgs))

    # delete submission script
    os.remove(submitScript)
    # space out submissions by 1 second
    time.sleep(1)
