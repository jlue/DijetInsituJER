# run locally
def runLocal(name,cmd,args,bkg=False):
	import os
	import subprocess
	logDir = 'logs'
	if not os.path.exists(logDir):
		os.mkdir(logDir)
	cmd_and_args = [cmd] + args
	if bkg:
		subprocess.Popen(cmd_and_args,stdout=open(logDir+'/'+name+'.out', 'w'),stderr=open(logDir+'/'+name+'.err','w'))
	else:
		proc = subprocess.Popen(cmd_and_args)
		subprocess.Popen.communicate(proc)