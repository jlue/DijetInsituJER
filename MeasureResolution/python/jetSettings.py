# jet algorithms
jetAlgos = {
  "smallR": ['AntiKt4EMPFlow','AntiKt4EMTopo'],
  "largeR": ['AntiKt10LCTopoTrimmedPtFrac5SmallR20'],
  "Rscan":  ['AntiKt2LCTopo','AntiKt6LCTopo'],
  "pflow":  ['AntiKt4EMPFlow'],
  "topo":  ['AntiKt4EMTopo'],
  "R02scan":  ['AntiKt2LCTopo'],
  "R06scan":  ['AntiKt6LCTopo']
}

# existing systematics in tree
treeSysts = {
    'AntiKt2LCTopo': [
    'JET_EtaIntercalibration_NonClosure_highE',
	'JET_EtaIntercalibration_NonClosure_negEta',
	'JET_EtaIntercalibration_NonClosure_posEta',
    ],
    'AntiKt6LCTopo': [
    'JET_EtaIntercalibration_NonClosure_highE',
	'JET_EtaIntercalibration_NonClosure_negEta',
	'JET_EtaIntercalibration_NonClosure_posEta',
    ],
    'AntiKt4EMPFlow': [
        'JET_EffectiveNP_R4_1',
        'JET_EffectiveNP_R4_2',
        'JET_EffectiveNP_R4_3',
        'JET_EffectiveNP_R4_4',
        'JET_EffectiveNP_R4_5',
        'JET_EffectiveNP_R4_6',
        'JET_EffectiveNP_R4_7',
        'JET_EffectiveNP_R4_8restTerm'
    ],
    'AntiKt10LCTopoTrimmedPtFrac5SmallR20': [
        'JET_EffectiveNP_R10_1',
        'JET_EffectiveNP_R10_2',
        'JET_EffectiveNP_R10_3',
        'JET_EffectiveNP_R10_4',
        'JET_EffectiveNP_R10_5',
        'JET_EffectiveNP_R10_6restTerm',
        'JET_EtaIntercalibration_Modelling',
        'JET_EtaIntercalibration_R10_TotalStat',
        'JET_Flavor_Composition',
        'JET_Flavor_Response',
        'JET_SingleParticle_HighPt'
    ]
}

# systematics to be added histogram-making step
histSysts = { 
    'AntiKt2LCTopo': [
        'DijetTopologySelection'
    ],
    'AntiKt6LCTopo': [
        'DijetTopologySelection'
    ],
    'AntiKt4EMPFlow': [
        'DijetTopologySelection',
        # 'PileupBin'
    ],
    'AntiKt10LCTopoTrimmedPtFrac5SmallR20': [
        'DijetTopologySelection',
        # 'PileupBin'
    ]
}

# systematics to be added during fitting
fitSysts = { 
    'AntiKt2LCTopo': [
        'ParticleLevel'
    ],
    'AntiKt6LCTopo': [
        'ParticleLevel'
    ],
    'AntiKt4EMPFlow': [
        'ParticleLevel'
    ],
    'AntiKt10LCTopoTrimmedPtFrac5SmallR20': [
        'ParticleLevel'
    ]
}

systVars = {
    # smallR JES
    'JET_EffectiveNP_R4_1':         ['1up'],
    'JET_EffectiveNP_R4_2':         ['1up'],
    'JET_EffectiveNP_R4_3':         ['1up'],
    'JET_EffectiveNP_R4_4':         ['1up'],
    'JET_EffectiveNP_R4_5':         ['1up'],
    'JET_EffectiveNP_R4_6':         ['1up'],
    'JET_EffectiveNP_R4_7':         ['1up'],
    'JET_EffectiveNP_R4_8restTerm': ['1up'],
    # largeR JES
    'JET_EffectiveNP_R10_1':                 ['1up'],
    'JET_EffectiveNP_R10_2':                 ['1up'],
    'JET_EffectiveNP_R10_3':                 ['1up'],
    'JET_EffectiveNP_R10_4':                 ['1up'],
    'JET_EffectiveNP_R10_5':                 ['1up'],
    'JET_EffectiveNP_R10_6restTerm':         ['1up'],
    'JET_EtaIntercalibration_Modelling':     ['1up'],
    'JET_EtaIntercalibration_R10_TotalStat': ['1up'],
    'JET_Flavor_Composition':                ['1up'],
    'JET_Flavor_Response':                   ['1up'],
    'JET_SingleParticle_HighPt':             ['1up'],
    # common systematics
    'DijetTopologySelection': ['Loose','Tight'],
    'PileupBin':              ['Low','Medium','High'],
    'ParticleLevel':          ['Sherpa']
}