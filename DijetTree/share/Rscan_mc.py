from DijetTree import mc_config as mc
jetRadius = "Rscan"
#jetAlgos = mc.jetAlgos[jetRadius]
jetAlgos = ['AntiKt6LCTopo']
jetAlgoRef = 'AntiKt4EMTopo'
systMode = "JES"
#systMode = "JES"
systVal = 0.0 if systMode=="nominal" else 1.0

# configure xAH algorithms
from xAODAnaHelpers import Config
c = Config()

c.algorithm("BasicEventSelection", {"m_name":                      "BasicEventSelection",
                                    "m_truthLevelOnly":            False,
                                    "m_doPUreweighting":           False,
                                    "m_applyCoreFlagsCut":         True,
                                    "m_applyEventCleaningCut":     True,
                                    "m_applyJetCleaningEventFlag": True,
                                    "m_vertexContainerName":       "PrimaryVertices",
                                    "m_applyPrimaryVertexCut":     True,
                                    "m_PVNTrack":                  2,
                                    "m_cutFlowStreamName":         "cutflow",
                                    "m_metaDataStreamName":        "metadata"
                                    })


c.algorithm("JetCalibrator", {"m_name":                    jetAlgoRef+"JetsCalibration",
			      "m_inContainerName":         jetAlgoRef+"Jets",
			      "m_outContainerName":        jetAlgoRef+"JetsCalib",
			      "m_jetAlgo":                 jetAlgoRef,
			      "m_calibConfigData":         mc.jetCalibConfig[jetAlgoRef],
			      "m_calibConfigFullSim":      mc.jetCalibConfig[jetAlgoRef],
			      "m_calibSequence":           mc.jetCalibSeq[jetAlgoRef],
			      "m_overrideCalibArea":       mc.jetCalibArea[jetAlgoRef],
			      "m_forceInsitu":             False,
			      "m_forceSmear":              False,
			      "m_doCleaning":              False,
			      "m_redoJVT":                 mc.doJVT[jetAlgoRef],
			      "m_calculatefJVT":           mc.doJVT[jetAlgoRef],
			      "m_uncertConfig":            mc.jetUncertConfig[jetAlgoRef],
			      "m_overrideUncertCalibArea": mc.jetUncertCalibArea[jetAlgoRef],
			      "m_systName":                ','.join(mc.jetSystematics[systMode][jetAlgoRef]),
			      "m_systVal":                 systVal,
			      "m_outputAlgo":              jetAlgoRef+"Calib"
			      })

c.algorithm("JetSelector", {"m_name":                    jetAlgoRef+"JetsSelection",
			    "m_inContainerName":         jetAlgoRef+"JetsCalib",
			    "m_outContainerName":        jetAlgoRef+"JetsCalibSel",
			    "m_createSelectedContainer": True,
			    "m_doMCCleaning":            True,
			    "m_mcCleaningCut":           1.5,
			    "m_cleanJets":               False,
			    "m_detEta_max":              mc.detEta_max[jetAlgoRef],
			    "m_doJVT":                   mc.doJVT[jetAlgoRef],
			    "m_dofJVT":                  mc.doJVT[jetAlgoRef],
			    "m_WorkingPointJVT":         mc.workingPointJVT[jetAlgoRef],
			    "m_WorkingPointfJVT":        mc.workingPointfJVT[jetAlgoRef],
			    "m_inputAlgo":               jetAlgoRef+"Calib",
			    "m_outputAlgo":              jetAlgoRef+"CalibSel"
			    })
# Rscan 
for jetAlgo in jetAlgos:
  c.algorithm("JetCalibrator", {"m_name":                    jetAlgo+"JetsCalibration",
                                "m_inContainerName":         jetAlgo+"Jets",
                                "m_outContainerName":        jetAlgo+"JetsCalib",
                                "m_jetAlgo":                 jetAlgo,
                                "m_calibConfigData":         mc.jetCalibConfig[jetAlgo],
                                "m_calibConfigFullSim":      mc.jetCalibConfig[jetAlgo],
                                "m_calibSequence":           mc.jetCalibSeq[jetAlgo],
                                "m_overrideCalibArea":       mc.jetCalibArea[jetAlgo],
                                "m_forceInsitu":             False,
                                "m_forceSmear":              False,
                                "m_doCleaning":              False,
                                "m_redoJVT":                 mc.doJVT[jetAlgo],
                                "m_calculatefJVT":           mc.dofJVT[jetAlgo],
                                "m_uncertConfig":            mc.jetUncertConfig[jetAlgo],
                                'm_uncertMCType':            'MC16',
                                # 'm_overrideUncertPath':              '/afs/cern.ch/work/f/fnapolit/private/simpleJER/DijetInsituJER/DijetTree/share',
				#'m_overrideUncertPath':              '/afs/cern.ch/work/f/fdelrio/public/RScan/rscan/DijetInsituJER/share4DijetTree',
                                #'m_overrideUncertPath':              '/srv/workDir/usr/WorkDir/21.2.101/InstallArea/x86_64-centos7-gcc8-opt/share/JetUncertainties',
				"m_overrideUncertPath":     "DijetTree/JetUncertainties",
			        #'m_overrideUncertPath':              '/eos/atlas/atlascerngroupdisk/perf-jets/JESJER/Recommendations',
                                "m_overrideUncertCalibArea": mc.jetUncertCalibArea[jetAlgo],
                                "m_systName":                ','.join(mc.jetSystematics[systMode][jetAlgo]),
                                "m_systVal":                 systVal,
                                "m_outputAlgo":              jetAlgo+"Calib"
                                })

  c.algorithm("JetSelector", {"m_name":                    jetAlgo+"JetsSelection",
                              "m_inContainerName":         jetAlgo+"JetsCalib",
                              "m_outContainerName":        jetAlgo+"JetsCalibSel",
                              "m_createSelectedContainer": True,
                              "m_doMCCleaning":            True,
                              "m_mcCleaningCut":           1.5,
                              "m_cleanJets":               False,
                              "m_detEta_max":              mc.detEta_max[jetAlgo],
                              "m_doJVT":                   mc.doJVT[jetAlgo],
                              "m_dofJVT":                  mc.doJVT[jetAlgo],
                              "m_WorkingPointJVT":         mc.workingPointJVT[jetAlgo],
                              "m_WorkingPointfJVT":        mc.workingPointfJVT[jetAlgo],
                              "m_inputAlgo":               jetAlgo+"Calib",
                              "m_outputAlgo":              jetAlgo+"CalibSel"
                              })
  #matching
  c.algorithm("RscanJetMatcher", {"m_name":               jetAlgo+"JetsRscanMatch",
                                  "m_RrefContainerName":  "AntiKt4EMTopoJetsCalibSel",
                                  "m_RscanContainerName": jetAlgo+"JetsCalibSel",
                                  "m_matchDeltaRCut":     0.2,
                                  "m_inputAlgo":          jetAlgo+"CalibSel"
                                  })

  c.algorithm("TruthJetMatcher", {"m_name":               jetAlgo+"JetsTruthMatch",
                                  "m_recoContainerName":  jetAlgo+"JetsCalibSel",
                                  "m_truthContainerName": mc.truthJetAlgo[jetAlgo]+"Jets",
                                  "m_truth_pT_min":       10.0,
                                  "m_doDeltaR":           True,
                                  "m_deltaRCut":          mc.deltaRCut[jetAlgo],
                                  "m_doGhostAssoc":       mc.doGhostAssoc[jetAlgo],
                                  "m_ghostAssocFracCut":  0.75,
                                  "m_inputAlgo":          jetAlgo+"CalibSel"
                                  })

  c.algorithm("DijetTreeAlgo", {"m_name":                  jetAlgo+"JetsDijetTree",
                                      "m_recoJetContainerName":  jetAlgo+"JetsCalibSel",
                                      "m_truthJetContainerName": mc.truthJetAlgo[jetAlgo]+"Jets",
                                      "m_jetAlgo":               jetAlgo,
                                      "m_eventDetailStr":        "pileup",
                                      # "m_vertexDetailStr":       "primary",
                                      "m_vertexDetailStr":       "",
                                      # "m_truthVertexDetailStr":  "primary",
                                      "m_truthVertexDetailStr":  "",
                                      "m_trigDetailStr":         "",
                                      "m_recoJetDetailStr":      "leadingDijet thirdJet truthMatch Rscan",
                                      "m_truthJetDetailStr":     "leadingJet",
                                      "m_jetSystematics":        jetAlgo+"CalibSel"
                                      })
