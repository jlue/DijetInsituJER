from DijetTree import data_config as data
jetRadius = "Rscan"
jetAlgos = ["AntiKt6LCTopo"]#data.jetAlgos[jetRadius]
jetAlgoRef = 'AntiKt4EMTopo'
trigJetAlgos = data.trigJetAlgos[jetRadius]

# run xAH algorithm config
from xAODAnaHelpers import Config
c = Config()

c.algorithm("BasicEventSelection", {"m_name":                      "BasicEventSelection",
                                    "m_truthLevelOnly":            False,
                                    "m_doPUreweighting":           False,
                                    "m_applyGRLCut":               True,
                                    "m_applyCoreFlagsCut":         True,
                                    "m_applyEventCleaningCut":     True,
                                    "m_applyJetCleaningEventFlag": True,
                                    "m_applyTriggerCut":           True,
                                    "m_GRLxml":                    data.GRLs,
                                    "m_vertexContainerName":       "PrimaryVertices",
                                    "m_applyPrimaryVertexCut":     True,
                                    "m_PVNTrack":                  2,
                                    "m_triggerSelection":          data.allJetTriggers,
                                    "m_storeTrigDecisions":        True,
                                    "m_cutFlowStreamName":         "cutflow",
                                    "m_metaDataStreamName":        "metadata"
                                    })

for trigJetAlgo in trigJetAlgos:

  c.algorithm("JetTriggerEmulator", {"m_name":             trigJetAlgo+"JetTriggerEmulation",
                                     "m_trigJetAlgo":      trigJetAlgo,
                                     "m_emulatedTriggers": data.jetTriggers[trigJetAlgo],
                                     "m_trigThresholdETs": data.trigThresholdETs[trigJetAlgo],
                                     "m_trigRequiredEtas": data.trigRequiredEtas[trigJetAlgo],
                                     "m_emulatedSeeds":    data.L1seeds[trigJetAlgo],
                                     "m_seedThresholdETs": data.seedThresholdETs[trigJetAlgo],
                                     "m_seedRequiredEtas": data.seedRequiredEtas[trigJetAlgo],
                                     "m_checkTrigDecTool": False
                                      })


c.algorithm("JetCalibrator", {"m_name":                    jetAlgoRef+"JetsCalibration",
			      "m_inContainerName":         jetAlgoRef+"Jets",
			      "m_outContainerName":        jetAlgoRef+"JetsCalib",
			      "m_jetAlgo":                 jetAlgoRef,
			      "m_calibConfigData":         data.jetCalibConfig[jetAlgoRef],
			      "m_calibConfigFullSim":      data.jetCalibConfig[jetAlgoRef],
			      "m_calibSequence":           data.jetCalibSeq[jetAlgoRef],
			      "m_overrideCalibArea":       data.jetCalibArea[jetAlgoRef],
			      "m_forceInsitu":             False,  # in-situ JES *is* the measurement, only apply (manually) for in-situ JER!
			      "m_forceSmear":              False,  # in-situ JER *is* the measurement, only apply (manually) for final checks!
			      "m_doCleaning":              False,  # recommendation is to use event-level cleaning flags
			      "m_redoJVT":                 data.doJVT[jetAlgoRef],
			      "m_calculatefJVT":           data.dofJVT[jetAlgoRef],
			      "m_outputAlgo":              jetAlgoRef+"Calib"
			      })

c.algorithm("JetSelector", {"m_name":                    jetAlgoRef+"JetsSelection",
			    "m_inContainerName":         jetAlgoRef+"JetsCalib",
			    "m_outContainerName":        jetAlgoRef+"JetsCalibSel",
			    "m_createSelectedContainer": True,
			    "m_cleanJets":               False,
			    "m_detEta_max":              data.detEta_max[jetAlgoRef],
			    "m_doJVT":                   data.doJVT[jetAlgoRef],
			    "m_dofJVT":                  data.dofJVT[jetAlgoRef],
			    "m_WorkingPointJVT":         data.workingPointJVT[jetAlgoRef],
			    "m_WorkingPointfJVT":        data.workingPointfJVT[jetAlgoRef],
			    "m_inputAlgo":               jetAlgoRef+"Calib",
			    "m_outputAlgo":              jetAlgoRef+"CalibSel"
			    })

#c.algorithm("HLTJetGetter", {"m_name":                   "HLTJetGetter",
#                             "m_inContainerName":        "a4tcemsubjesFS",
#  			     "m_outContainerName":       "HLT_jets",
#			     "m_triggerList":            data.allJetTriggers
#			     })

##Rscan
for jetAlgo in jetAlgos:
  c.algorithm("JetCalibrator", {"m_name":                    jetAlgo+"JetsCalibration",
                                "m_inContainerName":         jetAlgo+"Jets",
                                "m_outContainerName":        jetAlgo+"JetsCalib",
                                "m_jetAlgo":                 jetAlgo,
                                "m_calibConfigData":         data.jetCalibConfig[jetAlgo],
                                "m_calibConfigFullSim":      data.jetCalibConfig[jetAlgo],
                                "m_calibSequence":           data.jetCalibSeq[jetAlgo],
                                "m_overrideCalibArea":       data.jetCalibArea[jetAlgo],
                                "m_forceInsitu":             False,  # in-situ JES *is* the measurement, only apply (manually) for in-situ JER!
                                "m_forceSmear":              False,  # in-situ JER *is* the measurement, only apply (manually) for final checks!
                                "m_doCleaning":              False,  # recommendation is to use event-level cleaning flags
                                "m_redoJVT":                 data.doJVT[jetAlgo],
                                "m_calculatefJVT":           data.dofJVT[jetAlgo],
                                "m_outputAlgo":              jetAlgo+"Calib"
                                })

  c.algorithm("JetSelector", {"m_name":                    jetAlgo+"JetsSelection",
                              "m_inContainerName":         jetAlgo+"JetsCalib",
                              "m_outContainerName":        jetAlgo+"JetsCalibSel",
                              "m_createSelectedContainer": True,
                              "m_cleanJets":               False,
                              "m_detEta_max":              data.detEta_max[jetAlgo],
                              "m_doJVT":                   data.doJVT[jetAlgo],
                              "m_dofJVT":                  data.dofJVT[jetAlgo],
                              "m_WorkingPointJVT":         "Medium",
                              "m_WorkingPointfJVT":        "Medium",
                              "m_inputAlgo":               jetAlgo+"Calib",
                              "m_outputAlgo":              jetAlgo+"CalibSel"
                              })

  #matching
  c.algorithm("RscanJetMatcher", {"m_name":               jetAlgo+"JetsRscanMatch",
                                  "m_RrefContainerName":  "AntiKt4EMTopoJetsCalibSel",
                                  "m_RscanContainerName": jetAlgo+"JetsCalibSel",
                                  "m_matchDeltaRCut":     0.2,
                                  "m_inputAlgo":          jetAlgo+"CalibSel"
                                  })


  c.algorithm("DijetTreeAlgo", {"m_name":                  jetAlgo+"JetsDijetTree",
                                "m_recoJetContainerName":  jetAlgo+"JetsCalibSel",
                                "m_jetAlgo":               jetAlgo,
				#"m_HLTJetContainerName" :  "HLT_xAOD__JetContainer_a4tcemsubjesFS",
                                "m_eventDetailStr":        "pileup",
                                # "m_vertexDetailStr":       "primary",
                                "m_vertexDetailStr":       "",
                                "m_truthVertexDetailStr":  "",
                                "m_trigDetailStr":         "passTriggers passTriggersBeforePrescale passTrigBits prescales",
                                # "m_recoJetDetailStr":      "thirdJet",
                                "m_recoJetDetailStr":      "leadingDijet thirdJet refJetMatch Rscan HLT",
                                "m_truthJetDetailStr":     "",
                                "m_jetSystematics":        jetAlgo+"CalibSel"
                                })
