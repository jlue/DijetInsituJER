from DijetTree import mc_config as mc
jetRadius = "largeR"
jetAlgos = mc.jetRadius[jetRadius]
systMode = "nominal"
systVal = 0.0 if systMode=="nominal" else 1.0

# configure xAH algorithms
from xAODAnaHelpers import Config
c = Config()

c.algorithm("BasicEventSelection", {"m_name":                      "BasicEventSelection",
                                    "m_truthLevelOnly":            False,
                                    "m_doPUreweighting":           False,
                                    "m_applyCoreFlagsCut":         True,
                                    "m_applyEventCleaningCut":     True,
                                    "m_applyJetCleaningEventFlag": True,
                                    "m_vertexContainerName":       "PrimaryVertices",
                                    "m_applyPrimaryVertexCut":     True,
                                    "m_PVNTrack":                  2,
                                    "m_cutFlowStreamName":         "cutflow",
                                    "m_metaDataStreamName":        "metadata"
                                    })

for jetAlgo in jetAlgos:

  c.algorithm("JetCalibrator", {"m_name":                    jetAlgo+"JetsCalibration",
                                "m_inContainerName":         jetAlgo+"Jets",
                                "m_outContainerName":        jetAlgo+"JetsCalib",
                                "m_jetAlgo":                 jetAlgo,
                                "m_calibConfigData":         mc.jetCalibConfig[jetAlgo],
                                "m_calibConfigFullSim":      mc.jetCalibConfig[jetAlgo],
                                "m_calibSequence":           mc.jetCalibSeq[jetAlgo],
                                "m_overrideCalibArea":       mc.jetCalibArea[jetAlgo],
                                "m_forceInsitu":             False,
                                "m_forceSmear":              False,
                                "m_doCleaning":              False,
                                "m_redoJVT":                 mc.doJVT[jetAlgo],
                                "m_calculatefJVT":           mc.dofJVT[jetAlgo],
                                "m_fJVTWorkingPoint":        mc.workingPointfJVT[jetAlgo],
                                "m_uncertConfig":            mc.jetUncertConfig[jetAlgo],
                                "m_overrideUncertCalibArea": mc.jetUncertCalibArea[jetAlgo],
                                "m_systName":                ','.join(mc.jetSystematics[systMode][jetAlgo]),
                                "m_systVal":                 systVal,
                                "m_outputAlgo":              jetAlgo+"Calib"
                                })
  
  c.algorithm("JetSelector", {"m_name":                    jetAlgo+"JetsSelection",
                              "m_inContainerName":         jetAlgo+"JetsCalib",
                              "m_outContainerName":        jetAlgo+"JetsCalibSel",
                              "m_createSelectedContainer": True,
                              "m_doMCCleaning":            True,
                              "m_mcCleaningCut":           1.5,
                              "m_cleanJets":               False,
                              "m_detEta_max":              mc.detEta_max[jetAlgo],
                              "m_doJVT":                   mc.doJVT[jetAlgo],
                              "m_dofJVT":                  mc.dofJVT[jetAlgo],
                              "m_WorkingPointJVT":         mc.workingPointJVT[jetAlgo],
                              "m_WorkingPointfJVT":        mc.workingPointfJVT[jetAlgo],
                              "m_inputAlgo":               jetAlgo+"Calib",
                              "m_outputAlgo":              jetAlgo+"CalibSel"
                              })

  c.algorithm("TruthJetMatcher", {"m_name":               jetAlgo+"JetsTruthMatch",
                                  "m_recoContainerName":  jetAlgo+"JetsCalibSel",
                                  "m_truthContainerName": mc.truthJetAlgo[jetAlgo]+"Jets",
                                  "m_truth_pT_min":       10.0,
                                  "m_doDeltaR":           True,
                                  "m_deltaRCut":          mc.deltaRCut[jetAlgo],
                                  "m_doGhostAssoc":       mc.doGhostAssoc[jetAlgo],
                                  "m_ghostAssocFracCut":  0.75,
                                  "m_inputAlgo":          jetAlgo+"CalibSel"
                                  })

  c.algorithm("DijetTreeAlgo", {"m_name":                  jetAlgo+"JetsDijetTree",
                                "m_recoJetContainerName":  jetAlgo+"JetsCalibSel",
                                "m_truthJetContainerName": mc.truthJetAlgo[jetAlgo]+"Jets",
                                "m_jetAlgo":               jetAlgo,
                                "m_eventDetailStr":        "pileup",
                                "m_vertexDetailStr":       "primary",
                                "m_truthVertexDetailStr":  "primary",
                                "m_trigDetailStr":         "",
                                "m_recoJetDetailStr":      "leadingDijet thirdJet truthMatch",
                                "m_truthJetDetailStr":     "leadingJet",
                                "m_jetSystematics":        jetAlgo+"CalibSel"
                                })
