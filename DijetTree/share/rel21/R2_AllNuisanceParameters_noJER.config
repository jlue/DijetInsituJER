##############################################################################
#
#  Set of NPs for Rscan jets AntiKt2LCTopo.
#
#  Stats for the insitu correction have been combined aready, otherwise there's
#  loads of components, given that it is a correction vs pt and eta. 
#
#  Including OOC component as a separate component, independent
# 
#  Treating JVT from ZJets Reference and JVT from ZJets Rscan as correlated, made group.
#
#  M.Toscani - October 15th, 2019
#
# 
##############################################################################

UncertaintyRelease:                 rel21_Rscan2019-2LC
SupportedJetDefs:                   AntiKt2LCTopo
SupportedMCTypes:                   MC16
UncertaintyRootFile:                rel21/Rscan2019/R2_AllComponents.root
AnalysisRootFile:                   analysisInputs/UnknownFlavourComp.root

FileValidHistogram:                 ValidRange
FileValidHistParam:                 PtEta

# <Mu> obtained from ATLAS public plots
# <NPV> from Eric's pileup studies
# Need to investigate this for Rscan? or is it the same?
# Note that Eric's studies find a MuRef of 22.8 instead.
Pileup.MuRef:                       24.2
Pileup.NPVRef:                      13.2

##############################################################################
# 
#   Settings for JES Components
#
##############################################################################

# gamma+jet: energy scale, Z->ee
JESComponent.1.Desc:                LAr energy scale - Zee component - Ref
JESComponent.1.Name:                Gjet_GamESZee
JESComponent.1.Type:                Mixed
JESComponent.1.Param:               Pt

# gamma+jet: energy smearing
JESComponent.2.Desc:                LAr energy smearing - Ref
JESComponent.2.Name:                Gjet_GamEsmear
JESComponent.2.Type:                Mixed
JESComponent.2.Param:               Pt
JESComponent.2.SubComp:             Gjet_GamEsmear

# gamma+jet: Generator
JESComponent.3.Name:                Gjet_Generator
JESComponent.3.Desc:                gamma+jet Monte Carlo generator difference - Ref
JESComponent.3.Type:                Modelling
JESComponent.3.Param:               Pt

# gamma+jet: JVT term
JESComponent.4.Name:                Gjet_Jvt
JESComponent.4.Desc:                LAr JVT - Ref
JESComponent.4.Type:                Modelling
JESComponent.4.Param:               Pt

# gamma+jet: Out-of-cone
JESComponent.5.Name:                Gjet_OOC
JESComponent.5.Desc:                gamma+jet out-of-cone radiation - Ref
JESComponent.5.Type:                Modelling
JESComponent.5.Param:               Pt

# gamma+jet: Purity
JESComponent.6.Name:                Gjet_Purity
JESComponent.6.Desc:                gamma+jet photon purity - Ref
JESComponent.6.Type:                Detector
JESComponent.6.Param:               Pt

# gamma+jet: Veto
JESComponent.7.Name:                Gjet_Veto
JESComponent.7.Desc:                gamma+jet radiation suppression (second jet veto) - Ref
JESComponent.7.Type:                Modelling
JESComponent.7.Param:               Pt

# gamma+jet: dPhi 
JESComponent.8.Name:                Gjet_dPhi
JESComponent.8.Desc:                gamma+jet dPhi - Ref
JESComponent.8.Type:                Modelling
JESComponent.8.Param:               Pt

# Z+jet: energy scale, Z->ee
JESComponent.9.Desc:                LAr energy scale - Zee component - Ref
JESComponent.9.Name:                Zjet_ElecESZee
JESComponent.9.Type:                Mixed
JESComponent.9.Param:               Pt

# Z+jet: energy smearing
JESComponent.10.Name:               Zjet_ElecEsmear
JESComponent.10.Desc:		    Zjet_ElecEsmear - Ref
JESComponent.10.Type:               Mixed
JESComponent.10.Param:              Pt
JESComponent.10.SubComp:            Zjet_ElecEsmear

# Z+jet: k-term
JESComponent.12.Name:               Zjet_KTerm
JESComponent.12.Desc:               Z+jet out-of-cone radiation - Ref
JESComponent.12.Type:               Modelling
JESComponent.12.Param:              Pt

# Z+jet: MC
JESComponent.13.Name:               Zjet_MC
JESComponent.13.Desc:               Z+jet Monte Carlo generator difference - Ref
JESComponent.13.Type:               Modelling
JESComponent.13.Param:              Pt

# Z+jet: muon scale
JESComponent.14.Name:               Zjet_MuScale
JESComponent.14.Desc:               Z+jet muon scale - Ref
JESComponent.14.Type:               Detector
JESComponent.14.Param:              Pt

# Z+jet: muon smearing, inner detector
JESComponent.15.Name:               Zjet_MuSmearID
JESComponent.15.Desc:               Z+jet muon smearing (inner detector) - Ref
JESComponent.15.Type:               Modelling
JESComponent.15.Param:              Pt

# Z+jet: muon smearing, muon spectrometer
JESComponent.16.Name:               Zjet_MuSmearMS
JESComponent.16.Desc:               Z+jet muon smearing (muon spectrometer) - Ref
JESComponent.16.Type:               Modelling
JESComponent.16.Param:              Pt

# Z+jet: Veto
JESComponent.17.Name:               Zjet_Veto
JESComponent.17.Desc:               Z+jet radiation suppression (second jet veto) - Ref
JESComponent.17.Type:               Modelling
JESComponent.17.Param:              Pt

# Z+jet: dPhi
JESComponent.18.Name:               Zjet_dPhi
JESComponent.18.Desc:               Z+jet dPhi cut - Ref
JESComponent.18.Type:               Modelling
JESComponent.18.Param:              Pt

##############################################################################
# 
#   Statistical components, identified by source
#
##############################################################################

# Z+jet statistical component No. 1
JESComponent.20.Name:               Zjet_Stat1
JESComponent.20.Desc:               Z+jet statistical uncertainty No. 1 - Ref
JESComponent.20.Type:               Statistical
JESComponent.20.Param:              Pt

# Z+jet statistical component No. 2
JESComponent.21.Name:               Zjet_Stat2
JESComponent.21.Desc:               Z+jet statistical uncertainty No. 2 - Ref
JESComponent.21.Type:               Statistical
JESComponent.21.Param:              Pt

# Z+jet statistical component No. 3
JESComponent.22.Name:               Zjet_Stat3
JESComponent.22.Desc:               Z+jet statistical uncertainty No. 3 - Ref
JESComponent.22.Type:               Statistical
JESComponent.22.Param:              Pt

# Z+jet statistical component No. 4
JESComponent.23.Name:               Zjet_Stat4
JESComponent.23.Desc:               Z+jet statistical uncertainty No. 4 - Ref
JESComponent.23.Type:               Statistical
JESComponent.23.Param:              Pt

# Z+jet statistical component No. 5
JESComponent.24.Name:               Zjet_Stat5
JESComponent.24.Desc:               Z+jet statistical uncertainty No. 5 - Ref
JESComponent.24.Type:               Statistical
JESComponent.24.Param:              Pt
                                                                             
# Z+jet statistical component No. 6                                         
JESComponent.25.Name:               Zjet_Stat6
JESComponent.25.Desc:               Z+jet statistical uncertainty No. 6 - Ref
JESComponent.25.Type:               Statistical
JESComponent.25.Param:              Pt
                                                                             
# Z+jet statistical component No. 7                                         
JESComponent.26.Name:               Zjet_Stat7
JESComponent.26.Desc:               Z+jet statistical uncertainty No. 7 - Ref
JESComponent.26.Type:               Statistical
JESComponent.26.Param:              Pt
                                                                             
# Z+jet statistical component No. 8                                         
JESComponent.27.Name:               Zjet_Stat8
JESComponent.27.Desc:               Z+jet statistical uncertainty No. 8 - Ref
JESComponent.27.Type:               Statistical
JESComponent.27.Param:              Pt
                                                                             
# Z+jet statistical component No. 9                                        
JESComponent.28.Name:               Zjet_Stat9
JESComponent.28.Desc:               Z+jet statistical uncertainty No. 9 - Ref
JESComponent.28.Type:               Statistical
JESComponent.28.Param:              Pt
                                                                             
# Z+jet statistical component No. 10
JESComponent.30.Name:               Zjet_Stat10
JESComponent.30.Desc:               Z+jet statistical uncertainty No. 10 - Ref
JESComponent.30.Type:               Statistical
JESComponent.30.Param:              Pt
                                                                             
# Z+jet statistical component No. 11
JESComponent.31.Name:               Zjet_Stat11
JESComponent.31.Desc:               Z+jet statistical uncertainty No. 11 - Ref
JESComponent.31.Type:               Statistical
JESComponent.31.Param:              Pt
                                                                             
# Z+jet statistical component No. 12
JESComponent.32.Name:               Zjet_Stat12
JESComponent.32.Desc:               Z+jet statistical uncertainty No. 12 - Ref
JESComponent.32.Type:               Statistical
JESComponent.32.Param:              Pt
                                                                             
# Z+jet statistical component No. 13
JESComponent.33.Name:               Zjet_Stat13
JESComponent.33.Desc:               Z+jet statistical uncertainty No. 13 - Ref
JESComponent.33.Type:               Statistical
JESComponent.33.Param:              Pt
                                                                             
# gamma+jet statistical component No. 1
JESComponent.34.Name:               Gjet_Stat1
JESComponent.34.Desc:               gamma+jet statistical uncertainty No. 1 - Ref
JESComponent.34.Type:               Statistical
JESComponent.34.Param:              Pt
                                                                             
# gamma+jet statistical component No. 2
JESComponent.35.Name:               Gjet_Stat2
JESComponent.35.Desc:               gamma+jet statistical uncertainty No. 2 - Ref
JESComponent.35.Type:               Statistical
JESComponent.35.Param:              Pt
                                                                             
# gamma+jet statistical component No. 3
JESComponent.36.Name:               Gjet_Stat3
JESComponent.36.Desc:               gamma+jet statistical uncertainty No. 3 - Ref
JESComponent.36.Type:               Statistical
JESComponent.36.Param:              Pt
                                                                             
# gamma+jet statistical component No. 4
JESComponent.37.Name:               Gjet_Stat4
JESComponent.37.Desc:               gamma+jet statistical uncertainty No. 4 - Ref
JESComponent.37.Type:               Statistical
JESComponent.37.Param:              Pt
                                                                             
# gamma+jet statistical component No. 5
JESComponent.38.Name:               Gjet_Stat5
JESComponent.38.Desc:               gamma+jet statistical uncertainty No. 5 - Ref
JESComponent.38.Type:               Statistical
JESComponent.38.Param:              Pt
                                                                             
# gamma+jet statistical component No. 6
JESComponent.39.Name:               Gjet_Stat6
JESComponent.39.Desc:               gamma+jet statistical uncertainty No. 6 - Ref
JESComponent.39.Type:               Statistical
JESComponent.39.Param:              Pt
                                                                             
# gamma+jet statistical component No. 7
JESComponent.40.Name:               Gjet_Stat7
JESComponent.40.Desc:               gamma+jet statistical uncertainty No. 7 - Ref
JESComponent.40.Type:               Statistical
JESComponent.40.Param:              Pt
                                                                             
# gamma+jet statistical component No. 8
JESComponent.41.Name:               Gjet_Stat8
JESComponent.41.Desc:               gamma+jet statistical uncertainty No. 8 - Ref
JESComponent.41.Type:               Statistical
JESComponent.41.Param:              Pt
                                                                             
# gamma+jet statistical component No. 9
JESComponent.42.Name:               Gjet_Stat9
JESComponent.42.Desc:               gamma+jet statistical uncertainty No. 9 - Ref
JESComponent.42.Type:               Statistical
JESComponent.42.Param:              Pt
                                                                             
# gamma+jet statistical component No. 10
JESComponent.43.Name:               Gjet_Stat10
JESComponent.43.Desc:               gamma+jet statistical uncertainty No. 10 - Ref
JESComponent.43.Type:               Statistical
JESComponent.43.Param:              Pt
                                                                             
# gamma+jet statistical component No. 11
JESComponent.44.Name:              Gjet_Stat11
JESComponent.44.Desc:              gamma+jet statistical uncertainty No. 11 - Ref
JESComponent.44.Type:              Statistical
JESComponent.44.Param:             Pt
                                                                             
# gamma+jet statistical component No. 12
JESComponent.45.Name:              Gjet_Stat12
JESComponent.45.Desc:              gamma+jet statistical uncertainty No. 12 - Ref
JESComponent.45.Type:              Statistical
JESComponent.45.Param:             Pt
                                                                             
# gamma+jet statistical component No. 13
JESComponent.46.Name:              Gjet_Stat13
JESComponent.46.Desc:              gamma+jet statistical uncertainty No. 13 - Ref
JESComponent.46.Type:              Statistical
JESComponent.46.Param:             Pt
                                                                             
# gamma+jet statistical component No. 14
JESComponent.47.Name:              Gjet_Stat14
JESComponent.47.Desc:              gamma+jet statistical uncertainty No. 14 - Ref
JESComponent.47.Type:              Statistical
JESComponent.47.Param:             Pt

##############################################################################
# 
#   Special components
#
##############################################################################

# Eta intercalibration: theory uncertainty
JESComponent.76.Name:               EtaIntercalibration_Modelling
JESComponent.76.Desc:               Eta intercalibration: MC generator modelling uncertainty - Ref
JESComponent.76.Type:               Modelling
JESComponent.76.Param:              PtEta
JESComponent.76.Hists:              EtaIntercalibration_Modelling
JESComponent.76.Reducible:          False

# Eta intercalibration: total statistical and method uncertainty
JESComponent.78.Name:               EtaIntercalibration_TotalStat
JESComponent.78.Desc:               Eta intercalibration: statistical uncertainty - Ref
JESComponent.78.Type:               Statistical
JESComponent.78.Param:              PtEta
JESComponent.78.Hists:              EtaIntercalibration_TotalStat
JESComponent.78.Reducible:          False

# Eta intercalibration non-closure uncertainty
JESComponent.80.Name:               EtaIntercalibration_NonClosure_highE
JESComponent.80.Desc:               Eta intercalibration: non-closure uncertainty -Ref
JESComponent.80.Type:               Other
JESComponent.80.Param:              PtEta
JESComponent.80.Interp:             False
JESComponent.80.Hists:              EtaIntercalibration_NonClosure_highE
JESComponent.80.Reducible:          False

# Eta intercalibration non-closure uncertainty
JESComponent.98.Name:               EtaIntercalibration_NonClosure_posEta
JESComponent.98.Desc:               Eta intercalibration: non-closure uncertainty - Ref
JESComponent.98.Type:               Other
JESComponent.98.Param:              PtEta
JESComponent.98.Interp:             False
JESComponent.98.Hists:              EtaIntercalibration_NonClosure_posEta
JESComponent.98.Reducible:          False

# Eta intercalibration non-closure uncertainty
JESComponent.99.Name:               EtaIntercalibration_NonClosure_negEta
JESComponent.99.Desc:               Eta intercalibration: non-closure uncertainty - Ref
JESComponent.99.Type:               Other
JESComponent.99.Param:              PtEta
JESComponent.99.Interp:             False
JESComponent.99.Hists:              EtaIntercalibration_NonClosure_negEta
JESComponent.99.Reducible:          False

# High pT term
JESComponent.82.Name:               SingleParticle_HighPt
JESComponent.82.Desc:               High pT term (2012 version) - Ref
JESComponent.82.Type:               Detector
JESComponent.82.Param:              Pt
JESComponent.82.Reducible:          False

# Pileup: Original mu term
JESComponent.84.Name:               Pileup_OffsetMu
JESComponent.84.Desc:               Pileup: Offset, mu term, independent (derived for Rscan)
JESComponent.84.Type:               Other
JESComponent.84.Param:              PtEta
JESComponent.84.Special:            True
JESComponent.84.Hists:              Pileup_OffsetMu
JESComponent.84.Reducible:          False

# Pileup: Original NPV term
JESComponent.86.Name:               Pileup_OffsetNPV
JESComponent.86.Desc:               Pileup: Offset, NPV term, independent (derived for Rscan)
JESComponent.86.Type:               Other
JESComponent.86.Param:              PtEta
JESComponent.86.Special:            True
JESComponent.86.Hists:              Pileup_OffsetNPV
JESComponent.86.Reducible:          False

# Original pT term
JESComponent.88.Name:               Pileup_PtTerm
JESComponent.88.Desc:               Pileup: Offset, pT term, independent (derived for Rscan)
JESComponent.88.Type:               Other
JESComponent.88.Corr:               Correlated
JESComponent.88.Param:              PtEta
JESComponent.88.Special:            True
JESComponent.88.Hists:              Pileup_PtTerm_mu,Pileup_PtTerm_npv
JESComponent.88.Reducible:          False

# Original JetAreas rho topology
JESComponent.90.Name:               Pileup_RhoTopology
JESComponent.90.Desc:               Rho topology uncertainty (jet areas), independent (scaled for Rscan)
JESComponent.90.Type:               Other
JESComponent.90.Param:              PtEta
JESComponent.90.Special:            True
JESComponent.90.Hists:              Pileup_RhoTopology
JESComponent.90.Reducible:          False

# Flavour composition uncertainty
JESComponent.91.Name:               Flavor_Composition
JESComponent.91.Desc:               Flavor composition uncertainty (derived for Rscan)
JESComponent.91.Type:               Modelling
JESComponent.91.Corr:               Correlated
JESComponent.91.Param:              PtEta
JESComponent.91.Special:            True
JESComponent.91.Hists:              flavorCompGlu,flavorCompLight

# Flavour response uncertainty
JESComponent.92.Name:               Flavor_Response
JESComponent.92.Desc:               Flavor response uncertainty (dominated by gluon response) (derived for Rscan)
JESComponent.92.Type:               Modelling
JESComponent.92.Param:              PtEta
JESComponent.92.Special:            True
JESComponent.92.Hists:              FlavorResponse

# bJES uncertainty
JESComponent.96.Name:               BJES_Response
JESComponent.96.Desc:               JES uncertainty for b jets (derived for Rscan)
JESComponent.96.Type:               Modelling
JESComponent.96.Param:              PtEta
JESComponent.96.Special:            True
JESComponent.96.Hists:              bJES
JESComponent.96.Reducible:          False

# Original punch-through uncertainty
JESComponent.97.Name:               PunchThrough_MCTYPE
JESComponent.97.Desc:               Punch-through correction uncertainty, orig - Ref
JESComponent.97.Type:               Detector
JESComponent.97.Param:              PtAbsEta
JESComponent.97.Interp:             False
JESComponent.97.Special:            True
JESComponent.97.Hists:              PunchThrough_MCTYPE
JESComponent.97.Reducible:          False

##############################################################################
##                  Specific components for the Rscan method
##############################################################################

# Rscan OOC Term
JESComponent.110.Name:              Rscan_OOC
JESComponent.110.Desc:              OOC Term - Rscan
JESComponent.110.Type:              Modelling
JESComponent.110.Param:             Pt
JESComponent.110.Reducible:	    True

# Z+jet: Delta R
JESComponent.100.Desc:              Zjet Direct matching, delta R between Rscan and Ref jets 
JESComponent.100.Name:              Rscan_Zjet_DeltaR
JESComponent.100.Type:              Modelling
JESComponent.100.Param:             PtEta
JESComponent.100.Reducible:         False

# Z+jet: Isolation 
JESComponent.101.Desc:              Zjet Rscan isolation requirement 
JESComponent.101.Name:              Rscan_Zjet_Isolation
JESComponent.101.Type:              Modelling
JESComponent.101.Param:             PtEta
JESComponent.101.Reducible:         False

# Z+jet: Generator
JESComponent.102.Name:              Rscan_Zjet_MC
JESComponent.102.Desc:              Zjet Rscan Monte Carlo generator difference 
JESComponent.102.Type:              Modelling
JESComponent.102.Param:             PtEta
JESComponent.102.Reducible:         False

# Make one explicit group for Zjets JVT
JESGroup.1.Name:                   Zjet_Jvt
JESGroup.1.Desc:                   Zjet JVT 
JESGroup.1.Type:                   Modelling 
JESGroup.1.Corr:                   Correlated
JESGroup.1.Group:                  1
JESGroup.1.Reducible:              False

# Z+jets Rscan: JVT term
JESComponent.103.Name:              Rscan_Zjet_Jvt
JESComponent.103.Desc:              Zjet LAr JVT - Rscan
JESComponent.103.Type:              Modelling
JESComponent.103.Param:             PtEta
JESComponent.103.Reducible:         False
JESComponent.103.Group:             1

# Z+jets Ref: JVT term
JESComponent.11.Name:               Zjet_Jvt
JESComponent.11.Desc:               LAr JVT? - Ref
JESComponent.11.Type:               Modelling
JESComponent.11.Param:              Pt
JESComponent.11.Reducible:          False
JESComponent.11.Group:              1

# Z+jet: Statistical
JESComponent.104.Name:              Rscan_Zjet_stat
JESComponent.104.Desc:              Zjet Rscan comb. of stat. components 
JESComponent.104.Type:              Statistical
JESComponent.104.Param:             PtEta
JESComponent.104.Reducible:         False

# Dijet: Delta R
JESComponent.105.Desc:              Dijet Direct matching, delta R between Rscan and Ref jets 
JESComponent.105.Name:              Rscan_Dijet_DeltaR
JESComponent.105.Type:              Modelling
JESComponent.105.Param:             PtEta
JESComponent.105.Reducible:         False

# Dijet: Isolation 
JESComponent.106.Desc:              Dijet Rscan isolation requirement 
JESComponent.106.Name:              Rscan_Dijet_Isolation
JESComponent.106.Type:              Modelling
JESComponent.106.Param:             PtEta
JESComponent.106.Reducible:         False

# Dijet: Generator
JESComponent.107.Name:              Rscan_Dijet_MC
JESComponent.107.Desc:              Dijet Rscan Monte Carlo generator difference 
JESComponent.107.Type:              Modelling
JESComponent.107.Param:             PtEta
JESComponent.107.Reducible:         False

# Dijet: JVT term
JESComponent.108.Name:              Rscan_Dijet_Jvt
JESComponent.108.Desc:              Dijet LAr JVT - Rscan
JESComponent.108.Type:              Modelling
JESComponent.108.Param:             PtEta
JESComponent.108.Reducible:         False

# Dijet: Statistical
JESComponent.109.Name:              Rscan_Dijet_Stat
JESComponent.109.Desc:              Dijet Rscan comb. of stat. components 
JESComponent.109.Type:              Statistical
JESComponent.109.Param:             PtEta
JESComponent.109.Reducible:         False

# ZJet: Non-Closure Term
# for studies wanting to calibrate 2LC jets from 15 GeV
JESComponent.111.Name:              Rscan_NonClosure
JESComponent.111.Desc:              Non closure observed at low pT in the Z+jets method 
JESComponent.111.Type:              Other
JESComponent.111.Param:             Pt
JESComponent.111.Reducible:         False
