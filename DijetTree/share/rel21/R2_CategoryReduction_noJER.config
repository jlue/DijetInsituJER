##############################################################################
# JESProvider Input Settings
#  
#   Set of NPs for the category reduced scenario for Rscan2019 JES recommendation.
#   AntiKt2LCTopo jets 
#   October 11th, 2019
#
#   M. Toscani (JES)
# 
##############################################################################

UncertaintyRelease:              rel21_Rscan2019-2LC
SupportedJetDefs:                AntiKt2LCTopo
SupportedMCTypes:                MC16
UncertaintyRootFile:             rel21/Rscan2019/R2_AllComponents.root
AnalysisRootFile:                analysisInputs/UnknownFlavourComp.root

FileValidHistogram:                 ValidRange
FileValidHistParam:                 PtEta

# <Mu> obtained from ATLAS public plots
# <NPV> from Eric's pileup studies
# Note that Eric's studies find a MuRef of 22.8 instead.
Pileup.MuRef:                       24.2
Pileup.NPVRef:                      13.2

##############################################################################
#
#   Settings for JES Components
#
##############################################################################

JESComponent.1.Name:                EffectiveNP_Statistical1
JESComponent.1.Desc:                Effective JES Uncertainty Statistical Component 1
JESComponent.1.Type:                Effective
JESComponent.1.Param:               Pt
JESComponent.1.Hists:               EffectiveNP_Statistical1

JESComponent.2.Name:                EffectiveNP_Statistical2
JESComponent.2.Desc:                Effective JES Uncertainty Statistical Component 2
JESComponent.2.Type:                Effective
JESComponent.2.Param:               Pt
JESComponent.2.Hists:               EffectiveNP_Statistical2

JESComponent.3.Name:                EffectiveNP_Statistical3
JESComponent.3.Desc:                Effective JES Uncertainty Statistical Component 3
JESComponent.3.Type:                Effective
JESComponent.3.Param:               Pt
JESComponent.3.Hists:               EffectiveNP_Statistical3

JESComponent.4.Name:                EffectiveNP_Statistical4
JESComponent.4.Desc:                Effective JES Uncertainty Statistical Component 4
JESComponent.4.Type:                Effective
JESComponent.4.Param:               Pt
JESComponent.4.Hists:               EffectiveNP_Statistical4

JESComponent.5.Name:                EffectiveNP_Statistical5
JESComponent.5.Desc:                Effective JES Uncertainty Statistical Component 5
JESComponent.5.Type:                Effective
JESComponent.5.Param:               Pt
JESComponent.5.Hists:               EffectiveNP_Statistical5

JESComponent.6.Name:                EffectiveNP_Statistical6
JESComponent.6.Desc:                Effective JES Uncertainty Statistical Component 6
JESComponent.6.Type:                Effective
JESComponent.6.Param:               Pt
JESComponent.6.Hists:               EffectiveNP_Statistical6

JESComponent.7.Name:                 EffectiveNP_Modelling1
JESComponent.7.Desc:                 Effective JES Uncertainty Modelling Component 1
JESComponent.7.Type:                 Effective
JESComponent.7.Param:                Pt
JESComponent.7.Hists:                EffectiveNP_Modelling1
                                 
JESComponent.8.Name:                 EffectiveNP_Modelling2
JESComponent.8.Desc:                 Effective JES Uncertainty Modelling Component 2
JESComponent.8.Type:                 Effective
JESComponent.8.Param:                Pt
JESComponent.8.Hists:                EffectiveNP_Modelling2
                                 
JESComponent.9.Name:                 EffectiveNP_Modelling3
JESComponent.9.Desc:                 Effective JES Uncertainty Modelling Component 3
JESComponent.9.Type:                 Effective
JESComponent.9.Param:                Pt
JESComponent.9.Hists:                EffectiveNP_Modelling3
                                 
JESComponent.10.Name:                EffectiveNP_Modelling4
JESComponent.10.Type:                Effective
JESComponent.10.Param:               Pt
JESComponent.10.Hists:               EffectiveNP_Modelling4
                                 
JESComponent.11.Name:                EffectiveNP_Modelling5
JESComponent.11.Type:                Effective
JESComponent.11.Param:               Pt
JESComponent.11.Hists:               EffectiveNP_Modelling5

JESComponent.0.Name:                 EffectiveNP_Modelling6
JESComponent.0.Type:                 Effective
JESComponent.0.Param:                Pt
JESComponent.0.Hists:                EffectiveNP_Modelling6

JESComponent.12.Name:                EffectiveNP_Detector1
JESComponent.12.Desc:                Effective JES Uncertainty Detector Component 1
JESComponent.12.Type:                Effective
JESComponent.12.Param:               Pt
JESComponent.12.Hists:               EffectiveNP_Detector1

JESComponent.13.Name:                EffectiveNP_Detector2
JESComponent.13.Desc:                Effective JES Uncertainty Detector Component 2
JESComponent.13.Type:                Effective
JESComponent.13.Param:               Pt
JESComponent.13.Hists:               EffectiveNP_Detector2

JESComponent.14.Name:                EffectiveNP_Mixed1
JESComponent.14.Desc:                Effective JES Uncertainty Mixed Component 1
JESComponent.14.Type:                Effective
JESComponent.14.Param:               Pt
JESComponent.14.Hists:               EffectiveNP_Mixed1

JESComponent.15.Name:                EffectiveNP_Mixed2
JESComponent.15.Desc:                Effective JES Uncertainty Mixed Component 2
JESComponent.15.Type:                Effective
JESComponent.15.Param:               Pt
JESComponent.15.Hists:               EffectiveNP_Mixed2

JESComponent.16.Name:                EffectiveNP_Mixed3
JESComponent.16.Desc:                Effective JES Uncertainty Mixed Component 3
JESComponent.16.Type:                Effective
JESComponent.16.Param:               Pt
JESComponent.16.Hists:               EffectiveNP_Mixed3

##############################################################################
# 
#   Special components
#
##############################################################################

# Eta intercalibration: theory uncertainty
JESComponent.76.Name:               EtaIntercalibration_Modelling
JESComponent.76.Desc:               Eta intercalibration: MC generator modelling uncertainty - Ref
JESComponent.76.Type:               Modelling
JESComponent.76.Param:              PtEta
JESComponent.76.Hists:              EtaIntercalibration_Modelling
JESComponent.76.Reducible:          False

# Eta intercalibration: total statistical and method uncertainty
JESComponent.78.Name:               EtaIntercalibration_TotalStat
JESComponent.78.Desc:               Eta intercalibration: statistical uncertainty - Ref
JESComponent.78.Type:               Statistical
JESComponent.78.Param:              PtEta
JESComponent.78.Hists:              EtaIntercalibration_TotalStat
JESComponent.78.Reducible:          False

# Eta intercalibration non-closure uncertainty
JESComponent.80.Name:               EtaIntercalibration_NonClosure_highE
JESComponent.80.Desc:               Eta intercalibration: non-closure uncertainty -Ref
JESComponent.80.Type:               Other
JESComponent.80.Param:              PtEta
JESComponent.80.Interp:             False
JESComponent.80.Hists:              EtaIntercalibration_NonClosure_highE
JESComponent.80.Reducible:          False

# Eta intercalibration non-closure uncertainty
JESComponent.98.Name:               EtaIntercalibration_NonClosure_posEta
JESComponent.98.Desc:               Eta intercalibration: non-closure uncertainty - Ref
JESComponent.98.Type:               Other
JESComponent.98.Param:              PtEta
JESComponent.98.Interp:             False
JESComponent.98.Hists:              EtaIntercalibration_NonClosure_posEta
JESComponent.98.Reducible:          False

# Eta intercalibration non-closure uncertainty
JESComponent.99.Name:               EtaIntercalibration_NonClosure_negEta
JESComponent.99.Desc:               Eta intercalibration: non-closure uncertainty - Ref
JESComponent.99.Type:               Other
JESComponent.99.Param:              PtEta
JESComponent.99.Interp:             False
JESComponent.99.Hists:              EtaIntercalibration_NonClosure_negEta
JESComponent.99.Reducible:          False

# High pT term
JESComponent.82.Name:               SingleParticle_HighPt
JESComponent.82.Desc:               High pT term (2012 version) - Ref
JESComponent.82.Type:               Detector
JESComponent.82.Param:              Pt
JESComponent.82.Reducible:          False

# Pileup: Original mu term
JESComponent.84.Name:               Pileup_OffsetMu
JESComponent.84.Desc:               Pileup: Offset, mu term, independent (derived for Rscan)
JESComponent.84.Type:               Other
JESComponent.84.Param:              PtEta
JESComponent.84.Special:            True
JESComponent.84.Hists:              Pileup_OffsetMu
JESComponent.84.Reducible:          False

# Pileup: Original NPV term
JESComponent.86.Name:               Pileup_OffsetNPV
JESComponent.86.Desc:               Pileup: Offset, NPV term, independent (derived for Rscan)
JESComponent.86.Type:               Other
JESComponent.86.Param:              PtEta
JESComponent.86.Special:            True
JESComponent.86.Hists:              Pileup_OffsetNPV
JESComponent.86.Reducible:          False

# Original pT term
JESComponent.88.Name:               Pileup_PtTerm
JESComponent.88.Desc:               Pileup: Offset, pT term, independent (derived for Rscan)
JESComponent.88.Type:               Other
JESComponent.88.Corr:               Correlated
JESComponent.88.Param:              PtEta
JESComponent.88.Special:            True
JESComponent.88.Hists:              Pileup_PtTerm_mu,Pileup_PtTerm_npv
JESComponent.88.Reducible:          False

# Original JetAreas rho topology
JESComponent.90.Name:               Pileup_RhoTopology
JESComponent.90.Desc:               Rho topology uncertainty (jet areas), independent (scaled for Rscan)
JESComponent.90.Type:               Other
JESComponent.90.Param:              PtEta
JESComponent.90.Special:            True
JESComponent.90.Hists:              Pileup_RhoTopology
JESComponent.90.Reducible:          False

# Flavour composition uncertainty
JESComponent.91.Name:               Flavor_Composition
JESComponent.91.Desc:               Flavor composition uncertainty (derived for Rscan)
JESComponent.91.Type:               Modelling
JESComponent.91.Corr:               Correlated
JESComponent.91.Param:              PtEta
JESComponent.91.Special:            True
JESComponent.91.Hists:              flavorCompGlu,flavorCompLight

# Flavour response uncertainty
JESComponent.92.Name:               Flavor_Response
JESComponent.92.Desc:               Flavor response uncertainty (dominated by gluon response) (derived for Rscan)
JESComponent.92.Type:               Modelling
JESComponent.92.Param:              PtEta
JESComponent.92.Special:            True
JESComponent.92.Hists:              FlavorResponse

# bJES uncertainty
JESComponent.96.Name:               BJES_Response
JESComponent.96.Desc:               JES uncertainty for b jets (derived for Rscan)
JESComponent.96.Type:               Modelling
JESComponent.96.Param:              PtEta
JESComponent.96.Special:            True
JESComponent.96.Hists:              bJES
JESComponent.96.Reducible:          False

# Original punch-through uncertainty
JESComponent.97.Name:               PunchThrough_MCTYPE
JESComponent.97.Desc:               Punch-through correction uncertainty, orig - Ref
JESComponent.97.Type:               Detector
JESComponent.97.Param:              PtAbsEta
JESComponent.97.Interp:             False
JESComponent.97.Special:            True
JESComponent.97.Hists:              PunchThrough_MCTYPE
JESComponent.97.Reducible:          False


##############################################################################
##                  Specific components for the Rscan method
##############################################################################

# Z+jet: Delta R
JESComponent.100.Desc:              Zjet Direct matching, delta R between Rscan and Ref jets 
JESComponent.100.Name:              Rscan_Zjet_DeltaR
JESComponent.100.Type:              Modelling
JESComponent.100.Param:             PtEta
JESComponent.100.Reducible:         False

# Z+jet: Isolation 
JESComponent.101.Desc:              Zjet Rscan isolation requirement 
JESComponent.101.Name:              Rscan_Zjet_Isolation
JESComponent.101.Type:              Modelling
JESComponent.101.Param:             PtEta
JESComponent.101.Reducible:         False

# Z+jet: Generator
JESComponent.102.Name:              Rscan_Zjet_MC
JESComponent.102.Desc:              Zjet Rscan Monte Carlo generator difference 
JESComponent.102.Type:              Modelling
JESComponent.102.Param:             PtEta
JESComponent.102.Reducible:         False

# Make one explicit group for Zjets JVT
JESGroup.13.Name:                   Zjet_Jvt
JESGroup.13.Desc:                   Zjet JVT 
JESGroup.13.Type:                   Modelling 
JESGroup.13.Corr:                   Correlated
JESGroup.13.Group:                  13
JESGroup.13.Reducible:              False

# Z+jets Rscan: JVT term
JESComponent.103.Name:              Rscan_Zjet_Jvt
JESComponent.103.Desc:              Zjet LAr JVT - Rscan
JESComponent.103.Type:              Modelling
JESComponent.103.Param:             PtEta
JESComponent.103.Reducible:         False
JESComponent.103.Group:             13

# Z+jets Ref: JVT term
JESComponent.11.Name:               Zjet_Jvt
JESComponent.11.Desc:               LAr JVT? - Ref
JESComponent.11.Type:               Modelling
JESComponent.11.Param:              Pt
JESComponent.11.Reducible:          False
JESComponent.11.Group:              13

# Z+jet: Statistical
JESComponent.104.Name:              Rscan_Zjet_stat
JESComponent.104.Desc:              Zjet Rscan comb. of stat. components 
JESComponent.104.Type:              Statistical
JESComponent.104.Param:             PtEta
JESComponent.104.Reducible:         False

# Dijet: Delta R
JESComponent.105.Desc:              Dijet Direct matching, delta R between Rscan and Ref jets 
JESComponent.105.Name:              Rscan_Dijet_DeltaR
JESComponent.105.Type:              Modelling
JESComponent.105.Param:             PtEta
JESComponent.105.Reducible:         False

# Dijet: Isolation 
JESComponent.106.Desc:              Dijet Rscan isolation requirement 
JESComponent.106.Name:              Rscan_Dijet_Isolation
JESComponent.106.Type:              Modelling
JESComponent.106.Param:             PtEta
JESComponent.106.Reducible:         False

# Dijet: Generator
JESComponent.107.Name:              Rscan_Dijet_MC
JESComponent.107.Desc:              Dijet Rscan Monte Carlo generator difference 
JESComponent.107.Type:              Modelling
JESComponent.107.Param:             PtEta
JESComponent.107.Reducible:         False

# Dijet: JVT term
JESComponent.108.Name:              Rscan_Dijet_Jvt
JESComponent.108.Desc:              Dijet LAr JVT - Rscan
JESComponent.108.Type:              Modelling
JESComponent.108.Param:             PtEta
JESComponent.108.Reducible:         False

# Dijet: Statistical
JESComponent.109.Name:              Rscan_Dijet_Stat
JESComponent.109.Desc:              Dijet Rscan comb. of stat. components 
JESComponent.109.Type:              Statistical
JESComponent.109.Param:             PtEta
JESComponent.109.Reducible:         False

# ZJet: Non-Closure Term
# for studies wanting to calibrate 2LC jets from 15 GeV
JESComponent.111.Name:              Rscan_NonClosure
JESComponent.111.Desc:              Non closure observed at low pT in the Z+jets method 
JESComponent.111.Type:              Other
JESComponent.111.Param:             Pt
JESComponent.111.Reducible:         False
