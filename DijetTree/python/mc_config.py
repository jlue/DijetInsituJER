# jet algorithms per radius
jetAlgos = {
  "smallR": ['AntiKt4EMPFlow','AntiKt4EMTopo'],
  "pflow":  ['AntiKt4EMPFlow'],
  "topo":   ['AntiKt4EMTopo'],
  "largeR": ['AntiKt10LCTopoTrimmedPtFrac5SmallR20'],
  "Rscan":  ['AntiKt2LCTopo','AntiKt6LCTopo']
}

jetSystematics = {
  "nominal": {
    'AntiKt2LCTopo': [''],
    'AntiKt6LCTopo': [''],
    'AntiKt4EMTopo': [''],
    'AntiKt4EMPFlow': [''],
    'AntiKt10LCTopoTrimmedPtFrac5SmallR20': ['']
  },
  "JES": {    
    'AntiKt2LCTopo': [
      'JET_EtaIntercalibration_NonClosure_highE',
      'JET_EtaIntercalibration_NonClosure_negEta',
      'JET_EtaIntercalibration_NonClosure_posEta',
      'JET_EffectiveNP_1',
      'JET_EffectiveNP_2',
      'JET_EffectiveNP_3',
      'JET_EffectiveNP_4',
      'JET_EffectiveNP_5',
      'JET_EffectiveNP_6',
      'JET_EffectiveNP_7',
      'JET_EffectiveNP_8restTerm'
    ],
    'AntiKt6LCTopo': [
      'JET_EtaIntercalibration_NonClosure_highE',
      'JET_EtaIntercalibration_NonClosure_negEta',
      'JET_EtaIntercalibration_NonClosure_posEta',
      'JET_EffectiveNP_1',
      'JET_EffectiveNP_2',
      'JET_EffectiveNP_3',
      'JET_EffectiveNP_4',
      'JET_EffectiveNP_5',
      'JET_EffectiveNP_6',
      'JET_EffectiveNP_7',
      'JET_EffectiveNP_8restTerm'
    ],
    'AntiKt4EMTopo': [
      'JET_EtaIntercalibration_NonClosure_highE',
      'JET_EtaIntercalibration_NonClosure_negEta',
      'JET_EtaIntercalibration_NonClosure_posEta',
      'JET_EffectiveNP_1',
      'JET_EffectiveNP_2',
      'JET_EffectiveNP_3',
      'JET_EffectiveNP_4',
      'JET_EffectiveNP_5',
      'JET_EffectiveNP_6',
      'JET_EffectiveNP_7',
      'JET_EffectiveNP_8restTerm'
    ],
    'AntiKt4EMPFlow': [
      'JET_EtaIntercalibration_NonClosure_highE',
      'JET_EtaIntercalibration_NonClosure_negEta',
      'JET_EtaIntercalibration_NonClosure_posEta',
      'JET_EffectiveNP_1',
      'JET_EffectiveNP_2',
      'JET_EffectiveNP_3',
      'JET_EffectiveNP_4',
      'JET_EffectiveNP_5',
      'JET_EffectiveNP_6',
      'JET_EffectiveNP_7',
      'JET_EffectiveNP_8restTerm'
    ],
    'AntiKt10LCTopoTrimmedPtFrac5SmallR20': [
      'JET_EffectiveNP_R10_1',
      'JET_EffectiveNP_R10_2',
      'JET_EffectiveNP_R10_3',
      'JET_EffectiveNP_R10_4',
      'JET_EffectiveNP_R10_5',
      'JET_EffectiveNP_R10_6restTerm'
    ]
  }
}

# JetCalibrator
jetCalibConfig = {
  'AntiKt2LCTopo' : 'JES_2015_2016_data_Rscan2LC_18Dec2018_R21.config',
  'AntiKt6LCTopo' : 'JES_2015_2016_data_Rscan6LC_27Sep2019_R21.config',
  'AntiKt4EMTopo' : 'JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config',
  'AntiKt4EMPFlow': 'JES_MC16_PU_MCJES_GNNC_PFlow_Nov2021.config',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config'
}
jetCalibSeq = {
  'AntiKt2LCTopo' : 'JetArea_Residual_EtaJES_GSC',
  'AntiKt6LCTopo' : 'JetArea_Residual_EtaJES_GSC',
  'AntiKt4EMTopo' : 'JetArea_Residual_EtaJES_GSC',
  'AntiKt4EMPFlow': 'JetArea_Residual_EtaJES_GNNC',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'EtaJES_JMS'
}
jetCalibArea = {
  'AntiKt2LCTopo' : '00-04-82',
  'AntiKt6LCTopo' : '00-04-82',
  'AntiKt4EMTopo' : '00-04-82',
  'AntiKt4EMPFlow': '00-04-82',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': '00-04-82'
}
# JetUncertainties
jetUncertConfig = {
  'AntiKt2LCTopo': 'rel21/Rscan2019/R2_CategoryReduction_noJER.config', 
  'AntiKt6LCTopo': 'rel21/Rscan2019/R6_CategoryReduction_noJER.config', 
  'AntiKt4EMTopo': 'rel21/Fall2018/R4_GlobalReduction_SimpleJER.config',
  'AntiKt4EMPFlow': 'rel21/Fall2018/R4_GlobalReduction_SimpleJER.config',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'rel21/Spring2019/R10_GlobalReduction.config'
}
jetUncertCalibArea = {
  'AntiKt2LCTopo': 'CalibArea-06',
  'AntiKt6LCTopo': 'CalibArea-06',
  'AntiKt4EMTopo': 'CalibArea-06',
  'AntiKt4EMPFlow': 'CalibArea-06',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'CalibArea-07'
}

# JetSelector
detEta_max = {
  'AntiKt2LCTopo': 4.5,
  'AntiKt6LCTopo': 4.5,
  'AntiKt4EMTopo': 4.5,
  'AntiKt4EMPFlow': 4.5,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 2.5
}
doJVT = {
  'AntiKt2LCTopo': False,
  'AntiKt6LCTopo': False,
  'AntiKt4EMTopo': True,
  'AntiKt4EMPFlow': True,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': False
}
workingPointJVT = {
  'AntiKt2LCTopo': "",
  'AntiKt6LCTopo': "",
  'AntiKt4EMTopo': "Tight",
  'AntiKt4EMPFlow': "Tight",
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': ""
}
dofJVT = {
  'AntiKt2LCTopo': False,
  'AntiKt6LCTopo': False,
  'AntiKt4EMTopo': True,
  'AntiKt4EMPFlow': True,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': False
}
workingPointfJVT = {
  'AntiKt2LCTopo': "",
  'AntiKt6LCTopo': "",
  'AntiKt4EMTopo': "Tight",
  'AntiKt4EMPFlow': "Tight",
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': ""
}

# TruthJetMatcher
truthJetAlgo = {
  'AntiKt2LCTopo': 'AntiKt2Truth',
  'AntiKt6LCTopo': 'AntiKt6Truth',
  'AntiKt4EMTopo': 'AntiKt4Truth',
  'AntiKt4EMPFlow': 'AntiKt4Truth',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'AntiKt10TruthTrimmedPtFrac5SmallR20'
}
deltaRCut = {
  'AntiKt2LCTopo': 0.2,
  'AntiKt6LCTopo': 0.6,
  'AntiKt4EMTopo': 0.4,
  'AntiKt4EMPFlow': 0.4,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 1.0
}
doGhostAssoc = {
  'AntiKt2LCTopo': False,
  'AntiKt6LCTopo': False,
  'AntiKt4EMTopo': True,
  'AntiKt4EMPFlow': True,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': False
}
