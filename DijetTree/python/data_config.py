# jet algorithms per radius
jetAlgos = {
  "smallR": ['AntiKt4EMPFlow','AntiKt4EMTopo'],
  "pflow":  ['AntiKt4EMPFlow'],
  "topo":   ['AntiKt4EMTopo'],
  "largeR": ['AntiKt10LCTopoTrimmedPtFrac5SmallR20'],
  "Rscan":  ['AntiKt2LCTopo','AntiKt6LCTopo']
}

# BasicEventSelection
# GRLs
GRLlist = ["DijetTree/GRL/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
           "DijetTree/GRL/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
           "DijetTree/GRL/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
           "DijetTree/GRL/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]
GRLs = ','.join(GRLlist)

# JetTriggerEmulator
# trigger jet algorithms to run over
# smallR:
#   2015+2016: a4tcemsubjesFS
#   2017+2018: a4tcemsubjesISFS
trigJetAlgos = {
  'smallR' : ['a4tcemsubjesISFS'],
  'pflow'  : ['a4tcemsubjesISFS'],
  'topo'   : ['a4tcemsubjesISFS'],
  'largeR' : ['a4tcemsubjesISFS','a10tclcwsubjesFS'],
  'Rscan'  : ['a4tcemsubjesISFS']
}
# "master" array containing jet trigger information
smallRJetTriggers = [['HLT_j0_perf_L1RD0_FILLED', 0,   0,3.2,   'L1_RD0_FILLED',   0,   0,4.9],
                     ['HLT_j15',                  15,  0,3.2,   'L1_RD0_FILLED',   0,   0,4.9],
                     ['HLT_j25',                  25,  0,3.2,   'L1_RD0_FILLED',   0,   0,4.9],
                     ['HLT_j35',                  35,  0,3.2,   'L1_RD0_FILLED',   0,   0,4.9],
                     ['HLT_j45',                  45,  0,3.2,   'L1_J15',          15,  0,3.1],
                     ['HLT_j60',                  60,  0,3.2,   'L1_J20',          20,  0,3.1],
                     ['HLT_j85',                  85,  0,3.2,   'L1_J20',          20,  0,3.1],
                     ['HLT_j110',                 110, 0,3.2,   'L1_J30',          30,  0,3.1],
                     ['HLT_j175',                 175, 0,3.2,   'L1_J50',          50,  0,3.1],
                     ['HLT_j260',                 260, 0,3.2,   'L1_J75',          75,  0,3.1],
                     ['HLT_j360',                 360, 0,3.2,   'L1_J100',         100, 0,3.1],
                     ['HLT_j380',                 380, 0,3.2,   'L1_J100',         100, 0,3.1],
                     ['HLT_j400',                 400, 0,3.2,   'L1_J100',         100, 0,3.1],
                     ['HLT_j420',                 420, 0,3.2,   'L1_J100',         100, 0,3.1],
                     ['HLT_j15_320eta490',        15,  3.2,4.9, 'L1_RD0_FILLED',   0,   0,4.9],
                     ['HLT_j25_320eta490',        25,  3.2,4.9, 'L1_RD0_FILLED',   0,   0,4.9],
                     ['HLT_j35_320eta490',        35,  3.2,4.9, 'L1_RD0_FILLED',   0,   0,4.9],
                     ['HLT_j45_320eta490',        45,  3.2,4.9, 'L1_J15.31ETA49',  15,  3.1,4.9],
                     ['HLT_j60_320eta490',        60,  3.2,4.9, 'L1_J20.31ETA49',  20,  3.1,4.9],
                     ['HLT_j85_320eta490',        85,  3.2,4.9, 'L1_J20.31ETA49',  20,  3.1,4.9],
                     ['HLT_j110_320eta490',       110, 3.2,4.9, 'L1_J30.31ETA49',  30,  3.1,4.9],
                     ['HLT_j175_320eta490',       175, 3.2,4.9, 'L1_J50.31ETA49',  50,  3.1,4.9],
                     ['HLT_j260_320eta490',       260, 3.2,4.9, 'L1_J75.31ETA49',  75,  3.1,4.9],
                     ['HLT_j360_320eta490',       360, 3.2,4.9, 'L1_J100.31ETA49', 100, 3.1,4.9]]
largeRJetTriggers = [['HLT_j260_a10t_lcw_jes_L1J75',  260, 0,3.2, 'L1_J75',  75,  0,3.1],
                     ['HLT_j420_a10t_lcw_jes_L1J100', 420, 0,3.2, 'L1_J100', 100, 0,3.1],
                     ['HLT_j440_a10t_lcw_jes_L1J100', 440, 0,3.2, 'L1_J100', 100, 0,3.1],
                     ['HLT_j460_a10t_lcw_jes_L1J100', 460, 0,3.2, 'L1_J100', 100, 0,3.1],
                     ['HLT_j480_a10t_lcw_jes_L1J100', 480, 0,3.2, 'L1_J100', 100, 0,3.1],
                     ['HLT_j500_a10t_lcw_jes_L1J100', 500, 0,3.2, 'L1_J100', 100, 0,3.1],
                     ['HLT_j520_a10t_lcw_jes_L1J100', 520, 0,3.2, 'L1_J100', 100, 0,3.1],
                     ['HLT_j540_a10t_lcw_jes_L1J100', 540, 0,3.2, 'L1_J100', 100, 0,3.1]]
# flatten master array for algorithm inputs
allJetTriggers = '|'.join( [trigInfo[0] for trigInfo in smallRJetTriggers+largeRJetTriggers] )
jetTriggers = {
  'a4tcemsubjesFS': '|'.join( [trigInfo[0] for trigInfo in smallRJetTriggers] ),
  'a4tcemsubjesISFS': '|'.join( [trigInfo[0] for trigInfo in smallRJetTriggers] ),
  'a10tclcwsubjesFS': '|'.join( [trigInfo[0] for trigInfo in largeRJetTriggers] )
}
trigThresholdETs = {
  'a4tcemsubjesFS': '|'.join( [str(trigInfo[1]) for trigInfo in smallRJetTriggers] ),
  'a4tcemsubjesISFS': '|'.join( [str(trigInfo[1]) for trigInfo in smallRJetTriggers] ),
  'a10tclcwsubjesFS': '|'.join( [str(trigInfo[1]) for trigInfo in largeRJetTriggers] )
}
trigRequiredEtas = {
  'a4tcemsubjesFS': '|'.join( [str(trigInfo[2])+','+str(trigInfo[3]) for trigInfo in smallRJetTriggers] ),
  'a4tcemsubjesISFS': '|'.join( [str(trigInfo[2])+','+str(trigInfo[3]) for trigInfo in smallRJetTriggers] ),
  'a10tclcwsubjesFS': '|'.join( [str(trigInfo[2])+','+str(trigInfo[3]) for trigInfo in largeRJetTriggers] )
}
L1seeds = {
  'a4tcemsubjesFS': '|'.join( [trigInfo[4] for trigInfo in smallRJetTriggers] ),
  'a4tcemsubjesISFS': '|'.join( [trigInfo[4] for trigInfo in smallRJetTriggers] ),
  'a10tclcwsubjesFS': '|'.join( [trigInfo[4] for trigInfo in largeRJetTriggers] )
}
seedThresholdETs = {
  'a4tcemsubjesFS': '|'.join( [str(trigInfo[5]) for trigInfo in smallRJetTriggers] ),
  'a4tcemsubjesISFS': '|'.join( [str(trigInfo[5]) for trigInfo in smallRJetTriggers] ),
  'a10tclcwsubjesFS': '|'.join( [str(trigInfo[5]) for trigInfo in largeRJetTriggers] )
}
seedRequiredEtas = {
  'a4tcemsubjesFS': '|'.join( [str(trigInfo[6])+','+str(trigInfo[7]) for trigInfo in smallRJetTriggers] ),
  'a4tcemsubjesISFS': '|'.join( [str(trigInfo[6])+','+str(trigInfo[7]) for trigInfo in smallRJetTriggers] ),
  'a10tclcwsubjesFS': '|'.join( [str(trigInfo[6])+','+str(trigInfo[7]) for trigInfo in largeRJetTriggers] )
}

# JetCalibrator
jetCalibConfig = {
  'AntiKt2LCTopo' : 'JES_2015_2016_data_Rscan2LC_18Dec2018_R21.config',
  'AntiKt6LCTopo' : 'JES_2015_2016_data_Rscan6LC_27Sep2019_R21.config',
  'AntiKt4EMTopo' : 'JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config',
  'AntiKt4EMPFlow': 'JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config'
}
jetCalibSeq = {
  'AntiKt2LCTopo' : 'JetArea_Residual_EtaJES_GSC_Insitu',
  'AntiKt6LCTopo' : 'JetArea_Residual_EtaJES_GSC_Insitu',
  'AntiKt4EMTopo' : 'JetArea_Residual_EtaJES_GSC_Insitu',
  'AntiKt4EMPFlow': 'JetArea_Residual_EtaJES_GSC_Insitu',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'EtaJES_JMS_Insitu_InsituCombinedMass'
}
jetCalibArea = {
  'AntiKt2LCTopo' : '00-04-82',
  'AntiKt6LCTopo' : '00-04-82',
  'AntiKt4EMTopo' : '00-04-82',
  'AntiKt4EMPFlow': '00-04-82',
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': '00-04-82'
}

# JetSelector
detEta_max = {
  'AntiKt2LCTopo': 4.5,
  'AntiKt6LCTopo' : 4.5,
  'AntiKt4EMTopo': 4.5,
  'AntiKt4EMPFlow': 4.5,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 2.5
}
doJVT = {
  'AntiKt2LCTopo': False,
  'AntiKt6LCTopo': False,
  'AntiKt4EMTopo': True,
  'AntiKt4EMPFlow': True,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': False
}
workingPointJVT = {
  'AntiKt2LCTopo': "",
  'AntiKt6LCTopo': "",
  'AntiKt4EMTopo': "Tight",
  'AntiKt4EMPFlow': "Tight",
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': ""
}
dofJVT = {
  'AntiKt2LCTopo': False,
  'AntiKt6LCTopo': False,
  'AntiKt4EMTopo': True,
  'AntiKt4EMPFlow': True,
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': False
}
workingPointfJVT = {
  'AntiKt2LCTopo': "",
  'AntiKt6LCTopo': "",
  'AntiKt4EMTopo': "Tight",
  'AntiKt4EMPFlow': "Tight",
  'AntiKt10LCTopoTrimmedPtFrac5SmallR20': ""
}
