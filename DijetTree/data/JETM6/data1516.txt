[inputs]

DSIDs: 15D 15E 15F 15G 15H 16A 16B 16C 16D 16E 16F 16G 16I 16K 16L

15D.File: data15_13TeV:data15_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM6.grp15_v01_p4129
15E.File: data15_13TeV:data15_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM6.grp15_v01_p4129
15F.File: data15_13TeV:data15_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM6.grp15_v01_p4129
15G.File: data15_13TeV:data15_13TeV.periodG.physics_Main.PhysCont.DAOD_JETM6.grp15_v01_p4129
15H.File: data15_13TeV:data15_13TeV.periodH.physics_Main.PhysCont.DAOD_JETM6.grp15_v01_p4129
16A.File: data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16B.File: data16_13TeV:data16_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16C.File: data16_13TeV:data16_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16D.File: data16_13TeV:data16_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16E.File: data16_13TeV:data16_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16F.File: data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16G.File: data16_13TeV:data16_13TeV.periodG.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16I.File: data16_13TeV:data16_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16K.File: data16_13TeV:data16_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129
16L.File: data16_13TeV:data16_13TeV.periodL.physics_Main.PhysCont.DAOD_JETM6.grp16_v01_p4129

15D.Name: data15_D
15E.Name: data15_E
15F.Name: data15_F
15G.Name: data15_G
15H.Name: data15_H
16A.Name: data16_A
16B.Name: data16_B
16C.Name: data16_C
16D.Name: data16_D
16E.Name: data16_E
16F.Name: data16_F
16G.Name: data16_G
16I.Name: data16_I
16K.Name: data16_K
16L.Name: data16_L