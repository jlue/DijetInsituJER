[inputs]

DSIDs: B C D E F H I K

B.File: data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129
C.File: data17_13TeV:data17_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129
D.File: data17_13TeV:data17_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129
E.File: data17_13TeV:data17_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129
F.File: data17_13TeV:data17_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129
H.File: data17_13TeV:data17_13TeV.periodH.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129
I.File: data17_13TeV:data17_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129
K.File: data17_13TeV:data17_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM6.grp17_v01_p4129

B.Name: data17_B
C.Name: data17_C
D.Name: data17_D
E.Name: data17_E
F.Name: data17_F
H.Name: data17_H
I.Name: data17_I
K.Name: data17_K