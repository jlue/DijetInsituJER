#!/usr/bin/env python

import argparse
parser = argparse.ArgumentParser(description='DijetHistograms inputs, config, and output')
# input files
parser.add_argument('--files', dest='files', type=str, nargs='+', required=True,
                    help='Ntuples with cut-flow histogram to sum over weights')
args = parser.parse_args()

if __name__ == '__main__':

    import ROOT
    from ROOT import Config

    totalEventCount = 0
    for filePath in args.files:

        file = ROOT.TFile(filePath,'read')
        fileEventCount = file.Get('eventCount')
        fileEventCount.SetDirectory(0)
        if totalEventCount==0: totalEventCount = fileEventCount
        else: totalEventCount.Add(totalEventCount,fileEventCount)

    print("numOfEvents:  {}".format(totalEventCount.GetBinContent(1)))
    print("sumOfWeights: {}".format(totalEventCount.GetBinContent(3)))