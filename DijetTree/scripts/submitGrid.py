#!/usr/bin/env python

import os
import argparse
import subprocess
import datetime
import configparser

# argument parser
parser = argparse.ArgumentParser()
# input files
parser.add_argument('--inputs', dest='inputs', nargs='+', type=str, required=True,
                    help='list of .txt files containing input datasets and submission names')
# manual configuration
parser.add_argument('--config', dest='config', type=str, required=True,
                    help='run configuration files')
# identifier
parser.add_argument('--identifier', dest='identifier', type=str, default='',
                    help='unique identifier name')
# check
parser.add_argument('--check', dest='check', action='store_true',
                    help='only check submission commands')
# parse arguments
args = parser.parse_args()

# get rucio account
username = os.environ['RUCIO_ACCOUNT']
# create timestamp
identifier = '{date:%Y%m%d_%H%M}'.format(date=datetime.datetime.now())
if args.identifier: identifier += '_'+args.identifier

def inputFilesAndNames(filePath):
    inputsParser = configparser.ConfigParser()
    inputsParser.read(filePath)
    inputsList = inputsParser['inputs']
    DSIDs = inputsList.get('DSIDs').split(' ')
    Files = []
    Names = []
    for id in DSIDs:
        Files.append(inputsList.get('{}.File'.format(id)))
        Names.append(inputsList.get('{}.Name'.format(id)))
    return Files, Names

def outputNames(names):
    return ['user.{}.DijetTree_{}_{}'.format(username,name,identifier) for name in names] 

# execute program(s)
if __name__ == "__main__":
    for fileList in args.inputs:
        files, names = inputFilesAndNames(fileList)
        outs = outputNames(names)
        for ids in range(len(files)):
            runCmd = ['xAH_run.py',
                        '--files={}'.format(files[ids]),
                        '--inputRucio',
                        '--submitDir={}'.format(names[ids]),
                        '--config={}'.format(args.config),
                        '--force']
            gridOpts = ['prun',
                        '--optGridOutputSampleName={}'.format(outs[ids]),
                        '--optGridNGBPerJob=MAX',
                        # '--optGridSite=ANALY_MPPMU',
                        # '--optGridExcludedSite=ANALY_INFN-T1',
                        '--optGridMergeOutput=1']
            submitCommand = runCmd + gridOpts
            if args.check:
                print(' '.join(submitCommand))
            else:
                subprocess.call(submitCommand)
