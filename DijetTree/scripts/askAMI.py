#!/usr/bin/env python

## IMPORTANT: always use EVNT datasets to extract xsecs, filter effs, etc.

## To setup
# $ setupATLAS
# $ localSetupPyAMI pyAMI-4.1.3a-x86_64-slc6
# $ ami auth
# $ voms-proxy-init -voms atlas

## To run, provide a (space-separated) list of datasets, e.g.
# ./getMCMetaDataFromAMI.py mc15_13TeV.426131.Sherpa_CT10_jets_JZ1.evgen.EVNT.e4355 mc15_13TeV.426132.Sherpa_CT10_jets_JZ2.evgen.EVNT.e4355 mc15_13TeV.426133.Sherpa_CT10_jets_JZ3.evgen.EVNT.e4355 mc15_13TeV.426134.Sherpa_CT10_jets_JZ4.evgen.EVNT.e4355 mc15_13TeV.426135.Sherpa_CT10_jets_JZ5.evgen.EVNT.e4355 mc15_13TeV.426136.Sherpa_CT10_jets_JZ6.evgen.EVNT.e4355
#./getMCMetaDataFromAMI.py mc15_13TeV.426001.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ1.evgen.EVNT.e3788 mc15_13TeV.426002.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ2.evgen.EVNT.e3788 mc15_13TeV.426003.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ3.evgen.EVNT.e3788 mc15_13TeV.426004.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ4.evgen.EVNT.e3788 mc15_13TeV.426005.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ5.evgen.EVNT.e3788 mc15_13TeV.426006.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ6.evgen.EVNT.e3788 mc15_13TeV.426007.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ7.evgen.EVNT.e3788 mc15_13TeV.426008.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ8.evgen.EVNT.e3788 mc15_13TeV.426009.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ9.evgen.EVNT.e3788

import argparse
parser = argparse.ArgumentParser(description='DijetHistograms inputs, config, and output')
# input files
parser.add_argument('--inputs', dest='inputs', type=str, required=True,
                    help='Input .txt file containing sample list path used by DijetTree grid submission')
parser.add_argument('--output', dest='output', type=str, default='',
                    help='Output .txt file path')
args = parser.parse_args()

if __name__ == '__main__':

    import pyAMI.client
    import pyAMI.atlas.api as AtlasAPI
    client = pyAMI.client.Client('atlas')
    AtlasAPI.init()

    from ROOT import Config
    inputTable = Config(args.inputs)
    mcChannelNumbers = inputTable.getStrV('DSIDs')

    mcTotalEvents = {}
    mcCrossSection = {}
    mcGenFiltEff ={}
    for mc in mcChannelNumbers :
        
        dataset = inputTable.getStr(mc+".File")
        dataset = dataset.replace("mc16_13TeV:","")
        info = (AtlasAPI.get_dataset_info(client, dataset))[0]

        mcTotalEvents[mc] = info['totalEvents'].encode()
        mcCrossSection[mc] = info['crossSection'].encode()
        mcGenFiltEff[mc] = info['genFiltEff'].encode()

    dataTable = Config()
    for mc in mcChannelNumbers :
        dataTable.setStr(mc+".NumOfEvents",mcTotalEvents[mc])
        dataTable.setStr(mc+".CrossSection",mcCrossSection[mc])
        dataTable.setStr(mc+".GenFiltEff",mcGenFiltEff[mc])

    dataTable.printTable()
    if args.output:
        dataTable.writeTable(args.output)
