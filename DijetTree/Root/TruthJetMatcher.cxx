#include "DijetTree/TruthJetMatcher.h"

// this is needed to distribute the algorithm to the workers
ClassImp(TruthJetMatcher)

TruthJetMatcher :: TruthJetMatcher (std::string className) :
xAH::Algorithm(className)
{
}

EL::StatusCode TruthJetMatcher :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("TruthJetMatcher").ignore();
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode TruthJetMatcher :: initialize ()
{
  // initialize worker
  Info("initialize()", m_name.c_str());
  
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthJetMatcher::execute()
{
  
  // truth jets
  const xAOD::JetContainer* truthJets(nullptr);
  ANA_CHECK ( HelperFunctions::retrieve(truthJets,m_truthContainerName,m_event,m_store) );
  
  // reconstructed jets to truth-match
  const xAOD::JetContainer* recoJets(nullptr);
  
  // nominal
  if (m_inputAlgo.empty()) {
    
    // get nominal jet container
    ANA_CHECK ( HelperFunctions::retrieve(recoJets,m_recoContainerName,m_event,m_store) );

    // truth-match jets
    matchTruthJets(recoJets, truthJets, m_truth_pT_min, m_doDeltaR, m_deltaRCut, m_doGhostAssoc, m_ghostAssocFracCut );
    
  } else {  // systematics
    
    // get systematics
    std::vector<std::string>* systNames(nullptr);
    ANA_CHECK( HelperFunctions::retrieve(systNames, m_inputAlgo, m_event, m_store, msg()) );
    
    // for each systematic
    for (auto systName : *systNames) {
      
      // get systematic jet container
      ANA_CHECK ( HelperFunctions::retrieve(recoJets,m_recoContainerName+systName,m_event,m_store) );

      // truth-match jets
      matchTruthJets(recoJets, truthJets, m_truth_pT_min, m_doDeltaR, m_deltaRCut, m_doGhostAssoc, m_ghostAssocFracCut );

    }  // each systematic
    
  }  // systematics
  
  // reconstructed -> matched truth jet
  


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthJetMatcher :: finalize ()
{
  ANA_MSG_INFO("finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}

void TruthJetMatcher:: matchTruthJets( const xAOD::JetContainer* recoJets,
                                       const xAOD::JetContainer* truthJets,
                                       float truth_pT_min,
                                       bool  doDeltaR,
                                       float deltaRCut,
                                       bool  doGhostAssoc,
                                       float ghostAssocFracCut )
{
  // each reconstructed jet
  for ( const auto recoJet : *recoJets ) {
    
    // may have a truth-matched jet
    const xAOD::Jet* matchedTruthJet(nullptr);
  
    // use ghost association ( & DR )
    if (doGhostAssoc) {
      if ((recoJet->isAvailable<ElementLink<xAOD::JetContainer>>("GhostTruthAssociationLink") && recoJet->auxdata<ElementLink<xAOD::JetContainer>>("GhostTruthAssociationLink").isValid())) {
        // get the ghost associated truth jet
        const xAOD::Jet* candidateTruthJet = *recoJet->auxdata<ElementLink<xAOD::JetContainer>>("GhostTruthAssociationLink");
        // check minimum pT requirement
        bool passPt = candidateTruthJet->pt()/GeV > truth_pT_min;
        // check DR (only if used)
        bool passDeltaR = doDeltaR ? recoJet->p4().DeltaR(candidateTruthJet->p4()) < deltaRCut : true;
        // check ghost assocation fraction
        bool passGhostAssocFrac = ( recoJet->auxdata<float>("GhostTruthAssociationFraction") > ghostAssocFracCut );
        // if all conditions pass, match is valid
        if ( passPt && passDeltaR && passGhostAssocFrac ) matchedTruthJet = candidateTruthJet;
      }
    } else {  // DR
      for (auto candidateTruthJet : *truthJets) {
        // skip if minimum pT requirement not satisfied
        if ( candidateTruthJet->pt()/GeV < truth_pT_min ) continue;
        // check DR
        bool passDeltaR = (recoJet->p4().DeltaR(candidateTruthJet->p4()) < deltaRCut );
        // if DR passes, match is valid
        if ( passDeltaR ) { matchedTruthJet = candidateTruthJet; break; }
      }
    }
    
    // decorate truth-matched jet information
    if ( matchedTruthJet ) {
      recoJet->auxdecor<char>("isTruthMatched") = true;
      recoJet->auxdecor<float>("pT_true") = matchedTruthJet->pt();
      recoJet->auxdecor<float>("eta_true") = matchedTruthJet->eta();
      recoJet->auxdecor<float>("phi_true") = matchedTruthJet->phi();
      recoJet->auxdecor<float>("E_true") = matchedTruthJet->e();
      recoJet->auxdecor<float>("m_true") = matchedTruthJet->m();
    }
    
  }  // reconstructed jet
}
