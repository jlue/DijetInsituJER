#include "DijetTree/DijetTreeAlgo.h"

#ifdef __CINT__

  #pragma link off all globals;
  #pragma link off all classes;
  #pragma link off all functions;
  #pragma link C++ nestedclass;

  #pragma link C++ class JetTriggerEmulator+;
  #pragma link C++ class TruthJetMatcher+;
  #pragma link C++ class RscanJetMatcher+;
  #pragma link C++ class DijetTreeAlgo+;
  #pragma link C++ class DijetTreeBase+;

#endif
