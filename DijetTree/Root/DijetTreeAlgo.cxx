#include "DijetTree/DijetTreeAlgo.h"

// this is needed to distribute the algorithm to the workers
ClassImp(DijetTreeAlgo)

DijetTreeAlgo::DijetTreeAlgo (std::string className) :
  xAH::Algorithm(className)
{
}

EL::StatusCode DijetTreeAlgo::setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("DijetTreeAlgo").ignore();

  // setup tree output
  EL::OutputStream outForTree("tree");
  if ( !job.outputHas("tree") ) job.outputAdd(outForTree);

  return EL::StatusCode::SUCCESS;
}

  
EL::StatusCode DijetTreeAlgo::initialize ()
{
  Info("initialize()", "DijetTreeAlgo");
  
  // initialize worker
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DijetTreeAlgo::execute()
{
  
  // handle the nominal case on every event
  std::vector<std::string> appliedSystNames({""});
  
  // get systematics to be applied
  std::vector<std::string>* systNames(nullptr);
  if( not m_jetSystematics.empty() ) {
    ANA_CHECK( HelperFunctions::retrieve(systNames, m_jetSystematics, 0, m_store, msg()) );
    for(const auto& systName: *systNames) {
      // skip if duplicate (e.g. nominal)
      if (std::find(appliedSystNames.begin(), appliedSystNames.end(), systName) != appliedSystNames.end()) continue;
      appliedSystNames.push_back(systName);
    }
  }
  
  // check for tree output file
  TFile * treeFile = wk()->getOutputFile("tree");
  if( !treeFile ) {
    ANA_MSG_ERROR("Failed to get output file!");
    return EL::StatusCode::FAILURE;
  }
  
  // create output tree if it doesn't exist
  for ( const auto& systName: appliedSystNames ) {
    // tree name: jetAlgo_systName_systVar
    // e.g. AntiKt4EMPFlow_JET_EffectiveNP1_R04__1up
    std::string treeName = systName.empty() ? m_jetAlgo : m_jetAlgo+"_"+systName;
    
    // skip if tree already made
    if(m_trees.find(treeName) != m_trees.end()) continue;
    
    // create tree (first event)
    ANA_MSG_INFO( "Making tree " << treeName );
    TTree * outTree = new TTree(treeName.c_str(),treeName.c_str());
    if ( !outTree ) {
      ANA_MSG_ERROR("Failed to instantiate output tree!");
      return EL::StatusCode::FAILURE;
    }
    m_trees[treeName] = new DijetTreeBase(m_event, outTree, treeFile);
    
    // tell the tree to go into the file
    outTree->SetDirectory( treeFile );
    
    // initialize all branch addresses since we just added this tree
    m_trees[treeName]->AddEvent(m_eventDetailStr);
    m_trees[treeName]->AddVertices(m_vertexDetailStr,"vtx");
    if ( isMC() ) m_trees[treeName]->AddTruthVertices(m_truthVertexDetailStr,"truthvtx");
    m_trees[treeName]->AddTrigger(m_trigDetailStr);
    // jet information
    m_trees[treeName]->AddJets(m_recoJetDetailStr,"jet");
    if ( isMC() ) m_trees[treeName]->AddJets(m_truthJetDetailStr,"truthjet");
    //m_trees[treeName]->AddJets(m_recoJetDetailStr,"HLTjet");
  }
  
  // get event info
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK( HelperFunctions::retrieve(eventInfo,m_eventInfoContainerName,m_event,m_store,msg()) );
  
  // primary vertices
  const xAOD::VertexContainer* vertices(nullptr);
  ANA_CHECK( HelperFunctions::retrieve(vertices,m_vertexContainerName,m_event,m_store,msg()) );
  const int pvLocation = HelperFunctions::getPrimaryVertexLocation(vertices);
  // truth vertices
  const xAOD::TruthVertexContainer* tvertices(nullptr);
  if ( isMC() ) ANA_CHECK( HelperFunctions::retrieve(tvertices,m_truthVertexContainerName,m_event,m_store,msg()) );
  
  // for each systematic
  for (const auto& systName : appliedSystNames) {
    std::string treeName = systName.empty() ? m_jetAlgo : m_jetAlgo+"_"+systName;

    // event-level info
    m_trees[treeName]->FillEvent( eventInfo, m_event, vertices );
    m_trees[treeName]->FillVertices(vertices,"vtx");
    if ( isMC() ) m_trees[treeName]->FillTruthVertices(tvertices,"truthvtx");
    m_trees[treeName]->FillTrigger( eventInfo );

    // per-jet info
    const xAOD::JetContainer* recoJets(nullptr);
    ANA_CHECK ( HelperFunctions::retrieve(recoJets,m_recoJetContainerName+systName,m_event,m_store) );
    const xAOD::JetContainer* truthJets(nullptr);
    if ( isMC() ) ANA_CHECK ( HelperFunctions::retrieve(truthJets,m_truthJetContainerName,m_event,m_store) );
    m_trees[treeName]->FillJets( recoJets, pvLocation, "jet");
    if ( isMC() ) m_trees[treeName]->FillJets( truthJets, pvLocation, "truthjet");
    //const xAOD::JetContainer* HLTJets(nullptr);
    //ANA_CHECK ( HelperFunctions::retrieve(HLTJets,m_HLTJetContainerName+systName,m_event,m_store) );
    
    //m_trees[treeName]->FillJets( HLTJets, pvLocation, "HLTjet");
    // fill tree entry
    m_trees[treeName]->Fill();
  }
  
  // finished processing event
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DijetTreeAlgo::finalize ()
{
  ANA_MSG_INFO("finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  
  // Write additional metadata information in the output file
  TFile* treeFile = wk()->getOutputFile( "tree" );
  
  // isMC flag
  TH1I* isMCFlagHist = nullptr; treeFile->GetObject("isMC",isMCFlagHist);
  if ( !isMCFlagHist ) {
    isMCFlagHist = new TH1I("isMC","isMC",1,0,1);
    isMCFlagHist->GetXaxis()->SetBinLabel(1,"isMC");
    isMCFlagHist->SetBinContent(1,isMC());
    isMCFlagHist->SetDirectory( treeFile );
  }
  
  // number of events & sum of weights
  TH1D* eventCountHist = nullptr; treeFile->GetObject("eventCount",eventCountHist);
  if ( !eventCountHist ) {
    TFile* metaDataFile = wk()->getOutputFile( "metadata" );
    metaDataFile->GetObject("MetaData_EventCount",eventCountHist);
    TH1D* eventCountHistToWrite = (TH1D*)eventCountHist->Clone();
    eventCountHistToWrite->SetNameTitle("eventCount","eventCount");
    eventCountHistToWrite->SetDirectory( treeFile );
  }
  
  ANA_MSG_INFO("Done finalize");
  return EL::StatusCode::SUCCESS;
}
