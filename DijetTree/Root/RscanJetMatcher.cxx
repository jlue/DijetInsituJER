#include "DijetTree/RscanJetMatcher.h"

// this is needed to distribute the algorithm to the workers
ClassImp(RscanJetMatcher)

RscanJetMatcher :: RscanJetMatcher (std::string className) :
xAH::Algorithm(className)
{
}

EL::StatusCode RscanJetMatcher :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("RscanJetMatcher").ignore();
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode RscanJetMatcher :: initialize ()
{
  // initialize worker
  Info("initialize()", m_name.c_str());
  
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanJetMatcher::execute()
{

  // Rref jets
  const xAOD::JetContainer* RrefJets(nullptr);
  ANA_CHECK ( HelperFunctions::retrieve(RrefJets,m_RrefContainerName,m_event,m_store) );
  
  // Rscan jets
  const xAOD::JetContainer* RscanJets(nullptr);
  
  // nominal
  if (m_inputAlgo.empty()) {
    
    // get nominal jet container
    ANA_CHECK ( HelperFunctions::retrieve(RscanJets,m_RscanContainerName,m_event,m_store) );

    // for each Rscan jet
    for (auto RscanJet : *RscanJets) {

      // match to Rref jet
      auto *matchedRrefJet = matchRscanJet(RscanJet, RrefJets, m_matchDeltaRCut );

      // decorate with matched jet information
      if ( matchedRrefJet ) {
        RscanJet->auxdecor<char>("isRscanMatched") = true;
        RscanJet->auxdecor<float>("pT_Rref") = matchedRrefJet->pt();
        RscanJet->auxdecor<float>("eta_Rref") = matchedRrefJet->eta();
        RscanJet->auxdecor<float>("phi_Rref") = matchedRrefJet->phi();
        RscanJet->auxdecor<float>("E_Rref") = matchedRrefJet->e();
        RscanJet->auxdecor<float>("m_Rref") = matchedRrefJet->m();
        RscanJet->auxdecor<float>("etadet_Rref") = matchedRrefJet->auxdata<float>("DetectorEta");
        RscanJet->auxdecor<float>("phidet_Rref") = matchedRrefJet->auxdata<float>("DetectorPhi");
      }
    }  // Rscan jet

  // systematics
  } else {
    
    // get systematics
    std::vector<std::string>* systNames(nullptr);
    ANA_CHECK( HelperFunctions::retrieve(systNames, m_inputAlgo, m_event, m_store, msg()) );
    // for each systematic
    for (auto systName : *systNames) {
      
      // get systematic-varied Rscan jet container
      ANA_CHECK ( HelperFunctions::retrieve(RscanJets,m_RscanContainerName+systName,m_event,m_store) );

      // for each Rscan jet
      for (auto RscanJet : *RscanJets) {
        // match to Rref jet
        auto *matchedRrefJet = matchRscanJet(RscanJet, RrefJets, m_matchDeltaRCut );
        // decorate with matched jet information
        if ( matchedRrefJet ) {
          RscanJet->auxdecor<char>("isRscanMatched") = true;
          RscanJet->auxdecor<float>("pT_Rref") = matchedRrefJet->pt();
          RscanJet->auxdecor<float>("eta_Rref") = matchedRrefJet->eta();
          RscanJet->auxdecor<float>("phi_Rref") = matchedRrefJet->phi();
          RscanJet->auxdecor<float>("E_Rref") = matchedRrefJet->e();
          RscanJet->auxdecor<float>("m_Rref") = matchedRrefJet->m();
          RscanJet->auxdecor<float>("etadet_Rref") = matchedRrefJet->auxdata<float>("DetectorEta");
          RscanJet->auxdecor<float>("phidet_Rref") = matchedRrefJet->auxdata<float>("DetectorPhi");
        }
      }  // Rscan jet

    }  // each systematic
  }  // systematics
  
  // finished processing event
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanJetMatcher :: finalize ()
{
  ANA_MSG_INFO("finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}

// (for each Rscan jet) look for a matched reference jet, return it if found
const xAOD::Jet* RscanJetMatcher :: matchRscanJet( const xAOD::Jet* RscanJet,
                                                   const xAOD::JetContainer* RrefJets,
                                                   float deltaRCut
                                                   )
{
  //  matched Rref jet (no match by default)
  const xAOD::Jet* matchedRrefJet(nullptr);
  for (auto candidateRrefJet : *RrefJets) {
    // check DR
    bool matchDeltaR = (RscanJet->p4().DeltaR(candidateRrefJet->p4()) < deltaRCut );
    // if DR passes, match is found
    if ( matchDeltaR ) { matchedRrefJet = candidateRrefJet; break; }
  }
  return matchedRrefJet;
}
