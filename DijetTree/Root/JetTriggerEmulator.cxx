#include "DijetTree/JetTriggerEmulator.h"

// this is needed to distribute the algorithm to the workers
ClassImp(JetTriggerEmulator)

JetTriggerEmulator :: JetTriggerEmulator (std::string className) :
xAH::Algorithm(className)
{
}

EL::StatusCode JetTriggerEmulator :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("JetTriggerEmulator").ignore();
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetTriggerEmulator :: initialize ()
{
  // initialize worker
  Info("initialize()", m_name.c_str());
  
  // make HLT jet container name
  m_triggerJetContainerName = "HLT_xAOD__JetContainer_"+m_trigJetAlgo;
  
  // parse the trigger information
  // HLT_jX|HLT_jY|HLT_jZ_320eta490|...
  // X|Y|Z|...
  // 0,3.2|0,3.2|3.2,4.9|...
  // L1 seeds...
  m_emulatedTriggerList = vectorizeStr(m_emulatedTriggers,"|");
  m_emulatedSeedList    = vectorizeStr(m_emulatedSeeds,"|");

  std::vector<float>       trigThresholdETValueList  = vectorizeNum(m_trigThresholdETs,"|");
  std::vector<std::string> trigRequiredEtaBoundsList = vectorizeStr(m_trigRequiredEtas,"|");
  std::vector<float>       seedThresholdETValueList  = vectorizeNum(m_seedThresholdETs,"|");
  std::vector<std::string> seedRequiredEtaBoundsList = vectorizeStr(m_seedRequiredEtas,"|");
  for (uint itrig=0 ; itrig<m_emulatedTriggerList.size() ; ++itrig) {
    // trigger & seed strings
    std::string trig = m_emulatedTriggerList.at(itrig);
    std::string seed = m_emulatedSeedList.at(itrig);
    // trigger information
    m_trigThresholdET[trig] = trigThresholdETValueList.at(itrig);
    m_trigRequiredEta[trig] = vectorizeNum(trigRequiredEtaBoundsList.at(itrig).c_str(),",");
    // seed information
    m_trigSeed[trig] = seed;
    m_seedThresholdET[seed] = seedThresholdETValueList.at(itrig);
    m_seedRequiredEta[seed] = vectorizeNum(seedRequiredEtaBoundsList.at(itrig).c_str(),",");
    
    Info("JetTriggerEmulator", "  trigger %s: ET > %.1f GeV, %.1f < eta < %.1f => seed %s: ET > %.1f, %.1f < eta < %.1f",
         trig.c_str(),
         m_trigThresholdET[trig],
         m_trigRequiredEta[trig][0],m_trigRequiredEta[trig][1],
         m_trigSeed[trig].c_str(),
         m_seedThresholdET[seed],
         m_seedRequiredEta[seed][0],m_seedRequiredEta[seed][1]);
  }
  
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  
  // Grab the TrigDecTool from the ToolStore
  if (m_checkTrigDecTool && !m_trigDecTool_handle.isUserConfigured()) {
    ANA_MSG_FATAL("A configured " << m_trigDecTool_handle.typeAndName() << " must have been previously created! Are you creating one in xAH::BasicEventSelection?" );
    return EL::StatusCode::FAILURE;
  }
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetTriggerEmulator::execute()
{
  // final information that we want from the algorithm
  std::vector<std::string> passedTriggersBeforePrescale{};
  
  // enable TDT expert mode
  if (m_checkTrigDecTool) m_trigDecTool_handle->ExperimentalAndExpertMethods()->enable();
  
  // retrieve trigger jet container
  const xAOD::JetContainer * triggerJets(nullptr);
  ANA_CHECK ( HelperFunctions::retrieve(triggerJets,m_triggerJetContainerName.c_str(),m_event,m_store) );
  
  // retrieve seed container
  const xAOD::JetRoIContainer * seedJetRoIs(nullptr);
  ANA_CHECK ( HelperFunctions::retrieve(seedJetRoIs,"LVL1JetRoIs",m_event,m_store) );

  // look at all triggers
  for(const auto trig : m_emulatedTriggerList) {
    
    // HLT & L1 pass flags (before prescale)
    bool passedL1BeforePrescale = false;
    bool passedHLTBeforePrescale = false;
    
    // check if its HLT requirements passed
    for ( xAOD::JetContainer::const_iterator jet = triggerJets->begin(); jet < triggerJets->end(); ++jet ) {
      float ET = (*jet)->p4().Et()/GeV;
      float eta = fabs((*jet)->eta());
      if ( ET >= m_trigThresholdET.at(trig) && eta >= m_trigRequiredEta.at(trig).at(0) &&  eta <  m_trigRequiredEta.at(trig).at(1) ) {
        passedHLTBeforePrescale = true;
        break;
      }
    }  // HLT jet
    
    // check if its L1 requirements passed
    std::string seed = m_trigSeed.at(trig);
    for ( xAOD::JetRoIContainer::const_iterator jet = seedJetRoIs->begin(); jet < seedJetRoIs->end(); ++jet ) {
      float ET = (*jet)->et8x8()/GeV;
      float eta = fabs((*jet)->eta());
      if ( ET >= m_seedThresholdET.at(seed) && eta >= m_seedRequiredEta.at(seed).at(0) &&  eta <  m_seedRequiredEta.at(seed).at(1) ) {
        passedL1BeforePrescale = true;
        break;
      }
    }  // L1 jet RoI
    
    // special cases
    // L1_RD0_FILLED
    // HLT_j0_perf_L1RD0_FILLED
    if (seed=="L1_RD0_FILLED") passedL1BeforePrescale = true;
    if (trig=="HLT_j0_perf_L1RD0_FILLED") passedHLTBeforePrescale = true;
    
    // trigger configuration information must be available for it to be processed
    if (m_checkTrigDecTool) {
      const auto trigConf = m_trigDecTool_handle->ExperimentalAndExpertMethods()->getChainConfigurationDetails(trig);
      if ( !trigConf )
        Error("JetTriggerEmulator","%s configuration is not found in TDT.",trig.c_str());
      std::string tdtSeed = trigConf->lower_chain_name();
      if ( tdtSeed != seed )
        Warning("JetTriggerEmulator","%s's L1 seed: %s (user) | %s (TDT)", trig.c_str(), seed.c_str(), trigConf->lower_chain_name().c_str());
      bool tdtPassedL1BeforePrescale = m_trigDecTool_handle->isPassed(tdtSeed,TrigDefs::L1_isPassedBeforePrescale);
      if ( passedL1BeforePrescale != tdtPassedL1BeforePrescale ) {
        Warning("JetTriggerEmulator","%s decision before prescale: %i (emulation) | %i (TDT)", seed.c_str(),passedL1BeforePrescale,tdtPassedL1BeforePrescale);
        for ( xAOD::JetRoIContainer::const_iterator jet = seedJetRoIs->begin(); jet < seedJetRoIs->end(); ++jet ) {
          float ET = (*jet)->et8x8()/GeV;
          float eta = fabs((*jet)->eta());
          Info("JetTriggerEmulator","  L1 jet RoI: ET = %.1f GeV, eta = %.1f", ET,eta );
        }
      }
    }

    // the whole trigger chain must (L1+HLT) pass before prescale
    if (passedL1BeforePrescale && passedHLTBeforePrescale) passedTriggersBeforePrescale.push_back(trig);
  }  // trigger
  
  // save to event info
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK ( HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store, msg()) );
  // check for existing information, and add onto it if it exists, or create one if not
  if (eventInfo->isAvailable<std::vector<std::string>>("passedTriggersBeforePrescale")) {
    std::vector<std::string> allPassedTriggersBeforePrescale = eventInfo->auxdecor<std::vector<std::string>>("passedTriggersBeforePrescale");
    allPassedTriggersBeforePrescale.insert(allPassedTriggersBeforePrescale.end(),
                                           passedTriggersBeforePrescale.begin(),
                                           passedTriggersBeforePrescale.end());
    eventInfo->auxdecor<std::vector<std::string>>("passedTriggersBeforePrescale") = allPassedTriggersBeforePrescale;
  } else {
    eventInfo->auxdecor<std::vector<std::string>>("passedTriggersBeforePrescale") = passedTriggersBeforePrescale;
  }
  
  if ( m_checkTrigDecTool ) {
    std::vector<std::string> passedTriggers = eventInfo->auxdecor<std::vector<std::string>>("passedTriggers");
    for ( std::string passedTrigger : passedTriggers ) {
      if ( std::find(m_emulatedTriggerList.begin(),m_emulatedTriggerList.end(),passedTrigger)==m_emulatedTriggerList.end() ) continue;
      if ( std::find(passedTriggersBeforePrescale.begin(),passedTriggersBeforePrescale.end(),passedTrigger)==passedTriggersBeforePrescale.end() ) {
        Warning("JetTriggerEmulator", "%s (%s) passed in TDT, but failed in emulation!", passedTrigger.c_str(),m_trigSeed.at(passedTrigger).c_str());
        Info("JetTriggerEmulator",    "  pass through: %i", m_trigDecTool_handle->isPassed(passedTrigger,TrigDefs::EF_passThrough));
        Info("JetTriggerEmulator",    "  passed raw:   %i", m_trigDecTool_handle->isPassed(passedTrigger,TrigDefs::EF_passedRaw));
        Info("JetTriggerEmulator",    "  prescaled:    %i", m_trigDecTool_handle->isPassed(passedTrigger,TrigDefs::EF_prescaled));
        Info("JetTriggerEmulator",    "  resurrected:  %i", m_trigDecTool_handle->isPassed(passedTrigger,TrigDefs::EF_resurrected));
        for ( xAOD::JetContainer::const_iterator jet = triggerJets->begin(); jet < triggerJets->end(); ++jet ) {
          float ET = (*jet)->p4().Et()/GeV;
          float eta = fabs((*jet)->eta());
          Info("JetTriggerEmulator","  HLT jet: ET = %.1f GeV, eta = %.1f", ET,eta );
        }
        for ( xAOD::JetRoIContainer::const_iterator jet = seedJetRoIs->begin(); jet < seedJetRoIs->end(); ++jet ) {
          float ET = (*jet)->et8x8()/GeV;
          float eta = fabs((*jet)->eta());
          Info("JetTriggerEmulator","  L1 jet RoI: ET = %.1f GeV, eta = %.1f", ET,eta );
        }
      }
    }
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetTriggerEmulator :: finalize ()
{
  
  
  ANA_MSG_INFO("finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}
