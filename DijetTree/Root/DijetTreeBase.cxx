#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "DijetTree/DijetTreeBase.h"

void DijetTree::TriggerInfoSwtich::initialize()
{
  m_passTriggersBeforePrescale = has_exact("passTriggersBeforePrescale");
}
void DijetTree::JetInfoSwitch::initialize()
{
  // number of jets to save
  m_leadingJet = has_exact("leadingJet");
  m_leadingDijet = has_exact("leadingDijet");
  m_thirdJet = has_exact("thirdJet");
  m_allJets = has_exact("allJets");
  // detector eta & phi
  m_detectorEta = has_exact("detectorEta");
  m_detectorPhi = has_exact("detectorPhi");
  // truth matching
  m_truthMatch = has_exact("truthMatch");
  // Rscan matching
  m_Rscan = has_exact("Rscan");
  //HLT info
  //m_HLT = has_exact("HLT");
}

DijetTreeBase :: DijetTreeBase(xAOD::TEvent * event, TTree* tree, TFile* file) :
  HelpTreeBase(event, tree, file, GeV)
{
  Info("DijetTreeBase", "Creating output TTree  %s", tree->GetName());
}

// trigger information
void DijetTreeBase::AddTriggerUser(const std::string& detailStr)
{
  Info("AddTriggerUser","Using trigger detailStr %s", detailStr.c_str());
  m_triggerInfoSwitch = new DijetTree::TriggerInfoSwtich( detailStr );
  
  if ( m_triggerInfoSwitch->m_passTriggersBeforePrescale ) {
    m_tree->Branch("passedTriggersBeforePrescale", &m_passedTriggersBeforePrescale);
  }
}

// analyzed jet information
void DijetTreeBase::AddJetsUser(const std::string& detailStr, const std::string& jetName)
{
  //if (jetName=="HLTjet"){
   // m_tree->Branch((jetName+"_pT").c_str(),  &m_HLT_jet_pT);
  //}else{
  // truth jets
  if ( jetName.find("truth")!=std::string::npos ) {
    Info("AddJetsUser","Using %s detailStr %s", jetName.c_str(), detailStr.c_str());
    m_truthJetInfoSwitch = new DijetTree::JetInfoSwitch( detailStr );
    m_tree->Branch((jetName+"_pT").c_str(),     &m_truth_jet_pT);
    m_tree->Branch((jetName+"_eta").c_str(),    &m_truth_jet_eta);
    m_tree->Branch((jetName+"_phi").c_str(),    &m_truth_jet_phi);
    m_tree->Branch((jetName+"_E").c_str(),      &m_truth_jet_E);
    m_tree->Branch((jetName+"_m").c_str(),      &m_truth_jet_m);
  } else {  // reconstructed (-> matched truth jets)
    Info("AddJetsUser","Using %s detailStr %s", jetName.c_str(), detailStr.c_str());
    m_jetInfoSwitch = new DijetTree::JetInfoSwitch( detailStr );
    m_tree->Branch((jetName+"_pT").c_str(),     &m_jet_pT);
    m_tree->Branch((jetName+"_eta").c_str(),    &m_jet_eta);
    m_tree->Branch((jetName+"_phi").c_str(),    &m_jet_phi);
    m_tree->Branch((jetName+"_E").c_str(),      &m_jet_E);
    m_tree->Branch((jetName+"_m").c_str(),      &m_jet_m);
    m_tree->Branch((jetName+"_etadet").c_str(), &m_jet_etadet);
    m_tree->Branch((jetName+"_phidet").c_str(), &m_jet_phidet);
    // Rscan
    if ( m_jetInfoSwitch->m_Rscan ) {
      m_tree->Branch((jetName+"_pT_Rref").c_str(),     &m_jet_pT_Rref);
      m_tree->Branch((jetName+"_eta_Rref").c_str(),    &m_jet_eta_Rref);
      m_tree->Branch((jetName+"_phi_Rref").c_str(),    &m_jet_phi_Rref);
      m_tree->Branch((jetName+"_E_Rref").c_str(),      &m_jet_E_Rref);
      m_tree->Branch((jetName+"_m_Rref").c_str(),      &m_jet_m_Rref);
      m_tree->Branch((jetName+"_etadet_Rref").c_str(), &m_jet_etadet_Rref);
      m_tree->Branch((jetName+"_phidet_Rref").c_str(), &m_jet_phidet_Rref);
      m_tree->Branch((jetName+"_isRscanMatched").c_str(), &m_jet_isRscanMatched);
    }

    // truth
    if ( m_isMC && m_jetInfoSwitch->m_truthMatch ) {
      m_tree->Branch((jetName+"_isTruthMatched").c_str(),     &m_jet_isTruthMatched);
      m_tree->Branch((jetName+"_pT_true").c_str(),  &m_jet_pT_true);
      m_tree->Branch((jetName+"_eta_true").c_str(), &m_jet_eta_true);
      m_tree->Branch((jetName+"_phi_true").c_str(), &m_jet_phi_true);
      m_tree->Branch((jetName+"_E_true").c_str(),   &m_jet_E_true);
      m_tree->Branch((jetName+"_m_true").c_str(),   &m_jet_m_true);
    }
  //}
  } 
  return;
}

void DijetTreeBase::FillTriggerUser( const xAOD::EventInfo* eventInfo )
{
  if (eventInfo->isAvailable<std::vector<std::string>>("passedTriggersBeforePrescale")) {
    m_passedTriggersBeforePrescale = eventInfo->auxdata<std::vector<std::string>>("passedTriggersBeforePrescale");
  }
}

void DijetTreeBase::FillJetsUser( const xAOD::Jet* jet, const std::string& jetName )
{
  
  //if (jetName=="HLTjet") {
    //m_HLT_jet_pT.push_back(jet->pt()/GeV);
 
  //}else{
  // truth
  if (jetName.find("truth")!=std::string::npos) {
    if ( m_truthJetInfoSwitch->m_allJets ) {
      m_truth_jet_pT.push_back(jet->pt()/GeV);
      m_truth_jet_eta.push_back(jet->eta());
      m_truth_jet_phi.push_back(jet->phi());
      m_truth_jet_E.push_back(jet->e()/GeV);
      m_truth_jet_m.push_back(jet->m()/GeV);
    } else if ( m_truthJetInfoSwitch->m_leadingJet ) {
      if ( m_truth_jet_pT.size() ) return;
      m_truth_jet_pT.push_back(jet->pt()/GeV);
      m_truth_jet_eta.push_back(jet->eta());
      m_truth_jet_phi.push_back(jet->phi());
      m_truth_jet_E.push_back(jet->e()/GeV);
      m_truth_jet_m.push_back(jet->m()/GeV);
    }
  } else {  // reconstructed (-> matched truth)
    if ( m_jetInfoSwitch->m_allJets ) {
      m_jet_pT.push_back(jet->pt()/GeV);
      m_jet_eta.push_back(jet->eta());
      m_jet_phi.push_back(jet->phi());
      m_jet_E.push_back(jet->e()/GeV);
      m_jet_m.push_back(jet->m()/GeV);
      jet->isAvailable<float>("DetectorEta") ? m_jet_etadet.push_back( jet->auxdata<float>("DetectorEta") )
                                             : m_jet_etadet.push_back( -99 );
      jet->isAvailable<float>("DetectorPhi") ? m_jet_phidet.push_back( jet->auxdata<float>("DetectorPhi") )
                                             : m_jet_phidet.push_back( -99 );
      if( m_isMC && m_jetInfoSwitch->m_truthMatch ) {
        jet->isAvailable<char>("isTruthMatched") ? m_jet_isTruthMatched.push_back( jet->auxdata<char>("isTruthMatched") )
                                       : m_jet_isTruthMatched.push_back( false );
        jet->isAvailable<float>("pT_true") ? m_jet_pT_true.push_back( jet->auxdata<float>("pT_true")/GeV )
                                           : m_jet_pT_true.push_back( -99 );
        jet->isAvailable<float>("eta_true") ? m_jet_eta_true.push_back( jet->auxdata<float>("eta_true") )
                                            : m_jet_eta_true.push_back( -99 );
        jet->isAvailable<float>("phi_true") ? m_jet_phi_true.push_back( jet->auxdata<float>("phi_true") )
                                            : m_jet_phi_true.push_back( -99 );
        jet->isAvailable<float>("E_true") ? m_jet_E_true.push_back( jet->auxdata<float>("E_true")/GeV )
                                          : m_jet_E_true.push_back( -99 );
        jet->isAvailable<float>("m_true") ? m_jet_m_true.push_back( jet->auxdata<float>("m_true")/GeV )
                                          : m_jet_m_true.push_back( -99 );
                                 
      }
      if (m_jetInfoSwitch->m_Rscan){
        jet->isAvailable<char>("isRscanMatched") ? m_jet_isRscanMatched.push_back( jet->auxdata<char>("isRscanMatched") )
                                       : m_jet_isRscanMatched.push_back( false );
        jet->isAvailable<float>("pT_Rref") ? m_jet_pT_Rref.push_back( jet->auxdata<float>("pT_Rref")/GeV )
                                           : m_jet_pT_Rref.push_back( -99 );
        jet->isAvailable<float>("eta_Rref") ? m_jet_eta_Rref.push_back( jet->auxdata<float>("eta_Rref") )
                                            : m_jet_eta_Rref.push_back( -99 );
        jet->isAvailable<float>("phi_Rref") ? m_jet_phi_Rref.push_back( jet->auxdata<float>("phi_Rref") )
                                            : m_jet_phi_Rref.push_back( -99 );
        jet->isAvailable<float>("E_Rref") ? m_jet_E_Rref.push_back( jet->auxdata<float>("E_Rref")/GeV )
                                          : m_jet_E_Rref.push_back( -99 );
        jet->isAvailable<float>("m_Rref") ? m_jet_m_Rref.push_back( jet->auxdata<float>("m_Rref")/GeV )
                                          : m_jet_m_Rref.push_back( -99 );
        jet->isAvailable<float>("etadet_Rref") ? m_jet_etadet_Rref.push_back( jet->auxdata<float>("etadet_Rref") )
                                           : m_jet_etadet_Rref.push_back( -99 );
        jet->isAvailable<float>("phidet_Rref") ? m_jet_phidet_Rref.push_back( jet->auxdata<float>("phidet_Rref") )
                                           : m_jet_phidet_Rref.push_back( -99 );
      }
    } else if ( m_jetInfoSwitch->m_thirdJet ) {
      if ( m_jet_pT.size()>=3 ) return;
      m_jet_pT.push_back(jet->pt()/GeV);
      m_jet_eta.push_back(jet->eta());
      m_jet_phi.push_back(jet->phi());
      m_jet_E.push_back(jet->e()/GeV);
      m_jet_m.push_back(jet->m()/GeV);
      jet->isAvailable<float>("DetectorEta") ? m_jet_etadet.push_back( jet->auxdata<float>("DetectorEta") )
                                             : m_jet_etadet.push_back( -99 );
      jet->isAvailable<float>("DetectorPhi") ? m_jet_phidet.push_back( jet->auxdata<float>("DetectorPhi") )
                                             : m_jet_phidet.push_back( -99 );
      if( m_isMC && m_jetInfoSwitch->m_truthMatch ) {
        jet->isAvailable<char>("isTruthMatched") ? m_jet_isTruthMatched.push_back( jet->auxdata<char>("isTruthMatched") )
                                       : m_jet_isTruthMatched.push_back( false );
        jet->isAvailable<float>("pT_true") ? m_jet_pT_true.push_back( jet->auxdata<float>("pT_true")/GeV )
                                           : m_jet_pT_true.push_back( -99 );
        jet->isAvailable<float>("eta_true") ? m_jet_eta_true.push_back( jet->auxdata<float>("eta_true") )
                                            : m_jet_eta_true.push_back( -99 );
        jet->isAvailable<float>("phi_true") ? m_jet_phi_true.push_back( jet->auxdata<float>("phi_true") )
                                            : m_jet_phi_true.push_back( -99 );
        jet->isAvailable<float>("E_true") ? m_jet_E_true.push_back( jet->auxdata<float>("E_true")/GeV )
                                          : m_jet_E_true.push_back( -99 );
        jet->isAvailable<float>("m_true") ? m_jet_m_true.push_back( jet->auxdata<float>("m_true")/GeV )
                                          : m_jet_m_true.push_back( -99 );
      }
      if (m_jetInfoSwitch->m_Rscan){
        jet->isAvailable<char>("isRscanMatched") ? m_jet_isRscanMatched.push_back( jet->auxdata<char>("isRscanMatched") )
                                       : m_jet_isRscanMatched.push_back( false );
        jet->isAvailable<float>("pT_Rref") ? m_jet_pT_Rref.push_back( jet->auxdata<float>("pT_Rref")/GeV )
                                           : m_jet_pT_Rref.push_back( -99 );
        jet->isAvailable<float>("eta_Rref") ? m_jet_eta_Rref.push_back( jet->auxdata<float>("eta_Rref") )
                                            : m_jet_eta_Rref.push_back( -99 );
        jet->isAvailable<float>("phi_Rref") ? m_jet_phi_Rref.push_back( jet->auxdata<float>("phi_Rref") )
                                            : m_jet_phi_Rref.push_back( -99 );
        jet->isAvailable<float>("E_Rref") ? m_jet_E_Rref.push_back( jet->auxdata<float>("E_Rref")/GeV )
                                          : m_jet_E_Rref.push_back( -99 );
        jet->isAvailable<float>("m_Rref") ? m_jet_m_Rref.push_back( jet->auxdata<float>("m_Rref")/GeV )
                                          : m_jet_m_Rref.push_back( -99 );
        jet->isAvailable<float>("etadet_Rref") ? m_jet_etadet_Rref.push_back( jet->auxdata<float>("etadet_Rref") )
                                           : m_jet_etadet_Rref.push_back( -99 );
        jet->isAvailable<float>("phidet_Rref") ? m_jet_phidet_Rref.push_back( jet->auxdata<float>("phidet_Rref") )
                                           : m_jet_phidet_Rref.push_back( -99 );
      }
    } else if ( m_jetInfoSwitch->m_leadingDijet ) {
      if ( m_jet_pT.size()>=2 ) return;
      m_jet_pT.push_back(jet->pt()/GeV);
      m_jet_eta.push_back(jet->eta());
      m_jet_phi.push_back(jet->phi());
      m_jet_E.push_back(jet->e()/GeV);
      m_jet_m.push_back(jet->m()/GeV);
      jet->isAvailable<float>("DetectorEta") ? m_jet_etadet.push_back( jet->auxdata<float>("DetectorEta") )
                                             : m_jet_etadet.push_back( -99 );
      jet->isAvailable<float>("DetectorPhi") ? m_jet_phidet.push_back( jet->auxdata<float>("DetectorPhi") )
                                             : m_jet_phidet.push_back( -99 );
      if( m_isMC && m_jetInfoSwitch->m_truthMatch ) {
        jet->isAvailable<char>("isTruthMatched") ? m_jet_isTruthMatched.push_back( jet->auxdata<char>("isTruthMatched") )
                                       : m_jet_isTruthMatched.push_back( false );
        jet->isAvailable<float>("pT_true") ? m_jet_pT_true.push_back( jet->auxdata<float>("pT_true")/GeV )
                                           : m_jet_pT_true.push_back( -99 );
        jet->isAvailable<float>("eta_true") ? m_jet_eta_true.push_back( jet->auxdata<float>("eta_true") )
                                            : m_jet_eta_true.push_back( -99 );
        jet->isAvailable<float>("phi_true") ? m_jet_phi_true.push_back( jet->auxdata<float>("phi_true") )
                                            : m_jet_phi_true.push_back( -99 );
        jet->isAvailable<float>("E_true") ? m_jet_E_true.push_back( jet->auxdata<float>("E_true")/GeV )
                                          : m_jet_E_true.push_back( -99 );
        jet->isAvailable<float>("m_true") ? m_jet_m_true.push_back( jet->auxdata<float>("m_true")/GeV )
                                          : m_jet_m_true.push_back( -99 );
      }
      if (m_jetInfoSwitch->m_Rscan){
        jet->isAvailable<char>("isRscanMatched") ? m_jet_isRscanMatched.push_back( jet->auxdata<char>("isRscanMatched") )
                                       : m_jet_isRscanMatched.push_back( false );
        jet->isAvailable<float>("pT_Rref") ? m_jet_pT_Rref.push_back( jet->auxdata<float>("pT_Rref")/GeV )
                                           : m_jet_pT_Rref.push_back( -99 );
        jet->isAvailable<float>("eta_Rref") ? m_jet_eta_Rref.push_back( jet->auxdata<float>("eta_Rref") )
                                            : m_jet_eta_Rref.push_back( -99 );
        jet->isAvailable<float>("phi_Rref") ? m_jet_phi_Rref.push_back( jet->auxdata<float>("phi_Rref") )
                                            : m_jet_phi_Rref.push_back( -99 );
        jet->isAvailable<float>("E_Rref") ? m_jet_E_Rref.push_back( jet->auxdata<float>("E_Rref")/GeV )
                                          : m_jet_E_Rref.push_back( -99 );
        jet->isAvailable<float>("m_Rref") ? m_jet_m_Rref.push_back( jet->auxdata<float>("m_Rref")/GeV )
                                          : m_jet_m_Rref.push_back( -99 );
        jet->isAvailable<float>("etadet_Rref") ? m_jet_etadet_Rref.push_back( jet->auxdata<float>("etadet_Rref") )
                                           : m_jet_etadet_Rref.push_back( -99 );
        jet->isAvailable<float>("phidet_Rref") ? m_jet_phidet_Rref.push_back( jet->auxdata<float>("phidet_Rref") )
                                           : m_jet_phidet_Rref.push_back( -99 );
      }
    }
    //} 
  }

  return;
}

void DijetTreeBase::ClearTriggerUser()
{
  m_passedTriggersBeforePrescale.clear();
}

void DijetTreeBase::ClearJetsUser(const std::string& jetName)
{
  //if (jetName=="HLTjet") m_HLT_jet_pT.clear();
  //else{
  if (jetName.find("truth")!=std::string::npos) {
    m_truth_jet_pT.clear();
    m_truth_jet_eta.clear();
    m_truth_jet_phi.clear();
    m_truth_jet_E.clear();
    m_truth_jet_m.clear();
  } else {
    m_jet_pT.clear();
    m_jet_eta.clear();
    m_jet_phi.clear();
    m_jet_E.clear();
    m_jet_m.clear();
    m_jet_etadet.clear();
    m_jet_phidet.clear();
    m_jet_isTruthMatched.clear();
    m_jet_pT_true.clear();
    m_jet_eta_true.clear();
    m_jet_phi_true.clear();
    m_jet_E_true.clear();
    m_jet_m_true.clear();
    m_jet_pT_Rref.clear();
    m_jet_eta_Rref.clear();
    m_jet_phi_Rref.clear();
    m_jet_E_Rref.clear();
    m_jet_m_Rref.clear();
    m_jet_etadet_Rref.clear();
    m_jet_phidet_Rref.clear();
    m_jet_isRscanMatched.clear();
  }
  //}
}

