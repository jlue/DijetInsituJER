#pragma once

// EDM
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTracking/VertexContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODCore/ShallowCopy.h"
#include "xAODEventShape/EventShape.h"

// EventLoop
#include <EventLoop/StatusCode.h>
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

// xAH
#include "xAODAnaHelpers/HelperFunctions.h"
#include "xAODAnaHelpers/Algorithm.h"

#include "Utils/Utils.h"

class JetTriggerEmulator : public xAH::Algorithm
{
public:

  // this is a standard constructor
  JetTriggerEmulator (std::string className = "JetTriggerEmulator");

  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode histInitialize () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode changeInput (bool firstFile) { (void)firstFile; return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize () { return EL::StatusCode::SUCCESS; };

  /// @cond
  // this is needed to distribute the algorithm to the workers
  ClassDef(JetTriggerEmulator, 1);
  /// @endcond
  
public:
  
  // HLT string -> 1. its object container
  //               2. ET threshold
  //               3. etadet requirement
  
  std::string m_trigJetAlgo;
  
  std::string m_emulatedTriggers;
  std::string m_trigThresholdETs;
  std::string m_trigRequiredEtas;
  
  std::string m_emulatedSeeds;
  std::string m_seedThresholdETs;
  std::string m_seedRequiredEtas;
  
  /** @brief trigDecTool name for configurability if name is not default.  If empty, use the default name. If not empty, change the name. */
  bool m_checkTrigDecTool = false;
  
  std::string m_trigDecTool_name{""};

private:
  
  // jet trigger emulation
  std::string  m_triggerJetContainerName = "";
  
  std::vector<std::string> m_emulatedTriggerList;
  std::vector<std::string> m_emulatedSeedList;
  
  std::map<std::string,float> m_trigThresholdET;
  std::map<std::string,std::vector<float>> m_trigRequiredEta;
  
  std::map<std::string,std::string> m_trigSeed;
  std::map<std::string,float> m_seedThresholdET;
  std::map<std::string,std::vector<float>> m_seedRequiredEta;
  
  /** @brief Trigger decision tool.
   If you need to use a TDT that was previously created before this algorithm with a different name, set the name in m_trigDecTool_name.
  */
  asg::AnaToolHandle<Trig::TrigDecisionTool>    m_trigDecTool_handle{"Trig::TrigDecisionTool/TrigDecisionTool"}; //!
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfTool_handle{"TrigConf::xAODConfigTool/xAODConfigTool"}; //!

};

