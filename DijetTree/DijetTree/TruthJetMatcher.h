#pragma once

// EDM
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTracking/VertexContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODCore/ShallowCopy.h"
#include "xAODEventShape/EventShape.h"

// EventLoop
#include <EventLoop/StatusCode.h>
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include "AsgTools/AnaToolHandle.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

// xAH
#include "xAODAnaHelpers/HelperFunctions.h"
#include "xAODAnaHelpers/Algorithm.h"

#include "Utils/Utils.h"

class TruthJetMatcher : public xAH::Algorithm
{
public:

  // this is a standard constructor
  TruthJetMatcher (std::string className = "TruthJetMatcher");
  
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode histInitialize () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode changeInput (bool firstFile) { (void)firstFile; return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize () { return EL::StatusCode::SUCCESS; };
  
  /// @cond
  // this is needed to distribute the algorithm to the workers
  ClassDef(TruthJetMatcher, 1);
  /// @endcond
  
public:
  
  // reconstructed jets
  std::string m_recoContainerName;
  
  // truth jet matching
  std::string m_truthContainerName;
  float       m_truth_pT_min = 4.0;
  bool        m_doDeltaR = true;
  bool        m_doGhostAssoc = true;
  float       m_deltaRCut = 0.4;
  float       m_ghostAssocFracCut = 0.5;

  // systematics
  std::string m_inputAlgo = "";
  
private:
  
  void matchTruthJets( const xAOD::JetContainer* recoJets,
                       const xAOD::JetContainer* truthJets,
                       float truth_pT_min = 4.0,
                       bool doDeltaR = true,
                       float deltaRCut = 0.4,
                       bool doGhostAssoc = true,
                       float ghostAssocFracCut = 0.5 );
  
};

