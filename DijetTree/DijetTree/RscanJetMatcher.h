#pragma once

// EDM
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTracking/VertexContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODCore/ShallowCopy.h"
#include "xAODEventShape/EventShape.h"

// EventLoop
#include <EventLoop/StatusCode.h>
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include "AsgTools/AnaToolHandle.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

// xAH
#include "xAODAnaHelpers/HelperFunctions.h"
#include "xAODAnaHelpers/Algorithm.h"

#include "Utils/Utils.h"

class RscanJetMatcher : public xAH::Algorithm
{
public:

  // this is a standard constructor
  RscanJetMatcher (std::string className = "RscanJetMatcher");
  
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode histInitialize () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode changeInput (bool firstFile) { (void)firstFile; return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize () { return EL::StatusCode::SUCCESS; };
  
  /// @cond
  // this is needed to distribute the algorithm to the workers
  ClassDef(RscanJetMatcher, 1);
  /// @endcond
  
public:
  
  // reconstructed (Rscan, Rref) jets
  std::string m_RscanContainerName = "";
  std::string m_RrefContainerName  = "";
  
  // matching & isolation options
  float m_matchDeltaRCut = 0.2;
  float m_isoDeltaRCut   = 0.4;

  // systematics
  std::string m_inputAlgo = "";
  
private:
  
  const xAOD::Jet* matchRscanJet( const xAOD::Jet* RscanJet,
                                  const xAOD::JetContainer* RrefJets,
                                  float deltaRCut
  );

  bool isIsolated( const xAOD::Jet* jet,
                                  const xAOD::JetContainer* neighbourJets,
                                  float deltaRCut
  );
  
};

