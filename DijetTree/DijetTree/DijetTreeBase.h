#pragma once

#include "Utils/Utils.h"
#include "xAODAnaHelpers/HelpTreeBase.h"
#include "TTree.h"

namespace DijetTree
{

class TriggerInfoSwtich : public HelperClasses::InfoSwitch
{
public:
  bool m_passTriggersBeforePrescale = false;
  TriggerInfoSwtich(const std::string configStr) : InfoSwitch(configStr) { initialize(); };
protected:
  void initialize();
};

class JetInfoSwitch : public HelperClasses::InfoSwitch
{
public:
  bool m_leadingJet    = false;
  bool m_leadingDijet  = true;
  bool m_thirdJet      = true;
  bool m_allJets       = false;
  bool m_detectorEta   = false;
  bool m_detectorPhi   = false;
  bool m_truthMatch    = false;
  bool m_Rscan         = false;
  //bool m_HLT           = false;
  JetInfoSwitch(const std::string configStr) : InfoSwitch(configStr) { initialize(); };
protected:
  void initialize();
};
  
}

/**
 @brief Define and fill the TTrees.  Inherits from xAODAnaHelpers::HelpTreeBase
 */
class DijetTreeBase : public HelpTreeBase
{
private:
  
  // trigger decisions (beofre & after prescale)
  std::vector<std::string> m_passedTriggersBeforePrescale;
  std::vector<std::string> m_passedTriggers;
  
  // reconstructed jets (-> matched truth jets)
  std::vector<float> m_jet_pT;
  std::vector<float> m_jet_eta;
  std::vector<float> m_jet_phi;
  std::vector<float> m_jet_E;
  std::vector<float> m_jet_m;
  std::vector<float> m_jet_etadet;
  std::vector<float> m_jet_phidet;
  std::vector<bool>  m_jet_isTruthMatched;
  std::vector<float> m_jet_pT_true;
  std::vector<float> m_jet_eta_true;
  std::vector<float> m_jet_phi_true;
  std::vector<float> m_jet_E_true;
  std::vector<float> m_jet_m_true;
 
  // ref for Rscan
  std::vector<float> m_jet_pT_Rref;
  std::vector<float> m_jet_eta_Rref;
  std::vector<float> m_jet_phi_Rref;
  std::vector<float> m_jet_E_Rref;
  std::vector<float> m_jet_m_Rref;
  std::vector<float> m_jet_etadet_Rref;
  std::vector<float> m_jet_phidet_Rref;
  std::vector<bool> m_jet_isRscanMatched;

  // truth jets
  std::vector<float> m_truth_jet_pT;
  std::vector<float> m_truth_jet_eta;
  std::vector<float> m_truth_jet_phi;
  std::vector<float> m_truth_jet_E;
  std::vector<float> m_truth_jet_m;
 
  //HLT jets 
  //std::vector<float> m_HLT_jet_pT;
public:
  
  /** @brief Create the base HelpTreeBase instance */
  DijetTreeBase(xAOD::TEvent * event, TTree* tree, TFile* file);
  /** @brief Standard destructor*/
  ~DijetTreeBase() {
    delete m_triggerInfoSwitch;
    delete m_jetInfoSwitch;
    delete m_truthJetInfoSwitch;
  };
  
  /** @brief Connect the branches for jet-level variables */
  void AddTriggerUser( const std::string& detailStr );
  void AddJetsUser( const std::string& detailStr , const std::string& jetName );
  /** @brief Fill the TTree with trigger variables */
  void FillTriggerUser( const xAOD::EventInfo* eventInfo );
  /** @brief Fill the TTree with jet-level variables */
  void FillJetsUser( const xAOD::Jet* jet, const std::string& jetName );
  /** @brief Clear vectors used by trigger variables*/
  void ClearTriggerUser();
  /** @brief Clear vectors used by jet-level variables*/
  void ClearJetsUser( const std::string& jetName );
  
  DijetTree::TriggerInfoSwtich*  m_triggerInfoSwitch;
  DijetTree::JetInfoSwitch*  m_jetInfoSwitch;
  DijetTree::JetInfoSwitch*  m_truthJetInfoSwitch;
  
};

