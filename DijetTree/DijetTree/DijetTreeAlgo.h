#pragma once

// EDM
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTracking/VertexContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODCore/ShallowCopy.h"
#include "xAODEventShape/EventShape.h"

// EventLoop
#include <EventLoop/StatusCode.h>
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

// xAH
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODAnaHelpers/Algorithm.h>

// inlude the parent class header for tree
#include "DijetTree/DijetTreeBase.h"
#include "Utils/Utils.h"

class DijetTreeAlgo : public xAH::Algorithm
{
public:

  // this is a standard constructor
  DijetTreeAlgo (std::string className = "DijetTreeAlgo");
  ~DijetTreeAlgo() {}
  
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode histInitialize () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode changeInput (bool firstFile) { (void)firstFile; return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute () { return EL::StatusCode::SUCCESS; };
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize () { return EL::StatusCode::SUCCESS; };
  
  /// @cond
  // this is needed to distribute the algorithm to the workers
  ClassDef(DijetTreeAlgo, 1);
  /// @endcond
  
public:

  // vertex container
  std::string m_vertexContainerName = "PrimaryVertices";
  std::string m_truthVertexContainerName = "TruthVertices";
  
  // reconstructed jets
  std::string m_jetAlgo;
  std::string m_recoJetContainerName;
  std::string m_truthJetContainerName;
  //std::string m_HLTJetContainerName = "";
  
  // xAH base tree
  std::string m_eventDetailStr = "";
  std::string m_vertexDetailStr = "";
  std::string m_truthVertexDetailStr = "";
  std::string m_trigDetailStr   = "";
  std::string m_recoJetDetailStr  = "";
  std::string m_truthJetDetailStr = "";
  
  // systematics
  std::string m_jetSystematics = "";
  
public:
  
  // tree template
  std::map<std::string,DijetTreeBase*> m_trees; //!{
  
};
