#pragma once

// Bootstrap
#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH1FBootstrap.h"
#include "BootstrapGenerator/TH2FBootstrap.h"

// Utils
#include "Utils/Utils.h"
#include "Utils/Config.h"
#include "Utils/HistUtils.h"

// other include(s)
#include "Uncertainties/Smoother.h"

class Uncertainties
{
  
public:
  Uncertainties(std::string jetAlgo, Config settings);
  
public:
  
  void evaluateUncertainties();
  void evaluateStats();
  void evaluateSysts();
  void evaluateGroups();

  void writeOutputs(TFile* outputFile);
  
private:
  
  // read settings & initialize tools
  void readSettings();
  void initializeTools();

  // get histograms (bootstraps) from file
  void getBootstrap(TFile* file, std::string histPath, TH2FBootstrap*& boot) {
    file->GetObject(histPath.c_str(),boot);
  }
  void getBootstrap(TFile* file, std::string histPath, TH1FBootstrap*& boot) {
    file->GetObject(histPath.c_str(),boot);
  }
  void getHistogram(TFile* file, std::string histPath, TH1F*& hist) {
    file->GetObject(histPath.c_str(),hist);
    if (not hist) {
      TH1FBootstrap* boot = nullptr;
      file->GetObject(histPath.c_str(),boot);
      if (not boot) return;
      hist = (TH1F*)boot->GetNominal();
    }
  }
  void getHistogram(TFile* file, std::string histPath, TH2F*& hist) {
    file->GetObject(histPath.c_str(),hist);
    if (not hist) {
      TH2FBootstrap* boot = nullptr;
      file->GetObject(histPath.c_str(),boot);
      if (not boot) return;
      hist = (TH2F*)boot->GetNominal();
    }
  }

private:
  
  Config m_settings;

  // jet algo
  std::string m_jetAlgo;

  // input files
  // jet (pT,eta) bin map
  TFile* m_binsFile;
  // statistical uncertainties input file
  TFile* m_statFile;
  // input file containing systematic & its nominal values
  std::unordered_map<std::string,TFile*> m_systFile;
  std::unordered_map<std::string,TFile*> m_nomFile;

  // jet (pT,eta) bin map
  std::string m_pT_eta_binsPath;
  std::string m_pT_mapPath;
  std::string m_eta_mapPath;
  TH2F*       m_pT_eta_bins;
  TProfile2D* m_pT_map;
  TProfile2D* m_eta_map;

  // systematics to process
  std::vector<std::string> m_systematics;
  // flag to indicate if systematic uses bootstrap method
  std::unordered_map<std::string,bool> m_bootstrap;
  // variations for each systematic to be applied
  std::unordered_map<std::string,std::vector<std::string>> m_systVars;

  // group systematics together into "groups"
  std::vector<std::string> m_groups;
  std::unordered_map<std::string,std::vector<std::string>> m_grpComps;

  // smoothing
  bool m_doSmoothing;
  Smoother* m_smoother;
  bool m_useError;
  float m_width_x;
  float m_width_y;
  
  // comparison histogram(bootstrap) paths
  std::string m_ref_insituJER_statPath;
  std::unordered_map<std::string,std::string> m_ref_insituJER_nomPath;
  std::unordered_map<std::string,std::string> m_ref_insituJER_systPath;
  // probe
  std::string m_probe_insituJER_statPath;
  std::unordered_map<std::string,std::string> m_probe_insituJER_nomPath;
  std::unordered_map<std::string,std::string> m_probe_insituJER_systPath;
  
  // results
  // statistical uncertainties
  TH1F* m_ref_insituJER_statUnc;
  TH2F* m_probe_insituJER_statUnc;
  // variations summed
  std::unordered_map<std::string,TH1F*> m_ref_insituJER_systUnc;
  std::unordered_map<std::string,TH2F*> m_probe_insituJER_systUnc;
  // total
  TH1F* m_ref_insituJER_systTotUnc;
  TH2F* m_probe_insituJER_systTotUnc;
  
private:
  
  std::string systVarStr(std::string syst, std::string var) { return syst+"__"+var; }
  
};

namespace UncTools
{
  TH1F* relStatErr(TH1F* stat);
  TH1F* relSystShift(TH1F* nominal, TH1F* shift);
  TH1F* relSystShift(TH1FBootstrap* nominal, TH1FBootstrap* shift);
  
  TH2F* relStatErr(TH2F* stat);
  TH2F* relStatErr(TH2FBootstrap* stat);
  TH2F* relSystShift(TH2F* nominal, TH2F* shift);
  TH2F* relSystShift(TH2FBootstrap* nominal, TH2FBootstrap* shift);
} 


