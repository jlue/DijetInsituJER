#pragma once

#include "TMath.h"
#include "Utils/Utils.h"

class Smoother: public TNamed 
{

public:

  //  Constructor parameters:
  //  Width of unit Gaussian kernel
  Smoother(float widthx=0.10, float widthy=0.10);
  ~Smoother();

  virtual void useError(bool useError=true) { m_useError=useError; }
  virtual void setCoarseBins(std::vector<float> pT_bins, std::vector<float> eta_bins) { m_pT_bins_coarse = pT_bins; m_eta_bins_coarse = eta_bins; }
  virtual void setSmoothBins(std::vector<float> pT_bins, std::vector<float> eta_bins) { m_pT_bins_smooth = pT_bins; m_eta_bins_smooth = eta_bins; }
  virtual void setLog(bool logx=true, bool logy=false) { m_logx=logx; m_logy=logy; }
  virtual void doSmooth(bool smoothx=true, bool smoothy=false) { m_smoothx=smoothx; m_smoothy=smoothy; }
  virtual void setRange(float xmin=-99, float xmax=-99, float ymin=-99, float ymax=-99) { m_xmin=xmin; m_xmax=xmax; m_ymin=ymin, m_ymax=ymax; }

  float getSmoothValue(TGraph2DErrors *g, float x, float y);
  float getSmoothStat(TGraph2DErrors *g, int myPoint, float x, float y);

  TH2F *makeSmoothHist(TString hname);
  TH2F *smoothGraph2D(TGraph2DErrors *g2d, TString hname);

private:

  // width of kernel
  float m_width_x, m_width_y;

  // eta bins
  std::vector<float> m_eta_bins_coarse;
  std::vector<float> m_pT_bins_coarse;
  std::vector<float> m_eta_bins_smooth;
  std::vector<float> m_pT_bins_smooth;

  // whether to smooth
  bool m_smoothx, m_smoothy;
  // whether the smoothing width is logarithmic or not
  bool m_logx, m_logy;
  // whether to use the errors in the smoothing
  bool m_useError;
  // range to consider in smoothing
  float m_xmin, m_xmax, m_ymin, m_ymax;

};

