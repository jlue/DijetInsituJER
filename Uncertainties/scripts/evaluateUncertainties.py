#!/usr/bin/env python

# jet algo inputs/configs
jetAlgos = ['AntiKt10LCTopoTrimmedPtFrac5SmallR20']
inFiles = {
        'AntiKt10LCTopoTrimmedPtFrac5SmallR20': 'Uncertainties/files.txt'
        }
configs = {
        'AntiKt10LCTopoTrimmedPtFrac5SmallR20': ['Uncertainties/largeR.cfg']
        }


# execute C++ program
if __name__ == "__main__":


    # argument parser
    import argparse
    parser = argparse.ArgumentParser(description='Uncertainties')
    # output directory
    parser.add_argument('--outDir', dest='outDir', type=str, default='./',
    help='output directory')
    # parse arguments
    args = parser.parse_args()

    # output directory
    import os
    command = '{}/bin/evaluateUncertainties'.format(os.environ['DijetInsituJER_DIR']) 
    outDir = os.path.abspath(args.outDir)
    if not os.path.isdir(outDir):
        os.makedirs(outDir)

    # run over jet algorithms
    import subprocess
    for jetAlgo in jetAlgos:
        inFiles = os.path.abspath('{}/data/{}'.format(os.environ['DijetInsituJER_DIR'],inFiles[jetAlgo]))
        configs = [os.path.abspath('{}/data/{}'.format(os.environ['DijetInsituJER_DIR'],cfg)) for cfg in configs[jetAlgo]]
        outFile = os.path.join(outDir,jetAlgo+'.root')
        subprocess.call([command,jetAlgo,','.join(configs),inFiles,outFile])
