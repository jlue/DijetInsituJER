#include "Uncertainties/Uncertainties.h"

#include "Utils/Utils.h"
#include "Utils/Config.h"

int main(int argc, char** argv) 
{

  // parse arguments
  std::string jetAlgo = argv[1];
  std::vector<std::string> configFilePaths = vectorizeStr(argv[2],",");
  std::string inputFilesPath = argv[3];
  std::string outputFilePath = argv[4];
  bool overwrite = (argc>=6);

  // config files
  Config settings = Config();
  for (const auto& configFilePath : configFilePaths) {
    settings.addFile(configFilePath);
  }
  // input files
  settings.addFile(inputFilesPath);

  // uncertainties evaluator
  Uncertainties* jerUnc = new Uncertainties(jetAlgo, settings);
  // evaluate uncertainties & save results
  jerUnc->evaluateUncertainties();

  // output file 
  TFile* outputFile = openOutputFile(outputFilePath,overwrite);
  jerUnc->writeOutputs(outputFile);

  // outputFile->Clone();

}
