#include "Uncertainties/Smoother.h"

//  Constructor parameters:
//  - Relative width of Gaussina kernel: DeltaPt/Pt
Smoother::Smoother(float widthx, float widthy) : 
  m_width_x(widthx), m_width_y(widthy),
  m_smoothx(true), m_smoothy(false),
  m_logx(true), m_logy(false),
  m_useError(false),
  m_xmin(-99), m_xmax(-99), m_ymin(-99), m_ymax(-99)
{}
Smoother::~Smoother() {}

float Smoother::getSmoothValue(TGraph2DErrors *g, float x, float y) {
  float sumw=0, sumwz=0;
  int y_bin = getBinNumber(y,m_eta_bins_coarse);
  for (int i=0; i<g->GetN();++i) {
    float xi=g->GetX()[i], yi=g->GetY()[i], zi=g->GetZ()[i];      
    float dx = (x-xi)/m_width_x; if (m_logx) dx /= x;
    float dy = (y-yi)/m_width_y; if (m_logy) dy /= y;
    float dr = sqrt(dx*dx+dy*dy);
    if (!m_smoothy) {
      dr = abs(dx);
      int yi_bin = getBinNumber(yi,m_eta_bins_coarse);
      if (y_bin!=yi_bin) continue;
    }
    float wi = TMath::Gaus(dr);
    if (m_useError) { 
      float ezi = g->GetEZ()[i];
      if (ezi==0) continue;
      wi *= 1.0/pow(ezi,2);
    }
    sumw += wi; sumwz += wi*zi;
  }
  return sumwz/sumw;
}

TH2F* Smoother::makeSmoothHist(TString hname) {
  int NpT_bins = m_pT_bins_smooth.size()-1;
  int Neta_bins = m_eta_bins_smooth.size()-1;
  TH2F* h = new TH2F(hname,"",NpT_bins,&m_pT_bins_smooth[0],Neta_bins,&m_eta_bins_smooth[0]);
  h->SetXTitle("jet p_{T} [GeV]"); h->SetYTitle("jet #eta_{det}");
  return h;
}

TH2F* Smoother::smoothGraph2D( TGraph2DErrors* g2d, TString hname ) {

  TH2F* h = makeSmoothHist(hname);

  for (int ieta=1;ieta<=h->GetNbinsY();++ieta)
    for (int ipt=1;ipt<=h->GetNbinsX();++ipt) {
      float eta=h->GetYaxis()->GetBinCenter(ieta);
      float pt=h->GetXaxis()->GetBinCenter(ipt);

      float myPt = pt;
      if ( m_xmin!=-99 && pt < m_xmin )  {
        myPt = h->GetXaxis()->GetBinCenter(h->GetXaxis()->FindBin(m_xmin+1e-6));
      }
      if ( m_xmax!=-99 && pt > m_xmax )  {
        myPt = h->GetXaxis()->GetBinCenter(h->GetXaxis()->FindBin(m_xmax+1e-6));
      }
      float myEta=eta;
      if ( m_ymax!=-99 && fabs(eta) > m_ymax ){
        if ( eta>0 )  myEta = h->GetYaxis()->GetBinCenter( h->GetYaxis()->FindBin(m_ymax-1e-6) );
        else          myEta = h->GetYaxis()->GetBinCenter( h->GetYaxis()->FindBin(-m_ymax+1e-6) );
      }

      float R=getSmoothValue(g2d,myPt,myEta);

      h->SetBinContent(ipt,ieta,R);
    }

  return h;
}
