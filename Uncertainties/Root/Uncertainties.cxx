#include "Uncertainties/Uncertainties.h"

Uncertainties::Uncertainties(std::string jetAlgo, Config settings) :
  m_jetAlgo(jetAlgo),
  m_settings(settings)
{
  readSettings();
  initializeTools();
}

void Uncertainties::readSettings()
{
  // jet (pT,eta) bin map
  m_binsFile = openInputFile(m_settings.getStr("Bins.Input"));
  m_pT_mapPath = m_settings.getStr("Bins.pT_map");
  m_eta_mapPath = m_settings.getStr("Bins.eta_map");
  m_pT_eta_binsPath = m_settings.getStr("Bins.pT_eta_bins");

  // statistical uncertainties input
  m_statFile = openInputFile( m_settings.getStr("Statistical.Input") );
  m_ref_insituJER_statPath = m_settings.getStrV( "Statistical.Nominal" ).at(0);
  m_probe_insituJER_statPath = m_settings.getStrV( "Statistical.Nominal" ).at(1);
  
  // systematic uncertainties
  m_systematics = m_settings.getStrV( "Systematics" );
  for (const auto& syst : m_systematics) {  // for each systematic
    // read the input file
    m_systFile[syst] = openInputFile( m_settings.getStr(syst+".Input") );
    // flag to use bootstrap or not
    m_bootstrap[syst] = m_settings.getBool( syst+".Bootstrap" );
    // get applied variations
    m_systVars[syst] = m_settings.getStrV( syst+".Variations" );
    // nominal & variation value paths
    m_ref_insituJER_nomPath[syst] = m_settings.getStrV( syst+".Nominal" ).at(0);
    m_probe_insituJER_nomPath[syst] = m_settings.getStrV( syst+".Nominal" ).at(1);
    for (const auto& var : m_systVars.at(syst)) {
      std::string systVar = syst+"__"+var;
      m_ref_insituJER_systPath[systVar] = m_settings.getStrV( syst+"."+var ).at(0);
      m_probe_insituJER_systPath[systVar] = m_settings.getStrV( syst+"."+var ).at(1);
    }
  }  // systematic

  // systematic groups
  m_groups = m_settings.getStrV( "Groups" );
  for (const auto& grp : m_groups) {  // for each systematic
    // get applied variations
    m_grpComps[grp] = m_settings.getStrV( grp+".Components" );
  }

  // smoothing
  m_doSmoothing = m_settings.getBool ( "Smooth", false );
  m_useError = m_settings.getBool ( "Smoother.UseError", true );
  m_width_x = m_settings.getNum( "Smoother.WidthX", 0.10 );
  m_width_y = m_settings.getNum( "Smoother.WidthY", 0.10 );
}
void Uncertainties::initializeTools()
{
  m_binsFile->GetObject(m_pT_eta_binsPath.c_str(),m_pT_eta_bins);
  std::vector<float> pT_bins_coarse = getBins(m_pT_eta_bins,1);
  std::vector<float> eta_bins_coarse = getBins(m_pT_eta_bins,2);

  // Smoother
  m_smoother = new Smoother(m_width_x,m_width_y);
  std::vector<float> pT_bins_smooth = makeLogVec(100,150,3000);
  std::vector<float> eta_bins_smooth = make_lin(100,0.0,2.5);
  m_smoother->setCoarseBins(pT_bins_coarse,eta_bins_coarse);
  m_smoother->setSmoothBins(pT_bins_smooth,eta_bins_smooth);
  m_smoother->doSmooth(true,true);
  m_smoother->setLog(true,false);
  m_smoother->setRange(150.0,3000.0,0.0,2.5);
  m_smoother->useError(m_useError);
}

void Uncertainties::evaluateUncertainties()
{
  m_binsFile->GetObject(m_pT_mapPath.c_str(),m_pT_map);
  m_binsFile->GetObject(m_eta_mapPath.c_str(),m_eta_map);

  // statistical
  TH1F* ref_insituJER_stat = nullptr; getHistogram(m_statFile,m_ref_insituJER_statPath.c_str(),ref_insituJER_stat);
  m_ref_insituJER_statUnc = UncTools::relStatErr(ref_insituJER_stat);
  TH2FBootstrap* probe_insituJER_stat = getObject<TH2FBootstrap>(m_probe_insituJER_statPath.c_str(),m_statFile);
  m_probe_insituJER_statUnc = UncTools::relStatErr(probe_insituJER_stat);
  
  // systematic uncertainties
  for (const auto& syst : m_systematics) {
    for (const auto& var : m_systVars.at(syst)) {
      std::string systVar = syst+"__"+var;
      info("    "+systVar,"Uncertainties::evaluateUncertainties");
      if (m_bootstrap.at(syst)) {
        // TH1FBootstrap* ref_insituJER_nominal = nullptr, *ref_insituJER_shift = nullptr;
        // m_systFile.at(syst)->GetObject( (m_ref_insituJER_nomPath.at(syst)).c_str(), ref_insituJER_nominal );
        // m_systFile.at(syst)->GetObject( (m_ref_insituJER_systPath.at(systVar)).c_str(), ref_insituJER_shift );
        // m_ref_insituJER_systUnc[systVar] = UncTools::relSystShift(ref_insituJER_nominal, ref_insituJER_shift);
        TH2FBootstrap* probe_insituJER_nominal = nullptr, *probe_insituJER_shift = nullptr;
        m_systFile.at(syst)->GetObject( (m_probe_insituJER_nomPath.at(syst)).c_str(), probe_insituJER_nominal );
        m_systFile.at(syst)->GetObject( (m_probe_insituJER_systPath.at(systVar)).c_str(), probe_insituJER_shift );
        TH2F* probe_insituJER_systUnc = UncTools::relSystShift(probe_insituJER_nominal, probe_insituJER_shift);
        probe_insituJER_systUnc->SetName(Form("probe_%s",systVarStr(syst,var).c_str()));
        m_probe_insituJER_systUnc[systVar] = probe_insituJER_systUnc;
      } else {
        // TH1F* ref_insituJER_nominal = nullptr, *ref_insituJER_shift = nullptr;
        // getHistogram(m_systFile.at(syst),m_ref_insituJER_nomPath.at(syst).c_str(),ref_insituJER_nominal);
        // getHistogram(m_systFile.at(syst),m_ref_insituJER_systPath.at(systVar).c_str(),ref_insituJER_shift);
        // m_ref_insituJER_systUnc[systVar] = UncTools::relSystShift(ref_insituJER_nominal, ref_insituJER_shift);
        TH2F* probe_insituJER_nominal = nullptr, *probe_insituJER_shift = nullptr;
        getHistogram(m_systFile.at(syst),m_probe_insituJER_nomPath.at(syst).c_str(),probe_insituJER_nominal);
        getHistogram(m_systFile.at(syst),m_probe_insituJER_systPath.at(systVar).c_str(),probe_insituJER_shift);
        TH2F* probe_insituJER_systUnc = UncTools::relSystShift(probe_insituJER_nominal, probe_insituJER_shift);
        probe_insituJER_systUnc->SetName(Form("probe_%s",systVarStr(syst,var).c_str()));
        m_probe_insituJER_systUnc[systVar] = probe_insituJER_systUnc;
      }
    }
  }

  std::cout << "Done individual" << std::endl;

  // summed systematic uncertainties
  for (const auto& grp : m_groups) {
    TH2F* probe_insituJER_grpUnc = nullptr;
    for (const auto& comp : m_grpComps.at(grp)) {
      std::string systVar = comp;
      if (m_doSmoothing) systVar += "_Smooth";
      if (not probe_insituJER_grpUnc) {
        probe_insituJER_grpUnc = (TH2F*)m_probe_insituJER_systUnc.at(systVar)->Clone(("probe_SystematicUncertainty__"+grp).c_str());
        probe_insituJER_grpUnc->Reset();
      }
      probe_insituJER_grpUnc = HistUtils::addInQuadrature(probe_insituJER_grpUnc,m_probe_insituJER_systUnc.at(systVar));
    }
    m_probe_insituJER_systUnc[grp] = probe_insituJER_grpUnc;
  }

  std::cout << "Done grouped systematics" << std::endl;
  
  // finished
  return;
}

void Uncertainties::writeOutputs(TFile* outputFile)
{
  outputFile->cd();
  if (outputFile->GetDirectory(m_jetAlgo.c_str())) outputFile->rmdir(m_jetAlgo.c_str());
  TDirectory* outputFolder = outputFile->mkdir(m_jetAlgo.c_str(),m_jetAlgo.c_str());
  outputFolder->cd();
  // statistical
  m_ref_insituJER_statUnc->Write("ref_StatisticalUncertainty__Total",TObject::kOverwrite);
  m_probe_insituJER_statUnc->Write("probe_StatisticalUncertainty__Total",TObject::kOverwrite);
  // systematic
  for (const auto& systUnc : m_probe_insituJER_systUnc) {
    systUnc.second->Write();
  }
  info("Uncertainties histograms written.","Uncertainties::writeOutput");
  outputFile->Close();
}

TH1F* UncTools::relStatErr(TH1F *stat)
{
  TH1F* statUnc = (TH1F*)stat->Clone();
  for ( int ix=0 ; ix < statUnc->GetNbinsX() ; ++ix ) {
    statUnc->SetBinContent(ix+1,stat->GetBinError(ix+1)/stat->GetBinContent(ix+1));
    statUnc->SetBinError(ix+1,0.0);
  }
  return statUnc;
}
TH1F* UncTools::relSystShift(TH1F* nominal, TH1F* shift)
{
  TH1F* error = new TH1F(*shift);
  error->Add(nominal, -1);
  error->Divide(nominal);
  return error;
}
TH1F* UncTools::relSystShift(TH1FBootstrap* nominal, TH1FBootstrap* shift)
{
  TH1FBootstrap* error = new TH1FBootstrap(*shift);
  error->Add(nominal, -1);
  error->SetValBootstrapMean();
  error->SetErrBootstrapRMS();
  error->Divide(nominal);
  return (TH1F*)error->GetNominal();
}

TH2F* UncTools::relStatErr(TH2F *stat)
{
  TH2F* statUnc = (TH2F*)stat->Clone();
  for ( int ix=0 ; ix < statUnc->GetNbinsX() ; ++ix ) {
    for ( int iy=0 ; iy < statUnc->GetNbinsY() ; ++iy ) {
      statUnc->SetBinContent(ix+1,iy+1,stat->GetBinError(ix+1,iy+1)/stat->GetBinContent(ix+1,iy+1));
      statUnc->SetBinError(ix+1,iy+1,0.0);
    }
  }
  return statUnc;
}
TH2F* UncTools::relStatErr(TH2FBootstrap *stat)
{
  stat->SetErrBootstrapRMS();
  stat->SetValBootstrapMean();
  TH2F* statUnc = (TH2F*)stat->GetNominal()->Clone();
  for ( int ix=0 ; ix < statUnc->GetNbinsX() ; ++ix ) {
    for ( int iy=0 ; iy < statUnc->GetNbinsY() ; ++iy ) {
      statUnc->SetBinContent(ix+1,iy+1,statUnc->GetBinError(ix+1,iy+1)/statUnc->GetBinContent(ix+1,iy+1));
      statUnc->SetBinError(ix+1,iy+1,0.0);
    }
  }
  return statUnc;
}
TH2F* UncTools::relSystShift(TH2F* nominal, TH2F* shift)
{
  TH2F* error = new TH2F(*shift);
  // TEMPORARY HACK TO MAKE SURE JER >= 0 : sometimes the response fit returns sigma = -0.15 (when it's really 15%)
  HistUtils::abs(error);
  HistUtils::abs(nominal);
  error->Add(nominal, -1);
  error->Divide(nominal);
  return error;
}
TH2F* UncTools::relSystShift(TH2FBootstrap* nominal, TH2FBootstrap* shift)
{
  TH2FBootstrap* error = new TH2FBootstrap(*shift);
  error->Add(nominal, -1);
  error->SetValBootstrapMean();
  error->SetErrBootstrapRMS();
  error->Divide(nominal);
  return (TH2F*)error->GetNominal();
}
