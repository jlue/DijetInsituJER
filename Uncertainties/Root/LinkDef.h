#include "Uncertainties/Uncertainties.h"
#include "Uncertainties/Smoother.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ class Uncertainties+;
#pragma link C++ class Smoother+;

#endif


