#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include "Rtypes.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TStyle.h"
#include "TString.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TH1.h"
#include "TH2.h"
#include "TAttAxis.h"

namespace plt
{

class Figure;
class Frame;
class Plot;
class Legend;

enum class color
{
  // b & w
  black      = kBlack,
  white      = kWhite,
  light_grey = kGray,
  light_gray = kGray,
  grey       = kGray+1,
  gray       = kGray+1,
  dark_grey  = kGray+2,
  dark_gray  = kGray+2,
  // primary
  red        = 2,
  green      = 3,
  dark_green = kGreen+2,
  blue       = 1,
  light_blue = kBlue-9,
  dark_blue  = kBlue+2,
  // secondary
  orange   = kOrange,
  yellow   = kYellow,
  spring   = kSpring,
  teal     = kTeal,
  cyan     = kCyan,
  azure    = kAzure,
  purple   = kViolet,
  violet   = kViolet,
  mangenta = kMagenta,
  pink     = kPink,
};

struct margin
{
  struct left
  {
    left( unsigned int size ) : size(size) {}
    unsigned int size;
  };
  struct right
  {
    right( unsigned int size ) : size(size) {}
    unsigned int size;
  };
  struct bottom
  {
    bottom( unsigned int size ) : size(size) {}
    unsigned int size;
  };
  struct top
  {
    top( unsigned int size ) : size(size) {}
    unsigned int size;
  };
};

struct text_font
{

  text_font(unsigned int typeface) :
    typeface(typeface),
    color(plt::color::black),
    size(24)
  {}

  text_font operator|( const plt::color& color ) const
  {
    auto colored = text_font( *this );
    colored.color = color;
    return colored;
  }

  text_font operator|( unsigned int size ) const
  {
    auto resized = text_font( *this );
    resized.size = size;
    return resized;
  }

  void apply(TPad& pad) const;
  void apply(TLegend& leg) const;

  unsigned int typeface;
  plt::color   color;
  unsigned int size;

};

struct font
{
  static const text_font sans;
  static const text_font serif;
  static const text_font mono;
};

enum class emphasis
{
  regular = 0,
  italic = 1,
  bold = 2
};

struct marker_attributes
{
  marker_attributes( unsigned int style );

  marker_attributes operator|( const plt::color& clr ) const;
  marker_attributes operator|( unsigned int s ) const;

  unsigned int style;
  plt::color   color;
  unsigned int size;
};

struct line_attributes
{
  line_attributes(unsigned int style);

  line_attributes operator|(const plt::color& color) const;
  line_attributes operator|(unsigned int width) const;

  unsigned int style;
  plt::color   color;
  unsigned int width;
};

struct fill_attributes
{
  fill_attributes(double alpha);

  fill_attributes operator|(const plt::color& color) const;

  double     alpha;
  plt::color color;
};
using fill = fill_attributes;

struct marker
{
  static const plt::marker_attributes none;
  static const plt::marker_attributes full_circle;
  static const plt::marker_attributes full_triangle_up;
  static const plt::marker_attributes full_triangle_down;
  static const plt::marker_attributes full_square;
  static const plt::marker_attributes full_diamond;
  static const plt::marker_attributes full_cross;
  static const plt::marker_attributes full_cross_x;
};

struct line
{
  static const plt::line_attributes none;
  static const plt::line_attributes solid;
  static const plt::line_attributes dashed;
  static const plt::line_attributes dotted;
  static const plt::line_attributes dashdot;
};

struct axis
{
  
  static constexpr unsigned int x = 1;
  static constexpr unsigned int y = 2;
  static constexpr unsigned int z = 3;

  enum class scale
  {
    lin = 0,
    log = 1
  };

  struct ticks
  {
    enum class mode {
      manual   = 0,
      optimize = 1,
    };

    enum draw {
      one_side = 0,
      both_sides = 1,
      other_side = 2
    };

    ticks(int prim, int scnd, int tert);

    ticks operator|(int length) const;
    ticks operator|(const plt::axis::ticks::mode& mode) const;
    ticks operator|(const plt::axis::ticks::draw& draw) const;

    int  primary;
    int  secondary;
    int  tertiary;
    int  length;
    mode optimization;
    draw drawing;
  };

  struct grid_attributes
  {
    grid_attributes(unsigned int style);

    grid_attributes operator|(const plt::color& color) const;
    grid_attributes operator|(unsigned int width) const;

    unsigned int style;
    plt::color   color;
    unsigned int width;
  };

  struct grid
  {
    static const plt::axis::grid_attributes none;
    static const plt::axis::grid_attributes solid;
    static const plt::axis::grid_attributes dashed;
    static const plt::axis::grid_attributes dotted;
    static const plt::axis::grid_attributes dashdot;
  };

  static std::pair<double,double> limits(double min, double max);

  axis(const std::string& title);

  axis operator|(const std::pair<double,double>& minmax) const;
  axis operator|(const std::vector<double>& bdrs) const;
  axis operator|(const std::vector<std::string>& lbls) const;
  axis operator|(const plt::axis::scale& scl) const;

  void apply(TAxis& ax) const;

  std::string              title;
  std::pair<double,double> range;
  std::vector<double>      borders;
  std::vector<std::string> labels;
  bool                     log;
};

struct error_attributes
{
  error_attributes( unsigned int style );

  error_attributes operator|( const plt::line_attributes& ln ) const;
  error_attributes operator|( const plt::fill_attributes& f ) const;

  unsigned int style;
  plt::line_attributes line;
  plt::fill_attributes fill;
};

struct error
{
  static const error_attributes none;
  static const error_attributes bar;
  static const error_attributes boxed_band;
  static const error_attributes jagged_band;
  static const error_attributes smooth_band;
};

std::shared_ptr<plt::Figure> figure(unsigned int w=800, unsigned int h=600);

template <typename T, typename... Args>
std::shared_ptr<T> plot(Args&&... args);

class Figure
{

friend class Frame;
friend class Plot;
friend class Legend;

public:
  Figure(unsigned int w, unsigned int h);
  ~Figure() = default;

  // set properties

  void set( plt::text_font txt );

  void set( plt::margin::left lm );
  void set( plt::margin::right rm );
  void set( plt::margin::bottom bm );
  void set( plt::margin::top tm );

  std::shared_ptr<Frame>                           frame();
  std::vector<std::vector<std::shared_ptr<Frame>>> subplots(unsigned int nrows, unsigned int ncols, std::vector<unsigned int> hs={}, std::vector<unsigned int> ws={});
  std::shared_ptr<Frame>                           subplot(unsigned int row, unsigned int col=1);

  void sharex(bool share=true);
  void sharey(bool share=true);

  std::shared_ptr<Legend> legend(double x1, double x2, double y1, double y2);

  TCanvas* make();

  void open(const std::string& file);
  void save(const std::string& file);
  void save();
  void close();

  void clear();

  // getters

  double width() const;
  double height() const;
  size_t nrows() const;
  size_t ncols() const;

  size_t get_current_row() const;
  size_t get_current_col() const;

  unsigned int get_col_absw(unsigned int col) const;
  unsigned int get_row_absh(unsigned int row) const;
  double get_col_relw(unsigned int col) const;
  double get_row_relh(unsigned int row) const;

  unsigned int get_axis_absw(unsigned int col) const;
  unsigned int get_axis_absh(unsigned int row) const;
  double get_axis_relw(unsigned int col) const;
  double get_axis_relh(unsigned int row) const;

  // normalized values

  unsigned int get_lm() const;
  unsigned int get_rm() const;
  unsigned int get_tm() const;
  unsigned int get_bm() const;
  double get_lmfrac() const;
  double get_rmfrac() const;
  double get_tmfrac() const;
  double get_bmfrac() const;

  // ROOT objects

  TStyle*  getStyle() const;
  TCanvas* getCanvas() const;

protected:
  // style
  std::unique_ptr<TStyle> m_style;
  std::unique_ptr<TCanvas> m_canvas;
  std::vector<std::vector<std::shared_ptr<Frame>>> m_frames;

  // canvas
  unsigned int m_width;
  unsigned int m_height;

  // text
  plt::text_font      m_txtfnt;

  // margins
  plt::margin::left   m_lmargin;
  plt::margin::right  m_rmargin;
  plt::margin::bottom m_bmargin;
  plt::margin::top    m_tmargin;

  // frames
  // (x, y) coordinate
  std::vector<double> m_framex;
  std::vector<double> m_framey;
  // width, height
  std::vector<double> m_framew;
  std::vector<double> m_frameh;
  // axis sharing
  bool m_sharex;
  bool m_sharey;

  // current frame @ (row, col)
  std::shared_ptr<Frame> m_frame;
  unsigned int m_row;
  unsigned int m_col;

  // legend
  std::shared_ptr<Legend> m_legend;

  // output file
  std::string m_file;

};

class Frame
{

friend class Figure;
friend class Plot;
friend class Legend;

public:
  Frame();
  ~Frame() = default;

  void set( unsigned int dim, const plt::axis& ax );
  void setx( const plt::axis& xaxis );
  void sety( const plt::axis& yaxis );
  void setz( const plt::axis& zaxis );

  void set( unsigned int dim, const plt::axis::ticks& axticks );
  void setx( const plt::axis::ticks& xticks );
  void sety( const plt::axis::ticks& yticks );

  void set( unsigned int dim, const plt::axis::grid_attributes& axgrid );
  void setx( const plt::axis::grid_attributes& xgrid );
  void sety( const plt::axis::grid_attributes& ygrid );

  void add(std::shared_ptr<Plot> plot);

  void update();

  TH1*  getAxis() const;
  TPad* getPad() const;

protected:
  // void importFrom( const Frame& from, plt::axis dim );
  void drawOn( Figure& fig );
  
protected:
  std::shared_ptr<TPad> m_pad;
  std::shared_ptr<TH2>  m_axes;
  std::vector<std::shared_ptr<Plot>> m_plots;

  plt::axis m_xaxis;
  plt::axis m_yaxis;
  plt::axis m_zaxis;

  plt::axis::grid_attributes m_xgrid;
  plt::axis::grid_attributes m_ygrid;

  plt::axis::ticks m_xticks;
  plt::axis::ticks m_yticks;

};

class Plot
{

friend class Figure;
friend class Frame;
friend class Legend;

public:
  void label(const std::string& label);

  void set( plt::marker_attributes mk );
  void set( plt::line_attributes ln );
  void set( plt::fill_attributes f );
  void set( plt::error_attributes err );


protected:
  std::string getLabel() const;

  void drawOn(Frame& frame);

protected:
  // label
  std::string m_label;

  // marker
  plt::marker_attributes m_marker;
  // line
  // fill
  // error

};

class Legend
{

friend class Figure;
friend class Frame;
friend class Plot;

public:
  Legend();
  ~Legend() = default;

  void split(unsigned int ncols);

  void entry(std::shared_ptr<Plot> plot);

protected:
  void drawOn(Figure& figure);

protected:
  Figure* m_figure;

  unsigned int m_x1, m_x2;
  unsigned int m_y1, m_y2;

  std::unique_ptr<TLegend> m_legend;
  unsigned int m_ncols;

  std::vector<std::shared_ptr<Plot>> m_entries; 

};

}