#include "AnalysisHelpers/PlotLib.h"

// plt

// margin

// text

const plt::text_font plt::font::sans  = plt::text_font(43);
const plt::text_font plt::font::serif = plt::text_font(13);
const plt::text_font plt::font::mono  = plt::text_font(103);

void plt::text_font::apply(TPad& pad) const
{
  pad.SetTextFont(typeface);
  pad.SetTextSize(size);
  pad.SetTextColor(color);
}

// axis

// marker

plt::marker_attributes::marker_attributes( unsigned int style ) :
  style(style),
  color(plt::color::black),
  size(24)
{}

plt::marker_attributes plt::marker_attributes::operator|( const plt::color& color ) const
{
  auto colored = marker_attributes( *this );
  colored.color = color;
  return colored;
}

plt::marker_attributes plt::marker_attributes::operator|( unsigned int size ) const
{
  auto resized = marker_attributes( *this );
  resized.size = size;
  return resized;
}

// line

plt::line_attributes::line_attributes( unsigned int style ) :
  style(style),
  color(plt::color::black),
  width(2)
{}

plt::line_attributes plt::line_attributes::operator|( const plt::color& color ) const
{
  auto colored = line_attributes(*this);
  colored.color = color;
  return colored;
}

plt::line_attributes plt::line_attributes::operator|( unsigned int size ) const
{
  auto resized = line_attributes(*this);
  resized.width = size;
  return resized;
}

const plt::marker_attributes plt::marker::none = plt::marker_attributes(0) | 0;
const plt::marker_attributes plt::marker::full_circle = plt::marker_attributes(20);
const plt::marker_attributes plt::marker::full_square = plt::marker_attributes(21);
const plt::marker_attributes plt::marker::full_triangle_up = plt::marker_attributes(22);
const plt::marker_attributes plt::marker::full_triangle_down = plt::marker_attributes(23);
const plt::marker_attributes plt::marker::full_diamond = plt::marker_attributes(33);
const plt::marker_attributes plt::marker::full_cross = plt::marker_attributes(34);
const plt::marker_attributes plt::marker::full_cross_x = plt::marker_attributes(47);

const plt::line_attributes plt::line::none    = plt::line_attributes(0) | 0;
const plt::line_attributes plt::line::solid   = plt::line_attributes(1);
const plt::line_attributes plt::line::dashed  = plt::line_attributes(2);
const plt::line_attributes plt::line::dotted  = plt::line_attributes(3);
const plt::line_attributes plt::line::dashdot = plt::line_attributes(4);

// fill

plt::fill_attributes::fill_attributes(double alpha) :
  alpha(alpha),
  color(plt::color::light_blue)
{}

plt::fill_attributes plt::fill_attributes::operator|(const plt::color& color) const
{
  auto colored = fill_attributes( *this );
  colored.color = color;
  return colored;
}

// error

// axis

std::pair<double,double> plt::axis::limits(double min, double max)
{
  return std::make_pair<double,double>(std::move(min),std::move(max));
}

plt::axis::axis(const std::string& title):
  title(title),
  range(plt::axis::limits(0.0,1.0)),
  log(false)
{}

plt::axis plt::axis::operator|(const std::pair<double,double>& lim) const
{
  auto ranged = plt::axis(*this);
  ranged.range = lim;
  return ranged;
}

plt::axis plt::axis::operator|(const std::vector<double>& bdrs) const
{
  auto bordered = plt::axis(*this);
  bordered.borders = bdrs;
  return bordered;
}

plt::axis plt::axis::operator|(const std::vector<std::string>& lbls) const
{
  auto labeled = plt::axis(*this);
  labeled.labels = lbls;
  return labeled;
}

plt::axis plt::axis::operator|(const plt::axis::scale& scl) const
{
  auto scaled = plt::axis(*this);
  scaled.log = static_cast<bool>(scl);
  return scaled;
}

plt::axis::ticks::ticks(int prim, int scnd, int tert) : 
  primary(prim),
  secondary(scnd),
  tertiary(tert),
  optimization(mode::optimize),
  drawing(draw::one_side)
{}

plt::axis::ticks plt::axis::ticks::operator|(int length) const
{
  auto out = ticks(*this);
  out.length = length;
  return out;
}

plt::axis::ticks plt::axis::ticks::operator|(const plt::axis::ticks::mode& mode) const
{
  auto out = ticks(*this);
  out.optimization = mode;
  return out;
}

plt::axis::ticks plt::axis::ticks::operator|(const plt::axis::ticks::draw& draw) const
{
  auto out = ticks(*this);
  out.drawing = draw;
  return out;
}

// grid

plt::axis::grid_attributes::grid_attributes( unsigned int style ) :
  style(style),
  color(plt::color::black),
  width(1)
{}

plt::axis::grid_attributes plt::axis::grid_attributes::operator|( const plt::color& color ) const
{
  auto colored = grid_attributes(*this);
  colored.color = color;
  return colored;
}

plt::axis::grid_attributes plt::axis::grid_attributes::operator|( unsigned int size ) const
{
  auto resized = grid_attributes(*this);
  resized.width = size;
  return resized;
}

const plt::axis::grid_attributes plt::axis::grid::none    = plt::axis::grid_attributes(0) | 0;
const plt::axis::grid_attributes plt::axis::grid::solid   = plt::axis::grid_attributes(1);
const plt::axis::grid_attributes plt::axis::grid::dashed  = plt::axis::grid_attributes(2);
const plt::axis::grid_attributes plt::axis::grid::dotted  = plt::axis::grid_attributes(3);
const plt::axis::grid_attributes plt::axis::grid::dashdot = plt::axis::grid_attributes(4);

// plt::error::error( unsigned int style ) :
//   m_style(style),
//   m_line(plt::line_attributes(plt::color::white) | 0),
//   m_fill(plt::fill(plt::color::white) * 0.0)
// {}

// plt::error plt::error::operator|( const plt::line_attributes& ln ) const
// {
//   auto lined = error( *this );
//   lined.m_line = ln;
//   return lined;
// }

// plt::error plt::error::operator|( const plt::fill& f ) const
// {
//   auto filled = error( *this );
//   filled.m_fill = f;
//   return filled;
// }

// PlotLib

plt::Figure::Figure(unsigned int w, unsigned int h) :
  m_style(std::make_unique<TStyle>()),
  m_canvas(std::make_unique<TCanvas>()),
  m_width(w),
  m_height(h),
  m_file(""),
  m_txtfnt( plt::font::sans | plt::color::black | 0.04*h ),
  m_lmargin(plt::margin::left(0.15*w)),
  m_rmargin(plt::margin::right(0.05*w)),
  m_bmargin(plt::margin::bottom(0.15*h)),
  m_tmargin(plt::margin::top(0.05*h)),
  m_sharex(false),
  m_sharey(false)
{
  m_canvas->SetCanvasSize(w,h);
  if (!m_canvas->IsBatch()) m_canvas->SetWindowSize(w + (w - m_canvas->GetWw()), h + (h - m_canvas->GetWh()));
  m_canvas->cd();
}

void plt::Figure::set( plt::text_font txtfnt )
{
  m_txtfnt = txtfnt;
}
void plt::Figure::set( plt::margin::left lm )
{
  m_lmargin = lm;
}
void plt::Figure::set( plt::margin::right rm )
{
  m_rmargin = rm;
}
void plt::Figure::set( plt::margin::bottom bm )
{
  m_bmargin = bm;
}
void plt::Figure::set( plt::margin::top tm )
{
  m_tmargin = tm;
}

double plt::Figure::get_lmfrac() const
{
  return m_lmargin.size / double(m_width);
}

double plt::Figure::get_rmfrac() const
{
  return m_rmargin.size / double(m_width);
}

double plt::Figure::get_bmfrac() const
{
  return m_bmargin.size / double(m_height);
}

double plt::Figure::get_tmfrac() const
{
  return m_tmargin.size / double(m_height);
}

std::vector<std::vector<std::shared_ptr<plt::Frame>>> plt::Figure::subplots(unsigned int nrows, unsigned int ncols, std::vector<unsigned int> hs, std::vector<unsigned int> ws)
{
  // check default arguments
  if (!hs.size()) hs = std::vector<unsigned int>(nrows,1);
  if (!ws.size()) ws = std::vector<unsigned int>(ncols,1);

  // compute total widhts & heights
  double totw = 0.0, toth = 0.0;
  for (const auto& w : ws) totw += w;
  for (const auto& h : hs) toth += h;

  // save frame widths & heights
  for (const auto& w : ws) m_framew.push_back(w/totw);
  for (const auto& h : hs) m_frameh.push_back(h/toth);

  // save frame x & y coordinates
  // top left -> bottom right
  float x = 0.0, y = 1.0;
  m_framex = {x}; m_framey = {y};
  for (int icol=0 ; icol<ncols ; ++icol) {
    x += m_framew[icol];
    m_framex.push_back(x);
  }
  for (int irow = 0 ; irow<nrows ; ++irow) {
    y -= m_frameh[irow];
    m_framey.push_back(y);
  }

  // make frames
  m_frames.clear();
  m_frames = std::vector<std::vector<std::shared_ptr<Frame>>>(nrows,std::vector<std::shared_ptr<Frame>>(ncols));
  for (auto& row : m_frames) {
    for (auto& frame : row) {
      frame = std::make_shared<plt::Frame>();
    }
  }
  this->subplot(1,1);

  return m_frames;
}

void plt::Figure::sharex(bool share)
{
  m_sharex = share;
  if (share) {
    for (int irow=0 ; irow < nrows() ; ++irow) {
      for (int icol = 0; icol < ncols() ; ++icol) {
        // m_frames[irow][icol]->importFrom(*m_frame, plt::axis::x);
      }
    }
  }
}

void plt::Figure::sharey(bool share)
{
  m_sharey = share;
  if (share) {
    for (int irow=0 ; irow < nrows() ; ++irow) {
      for (int icol = 0; icol < ncols() ; ++icol) {
        // m_frames[irow][icol]->importFrom(*m_frame, plt::axis::y);
      }
    }
  }
}

std::shared_ptr<plt::Frame> plt::Figure::subplot(unsigned int row, unsigned int col)
{
  m_row = row; m_col = col;
  return m_frames[row-1][col-1];
}

size_t plt::Figure::nrows() const
{
  return m_frames.size();
}

size_t plt::Figure::ncols() const
{
  return m_frames[0].size();
}

TCanvas* plt::Figure::make()
{
  // m_style->cd();
  m_canvas->cd();
  m_canvas->Draw();

  for (int irow=0 ; irow < nrows() ; ++irow) {
    for (int icol=0; icol < ncols() ; ++icol) {
      m_canvas->cd();
      subplot(irow+1,icol+1)->drawOn(*this);
    }
  }

  // draw legend
  m_canvas->cd();
  if (m_legend) m_legend->drawOn(*this);

  m_canvas->Modified();
  m_canvas->Update();
  return m_canvas.get();
}

void plt::Figure::save(const std::string& file)
{
  this->make()->Print(file.c_str());
}

plt::Frame::Frame() :
  m_pad(std::make_shared<TPad>("","",0,0,1,1)),
  m_axes(std::make_shared<TH2D>("","",10,0,10,1,0,1)),
  m_xaxis(plt::axis("x")),
  m_xticks(plt::axis::ticks(5,0,3) | 12),
  m_xgrid(plt::axis::grid::dotted | 2),
  m_yaxis(plt::axis("y")),
  m_yticks(plt::axis::ticks(5,0,3) | 12),
  m_ygrid(plt::axis::grid::dotted | 2),
  m_zaxis(plt::axis("z"))
{
  m_axes->SetDirectory(0);
  m_pad->SetFillStyle(4000);
}

void plt::Frame::set( unsigned int dim, const plt::axis& ax )
{
  switch(dim) {
    case 1:
      m_xaxis = ax;
      break;
    case 2:
      m_yaxis = ax;
      break;
    case 3:
      m_zaxis = ax;
      break;
  }
}

void plt::Frame::set( unsigned int dim, const plt::axis::ticks& axticks )
{
  switch(dim) {
    case 1:
      m_xticks = axticks;
      break;
    case 2:
      m_yticks = axticks;
      break;
  }
}

void plt::Frame::set( unsigned int dim, const plt::axis::grid_attributes& axgrid )
{
  switch(dim) {
    case 1:
      m_xgrid = axgrid;
      break;
    case 2:
      m_ygrid = axgrid;
      break;
  }
}


// void plt::Frame::importFrom( const Frame& from, plt::axis dim )
// {
//   if (dim==plt::axis::x) {
//     m_axes->GetXaxis()->SetTitle(from.m_axes->GetXaxis()->GetTitle());
//     m_axes->GetXaxis()->Set(10,from.m_axes->GetXaxis()->GetXmin(),from.m_axes->GetXaxis()->GetXmax());
//     m_axes->GetXaxis()->SetTitleOffset(from.m_axes->GetXaxis()->GetTitleOffset());
//     m_pad->SetLogx(from.m_pad->GetLogx());
//     m_pad->SetGridx(from.m_pad->GetGridx());
//   } else if (dim==plt::axis::y) {
//     m_axes->GetYaxis()->SetTitle(from.m_axes->GetYaxis()->GetTitle());
//     m_axes->GetYaxis()->Set(10,from.m_axes->GetYaxis()->GetXmin(),from.m_axes->GetXaxis()->GetXmax());
//     m_axes->GetYaxis()->SetTitleOffset(from.m_axes->GetYaxis()->GetTitleOffset());
//     m_pad->SetLogy(from.m_pad->GetLogy());
//     m_pad->SetGridy(from.m_pad->GetGridy());
//   }
// }

void plt::Plot::set( plt::marker_attributes mk )
{
}

void plt::Plot::set( plt::line_attributes ln )
{
}

void plt::Plot::set( plt::fill_attributes f )
{
}

void plt::Plot::set( plt::error_attributes err )
{
}

void plt::Frame::drawOn( plt::Figure& fig )
{
  // go to pad
  m_pad->cd();

  // get its coordinates
  auto row = fig.m_row, col = fig.m_col;

  // (x0, y0)
  auto x1 = fig.m_framex[col-1], x2 = fig.m_framex[col];
  auto y1 = fig.m_framey[row], y2 = fig.m_framey[row-1];
  auto framew = fig.m_framew[col-1];
  auto frameh = fig.m_frameh[row-1];

  // global margins
  auto lm = fig.get_lmfrac()+x1;
  auto rm = fig.get_rmfrac()+1-x2;
  auto bm = fig.get_bmfrac()+y1;
  auto tm = fig.get_tmfrac()+1-y2;
  // tick length normalizations
  auto xticknorm = framew*fig.m_width - fig.m_lmargin.size - fig.m_rmargin.size;
  auto yticknorm = frameh*fig.m_height - fig.m_bmargin.size - fig.m_tmargin.size;
  // axes sharing
  if (fig.m_sharex) {
    // re-compute global margins
    bm = fig.get_bmfrac()+(1-fig.get_tmfrac()-fig.get_bmfrac())*y1;
    tm = fig.get_tmfrac()+(1-fig.get_tmfrac()-fig.get_bmfrac())*(1-y2);
    // correct tick length normalizations
    xticknorm *= 2.0;
    yticknorm = frameh*(fig.m_height - fig.m_bmargin.size - fig.m_tmargin.size);
    if (row!=fig.nrows()) {
      // erase shared x-axis
      m_axes->GetXaxis()->SetLabelSize(0);
      m_axes->GetXaxis()->SetTitleSize(0);
    }
    if (row!=1) {
      // erase over-lapping y-axis label
      m_axes->GetYaxis()->ChangeLabel(-1,-1,0);
    }
  }
  if (fig.m_sharey) {
    // re-compute margins
    lm = fig.get_lmfrac()+(1-fig.get_lmfrac()-fig.get_rmfrac())*x1;
    rm = fig.get_rmfrac()+(1-fig.get_lmfrac()-fig.get_rmfrac())*(1-x2);
    // correct normalizations
    xticknorm = framew*(fig.m_width - fig.m_lmargin.size - fig.m_rmargin.size);
    yticknorm *= 2.0;
    if (col!=1) {
      m_axes->GetYaxis()->SetLabelSize(0);
      m_axes->GetYaxis()->SetTitleSize(0);
    }
  }
  // set margins
  m_pad->SetLeftMargin(lm);
  m_pad->SetRightMargin(rm);
  m_pad->SetTopMargin(tm);
  m_pad->SetBottomMargin(bm);
  // scale tick lengths
  m_axes->GetXaxis()->SetTickLength(m_xticks.length/xticknorm);
  m_axes->GetYaxis()->SetTickLength(m_yticks.length/yticknorm);

  // apply axes
  // m_xaxis.apply(*m_pad, *m_axes->GetXaxis());
  // m_yaxis.apply(*m_pad, *m_axes->GetYaxis());
  // m_zaxis.apply(*m_pad, *m_axes->GetZaxis());

  // apply grid
  // m_xgrid.apply(*m_pad);
  // m_ygrid.apply(*m_pad);

  // apply text font
  fig.m_txtfnt.apply(*m_pad);

  // draw pad & axis
  m_pad->Draw();
  m_axes->Draw("axis");
  m_pad->Modified();
  m_pad->Update();
}


void plt::Plot::drawOn( plt::Frame& axis )
{}

void plt::Legend::drawOn( plt::Figure& figure )
{}

std::shared_ptr<plt::Figure> plt::figure(unsigned int w, unsigned int h)
{
  return std::make_shared<plt::Figure>(w,h);
}