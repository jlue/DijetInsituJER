# Dijet *in situ* jet energy resolution
- DijetTree: Dijet Ntuple production
	- xAODAnaHelpers: xAOD analysis framework
- DijetHistograms: Ntuple -> histograms
	- BootstrapGenerator: generate weight toys
- TriggerCombination: combined triggers' lumionsity & efficiency calculations
- MeasureResolution: JER measurement
	- JetResponseFitter: fitting tools
- Uncertainties: systematic uncertainties
- Plots: beautified plots
* Utils: helper functions/classes

## Compilation & run instructions
Framework can be compiled within the ATLAS AnalysisBase environment, which follows the following general structure:
```
Code/
├── Code/  
│   └── Code.h
├── Root/  
│   ├── LinkDef.h  
│   └── Code.cxx
├── data/ 
├── share/  
├── util/  
├── scripts/  
└── CMakeLists.txt  
```
``Code/* Root/*`` class header/source files  
``util`` executables  
``data`` configuration/data files  
``share`` same as above, use case depending on package  
``python`` python modules  
``scripts`` job-driving/secondary scripts  