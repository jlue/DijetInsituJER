#pragma once

#include <memory>

#include "TH1F.h"
#include "TH2F.h"

class TemplateFitter
{
  
public:

  TemplateFitter();
  ~TemplateFitter() = default;

  void set_nsigma(double nsigma);
  void set_nsmooth(int nsmooth);

  // separate s,r-fitting
  void  fit(TH1& data, TH2& sig_templates, TH1* bkg_hist = nullptr);

  float get_r() { return m_r; }
  float get_r_error() { return m_rerr; }

  std::shared_ptr<TH1> get_chi2() {
    return m_chi2;
  }

  std::shared_ptr<TH1> get_data() { return m_full_data; }
  std::shared_ptr<TH1> get_template() { return m_full_template; }

  std::shared_ptr<TH1> get_fit_data() { return m_fit_data; }
  std::shared_ptr<TH1> get_fit_template() { return m_fit_template; }
  
private:

  double m_nsigma = 1.5;
  int m_nsmooth = 1;

  // (reco,template) chi2 map
  std::shared_ptr<TH1> m_chi2;
  std::shared_ptr<TH1> m_ndf;

  // reco histogram
  std::shared_ptr<TH1> m_full_data;
  std::shared_ptr<TH1> m_fit_data;

  std::shared_ptr<TH1> m_full_template;
  std::shared_ptr<TH1> m_fit_template;

  std::shared_ptr<TH1> m_full_signal;
  std::shared_ptr<TH1> m_fit_signal;

  std::shared_ptr<TH1> m_bkg;

  // best-fit (s,r) value
  double m_r=1.0;
  double m_rerr=1.0;

};
