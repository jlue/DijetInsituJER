#!/usr/bin/env python

import os

import ROOT

from AnalysisHelpers import config
from Measurement import binning

def main(args):

  msmt_cfg = config.read(args.config)

  # load MC
  mc_hists_file, mc_hists_folder = msmt_cfg['mc']['path'].split(':')
  mc_hists_file = ROOT.TFile.Open(mc_hists_file,'read')
  mc_hists = mc_hists_file.GetDirectory(mc_hists_folder)

  # load MC
  mc_templates_file, mc_templates_folder = msmt_cfg['mc']['path'].split(':')
  mc_templates_file = ROOT.TFile.Open(mc_templates_file,'read')
  mc_templates = mc_templates_file.GetDirectory(mc_templates_folder)

  # load data
  data_hists_file, data_hists_folder = msmt_cfg['data']['path'].split(':')
  data_hists_file = ROOT.TFile.Open(data_hists_file,'read')
  data_hists = data_hists_file.GetDirectory(data_hists_folder)

  pT_eta_bins = data_hists.Get(msmt_cfg['mc']['bins'])
  pT_bins, eta_bins = binning.from_hist(pT_eta_bins)

  print(len(pT_bins))

  fitter = ROOT.TemplateFitter()
  fitter.set_nsigma(1.25)
  fitter.set_nsmooth(0)

  c = ROOT.TCanvas('c','c',800,600)
  ref_cfg = msmt_cfg['reference']
  ref_msmt = binning.to_hist('ref_r', pT_bins)

  for ipT, pT_bin in enumerate(pT_bins):
    data_hist = data_hists.Get(os.path.join('pt{}etaRefBin'.format(ipT+1),ref_cfg['data']))
    bkg_hist  = mc_hists.Get(os.path.join('pt{}etaRefBin'.format(ipT+1),ref_cfg['bkg']))
    sig_templates = mc_templates.Get(os.path.join('pt{}etaRefBin'.format(ipT+1),ref_cfg['sig']))
    ptcl_hist = data_hists.Get(os.path.join('pt{}etaRefBin'.format(ipT+1),ref_cfg['ptcl']))

    fitter.fit(data_hist, sig_templates, bkg_hist)
    ref_msmt.SetBinContent(ipT+1, fitter.get_r())
    print(fitter.get_r())

    full_data = fitter.get_data()
    full_template = fitter.get_template()

    fit_data  = fitter.get_fit_data()
    fit_template = fitter.get_fit_template()

    full_data.SetMarkerStyle(ROOT.kFullCircle)
    full_data.SetMarkerColor(ROOT.kBlack)
    full_data.SetLineColor(ROOT.kBlack)
    full_data.Draw('pe')

    # fit_data.SetMarkerStyle(ROOT.kFullCircle)
    # fit_data.SetMarkerColor(ROOT.kBlack)
    # fit_data.SetLineColor(ROOT.kBlack)
    # fit_data.Draw('pe')

    fit_template.SetMarkerStyle(0)
    fit_template.SetLineColor(ROOT.kRed)
    fit_template.Draw('hist same')
    c.Print('{}_fit.pdf'.format(ipT))

    fitter.get_chi2().Draw()
    c.Print('{}_chi2.pdf'.format(ipT))
    c.Clear()

  msmt_file = ROOT.TFile.Open(args.output, 'recreate' if args.force else 'create')
  msmt_file.WriteObject(ref_msmt,'ref_r')


if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()

  parser.add_argument("config", type=str)

  parser.add_argument("--response", action='store_true', default=True)
  parser.add_argument("--balance",  action='store_true', default=False)
  parser.add_argument("--folding",  action='store_true', default=True)

  parser.add_argument("--folding",  action='store_true', default=True)

  parser.add_argument("--output", type=str, default='measurement.root')
  parser.add_argument("--force",  action='store_true', default=False)

  # parser.add_argument("--dir", type=str, required=True)
  
  args = parser.parse_args()

  main(args)