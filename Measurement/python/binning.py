import os
import array

import ROOT

from AnalysisHelpers import config

def from_hist(hist):
  if hist.GetDimension()==1:
    nxbins = hist.GetNbinsX()
    xbins = [(hist.GetXaxis().GetBinLowEdge(ibin+1),hist.GetXaxis().GetBinLowEdge(ibin+2)) for ibin in range(nxbins)]
    return xbins
  if hist.GetDimension()==2:
    nxbins = hist.GetNbinsX()
    xbins = [(hist.GetXaxis().GetBinLowEdge(ibin+1),hist.GetXaxis().GetBinLowEdge(ibin+2)) for ibin in range(nxbins)]
    nybins = hist.GetNbinsY()
    ybins = [(hist.GetYaxis().GetBinLowEdge(ibin+1),hist.GetYaxis().GetBinLowEdge(ibin+2)) for ibin in range(nybins)]
    return xbins, ybins
  if hist.GetDimension()==3:
    nxbins = hist.GetNbinsX()
    xbins = [(hist.GetXaxis().GetBinLowEdge(ibin+1),hist.GetXaxis().GetBinLowEdge(ibin+2)) for ibin in range(nxbins)]
    nybins = hist.GetNbinsY()
    ybins = [(hist.GetYaxis().GetBinLowEdge(ibin+1),hist.GetYaxis().GetBinLowEdge(ibin+2)) for ibin in range(nybins)]
    nzbins = hist.GetNbinsZ()
    zbins = [(hist.GetZaxis().GetBinLowEdge(ibin+1),hist.GetZaxis().GetBinLowEdge(ibin+2)) for ibin in range(nzbins)]
    return xbins, ybins, zbins
    
def to_hist(name, xbinning, ybinning=None):
  xbins = array.array('d',sorted(list(set([x for t in xbinning for x in t]))))
  if ybinning:
    ybins = array.array('d',sorted(list(set([x for t in ybinning for x in t]))))
    return ROOT.TH2F(name, name, len(xbins)-1, xbins, len(ybins)-1, ybins)
  else:
    return ROOT.TH1F(name, name, len(xbins)-1, xbins)