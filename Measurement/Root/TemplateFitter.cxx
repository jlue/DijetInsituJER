#include "Measurement/TemplateFitter.h"

#include "AnalysisHelpers/HistogramUtils.h"

// constructor
TemplateFitter::TemplateFitter() :
	m_nsigma(1.5),
	m_nsmooth(1)
{}

void TemplateFitter::set_nsigma(double nsigma)
{
	m_nsigma = nsigma;
}

void TemplateFitter::set_nsmooth(int nsmooth)
{
	m_nsmooth = nsmooth;
}

// fit s,r-templates
void TemplateFitter::fit(TH1& data, TH2& signal_templates, TH1* bkg_hist)
{
	m_full_data = HistogramUtils::clone(data);
	auto rb = HistogramUtils::rebin(*m_full_data);
	HistogramUtils::normalize(*m_full_data);

	if (bkg_hist) m_bkg = HistogramUtils::clone(*bkg_hist);
	HistogramUtils::rebin(*bkg_hist, rb);

	// set range on data & bkg
	auto std_dev = m_full_data->GetStdDev();
	auto mean = m_full_data->GetMean();
	auto xmin = mean - m_nsigma * std_dev, xmax = mean + m_nsigma * std_dev;

	// fit data is limited to range & normalized to 1.0
	m_fit_data = HistogramUtils::clone(*m_full_data);
	m_fit_data->GetXaxis()->SetRangeUser(xmin, xmax);

	m_chi2 = HistogramUtils::projectX(signal_templates);
	m_ndf = HistogramUtils::projectX(signal_templates);
	m_chi2->Reset();
	m_ndf->Reset();

	for (int ir=0 ; ir<m_chi2->GetNbinsX() ; ++ir) {

		// get candidate template( r = X )
		auto candidate_template = HistogramUtils::projectY(signal_templates,ir+1);
		// if bkg histogram exists, add it
		if (m_bkg) candidate_template->Add(m_bkg.get());
		// set the same range & normalization as data 
		HistogramUtils::rebin(*candidate_template,rb);
		HistogramUtils::normalize(*candidate_template);
		candidate_template->GetXaxis()->SetRangeUser(xmin, xmax);

		double chi2;
		int ndf, igood;
    m_fit_data->Chi2TestX(candidate_template.get(), chi2, ndf, igood, "WW");
		m_chi2->SetBinContent(ir+1,chi2);
		m_ndf->SetBinContent(ir+1,ndf);
	}

	// minimize chi2
	if (m_nsmooth) m_chi2->Smooth(m_nsmooth);
	m_r = m_chi2->GetBinCenter(m_chi2->GetMinimumBin());
	m_fit_template = HistogramUtils::projectY(signal_templates,m_chi2->GetMinimumBin());
	HistogramUtils::rebin(*m_fit_template,rb);
	HistogramUtils::normalize(*m_fit_template);
	m_fit_template->GetXaxis()->SetRangeUser(xmin, xmax);

	m_full_template = HistogramUtils::projectY(signal_templates,m_chi2->GetMinimumBin());
	if (m_bkg) m_full_template->Add(m_bkg.get());
	HistogramUtils::rebin(*m_full_template,rb);

	return;
}
