################################################################################
# Project: Measurement
################################################################################
atlas_subdir( Measurement )

find_package( ROOT COMPONENTS Core RIO Hist Tree File MathCore Physics )
find_package( AnalysisBase QUIET )

atlas_depends_on_subdirs( PUBLIC
                          AnalysisHelpers
                          )

atlas_add_root_dictionary ( MeasurementLib MeasurementDictSource
                            ROOT_HEADERS Measurement/*.h Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT )

# build a shared library
atlas_add_library( MeasurementLib Measurement/*.h Root/*.cxx ${MeasurementDictSource}
                   PUBLIC_HEADERS Measurement
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES}
                   AnalysisHelpers
                   )

# Data files
atlas_install_python_modules( python/*.py )
atlas_install_scripts( scripts/*.py )
atlas_install_data( share/* )
atlas_install_data( data/* )
