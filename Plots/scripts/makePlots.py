#!/usr/bin/python

from Plots import results
from Plots import uncertainties
#from Plots import triggers

if __name__=="__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--inputs', type=str,
                        help='inputs file list path')
    parser.add_argument('--config', type=str,
                        help='config file path')
    parser.add_argument('--plots', nargs='*', type=str,
                        help='plots to make')
    parser.add_argument('--name', type=str, default='',
                        help='output file name'
    )
    args = parser.parse_args()

    import configparser
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read([args.inputs,args.config])

    macro = {
        #"triggerEfficiencies": triggers.triggerEfficiencies,
        "insitu": results.insitu,
        "NSC": results.NSC,
        "systematics": uncertainties.groupedSystematics
    }

    for plot in args.plots:
        macro[plot](config)
