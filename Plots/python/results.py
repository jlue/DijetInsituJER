from Plots import helpers as h
import ROOT

def insitu(config):

  jets = config['results'].get('jets')
  data = config['results'].get('data')
  mc = config['results'].get('mc')

  dataFiles = h.openFiles(data,config)
  dataHistFile = dataFiles[0]
  dataResFile = dataFiles[1]
  dataJER = h.getHistogram("probe_insituJER",jets,dataResFile)
  dataPt = h.getHistogram("<probe_pT>(dijet_pTavg,probe_etadet)",jets,dataHistFile)

  mcFiles = h.openFiles(mc,config)
  mcHistFile = mcFiles[0]
  mcResFile = mcFiles[1]
  mcJER = h.getHistogram("probe_insituJER",jets,mcResFile)
  mcPt = h.getHistogram("<probe_pT>(dijet_pTavg,probe_etadet)",jets,mcHistFile)

  pT_eta_bins = dataHistFile.GetDirectory(jets).Get("pT_bins:eta_bins")
  NpTbins = pT_eta_bins.GetNbinsX()
  Netabins = pT_eta_bins.GetNbinsY()

  from ROOT import Plotter
  plotter = Plotter.Plotter()
  plotter.openFile('{}_insitu'.format(jets))

  for ieta in range(Netabins):

    etalow, etaup = pT_eta_bins.GetYaxis().GetBinLowEdge(ieta+1),pT_eta_bins.GetYaxis().GetBinLowEdge(ieta+2)
    pTmin, pTmax = config.getfloat('axis','pT.min'), config.getfloat('axis','eta{}.pT.max'.format(ieta+1))
    JERmax = config.getfloat('axis','JER.max')
    plotter.makeAxis("Jet #it{p}_{T} [GeV]", pTmin, pTmax, "Relative jet energy resolution", 0.0,JERmax)
    plotter.logAxis(True,False)

    dataJER_eta = dataJER.ProjectionX("dataJER_eta{}".format(ieta),ieta+1,ieta+1)
    dataPt_eta = dataPt.ProjectionX("dataPt_eta{}".format(ieta),ieta+1,ieta+1)
    dataJERvsPt_eta = ROOT.HistUtils.makeGraph("dataJERvsPt_eta{}".format(ieta),10,dataPt_eta,dataJER_eta)
    plotter.addGraph(dataJERvsPt_eta,"EP")
    plotter.setLabel("Data","EP")
    mcJER_eta = mcJER.ProjectionX("mcJER_eta{}".format(ieta),ieta+1,ieta+1)
    mcPt_eta = mcPt.ProjectionX("mcPt_eta{}".format(ieta),ieta+1,ieta+1)
    mcJERvsPt_eta = ROOT.HistUtils.makeGraph("mcJERvsPt_eta{}".format(ieta),10,mcPt_eta,mcJER_eta,True,True)
    plotter.setFill(mcJERvsPt_eta,ROOT.kBlue,0.2)
    plotter.addGraph(mcJERvsPt_eta,"E2")
    plotter.setLabel("Pythia","F")
    plotter.makeLegend(0.5,0.85,0.6,0.8)

    plotter.addLabel(0.65,0.9,'{:.1f} #leq #||{{#it{{#eta}}_{{det}}}} < {:.1f}'.format(etalow,etaup))

    plotter.savePlot()

  plotter.closeFile()
  
def NSC(config):

  jets = config['results'].get('jets')
  data = config['results'].get('data')
  mc = config['results'].get('mc')

  dataFiles = h.openFiles(data,config)
  dataHistFile = dataFiles[0]
  dataResFile = dataFiles[1]
  dataNSC = h.getHistogram("insituJER_fit",jets,dataResFile)

  mcFiles = h.openFiles(mc,config)
  mcHistFile = mcFiles[0]
  mcResFile = mcFiles[1]
  mcNSC = h.getHistogram("insituJER_fit",jets,mcResFile)

  pT_eta_bins = dataHistFile.GetDirectory(jets).Get("pT_bins:eta_bins")
  NpTbins = pT_eta_bins.GetNbinsX()
  Netabins = pT_eta_bins.GetNbinsY()

  from ROOT import Plotter
  plotter = Plotter.Plotter()
  plotter.openFile('{}_NSC'.format(jets))

  for ieta in range(Netabins):

    etalow, etaup = pT_eta_bins.GetYaxis().GetBinLowEdge(ieta+1),pT_eta_bins.GetYaxis().GetBinLowEdge(ieta+2)
    pTmin, pTmax = config.getfloat('axis','pT.min'), config.getfloat('axis','eta{}.pT.max'.format(ieta+1))
    JERmax = config.getfloat('axis','JER.max')
    plotter.makeAxis("Jet #it{p}_{T} [GeV]", pTmin, pTmax, "Relative jet energy resolution", 0.0,JERmax)
    plotter.logAxis(True,False)

    dataNSC_eta = dataNSC.ProjectionX("data_eta{}".format(ieta),ieta+1,ieta+1)
    plotter.addHist(dataNSC_eta,"C")
    plotter.setLabel("Data","L")
    mcNSC_eta = mcNSC.ProjectionX("mc_eta{}".format(ieta),ieta+1,ieta+1)
    plotter.addHist(mcNSC_eta,"C")
    plotter.setLine(ROOT.kBlue)
    plotter.setLabel("Pythia","L")
    plotter.makeLegend(0.5,0.85,0.6,0.8)

    plotter.addLabel(0.65,0.9,'{:.1f} #leq #||{{#it{{#eta}}_{{det}}}} < {:.1f}'.format(etalow,etaup))

    plotter.savePlot()

  plotter.closeFile()
