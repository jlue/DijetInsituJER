import os
import ROOT

def openFiles(name, config):
    histFilePath = config['inputs'].get('file.hists.'+name)
    resFilePath = config['inputs'].get('file.results.'+name)
    errFilePath = config['inputs'].get('file.errors.'+name)
    histFile = ROOT.TFile.Open(histFilePath)
    resFile  = ROOT.TFile.Open(resFilePath)
    print("opening files: ")
    print("\t{}".format(histFilePath))
    print("\t{}".format(resFilePath))
    if os.path.exists(errFilePath):
        errFile = ROOT.TFile.Open(errFilePath)
        print("\t{}".format(resFilePath))
        return histFile,resFile,errFile
    else:
        return histFile,resFile

def getHistogram(name, dir, file):
    hist = file.GetDirectory(dir).Get(name)
    try:
        hist.GetEntries()
        return hist
    except:
        raise Exception("histogram {}/{} not found in file: {}".format(dir,name,file.GetPath()))