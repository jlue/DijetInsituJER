import ROOT

groups = [
    'JES',
    'DT',
    'MC',
    'NC'
    ]
line_colors = {
    'JES': ROOT.kGreen+1,
    'DT': ROOT.kBlue+1,
    'MC': ROOT.kRed+1,
    'NC': ROOT.kOrange+1,
}
line_styles = {
    'JES': 2,
    'DT': 4,
    'MC': 6,
    'NC': 8,
}
legend_labels = {
    'JES': 'Jet energy scale',
    'DT':  '#Delta#it{#phi}_{jj} & #it{p}_{T}^{j3}',
    'MC':  'Physics modelling',
    'NC':  'Method non-closure',
}

def groupedSystematics(settings):

    from Utils import plotter
    plotter = plotter.Plotter()
    plotter.canvas(4,3)

    jetAlgo = settings.get('uncertainties','jetalgo')
    dataset = settings.get('uncertainties','dataset')
    mc = settings.get('uncertainties','mc')

    uncFile = ROOT.TFile.Open(settings.get('inputs','file.errors.'+mc))
    jer_systuncs_group_2d = {}
    for group in groups:
        jer_systuncs_group_2d[group] = uncFile.GetDirectory(jetAlgo).Get("probe_SystematicUncertainty__"+group)
    jer_systunc_tot_2d = uncFile.GetDirectory(jetAlgo).Get("probe_SystematicUncertainty__Total")

    binFile = ROOT.TFile.Open(settings.get('inputs','file.hists.'+mc))
    pT_eta_bins = binFile.GetDirectory(jetAlgo).Get("pT_eta_bins")
    npTbins = pT_eta_bins.GetNbinsX()
    netabins = pT_eta_bins.GetNbinsY()

    plotter.open(jetAlgo+"_groupedSystematics")

    for ieta in range(netabins):

        pTmin,pTmax = settings.getfloat('axis','pTmin'), settings.getfloat('axis','eta{}.pTmax'.format(ieta+1))
        uncmax = settings.getfloat('uncertainties','uncmax')
        plotter.axis("Jet #it{p}_{T} [GeV]", "Relative uncertainty on JER",pTmin,pTmax,0.0,uncmax,logx=True)

        plotter.legend(0.25,0.70,0.45,0.70)
        # group
        for group in groups:
            grpUnc_pT = jer_systuncs_group_2d[group].ProjectionX("grpUnc_{}_eta{}".format(group,ieta+1),ieta+1,ieta+1)
            plotter.curve(grpUnc_pT,line_styles[group],line_colors[group])
            plotter.label(grpUnc_pT,legend_labels[group],"L")
        # total
        totUnc_pT = jer_systunc_tot_2d.ProjectionX("systUnc_eta{}".format(ieta+1),ieta+1,ieta+1)
        plotter.fill(totUnc_pT,ROOT.kBlue,0.2)
        plotter.label(totUnc_pT,"Total systematic uncertainty","CF")

        plot_captions = settings.get('caption','captions').split(',')
        plot_captions_pos = settings.get('caption','captions.pos').split(',')
        plot_captions_x = float(plot_captions_pos[0])
        plot_captions_y = float(plot_captions_pos[1])
        for plt_lbl in plot_captions:
            plotter.caption(plot_captions_x,plot_captions_y,settings.get('caption','caption.'+plt_lbl))
            plot_captions_y -= 0.05
        plotter.caption(0.65,0.9,'{} #leq #||{{#it{{#eta}}_{{det}}}} < {}'.format(pT_eta_bins.GetYaxis().GetBinLowEdge(ieta+1),pT_eta_bins.GetYaxis().GetBinLowEdge(ieta+2)))

        plotter.save()
    plotter.close()


    # axis

    
    # plotter.fill(jer_systunc_tot_ref,ROOT.kBlue,0.2)

    return
