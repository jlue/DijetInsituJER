#include "TriggerCombination/TriggerParser.h"
#include "TriggerCombination/TriggerLuminosity.h"
#include "TriggerCombination/TriggerEfficiency.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ struct TriggerParser+;
#pragma link C++ class TriggerLuminosity+;
#pragma link C++ class TriggerEfficiency+;

#endif