
#include "TriggerCombination/TriggerLuminosity.h"

LuminosityCalculator::LuminosityCalculator(Config& settings) :
  m_lumiTable(Config())
{
  m_periods = settings.getStrV("Periods");
  m_combTrigs = settings.getStrV("CombTrigs");
  for (std::string combTrig : m_combTrigs) {
    TriggerParser* trigParser = new TriggerParser(combTrig);
    m_trigParser[combTrig] = trigParser;
    m_ctrTrigs.push_back(trigParser->centralTrigger);
    m_fwdTrigs.push_back(trigParser->forwardTrigger);
  }

  // first get ilumicalc files for triggers in each period
  for (std::string ctrTrig : m_ctrTrigs) {
    // sum lumiblocks for each period
    for (std::string period : m_periods) {
      // periods joined with a "+" should have their lb's considered together in a chain
      m_lumiTree[ctrTrig][period] = new TChain("LumiMetaData");
      std::vector<std::string> periodsToAdd = vectorizeStr(period,"+");
      for (auto periodToAdd : periodsToAdd) {
        std::string lumiFilePath = settings.getStr(period+"."+ctrTrig);
        lumiFilePath = PathResolverFindCalibFile(lumiFilePath.c_str());
        m_lumiTree[ctrTrig][period]->AddFile(lumiFilePath.c_str());
      }
    }
  }

  // do the same for forward triggers
  for (std::string fwdTrig : m_fwdTrigs) {
    // sum lumiblocks for each period
    for (std::string period : m_periods) {
      // periods joined with a "+" should have their lb's considered together in a chain
      m_lumiTree[fwdTrig][period] = new TChain("LumiMetaData");
      std::vector<std::string> periodsToAdd = vectorizeStr(period,"+");
      for (auto periodToAdd : periodsToAdd) {
        std::string lumiFilePath = settings.getStr(period+"."+fwdTrig);
        lumiFilePath = PathResolverFindCalibFile(lumiFilePath.c_str());
        m_lumiTree[fwdTrig][period]->AddFile(lumiFilePath.c_str());
      }
    }
  }
}

void LuminosityCalculator::calculateLuminosities() {

  for (std::string combTrig : m_combTrigs) {
    for (std::string period : m_periods) {
      calculateLuminosity(combTrig,period);
    }
  }
}

void LuminosityCalculator::calculateLuminosity(const std::string& combTrig, const std::string& period) {

  std::string ctrTrig = m_trigParser[combTrig]->centralTrigger;
  std::string fwdTrig = m_trigParser[combTrig]->forwardTrigger;

  TChain *t_j  = m_lumiTree[ctrTrig][period];
  TChain *t_fj  = m_lumiTree[fwdTrig][period];
  int n = t_j->GetEntries();
  if (t_fj->GetEntries()!=n) error("Not same entries?");

  float j_ps1=0, j_ps2=0, fj_ps1=0, fj_ps2=0;
  float dt = 0, j_lumi = 0, fj_lumi = 0, livef = 0, larf = 0, instL=0;

  t_j->SetBranchAddress("L1Presc",&j_ps1);
  t_j->SetBranchAddress("L2Presc",&j_ps2);
  t_j->SetBranchAddress("IntLumi",&j_lumi);
  t_fj->SetBranchAddress("L1Presc",&fj_ps1);
  t_fj->SetBranchAddress("L2Presc",&fj_ps2);
  t_fj->SetBranchAddress("IntLumi",&fj_lumi);

  t_fj->SetBranchAddress("Livefrac",&livef);
  t_fj->SetBranchAddress("LArfrac",&larf);
  t_fj->SetBranchAddress("DeltaT",&dt);
  t_fj->SetBranchAddress("Inst_m_Lumi",&instL);

  float lsum_tot = 0, lsum_j = 0, lsum_fj = 0, lsum_or = 0;
  for (int i=0;i<n;++i) {
    t_j->GetEntry(i); t_fj->GetEntry(i);
    if (j_lumi==0 && fj_lumi==0) continue;
    //printf("Dt = %.2f, inst = %.2f\n",dt,instL);
    float tot    = instL*dt*livef*larf;
    float tot_j  = j_lumi*j_ps1*j_ps2;
    float tot_fj = fj_lumi*fj_ps1*fj_ps2;
    //if (i%1000==0) printf("LBN entry %7i, delivered: %7.2f/nb, %.1f\n",i,tot*1e-3,j_ps1*j_ps2/fj_ps1/fj_ps2);
    if ( (j_ps1>0 && j_ps2>0 && std::fabs(tot-tot_j)>100) || (fj_ps1>0 && fj_ps2>0 && std::fabs(tot-tot_fj)>100) ) {
      //printf("LBN entry %7i, delivered: %7.2f/nb, %.1f\n",i,tot*1e-3,j_ps1*j_ps2/fj_ps1/fj_ps2);
      //printf("run entry %7i, delivered: %7.2f/nb, %.1f\n",i,tot*1e-3,j_ps1*j_ps2/fj_ps1/fj_ps2);
      //printf("J:  prescale: %6.1f Lumi %6.1f  Tot: %.1f\n",j_ps1*j_ps2,j_lumi,tot_j);
      //printf("FJ: prescale: %6.1f Lumi %6.1f  Tot: %.1f\n",fj_ps1*fj_ps2,fj_lumi,tot_fj);
      //warn("Bad comparion");
      // how often does this happen?
      //continue;
    }
    lsum_tot += tot;
    lsum_j   += j_lumi;
    lsum_fj  += fj_lumi;
    float P_j = 1.0/j_ps1/j_ps2, P_fj = 1.0/fj_ps1/fj_ps2;
    if (P_j < 0) P_j = 0;
    if (P_fj < 0) P_fj = 0;
    lsum_or  += tot * (1.0 - (1.0 - P_j)*(1.0 - P_fj) );
  }

  // P(A OR B = 1 - P(not A && not B))
  //float P_OR = 1.0 - (1.0-1.0/avgPS_j)*(1.0-1.0/avgPS_fj);

  float avgPS_j = lsum_tot/lsum_j, avgPS_fj = lsum_tot/lsum_fj;
  // printf(  "TOT:  %1.6e/pb\n",lsum_tot*1e-6);
  // printf(  "CTR:  %1.6e/pb | prescale: %1.6e\n",lsum_j*1e-6,avgPS_j);
  // printf(  "FWD:  %1.6e/pb | prescale: %1.6e\n",lsum_fj*1e-6,avgPS_fj);
  // printf(  "OR:   %1.6e/pb | prescale: %1.6e\n",lsum_or*1e-6,lsum_tot/lsum_or);
  // LArfrac         = 1
  // DeltaT          = 60.3565
  // IntLumi         = 2.35027

  // Save values in pb^-1
  float ipb = 1e-6;
  std::vector<float> ctrLumis;
  std::vector<float> fwdLumis;
  std::vector<float> combLumis;
  for (auto period : m_periods) {
    ctrLumis.push_back(lsum_j*ipb);
    fwdLumis.push_back(lsum_fj*ipb);
    combLumis.push_back(lsum_or*ipb);
  }
  m_lumiTable.setNumV("TrigLumi."+ctrTrig,  ctrLumis);
  m_lumiTable.setNumV("TrigLumi."+fwdTrig,  fwdLumis);
  m_lumiTable.setNumV("TrigLumi."+combTrig, combLumis);

}

void LuminosityCalculator::writeResults(const std::string& filePath) {
  m_lumiTable.writeTable(filePath.c_str());
}