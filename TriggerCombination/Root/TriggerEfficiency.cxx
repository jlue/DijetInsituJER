#include "TriggerCombination/TriggerEfficiency.h"

EfficiencyCalculator::EfficiencyCalculator(const std::string& jetAlgo, Config& settings) :
  m_jetAlgo(jetAlgo)
{
  m_combTrigs = settings.getStrV(m_jetAlgo+".CombTrigs");
  for (const auto& combTrig : m_combTrigs) {
    TriggerParser* trigParser = new TriggerParser(combTrig);
    m_trigParser[combTrig] = trigParser;
    m_ctrTrigs.push_back(trigParser->centralTrigger);
    m_fwdTrigs.push_back(trigParser->forwardTrigger);
    std::vector<float> pT_min_max = settings.getNumV(combTrig+".pT_min_max");
    m_pT_min[combTrig] = pT_min_max[0];
    m_pT_min[combTrig] = pT_min_max[1];
  }
}