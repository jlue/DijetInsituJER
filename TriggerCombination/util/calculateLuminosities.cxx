#include "TriggerCombination/TriggerLuminosity.h"

int main(int argc, char** argv) {

  std::string configFilePath = argv[1];
  std::string outFile = argv[2];

  Config settings = Config(configFilePath);

  LuminosityCalculator* lumiCalc = new LuminosityCalculator(settings);
  lumiCalc->calculateLuminosities();
  lumiCalc->writeResults(outFile.c_str());

  return 0;
}
