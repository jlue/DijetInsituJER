#pragma once

// ROOT include(s)
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"

// AnalysisBase include(s)
#include "PathResolver/PathResolver.h"

// packages include(s)
#include "Utils/Utils.h"
#include "Utils/Config.h"

// local include(s)
#include "TriggerCombination/TriggerParser.h"

class LuminosityCalculator
{
public:
    LuminosityCalculator(Config& settings);
    ~LuminosityCalculator();

  void calculateLuminosity(const std::string& combTrig, const std::string& period);
  void calculateLuminosities();
  void writeResults(const std::string& filePath);

private:

    // periods to evaluate
    std::vector<std::string> m_periods;

    // central, forward, & combined triggers
    std::vector<std::string> m_combTrigs;
    std::map<std::string,TriggerParser*> m_trigParser;
    std::vector<std::string> m_ctrTrigs;
    std::vector<std::string> m_fwdTrigs;

    // trigger,period(s) -> lumi tree(chain)
    std::map<std::string,std::map<std::string,TChain*>> m_lumiTree;

    // results
    Config m_lumiTable;

};
