#pragma once 

#include "Utils/Utils.h"

struct TriggerParser
{
  TriggerParser(std::string combTrig) :
    combinedTrigger(combTrig)
  {
    std::vector<std::string> ctrFwdTrig = vectorizeStr(combTrig,"+");
    std::cout<< centralTrigger << "  " << forwardTrigger << std::endl;
    centralTrigger = ctrFwdTrig[0];
    forwardTrigger = ctrFwdTrig[1];

    // threshold ET
    centralTriggerThreshold = TString(centralTrigger.c_str()).ReplaceAll("HLT_j","").Atof();
    forwardTriggerThreshold = TString(forwardTrigger.c_str()).ReplaceAll("HLT_j","").ReplaceAll("_320eta490","").Atof();
  }

  std::string combinedTrigger;
  std::string centralTrigger;
  std::string forwardTrigger;
  float centralTriggerThreshold;
  float forwardTriggerThreshold;
};