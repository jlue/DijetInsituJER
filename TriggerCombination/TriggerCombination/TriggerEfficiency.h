#pragma once

// ROOT include(s)
#include "TEfficiency.h"

// package include(s)
#include "Utils/Utils.h"
#include "Utils/Config.h"

// local include(s)
#include "TriggerCombination/TriggerParser.h"

class EfficiencyCalculator
{

public:
  EfficiencyCalculator( const std::string& jetAlgo, Config& setting);
  ~EfficiencyCalculator() {
    for (auto eff : m_eff2D) delete eff.second;
    for (auto pass : m_pass2D) delete pass.second;
    for (auto all : m_all2D) delete all.second;
    for (auto eff : m_eff_pT) delete eff.second;
    for (auto eff : m_eff_eta) delete eff.second;
  }

  void calculateEfficiencies(TFile* effFile);
  void writeResults();

private:

  // calculating efficiency curves
  void calculateEfficiency(std::string combTrig);

  // drawing efficiency curves
  TGraphAsymmErrors* versusPt(std::string combTrig, int etabin);
  TGraphAsymmErrors* versusEta(std::string combTrig, int pTbin);
  float minEffPt(TEfficiency* eff, float minEff = 0.99);

  // jet algorithm
  std::string m_jetAlgo;
  // combination triggers
  std::vector<std::string> m_combTrigs;
  std::map<std::string,TriggerParser*> m_trigParser;
  std::vector<std::string> m_ctrTrigs;
  std::vector<std::string> m_fwdTrigs;

  // input pT & eta binning
  std::vector<float> m_pT_bins;
  int m_NpTbins;
  std::vector<float> m_eta_bins;
  int m_Netabins;

  // eff = pass/all histograms
  // (pT, eta) histograms
  std::map<std::string,TEfficiency*> m_eff2D;
  std::map<std::string,TH2F*> m_pass2D;
  std::map<std::string,TH2F*> m_all2D;
  // vs. pT
  std::map<std::string,TEfficiency*> m_eff_pT;
  std::map<std::string,std::vector<TH1F*>> m_pass_pT;
  std::map<std::string,std::vector<TH1F*>> m_all_pT;
  // vs. eta
  std::map<std::string,TEfficiency*> m_eff_eta;
  std::map<std::string,std::vector<TH1F*>> m_pass_eta;
  std::map<std::string,std::vector<TH1F*>> m_all_eta;

  // plotting pT range
  std::map<std::string,float> m_pT_min;
  std::map<std::string,float> m_pT_max;

  // turn-on points
  std::map<std::string,float> m_trigOptPt;

};
