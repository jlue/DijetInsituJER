# AnalysisExample

This module performs the analysis of n-tuples created from `DijetTree` and creates histograms to be used for statistical fits.

Dependencies of this package include:
- `ana`: C++ library to perform multi-threaded columnar dataset transformations 
- `AnalysisHelpers`: implementation of the `ana` library for ROOT datasets (vanilla `TTree` or `xAOD`) and results (`TH1`, etc.).
- `SubmissionHelpers`: submit analysis jobs to batch scheduler (e.g. CondorHT on lxplus).

## Quickstart

`analyze.py` is the basic script that can be configured to perform the ntuple-to-histograms operation in a fully columnar fashion, spelled out via config files:

```
$ analyze.py --tree MyTree --files /my/tree/file.root --sample MySample --weight 1.0 --analysis MyAnalysis --compute /my/observables.cfg --filter /my/filters.cfg --count /my/counters.cfg --patch mc16d pythia DijetTopology__Nominal --tags  /my/tags.cfg --output my/analysis/histograms.root --mt 8
```
See below for an examplanation of the options specified in this command, which in turn have their own README's inside their configuration folders.

Most of the time, analyzers will be using `submit-analyze.py` to run the analysis processes through batch scheduler:
```
$ submit-analyze.py /AnalysisExample/share/analysis/analysis.cfg --campaign run2
```
The `--check` option can be used to explicitly print out the individual `analyze.py` commands which can be locally run for testing/troubleshooting purposes.

## 1. Configure samples

The data analysis must be performed on a large set of input samples.

- A "campaign" defines a set of samples to run over in an analysis. (see: `share/config/`)
- Campaign-specific properties of samples are "configured". (see: `share/samples/`)
- Campaign-agnostic properties of samples are "mapped". (see: `share/sample/` )

## 2. Perform the analysis

An analysis consists of perform the following steps on a collection of entries:

1. Read and define observables of interest, e.g. read the jet pTs, compute average pT of leading two jets. (see: `/share/observables`)
2. Apply filters to select and/or weight entries, e.g. select all events with at least 2 jets, weight entries by the MC event weight). (see: `/share/selections`)
3. Count entries, e.g. fill a histogram with the average leading dijet pT. (see: `/share/counters/`)

## 3. Allocate enough resources

Multi-threaded analysis of $O(10\ \textrm{million})$ `TTree` entries requires lots of computing resources (CPU/RAM/time). The jobs for `submit-analyze.py` should be submitted with reasonable resources for each sample. (see: `share/jobs`)