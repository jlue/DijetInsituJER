################################################################################
# Project: DijetHistograms
################################################################################
atlas_subdir( DijetHistograms )

find_package( ROOT REQUIRED COMPONENTS Core Imt RIO Net Hist Graf Graf3d Gpad ROOTVecOps Tree TreePlayer Rint Postscript Matrix Physics MathCore Thread MultiProc ROOTDataFrame )
find_package( AnalysisBase QUIET )

# Requirements
atlas_depends_on_subdirs( PUBLIC
                          BootstrapGenerator
                          ana 
                          AnalysisHelpers 
                          )

atlas_add_root_dictionary( DijetHistogramsLib DijetHistogramsDictSource
                           ROOT_HEADERS DijetHistograms/*.h Root/LinkDef.h
                           EXTERNAL_PACKAGES ROOT )

atlas_add_library( DijetHistogramsLib
                   DijetHistograms/*.h Root/*.cxx ${DijetHistogramsDictSource}
                   PUBLIC_HEADERS DijetHistograms
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES}
                   BootstrapGeneratorLib
                   ana
                   AnalysisHelpers
                   PathResolver
                  )

# Data files
atlas_install_data( share/* )
atlas_install_python_modules( python/*.py )
atlas_install_scripts( scripts/*.py )
