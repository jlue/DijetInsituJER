#pragma once 

#include <string>
#include <vector>
#include <algorithm>

#include <ROOT/RVec.hxx>
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3.h"

#include "ana/definition.h"

class ResponseAtPtEta
{
public:
	ResponseAtPtEta() = default;
	~ResponseAtPtEta() = default;
	void  setResponse(const std::string& filePath, const std::string& histPath);
	TH1D* getResponseAtPtEta(float pT, float eta) const;
protected:
	std::shared_ptr<TH3> m_jetResponseVsPtEta;
	std::vector<std::vector<std::shared_ptr<TH1D>>> m_jetResponseAtPtEta;
};

struct ResolutionScan
{
public:
	ResolutionScan() = default;
	~ResolutionScan() = default;
	void scanResolution(size_t nr, float rmin, float rmax);
protected:
	std::vector<float> m_rVals;
};

struct ResolutionFoldAtPt
{
	// jet response map
	void  setResolutionFold(const std::string& filePath, const std::string& histPath);
	float getResolutionFoldAtPt(float pT) const;
	std::unique_ptr<TFile> m_jetResolutionFile;
	TH1* m_jetResolutionFoldVsPt;
};

struct ScaleScan
{
	void scanScale(size_t ns, float smin, float smax);
	std::vector<float> m_sVals;
};

using JetPtResolutionScanDefinition = ana::column<std::vector<float>>::definition<float,float,float>;
class JetPtResolutionScan : public JetPtResolutionScanDefinition, public ResponseAtPtEta, public ResolutionScan
{
public:
	JetPtResolutionScan(const std::string& name);
	virtual ~JetPtResolutionScan() = default;
	virtual std::vector<float> evaluate(
		ana::observable<float> jetPtReco, 
		ana::observable<float> jetPtTrue, 
		ana::observable<float> jetEtaDet
	) const override;
};

using DeltaPtResolutionScanDefinition = ana::column<std::vector<float>>::definition<std::vector<float>,std::vector<float>>;
class DeltaPtResolutionScan : public DeltaPtResolutionScanDefinition
{
public:
	DeltaPtResolutionScan(const std::string& name);
	virtual ~DeltaPtResolutionScan() = default;
	virtual std::vector<float> evaluate(
		ana::observable<std::vector<float>> probeJetPtResolutionScan,
		ana::observable<std::vector<float>> refJetPtResolutionScan
	) const override;
};

using AsymmetryResolutionScanDefinition = ana::column<std::vector<float>>::definition<std::vector<float>,std::vector<float>>;
class AsymmetryResolutionScan : public AsymmetryResolutionScanDefinition
{
public:
	AsymmetryResolutionScan(const std::string& name);
	virtual ~AsymmetryResolutionScan() = default;
	virtual std::vector<float> evaluate(
		ana::observable<std::vector<float>> probeJetPtResolutionScan,
		ana::observable<std::vector<float>> refJetPtResolutionScan
	) const override;
};

using JetPtResolutionFoldDefinition = ana::column<float>::definition<float,float,float>;
class JetPtResolutionFold : public JetPtResolutionFoldDefinition, public ResponseAtPtEta, public ResolutionFoldAtPt
{
public:
	JetPtResolutionFold(const std::string& name);
	virtual ~JetPtResolutionFold() = default;
	virtual float evaluate(
		ana::observable<float> jetPtReco,
		ana::observable<float> jetPtTrue,
		ana::observable<float> jetPtEtaDet
	) const override;
};

using JetPtScaleResolutionScanDefinition = ana::column<std::vector<std::vector<float>>>::definition<float,float,float>;
class JetPtScaleResolutionScan : public JetPtScaleResolutionScanDefinition, public ResponseAtPtEta, public ScaleScan, public ResolutionScan
{
public:
	JetPtScaleResolutionScan(const std::string& name);
	virtual ~JetPtScaleResolutionScan() = default;
	virtual std::vector<std::vector<float>> evaluate(
		ana::observable<float> jetPtReco, 
		ana::observable<float> jetPtTrue, 
		ana::observable<float> jetEtaDet
	) const override;
};

using DeltaPtScaleResolutionScanDefinition = ana::column<std::vector<std::vector<float>>>::definition<std::vector<std::vector<float>>,float>;
class DeltaPtScaleResolutionScan : public DeltaPtScaleResolutionScanDefinition
{
public:
	DeltaPtScaleResolutionScan(const std::string& name);
	virtual ~DeltaPtScaleResolutionScan() = default;
	virtual std::vector<std::vector<float>> evaluate(
		ana::observable<std::vector<std::vector<float>>> jetScaleResolutionScan,
		ana::observable<float> refJetPt
	) const override;
};

using AsymmetryScaleResolutionScanDefinition = ana::column<std::vector<std::vector<float>>>::definition<std::vector<std::vector<float>>,float>;
class AsymmetryScaleResolutionScan : public AsymmetryScaleResolutionScanDefinition
{
public:
	AsymmetryScaleResolutionScan(const std::string& name);
	virtual ~AsymmetryScaleResolutionScan() = default;
	virtual std::vector<std::vector<float>> evaluate(
		ana::observable<std::vector<std::vector<float>>> jetScaleResolutionScan,
		ana::observable<float> refJetPt
	) const override;
};