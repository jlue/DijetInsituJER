#pragma once

// C++ include(s)
#include <string>
#include <vector>
#include <map>
#include <unordered_map>

#include "ana/definition.h"

using CombinedTriggerEfficiencyDefinition = ana::column<bool>::definition<std::vector<std::string>,std::vector<std::string>>;
class CombinedTriggerEfficiency : public CombinedTriggerEfficiencyDefinition
{
	CombinedTriggerEfficiency(const std::string& name);
	virtual ~CombinedTriggerEfficiency() = default;
  virtual bool evaluate(ana::observable<std::vector<std::string>> passedTriggers, ana::observable<std::vector<std::string>> passedTriggersBeforePrescale) const override;
	void addSeed(const std::string& seedTrigger);
	void addProbe(const std::string& probeTrigger);
protected:
	std::vector<std::string> m_seeds;
	std::vector<std::string> m_probes;
};

using OptimalCombinedTriggerDefinition = ana::column<std::string>::definition<float,std::vector<std::string>>;
class OptimalCombinedTrigger : public OptimalCombinedTriggerDefinition
{
public:
	OptimalCombinedTrigger(const std::string& name);
	virtual ~OptimalCombinedTrigger() = default;
  virtual std::string evaluate(ana::observable<float> pT, ana::observable<std::vector<std::string>> passedTriggers) const override;
	void addTrigger(const std::string& trig, float recoPtThreshold);
protected:
	std::vector<std::string> m_combinedTriggers;
	std::vector<float>       m_recoPtThresholds;
};

using CombinedTriggerPrescaleDefinition = ana::column<float>::definition<std::string,std::vector<std::string>>;
class CombinedTriggerPrescale : public CombinedTriggerPrescaleDefinition
{
public:
	CombinedTriggerPrescale(const std::string& name);
	virtual ~CombinedTriggerPrescale() = default;
  virtual float evaluate(ana::observable<std::string> optimalCombinedTrigger, ana::observable<std::vector<std::string>> passedTriggersBeforePrescale) const override;
	void setTotalLuminosity(float lumi);
	void setTriggerLuminosity(const std::string& trig, float lumi);
protected:
	float m_totalLuminosity;
	std::unordered_map<std::string,float> m_triggerLuminosity;
};
