#pragma once

#include <vector>

#include <ROOT/RVec.hxx>
#include "TLorentzVector.h"
#include "TRandom.h"

#include "ana/definition.h"

// jet(s) (at least one or both of leading & sub-leading) in reference region
using JetInRefRegionDefinition = ana::column<ROOT::RVec<bool>>::definition<ROOT::RVec<float>>;
class JetInRefRegion : public JetInRefRegionDefinition
{
public:
	JetInRefRegion(const std::string& name) :
		JetInRefRegionDefinition(name)
	{}
	virtual ~JetInRefRegion() = default;
	virtual ROOT::RVec<bool> evaluate(ana::observable<ROOT::RVec<float>> jetEtaDet) const override;
private:
	float m_refEtaLow = 0.2;
	float m_refEtaHigh = 0.7;
};

using RefJetIndexDefinition = ana::column<int>::definition<ROOT::RVec<bool>>;
class RefJetIndex : public RefJetIndexDefinition
{
public:
	RefJetIndex(const std::string& name) :
		RefJetIndexDefinition(name)
	{}
	virtual ~RefJetIndex() = default;
	virtual void initialize() override;
	virtual int evaluate(ana::observable<ROOT::RVec<bool>> jetInRefRegion) const override;
private:
	std::unique_ptr<TRandom> m_rng;
};

using JetResponseDefinition = ana::column<ROOT::RVec<float>>::definition<ROOT::RVec<float>,ROOT::RVec<float>>;
class JetResponse : public JetResponseDefinition
{
public:
	JetResponse(const std::string& name) :
		JetResponseDefinition(name)
	{}
	virtual ~JetResponse() = default;
	virtual ROOT::RVec<float> evaluate(ana::observable<ROOT::RVec<float>> jetPtReco, ana::observable<ROOT::RVec<float>> jetPtTrue) const override;
};
