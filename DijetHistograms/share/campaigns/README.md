## Campaigns

Each section heading declares a campaign, containing configurations for:

- `data.cfg`: Data samples (does not receive luminosity scaling).
- `mc.cfg`: MC samples (receives luminosity scaling).
- `lumi`: Value of the integrated luminosity of the campaign.

### Example

```
[c16a]
lumi   = 36207.66
mc.cfg = "/path/to/AnalysisExample/share/samples/mc16a.cfg"
data.cfg = "/path/to/AnalysisExample/share/samples/data1516.cfg"
```
