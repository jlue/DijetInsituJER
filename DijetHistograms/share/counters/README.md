# Counters

Entries for which observables of interest have been defined and passed relevant selections must be aggregated into desired results.

- `TH1`
  - "Scan": variation of `TH1` implementation in which it receives a constant
- `TProfile`
- `TEfficiency`

In order to create counters for the analysis, they must be named under sections specifying their types (as currently suppported by `AnalysisHelpers` below). Their names must be unique among other counters, but may coincide with observable names and/or filter paths. Their definitions are dependent on the type:

- Dimensionality & binning.
- Observable(s) to fill the counter with.
- Filter(s) at which the counter is booked, such that the filling is performed only if entry passes the filter, with the weight considered.
(See the respective implementation in `AnalysisHelpers` for more details -- and users can implement their own!)

## Example 

```
[histograms]
nJets = { dim=1, dtype="float", xbins=[0,1,2,3,4,5,6,7,8,9,10], xobs=["nJets"], at=["mcEventWeight"] }
leadJetPtVsEtaDet = { dim=2, dtype="float", xobs=["leadJetPt"], nx=198,xmin=20,xmax=2000, yobs=["leadJetEtaDet"], ybins=[0.0,0.2,0.7,1.0,1.3,1.8,2.5,2.8,3.2,3.5,4.5], at=["atLeast2JetsCut"] }
```