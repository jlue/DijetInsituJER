# Observables

Reading/defining quantities of interest is the first action to be performed when analysis dataset entries. These involve varied and complex logic, including:
- Read "columns" of the dataset.
  - One must define their data type and the name of the column inside the dataset.
- Custom "definition" containing how to compute the quantity based on existing observables.
  - For simple cases, one can simply write down a C++ expression.
  - For complex cases, one can compile a C++ class, which must implement `ana::column<T>::definition<Args...>` but may also have arbitrary methods to be called on-the-fly via python modules (see `AnalysisExample/python/`).

In order to define these observables, they must be named under sections corresponding to their primary type (column or definition) with unique names.

## Example
```
[columns]

nJets          = { dtype="int", branch="nJets" }
passedTriggers = { dtype="ROOT::RVec<string>", branch="passedTriggers" }

[definitions]

leadJetPt      = "jetsPt[0]"
subleadJetPt   = "jetsPt[1]"
optimalCombinedTrigger = { def="OptimalCombinedTrigger", obs=["dijetPtAvg", "passedTriggers"], py="DijetHistograms.TriggerCombination" }
```