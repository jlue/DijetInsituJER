# Sample(s) configuration

A sample must be configured with relevant properties 

## Campaign-specific

- Whitelist specifying `true/false` determining whether or not the sample will be processed in an analysis run
- List of input files.
- Name of the input `TTree` inside the files.
- List of patches containing tags to be assigned (see: `AnalysisExample/share/tags/`).
- Normalization factor.

### Example
```
[whitelist]
Pythia_JZ1WithSW = true

[tree]
Pythia_JZ1WithSW = "AnalysisTree"

[files]
Pythia_JZ1WithSW = ["/path/to/Pythia_JZ1WithSW/AnalysisTree/*.root"]

[norm]
Pythia_JZ1WithSW = 19963000.0

[patch]
Pythia_JZ1WithSW = [ "mc16d", "pythia", "DijetTopology__Nominal" ]
```

## Campaign-agnostic

- Path of `TDirectory` that results of the analysis on `<sample_id>` will be mapped to.
- (MC-only) cross-section value(s) of each sample to scale by.

### Example
```
[path]
Pythia_JZ1WithSW = "Pythia/JZ1WithSW/"

[xsec]
Pythia_JZ1WithSW = [ 1.0, 78050000.0 , 0.024425 ]
```

## Note: MC sample normalization

As seen above, there are several scaling factors applied to MC samples:

- Dataset integrated luminosity (campaign-specific, sample-agnostic), $L$.
- MC cross-section (campaign-agnostic, sample-specific), $(k \times \sigma \times \varepsilon)$. Usually it is a product of the the process cross-section, generator filter efficiency, and an ad-hoc $k$-factor.
- MC "sum of weights" normalization (campaign-specific, campaign-specific), \sum w_i^{\textrm{MC}}.

Each campaign/sample receives a global normalization factor across all its entries: $N = L \times \sigma / \sum w_i^{\textrm{MC}}$