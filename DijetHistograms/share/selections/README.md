# Filters

A "filter" applied to entries in analysis is either a selection (e.g. $N_{\textrm{jet}} \geq 2$) or a weight (e.g. MC event weight); the two can be described using the same language because they are identical in terms of their logic, with the only difference between a selection being a boolean and a weight being a floating-point value.

In order to define a filter, simply start by naming it and provide its full definition, which contains:

- Whether the filter is a cut or a weight, and its corresponding C++ expression of existing observables and/or tags (see `AnalysisExample/share/observables/`).
- Whether it should be applied "at" a previously-defined cut.
  - By default, all cuts are applied in sequence in one another, unless this is specified to apply selections at different points in the cutflow.
- Whether it should be designated as a "channel".
  - By default, no such designation is made, which means that all cutflow paths are flattened to the final selection name.

For each filter defined, its resulting path must be unique; their individual names, however, need not be unique (also can coincide with observable names).

## Example
```
inclusiveCut = { cut = "true" }
mcEventWeight = { weight = "${isData} ? 1.0 : mcEventWeight" }
analysisControlRegion = { cut = "analysisVariable < ${analysisCut}", at = ["mcEventWeight"], channel=true }
analysisSignalRegion = { cut = "analysisVariable >= ${analysisCut}", at = ["mcEventWeight"], channel=true }
discriminantAcceptance = { cut = "analysisDiscriminant >= ${discriminantThreshold}", at=["analysis*Region"] }
```

### Applying filter "at" a previous one.

By default, applying `analysisSignalRegion` after `analysisControlRegion` would have incorrectly yielded no entries in the signal region by construction, since the latter necessarily excludes the former. Instead, the `at = ["mcEventWeight"]` is specified to define disjoint selection regions from a specific point in the cutflow.

### Designating filter as a "channel".

By default, applying the `discriminantAcceptance` cut at both the `analysisControlRegion` and `analysisSignalRegion` results in a logical conflict, as the flattened filter path for both maps to `discriminantAcceptance`. Instead, the `channel=true` option specified for the preceeding cuts resolves this, as the paths are now: `analysisControlRegion/discriminantAcceptance/` and `analysisSignalRegion/discriminantAcceptance` respectively.