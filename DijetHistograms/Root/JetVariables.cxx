#include "DijetHistograms/JetVariables.h"

#include "TRandom.h"
#include "TRandomGen.h"

ROOT::RVec<bool> JetInRefRegion::evaluate (
	ana::observable<ROOT::RVec<float>> jetEtaDet
) const {
	ROOT::RVec<bool> jetInRefRegion;
	for (int i=0 ; i<jetEtaDet->size() ; ++i) {
		auto thisJetAbsEtaDet = fabs(jetEtaDet->at(i));
		jetInRefRegion.push_back((thisJetAbsEtaDet >= m_refEtaLow && thisJetAbsEtaDet < m_refEtaHigh) ? true : false);
	}
	return jetInRefRegion;
}

void RefJetIndex::initialize()
{
	m_rng = std::make_unique<TRandomMixMax>();
}

int RefJetIndex::evaluate (
	ana::observable<ROOT::RVec<bool>> jetInRefRegion
) const {
	if (jetInRefRegion->at(0) && jetInRefRegion->at(1)) {
		// either jet can be the reference
		// randomly decided between the two to avoid biases
		return m_rng->Integer(2);
		// or, by convention, always choose leading
		// event *must* be "double-counted" with (-1) of all ref/probe quantities later stages
		// return 0;
	} else if (jetInRefRegion->at(0)) {
		// leading jet is reference
		return 0;
	} else if (jetInRefRegion->at(1)) {
		// subleading jet is reference
		return 1;
	}
	// neither jet is reference
	return -1;
}

ROOT::RVec<float> JetResponse::evaluate (
	ana::observable<ROOT::RVec<float>> jetPtReco,
	ana::observable<ROOT::RVec<float>> jetPtTrue
) const {
	ROOT::RVec<float> jetPtResponse;
	for (int i=0 ; i<jetPtReco->size() ; ++i) {
		jetPtResponse.push_back(jetPtReco->at(i)/jetPtTrue->at(i));
	}
	return jetPtResponse;
}
