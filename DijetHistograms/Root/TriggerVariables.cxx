#include "DijetHistograms/TriggerVariables.h"

#include "ana/strutils.h"
#include "ana/vecutils.h"

OptimalCombinedTrigger::OptimalCombinedTrigger(const std::string& name) :
	OptimalCombinedTriggerDefinition(name)
{}

std::string OptimalCombinedTrigger::evaluate(ana::observable<float> pT, ana::observable<std::vector<std::string>> passedTriggers) const
{
	// find out trigger optimizing offline threshold
	std::string optimalCombinedTrigger;
	for (size_t itrig=0 ; itrig<m_combinedTriggers.size() ; ++itrig) {
		if (pT.value() >= m_recoPtThresholds[itrig]) optimalCombinedTrigger = m_combinedTriggers[itrig];
	}
	// return optimal trigger if any of its components passed
	auto triggerComponents = str::split(optimalCombinedTrigger,"+");
	for (auto const& triggerComponent : triggerComponents) {
		if (vec::contains(passedTriggers.value(),triggerComponent)) return optimalCombinedTrigger;
	}	
	// optimal trigger didn't pass -- return ""
	return "";
}

void OptimalCombinedTrigger::addTrigger(const std::string& trig, float recoPtThreshold)
{
	m_combinedTriggers.push_back(trig);
	m_recoPtThresholds.push_back(recoPtThreshold);
}

CombinedTriggerPrescale::CombinedTriggerPrescale(const std::string& name) :
	CombinedTriggerPrescaleDefinition(name)
{}

void CombinedTriggerPrescale::setTotalLuminosity(float lumi)
{
	m_totalLuminosity = lumi;
}

void CombinedTriggerPrescale::setTriggerLuminosity(const std::string& trig, float lumi)
{
	m_triggerLuminosity[trig] = lumi;
}

float CombinedTriggerPrescale::evaluate(ana::observable<std::string> optimalCombinedTrigger, ana::observable<std::vector<std::string>> passedTriggersBeforePrescale) const
{
	auto triggerComponents = str::split(optimalCombinedTrigger.value(),"+");
	std::string centralComponent = triggerComponents[0];
	std::string forwardComponent = triggerComponents[1];
	auto centralComponentPassedBeforePrescale = vec::contains(passedTriggersBeforePrescale.value(),centralComponent);
	auto forwardComponentPassedBeforePrescale = vec::contains(passedTriggersBeforePrescale.value(),forwardComponent);
	// check which trigger luminosity to apply
	if (centralComponentPassedBeforePrescale && forwardComponentPassedBeforePrescale) {
		// if both central & forward triggers fired, apply their combined luminosity
		return m_totalLuminosity / m_triggerLuminosity.at(optimalCombinedTrigger.value());
	} else if (centralComponentPassedBeforePrescale && !forwardComponentPassedBeforePrescale) {
		// if only central trigger fired, apply its luminosity
		return m_totalLuminosity / m_triggerLuminosity.at(centralComponent);
	} else if (!centralComponentPassedBeforePrescale && forwardComponentPassedBeforePrescale) {
		// if only forward trigger fired, apply its luminosity
		return m_totalLuminosity / m_triggerLuminosity.at(forwardComponent);
	}
	// if neither central nor forward passed before trigger, we cannot deal with this event; set it to zero
	// should never happen as long as trigger emulation is done perfectly
	// (if a trigger was passed, it should have fired prior to prescaling decision)
	// TODO: print warning and investigate how often this happens
	return 0;
}