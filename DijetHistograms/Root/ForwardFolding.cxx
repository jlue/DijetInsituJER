#include "DijetHistograms/ForwardFolding.h"

#include "ana/strutils.h"
#include "ana/vecutils.h"

#include "AnalysisHelpers/HistogramUtils.h"

void ResponseAtPtEta::setResponse(const std::string& filePath, const std::string& histPath)
{
	TDirectory::TContext c;
	auto file = std::unique_ptr<TFile>(TFile::Open(filePath.c_str()));
	m_jetResponseVsPtEta = HistogramUtils::clone<TH3>(*file->Get<TH3>(histPath.c_str()));
	if (!m_jetResponseVsPtEta) {
		throw std::runtime_error("No jet reponse(pT,eta) histogram found.");
	}
	m_jetResponseAtPtEta.resize(m_jetResponseVsPtEta->GetNbinsX()); for (auto& jetRespRow : m_jetResponseAtPtEta) jetRespRow.resize(m_jetResponseVsPtEta->GetNbinsY());
	for (size_t ipt=0 ; ipt<m_jetResponseVsPtEta->GetNbinsX() ; ++ipt) {
		for (size_t ieta=0 ; ieta<m_jetResponseVsPtEta->GetNbinsY() ; ++ieta) {
			m_jetResponseAtPtEta[ipt][ieta] = HistogramUtils::clone<TH1D>(*m_jetResponseVsPtEta->ProjectionZ(Form("resp_pt%i_eta%i",ipt,ieta),ipt+1,ipt+1,ieta+1,ieta+1));
		}
	}
}

TH1D* ResponseAtPtEta::getResponseAtPtEta(float pT, float eta) const
{
	auto jetPtBin = m_jetResponseVsPtEta->GetXaxis()->FindBin(pT);
	auto jetEtaBin = m_jetResponseVsPtEta->GetYaxis()->FindBin(fabs(eta));
	if (jetPtBin <=0 || jetPtBin > m_jetResponseAtPtEta.size() ) {
		throw std::runtime_error(TString::Format("jet pT out of range of response map: %f GeV -> %i",pT,jetPtBin).Data());
	}
	if (jetEtaBin <=0 || jetEtaBin > m_jetResponseAtPtEta.at(0).size() ) {
		throw std::runtime_error(TString::Format("jet eta out of range of response map: %f -> %i",eta,jetEtaBin).Data());
	}
	return m_jetResponseAtPtEta[jetPtBin-1][jetEtaBin-1].get();
}

void ResolutionFoldAtPt::setResolutionFold(const std::string& filePath, const std::string& histPath)
{
	m_jetResolutionFile = std::unique_ptr<TFile>(TFile::Open(filePath.c_str()));
	m_jetResolutionFoldVsPt = m_jetResolutionFile->Get<TH1>(histPath.c_str());
}

float ResolutionFoldAtPt::getResolutionFoldAtPt(float pT) const
{
	auto jetPtBin = m_jetResolutionFoldVsPt->GetXaxis()->FindBin(pT);
	return m_jetResolutionFoldVsPt->GetBinContent(jetPtBin);
}

JetPtResolutionScan::JetPtResolutionScan(const std::string& name) :
	JetPtResolutionScanDefinition(name)
{}

void ResolutionScan::scanResolution(size_t nr, float rmin, float rmax) 
{
	auto rVals = vec::make_lin(nr,rmin,rmax);
	m_rVals = std::vector<float>(rVals.begin(),rVals.end());
}

std::vector<float> JetPtResolutionScan::evaluate(ana::observable<float>  jetPtReco, ana::observable<float>  jetPtTrue, ana::observable<float>  jetEtaDet) const
{
	auto jetRespMu = this->getResponseAtPtEta(jetPtTrue.value(),jetEtaDet.value())->GetMean();
	std::vector<float> jetPtTemplates(m_rVals.size());
	for (size_t ir=0 ; ir<m_rVals.size() ; ++ir) {
		jetPtTemplates[ir] = 1.0*jetPtReco.value() + (jetPtReco.value() - jetPtTrue.value()*jetRespMu)*(m_rVals[ir]-1.0);
	}
	return jetPtTemplates;
}

DeltaPtResolutionScan::DeltaPtResolutionScan(const std::string& name) :
	DeltaPtResolutionScanDefinition(name)
{}

std::vector<float> DeltaPtResolutionScan::evaluate(ana::observable<std::vector<float>> probeJetPtResolutionScan, ana::observable<std::vector<float>> refJetPtResolutionScan) const
{
	if (probeJetPtResolutionScan->size() != refJetPtResolutionScan->size()) {
		throw std::runtime_error("probe & ref jet pt folding parameter values do not match!");
	}
	auto nr = probeJetPtResolutionScan->size();
	auto deltaPtJetPtResolutionScan = std::vector<float>(nr,0);
	for (size_t ir=0 ; ir<nr ; ++ir) {
		deltaPtJetPtResolutionScan[ir] = probeJetPtResolutionScan.value()[ir] - refJetPtResolutionScan.value()[ir];
	}
	return deltaPtJetPtResolutionScan;
}

AsymmetryResolutionScan::AsymmetryResolutionScan(const std::string& name) :
	AsymmetryResolutionScanDefinition(name)
{}

std::vector<float> AsymmetryResolutionScan::evaluate(ana::observable<std::vector<float>> probeJetPtResolutionScan, ana::observable<std::vector<float>> refJetPtResolutionScan) const
{
	if (probeJetPtResolutionScan->size() != refJetPtResolutionScan->size()) {
		throw std::runtime_error("probe & ref jet pt folding parameter values do not match!");
	}
	auto nr = probeJetPtResolutionScan->size();
	auto asymJetPtResolutionScan = std::vector<float>(nr,0);
	for (size_t ir=0 ; ir<nr ; ++ir) {
		asymJetPtResolutionScan[ir] = 2.0 * ( probeJetPtResolutionScan.value()[ir] - refJetPtResolutionScan.value()[ir] ) / ( probeJetPtResolutionScan.value()[ir] + refJetPtResolutionScan.value()[ir] );
	}
	return asymJetPtResolutionScan;
}

JetPtResolutionFold::JetPtResolutionFold(const std::string& name) :
	JetPtResolutionFoldDefinition(name)
{}

float JetPtResolutionFold::evaluate(ana::observable<float> jetPt, ana::observable<float> jetPtTrue, ana::observable<float> jetEtaDet) const
{
	auto jetRespMu = this->getResponseAtPtEta(jetPtTrue.value(),jetEtaDet.value())->GetMean();
	auto jetResolR = this->getResolutionFoldAtPt(jetPtTrue.value());
	return (1.0*jetPt.value() + (jetPt.value() - jetPtTrue.value()*jetRespMu)*(jetResolR-1.0));
}

JetPtScaleResolutionScan::JetPtScaleResolutionScan(const std::string& name) :
	JetPtScaleResolutionScanDefinition(name)
{}

void ScaleScan::scanScale(size_t ns, float smin, float smax) 
{
	auto sVals = vec::make_lin(ns,smin,smax);
	m_sVals = std::vector<float>(sVals.begin(),sVals.end());
}

std::vector<std::vector<float>> JetPtScaleResolutionScan::evaluate(ana::observable<float> jetPtReco, ana::observable<float> jetPtTrue, ana::observable<float> jetEtaDet) const
{
	auto jetRespMu = this->getResponseAtPtEta(jetPtTrue.value(),jetEtaDet.value())->GetMean();
	std::vector<std::vector<float>> jetPtTemplates(m_sVals.size(),std::vector<float>(m_rVals.size()));
	for (size_t is=0 ; is<m_sVals.size() ; ++is) {
		auto sVal = m_sVals[is];
		for (size_t ir=0 ; ir<m_rVals.size() ; ++ir) {
			auto rVal = m_rVals[ir];
			jetPtTemplates[is][ir] = sVal*jetPtReco.value() + (jetPtReco.value() - jetPtTrue.value()*jetRespMu)*(rVal-sVal);
		}
	}
	return jetPtTemplates;
}

DeltaPtScaleResolutionScan::DeltaPtScaleResolutionScan(const std::string& name) :
	DeltaPtScaleResolutionScanDefinition(name)
{}

std::vector<std::vector<float>> DeltaPtScaleResolutionScan::evaluate(ana::observable<std::vector<std::vector<float>>> jetScaleResolutionScan, ana::observable<float> refJetPtFold) const
{
	auto ns = jetScaleResolutionScan->size();
	auto nr = jetScaleResolutionScan->at(0).size();
	auto deltaPtJetPtScaleResolutionScan = std::vector<std::vector<float>>(ns,std::vector<float>(nr));
	for (size_t is=0 ; is<ns ; ++is) {
		for (size_t ir=0 ; ir<nr ; ++ir) {
			auto probeJetPtFold = jetScaleResolutionScan.value()[is][ir];
			deltaPtJetPtScaleResolutionScan[is][ir] = jetScaleResolutionScan.value()[is][ir] - refJetPtFold.value();
		}
	}
	return deltaPtJetPtScaleResolutionScan;
}

AsymmetryScaleResolutionScan::AsymmetryScaleResolutionScan(const std::string& name) :
	AsymmetryScaleResolutionScanDefinition(name)
{}

std::vector<std::vector<float>> AsymmetryScaleResolutionScan::evaluate(ana::observable<std::vector<std::vector<float>>> jetScaleResolutionScan, ana::observable<float> refJetPtFold) const
{
	auto ns = jetScaleResolutionScan->size();
	auto nr = jetScaleResolutionScan->at(0).size();
	auto asymJetPtScaleResolutionScan = std::vector<std::vector<float>>(ns,std::vector<float>(nr));
	for (size_t is=0 ; is<ns ; ++is) {
		for (size_t ir=0 ; ir<nr ; ++ir) {
			auto probeJetPtFold = jetScaleResolutionScan.value()[is][ir];
			asymJetPtScaleResolutionScan[is][ir] = 2.0 * ( probeJetPtFold - refJetPtFold.value() ) / ( probeJetPtFold + refJetPtFold.value() );
		}
	}
	return asymJetPtScaleResolutionScan;
}