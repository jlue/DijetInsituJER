#ifdef __CINT__

#include "DijetHistograms/JetVariables.h"
#include "DijetHistograms/TriggerVariables.h"
#include "DijetHistograms/ForwardFolding.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class JetInRefRegion+;
#pragma link C++ class RefJetIndex+;

#pragma link C++ class JetResponse+;

#pragma link C++ class ResponseAtPtEta+;
#pragma link C++ class ResolutionScan+;
#pragma link C++ class JetPtResolutionScan+;
#pragma link C++ class DeltaPtResolutionScan+;
#pragma link C++ class AsymmetryResolutionScan+;

#pragma link C++ class OptimalCombinedTrigger+;
#pragma link C++ class CombinedTriggerPrescale+;

#endif



