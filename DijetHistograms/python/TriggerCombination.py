
centralTriggers = {
  2017 : ["HLT_j15", "HLT_j25", "HLT_j35", "HLT_j45", "HLT_j60", "HLT_j85", "HLT_j110", "HLT_j175", "HLT_j260", "HLT_j360", "HLT_j420", "HLT_j460"]
}
forwardTriggers = {
  2017 : ["HLT_j15_320eta490", "HLT_j25_320eta490", "HLT_j35_320eta490", "HLT_j45_320eta490", "HLT_j60_320eta490", "HLT_j85_320eta490", "HLT_j110_320eta490", "HLT_j175_320eta490", "HLT_j260_320eta490"]
}
combinedTriggers = {
  2017 : ["HLT_j15+HLT_j15_320eta490", "HLT_j25+HLT_j25_320eta490", "HLT_j35+HLT_j35_320eta490", "HLT_j45+HLT_j45_320eta490", "HLT_j60+HLT_j60_320eta490", "HLT_j85+HLT_j85_320eta490", "HLT_j110+HLT_j110_320eta490", "HLT_j175+HLT_j175_320eta490", "HLT_j260+HLT_j260_320eta490", "HLT_j360+HLT_j260_320eta490", "HLT_j420+HLT_j260_320eta490"]
}
# seed triggers
seedTriggers = {
  2017 : {
    "HLT_j15"  : "HLT_j0_perf_L1RD0_FILLED",
    "HLT_j25"  : "HLT_j15",
    "HLT_j35"  : "HLT_j15",
    "HLT_j45"  : "HLT_j25",
    "HLT_j60"  : "HLT_j45",
    "HLT_j85"  : "HLT_j45",
    "HLT_j110" : "HLT_j85",
    "HLT_j175" : "HLT_j110",
    "HLT_j260" : "HLT_j175",
    "HLT_j360" : "HLT_j260",
    "HLT_j420" : "HLT_j360",
    "HLT_j460" : "HLT_j420",
    "HLT_j15_320eta490"  : "HLT_j0_perf_L1RD0_FILLED",
    "HLT_j25_320eta490"  : "HLT_j15_320eta490",
    "HLT_j35_320eta490"  : "HLT_j15_320eta490",
    "HLT_j45_320eta490"  : "HLT_j25_320eta490",
    "HLT_j60_320eta490"  : "HLT_j45_320eta490",
    "HLT_j85_320eta490"  : "HLT_j60_320eta490",
    "HLT_j110_320eta490" : "HLT_j80_320eta490",
    "HLT_j175_320eta490" : "HLT_j110_320eta490",
    "HLT_j260_320eta490" : "HLT_j175_320eta490",
    "HLT_j15+HLT_j15_320eta490"   : "HLT_j15+HLT_j15_320eta490",
    "HLT_j25+HLT_j25_320eta490"   : "HLT_j15+HLT_j15_320eta490",
    "HLT_j35+HLT_j35_320eta490"   : "HLT_j15+HLT_j15_320eta490",
    "HLT_j45+HLT_j45_320eta490"   : "HLT_j25+HLT_j25_320eta490",
    "HLT_j60+HLT_j60_320eta490"   : "HLT_j45+HLT_j45_320eta490",
    "HLT_j85+HLT_j85_320eta490"   : "HLT_j60+HLT_j60_320eta490",
    "HLT_j110+HLT_j110_320eta490" : "HLT_j85+HLT_j85_320eta490",
    "HLT_j175+HLT_j175_320eta490" : "HLT_j110+HLT_j110_320eta490",
    "HLT_j260+HLT_j260_320eta490" : "HLT_j175+HLT_j175_320eta490",
    "HLT_j360+HLT_j260_320eta490" : "HLT_j260+HLT_j260_320eta490",
    "HLT_j420+HLT_j260_320eta490" : "HLT_j360+HLT_j260_320eta490",
    "HLT_j460+HLT_j260_320eta490" : "HLT_j420+HLT_j420_320eta490" 
  }
}
# offline pT thresholds
recoPtThresholds = {
  2017 : {
    "HLT_j15+HLT_j15_320eta490"   :  25,
    "HLT_j25+HLT_j25_320eta490"   :  35,
    "HLT_j35+HLT_j35_320eta490"   :  50,
    "HLT_j45+HLT_j45_320eta490"   :  65,
    "HLT_j60+HLT_j60_320eta490"   :  85,
    "HLT_j85+HLT_j85_320eta490"   : 110,
    "HLT_j110+HLT_j110_320eta490" : 125,
    "HLT_j175+HLT_j175_320eta490" : 200,
    "HLT_j260+HLT_j260_320eta490" : 280,
    "HLT_j360+HLT_j260_320eta490" : 400,
    "HLT_j420+HLT_j260_320eta490" : 460,
    "HLT_j460+HLT_j260_320eta490" : 500,
  }
}
# luminosities
totalLuminosity = {
  2017 : 4.359311E+07
}
triggerLuminosities = {
  2017 : {
    "HLT_j15"  : 1.063358E+01,
    "HLT_j25"  : 1.145585E+01,
    "HLT_j35"  : 5.567760E+01,
    "HLT_j45"  : 6.492572E+02,
    "HLT_j60"  : 1.919951E+03,
    "HLT_j85"  : 7.551335E+03,
    "HLT_j110" : 2.249999E+04,
    "HLT_j175" : 1.507163E+05,
    "HLT_j260" : 9.007094E+05,
    "HLT_j360" : 4.741305E+06,
    "HLT_j420" : 4.359308E+07,
    "HLT_j460" : 4.359308E+07,
    "HLT_j15_320eta490"  : 1.674814E+01,
    "HLT_j25_320eta490"  : 1.209666E+02,
    "HLT_j35_320eta490"  : 4.021145E+02,
    "HLT_j45_320eta490"  : 2.357660E+03,
    "HLT_j60_320eta490"  : 8.558954E+03,
    "HLT_j85_320eta490"  : 4.740987E+04,
    "HLT_j110_320eta490" : 2.268584E+05,
    "HLT_j175_320eta490" : 4.844925E+06,
    "HLT_j260_320eta490" : 4.359311E+07,
    "HLT_j15+HLT_j15_320eta490"   : 2.738169E+01,
    "HLT_j25+HLT_j25_320eta490"   : 1.324222E+02,
    "HLT_j35+HLT_j35_320eta490"   : 4.577915E+02,
    "HLT_j45+HLT_j45_320eta490"   : 3.006771E+03,
    "HLT_j60+HLT_j60_320eta490"   : 1.047736E+04,
    "HLT_j85+HLT_j85_320eta490"   : 5.493956E+04,
    "HLT_j110+HLT_j110_320eta490" : 2.491191E+05,
    "HLT_j175+HLT_j175_320eta490" : 4.975694E+06,
    "HLT_j260+HLT_j260_320eta490" : 4.359311E+07,
    "HLT_j360+HLT_j260_320eta490" : 4.359311E+07,
    "HLT_j420+HLT_j260_320eta490" : 4.359311E+07,
    "HLT_j460+HLT_j260_320eta490" : 4.359311E+07
  }
}

from AnalysisHelpers import analysis

@analysis.definition
def optimalCombinedTrigger(var,year=2017):
  for trig in combinedTriggers[year]:
    var.addTrigger(trig,recoPtThresholds[year][trig])

@analysis.definition
def combinedTriggerPrescale(var,year=2017):
  var.setTotalLuminosity(totalLuminosity[year])
  for trig,lumi in triggerLuminosities[year].items():
    var.setTriggerLuminosity(trig,lumi)