from AnalysisHelpers import analysis

respFilePath = "/project/6061230/thpark/DijetInsituJER/DijetHistograms/jetResponse/jetResponse.root"
respHistPath = "jetResponse/Pythia/mcEventWeight/jetsPtResponse"

nr   = 50
rmin = 0.75
rmax = 1.25

@analysis.definition
def refJetPtResolutionScan(var):
  var.setResponse(respFilePath,respHistPath)
  var.scanResolution(nr,rmin,rmax)

@analysis.definition
def probeJetPtResolutionScan(var):
  var.setResponse(respFilePath,respHistPath)
  var.scanResolution(nr,rmin,rmax)

resolFoldFilePath = ""
resolFoldHistPath = ""

@analysis.definition
def refJetPtResolutionFold(var):
  var.setResponse(respFilePath,respHistPath)
  var.setResolutionFold(resolFoldFilePath,resolFoldHistPath)

ns   = 10
smin = 0.95
smax = 1.05

@analysis.definition
def probeJetPtScaleResolutionScan(var):
  var.setResponse(respFilePath,respHistPath)
  var.scanScale(ns,smin,smax)
  var.scanResolution(nr,rmin,rmax)